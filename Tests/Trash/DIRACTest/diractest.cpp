

#include "AudioEngine/vt_audiostretching_dirac.hpp"
#include "vt_midi.hpp"
#include "vt_time.hpp"

using namespace VT;

class CPercentPrinter: public CProgressDisplay
{
public:
    CPercentPrinter(): m_iClock(0){}

    virtual void PDBeginOperation( LPCVTCHAR statusMessage=NULL ){
        VTPRINTF(VTT("=== %s =================\n"), (statusMessage==NULL)?VTT("Starting..."): statusMessage );
        fflush(stdout);
    }

    virtual void PDShowProgress( float percentDone, LPCVTCHAR statusMessage=NULL ){
        const VTCHAR kClockChars[4] = { '/', '-', '\\', '|' };
        if( percentDone>=0.0f )
            VTPRINTF(VTT("=== [%c][%6.2f%%] %s ===========                 \r"),
                kClockChars[m_iClock], percentDone, (statusMessage==NULL)?VTT("completed"): statusMessage);
        else
            VTPRINTF(VTT("=== [%c] %s ===========                 \r"),
                kClockChars[m_iClock], (statusMessage==NULL)?VTT("Working... %-|"): statusMessage);
        fflush(stdout);
        m_iClock = (m_iClock+1) & 3;
    }

    virtual void PDEndOperation( LPCVTCHAR statusMessage=NULL ){
        VTPRINTF(VTT("\n=== %s =================\n\n"), (statusMessage==NULL)?VTT("Done..."): statusMessage);
        fflush(stdout);
    }
private:
    unsigned m_iClock;
} g_PercentPrinter;


void QualityCoefficientTest( double pk, double tk )
{
    printf("Pitch shifter quality: Pk = %f Tk = %f    Q = %f\n",
        float(pk),
        float(tk),
        float(AudioEngine::GetPitchConversionQuality( pk, tk )));
}


int main( int argc, char *argv[] )
{
    VTPRINTF(
        VTT("DIRAC Test\n")
        VTT("Using DIRAC %s, %s\n"),
        AudioEngine::CDIRACInterface::GetDIRACVersion(),
        AudioEngine::CDIRACInterface::GetDIRACCopyright() );

    QualityCoefficientTest( 1.0, 1.0 );
    QualityCoefficientTest( 2.0, 0.5 );
    QualityCoefficientTest( 0.5, 2.0 );
    QualityCoefficientTest( 1.2, 1.3 );
    QualityCoefficientTest( 0.7, 1.6 );
    QualityCoefficientTest( 0.5, 0.5 );
    QualityCoefficientTest( 2.0, 2.0 );
    QualityCoefficientTest( 2.0, 1.8 );


    if( argc!=7 ){
        VTPRINTF(
            VTT("USAGE: %s <lambda> <quality> <time factor> <pitch factor> <in file> <out file>\n")
            VTT("Lambda:\n")
            VTT("    0 - Fastest lambda (preview)\n")
            VTT("    1 - Full time localization\n")
            VTT("    2 - Time/frequency localization with emphasis on TIME\n")
            VTT("    3 - DEFAULT (halfway between time/frequency)\n")
            VTT("    4 - Time/frequency localization with emphasis on FREQUENCY\n")
            VTT("    5 - Highest frequency localization\n")
            VTT("Quality:\n")
            VTT("    0 - Fast (preview)\n")
            VTT("    1 - Good\n")
            VTT("    2 - Better\n")
            VTT("    3 - Best\n")
            VTT("Time factor is in CENTS.\n")
            VTT("Example:\n")
            VTT("%s 3 2 1 -400 in.wav out.wav\n"),

            argv[0], argv[0]
        );
        return 0;
    }

    int lambda; long l = kDiracLambda3;
    sscanf(    argv[1], "%d", &lambda );
    switch( Math::Clip( lambda, 0, 5 ) )
    {
        case 0: l = kDiracLambdaPreview; break;
        case 1: l = kDiracLambda1; break;
        case 2: l = kDiracLambda2; break;
        case 3: l = kDiracLambda3; break;
        case 4: l = kDiracLambda4; break;
        case 5: l = kDiracLambda5; break;
    }
    VTPRINTF(VTT("Lambda: %d (Internal DIRAC value: %ld)\n"), lambda, l );

    int quality; long q = kDiracQualityGood;
    sscanf(    argv[2], "%d", &quality );
    switch( Math::Clip( quality, 0, 3 ) )
    {
        case 0: q = kDiracQualityPreview; break;
        case 1: q = kDiracQualityGood; break;
        case 2: q = kDiracQualityBetter; break;
        case 3: q = kDiracQualityBest; break;
    }
    VTPRINTF(VTT("Quality: %d (Internal DIRAC value: %ld)\n"), quality, q );

    double timeFactor = 1.0, pitchCents = 0.0;
    sscanf(    argv[3], "%lf", &timeFactor );
    sscanf(    argv[4], "%lf", &pitchCents );
    timeFactor = Math::Clip( timeFactor, 0.5, 2.0 );
    pitchCents = Math::Clip( pitchCents, -1200.0, 1200.0 );
    double pitchFactor = Notes::SemitonesToPitch( pitchCents/100.0 );
    printf("Time factor: %lfx\n", timeFactor );
    printf("Pitch factor: %lfx (%+lf cents)\n", pitchFactor, pitchCents );


    CString infile( argv[5] );
    CString outfile( argv[6] );

    printf("Input file: %s\n", infile.Str());
    printf("Output file: %s\n", outfile.Str());

    double time1 = UNIXTimeD();

    AudioEngine::CFileInputAudioStream in;
    AudioEngine::CFileOutputAudioStream out;

    in.Open( infile );

    if( !in.IsOK() ){
        printf("Failed to open input file!\n");
        return 1;
    }

    out.Open( outfile, false, kFP32, in.GetSampleRate(), in.GetNChannels() );

    if( !out.IsOK() ){
        printf("Failed to open output file!\n");
        return 1;
    }

    AudioEngine::CDIRACInterface dirac;

    dirac.SetProgressDisplay( &g_PercentPrinter );
    dirac.SetDIRACLambda( l );
    dirac.SetDIRACQuality( q );

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! non-hardcoded quality param!
    if( !dirac.PrepareProcess( &in, &out ) ){
        printf("Process initialization error!\n");
        return 1;
    }

    dirac.SetPitchFactor( pitchFactor );
    dirac.SetTimeFactor( timeFactor );

    printf("Pitch shifter quality coefficient: %f\n", float(dirac.GetPitchConversionQuality()));

    double time2 = UNIXTimeD();

    if( !dirac.DoProcess() ){
        printf("Processing error!\n");
        return 1;
    }

    double end = UNIXTimeD();

    VTPRINTF(VTT("Done, DoProcess() time: %.2f, total time: %.2f (%.3f X input, %.3f X output)\n"),
        float(end-time2), float(end-time1),
        float((end-time2)/in.GetLengthSec()),
        float((end-time2)/out.GetLengthSec()));

    return 0;
}


