
#include "vt_portable.hpp"
#include "vt_audiomath.hpp"
#include "vt_rawaudio.hpp"
#include "vt_audiotypes.hpp"
#include "vt_midi.hpp"
#include "vt_fileutils.hpp"
#include "AudioEngine/vt_clipprocessor.hpp"
#include "AudioEngine/vt_audiostreamconverter.hpp"
#include "AudioEngine/vt_clipstretcher.hpp"
#include "AudioEngine/vt_clipfactory.hpp"

#if !defined(_CRT_SECURE_NO_DEPRECATE)
    #define _CRT_SECURE_NO_DEPRECATE 1
#endif

using namespace VT;

class CPercentPrinter: public CProgressDisplay
{
public:
    virtual void PDBeginOperation( LPCVTCHAR statusMessage=NULL ){
        CCSLock lock( &m_CS );
        VTPRINTF(VTT("=== %s =================\n"), (statusMessage==NULL)?VTT("Starting..."): statusMessage );
    }

    virtual void PDShowProgress( float percentDone, LPCVTCHAR statusMessage=NULL ){
        CCSLock lock( &m_CS );
        VTPRINTF(VTT("=== [%6.2f%%] %s ===========\r"), percentDone, (statusMessage==NULL)?VTT("completed"): statusMessage);
    }

    virtual void PDEndOperation( LPCVTCHAR statusMessage=NULL ){
        CCSLock lock( &m_CS );
        VTPRINTF(VTT("\n=== %s =================\n\n"), (statusMessage==NULL)?VTT("Done..."): statusMessage);
    }
private:
    CCriticalSection m_CS;
} g_PD;



void ChannelMixingTest()
{
    printf("********************** ChannelMixingTest ******************************\n");
    CInMemoryAudio<float, 2> inAudio;
    inAudio.LoadPCMWav( "Test.wav" );
    if( !inAudio.isLoaded() ){
        printf("Error loading Test.wav, last error was: %s\n",
            inAudio.GetLastErrorMessage());
        return;
    }

    size_t samples = inAudio.getNChannels()*inAudio.getLength();
    printf(
        "Input frames:   %8u (%x)\n"
        "Input channels: %8u (%x)\n"
        "Input samples:  %8u (%x)\n",
        inAudio.getLength(), inAudio.getLength(),
        inAudio.getNChannels(), inAudio.getNChannels(),
        samples, samples );


    // Channel mixing
    {
        printf("Channel remixing test...\n");
        CInMemoryAudio<float, 1> outAudio1;
        CInMemoryAudio<float, 2> outAudio2;
        CInMemoryAudio<float, 6> outAudio6;
        outAudio1.Allocate( inAudio.getLength() );
        outAudio2.Allocate( inAudio.getLength() );
        outAudio6.Allocate( inAudio.getLength() );
        AudioEngine::CChannelMixerMap mixmap;

        printf("Mixing stereo to mono...\n");
        mixmap.InitStereoToMono();
        mixmap.Remix( (TSample32F*)inAudio.GetRawDataAccess(), (TSample32F*)outAudio1.GetRawDataAccess(), inAudio.getLength() );
        outAudio1.SavePCMWav16SI( "Test_Out_ChanMixing_mono.wav" );

        printf("Mixing mono to stereo...\n");
        mixmap.InitMonoToStereo();
        mixmap.Remix( (TSample32F*)outAudio1.GetRawDataAccess(), (TSample32F*)outAudio2.GetRawDataAccess(), outAudio1.getLength() );
        outAudio2.SavePCMWav16SI( "Test_Out_ChanMixing_monotostereo.wav" );

        printf("Mixing stereo to 5.1...\n");
        mixmap.InitDefaultConverter(2, 6);
        mixmap.Remix( (TSample32F*)inAudio.GetRawDataAccess(), (TSample32F*)outAudio6.GetRawDataAccess(), inAudio.getLength() );
        outAudio6.SavePCMWav16SI( "Test_Out_ChanMixing_stereoto51.wav" );

        printf("Channel remixing test done.\n");
    }

    // Bit mode conversion
    {
        CMemoryFile raw;
        CAudioBitConverter conv;

        printf("Writing raw files with different bit format...\n");

        TDitheringMethod dmet = kDitheringShaping;

        // 8-bit test
        raw.Resize( inAudio.getLength()*inAudio.getNChannels() );
        printf("8-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt8S, dmet, (unsigned)inAudio.getNChannels() );
        conv.Convert( inAudio.GetRawDataAccess(), raw.getDataPtr(), inAudio.getLength()*inAudio.getNChannels() );
        raw.save( "Test_Out_8bitSigned.raw" );

        // 16-bit test
        raw.Resize( inAudio.getLength()*2*inAudio.getNChannels() );
        printf("16-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt16S, dmet, (unsigned)inAudio.getNChannels()  );
        conv.Convert( inAudio.GetRawDataAccess(), raw.getDataPtr(), inAudio.getLength()*inAudio.getNChannels() );
        raw.save( "Test_Out_16bitSigned.raw" );

        // 32-bit FP test
        raw.Resize( inAudio.getLength()*4*inAudio.getNChannels() );
        printf("32-bit floating... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kFP32, dmet, (unsigned)inAudio.getNChannels()  );
        conv.Convert( inAudio.GetRawDataAccess(), raw.getDataPtr(), inAudio.getLength()*inAudio.getNChannels() );
        raw.save( "Test_Out_32bitIEEEFloat.raw" );

        // 24-bit test
        raw.Resize( inAudio.getLength()*3*inAudio.getNChannels() );
        printf("24-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt24S, dmet, (unsigned)inAudio.getNChannels()  );
        conv.Convert( inAudio.GetRawDataAccess(), raw.getDataPtr(), inAudio.getLength()*inAudio.getNChannels() );
        raw.save( "Test_Out_24bitSigned.raw" );

        // 32-bit test
        raw.Resize( inAudio.getLength()*4*inAudio.getNChannels() );
        printf("32-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt32S, dmet, (unsigned)inAudio.getNChannels()  );
        conv.Convert( inAudio.GetRawDataAccess(), raw.getDataPtr(), inAudio.getLength()*inAudio.getNChannels() );
        raw.save( "Test_Out_32bitSigned.raw" );

        // 64-bit FP test
        raw.Resize( inAudio.getLength()*8*inAudio.getNChannels() );
        printf("64-bit floating... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kFP64, dmet, (unsigned)inAudio.getNChannels()  );
        conv.Convert( inAudio.GetRawDataAccess(), raw.getDataPtr(), inAudio.getLength()*inAudio.getNChannels() );
        raw.save( "Test_Out_64bitIEEEFloat.raw" );






        printf("================= Now 32 bit to anything... ====================\n");
        CInMemoryAudio<double, 2> inAudio32;
        inAudio32.LoadRaw( "Test_Out_32bitIEEEFloat.raw" );
        if( !inAudio32.isLoaded() ){
            printf("Error loading Test.wav, last error was: %s\n",
                inAudio32.GetLastErrorMessage());
            return;
        }


        // 8-bit test
        raw.Resize( inAudio32.getLength()*inAudio32.getNChannels() );
        printf("8-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt8S, dmet, (unsigned)inAudio32.getNChannels() );
        conv.Convert( inAudio32.GetRawDataAccess(), raw.getDataPtr(), inAudio32.getLength()*inAudio32.getNChannels() );
        raw.save( "Test_Out_32_to_8bitSigned.raw" );

        // 16-bit test
        raw.Resize( inAudio32.getLength()*2*inAudio32.getNChannels() );
        printf("16-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt16S, dmet, (unsigned)inAudio32.getNChannels()  );
        conv.Convert( inAudio32.GetRawDataAccess(), raw.getDataPtr(), inAudio32.getLength()*inAudio32.getNChannels() );
        raw.save( "Test_Out_32_to_16bitSigned.raw" );

        // 32-bit FP test
        raw.Resize( inAudio32.getLength()*4*inAudio32.getNChannels() );
        printf("32-bit floating... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kFP32, dmet, (unsigned)inAudio32.getNChannels()  );
        conv.Convert( inAudio32.GetRawDataAccess(), raw.getDataPtr(), inAudio32.getLength()*inAudio32.getNChannels() );
        raw.save( "Test_Out_32_to_32bitIEEEFloat.raw" );

        // 24-bit test
        raw.Resize( inAudio32.getLength()*3*inAudio32.getNChannels() );
        printf("24-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt24S, dmet, (unsigned)inAudio32.getNChannels()  );
        conv.Convert( inAudio32.GetRawDataAccess(), raw.getDataPtr(), inAudio32.getLength()*inAudio32.getNChannels() );
        raw.save( "Test_Out_32_to_24bitSigned.raw" );

        // 32-bit test
        raw.Resize( inAudio32.getLength()*4*inAudio32.getNChannels() );
        printf("32-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kInt32S, dmet, (unsigned)inAudio32.getNChannels()  );
        conv.Convert( inAudio32.GetRawDataAccess(), raw.getDataPtr(), inAudio32.getLength()*inAudio32.getNChannels() );
        raw.save( "Test_Out_32_to_32bitSigned.raw" );

        // 64-bit FP test
        raw.Resize( inAudio32.getLength()*8*inAudio32.getNChannels() );
        printf("64-bit floating... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP32, VT::kFP64, dmet, (unsigned)inAudio32.getNChannels()  );
        conv.Convert( inAudio32.GetRawDataAccess(), raw.getDataPtr(), inAudio32.getLength()*inAudio32.getNChannels() );
        raw.save( "Test_Out_32_to_64bitIEEEFloat.raw" );







        printf("================= Now 64 bit to anything... ====================\n");
        CInMemoryAudio<double, 2> inAudio64;
        inAudio64.LoadRaw( "Test_Out_64bitIEEEFloat.raw" );
        if( !inAudio64.isLoaded() ){
            printf("Error loading Test.wav, last error was: %s\n",
                inAudio64.GetLastErrorMessage());
            return;
        }


        // 8-bit test
        raw.Resize( inAudio64.getLength()*inAudio64.getNChannels() );
        printf("8-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP64, VT::kInt8S, dmet, (unsigned)inAudio64.getNChannels() );
        conv.Convert( inAudio64.GetRawDataAccess(), raw.getDataPtr(), inAudio64.getLength()*inAudio64.getNChannels() );
        raw.save( "Test_Out_64_to_8bitSigned.raw" );

        // 16-bit test
        raw.Resize( inAudio64.getLength()*2*inAudio64.getNChannels() );
        printf("16-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP64, VT::kInt16S, dmet, (unsigned)inAudio64.getNChannels()  );
        conv.Convert( inAudio64.GetRawDataAccess(), raw.getDataPtr(), inAudio64.getLength()*inAudio64.getNChannels() );
        raw.save( "Test_Out_64_to_16bitSigned.raw" );

        // 32-bit FP test
        raw.Resize( inAudio64.getLength()*4*inAudio64.getNChannels() );
        printf("32-bit floating... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP64, VT::kFP32, dmet, (unsigned)inAudio64.getNChannels()  );
        conv.Convert( inAudio64.GetRawDataAccess(), raw.getDataPtr(), inAudio64.getLength()*inAudio64.getNChannels() );
        raw.save( "Test_Out_64_to_32bitIEEEFloat.raw" );

        // 24-bit test
        raw.Resize( inAudio64.getLength()*3*inAudio64.getNChannels() );
        printf("24-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP64, VT::kInt24S, dmet, (unsigned)inAudio64.getNChannels()  );
        conv.Convert( inAudio64.GetRawDataAccess(), raw.getDataPtr(), inAudio64.getLength()*inAudio64.getNChannels() );
        raw.save( "Test_Out_64_to_24bitSigned.raw" );

        // 32-bit test
        raw.Resize( inAudio64.getLength()*4*inAudio64.getNChannels() );
        printf("32-bit... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP64, VT::kInt32S, dmet, (unsigned)inAudio64.getNChannels()  );
        conv.Convert( inAudio64.GetRawDataAccess(), raw.getDataPtr(), inAudio64.getLength()*inAudio64.getNChannels() );
        raw.save( "Test_Out_64_to_32bitSigned.raw" );

        // 64-bit FP test
        raw.Resize( inAudio64.getLength()*8*inAudio64.getNChannels() );
        printf("64-bit floating... File Size: %u\n", raw.getSize());
        conv.Init( VT::kFP64, VT::kFP64, dmet, (unsigned)inAudio64.getNChannels()  );
        conv.Convert( inAudio64.GetRawDataAccess(), raw.getDataPtr(), inAudio64.getLength()*inAudio64.getNChannels() );
        raw.save( "Test_Out_64_to_64bitIEEEFloat.raw" );


        printf("Done writing raw files.\n");
    }

}

void AudioStreamTest()
{
    printf("********************** AudioStreamTest ******************************\n");
    AudioEngine::CFileInputAudioStream fias;
    fias.Open("Test.wav");
    if( !fias.IsOK() ){
        printf("Failed to load Test.wav.\n");
        return;
    }
    printf(
        "Input file properties:\n"
        "Channels:      %u\n"
        "Sample Rate:   %.0lf\n"
        "File size:     %u frames (%.3lf seconds)\n"
        "Frame size:    %u bytes\n"
        "Sample size:   %u bytes\n"
        "Position:      %u frames\n",
        fias.GetNChannels(),
        fias.GetSampleRate(),
        unsigned(fias.GetLength()), fias.GetLengthSec(),
        unsigned(fias.GetFrameSize()),
        unsigned(fias.GetSampleSize()),
        unsigned(fias.Tell()));


    // Testing converters
    AudioEngine::CAudioStreamConverter converter;

    {
        printf("Testing Scheme 0 Conversion (no conversion)...\n");
        AudioEngine::CFileOutputAudioStream foas;
        if( !foas.Open("Test-scheme0.wav", false, fias.GetSampleType(), fias.GetSampleRate(), fias.GetNChannels() ) ){
            printf("Failed to create output file!\n");
            return;
        }
        fias.Seek( 0 );
        if( !converter.Init( &fias, &foas ) )
            printf("Error initializing conversion!\n");
        if( !converter.ConvertAll( &g_PD ) )
            printf("Converter error!\n");
    }


    {
        printf("Testing Scheme 1 Conversion (bitness only)...\n");
        AudioEngine::CFileOutputAudioStream foas;
        if( !foas.Open("Test-scheme1.wav", false, VT::kInt24S, fias.GetSampleRate(), fias.GetNChannels() ) ){
            printf("Failed to create output file!\n");
            return;
        }
        fias.Seek( 0 );
        if( !converter.Init( &fias, &foas ) )
            printf("Error initializing conversion!\n");
        if( !converter.ConvertAll( &g_PD ) )
            printf("Converter error!\n");
    }

    {
        printf("Testing Scheme 2 Conversion (bitness and channel mixer)...\n");
        AudioEngine::CFileOutputAudioStream foas;
        if( !foas.Open("Test-scheme2.wav", false, VT::kFP32, fias.GetSampleRate(), 1 ) ){
            printf("Failed to create output file!\n");
            return;
        }
        fias.Seek( 0 );
        if( !converter.Init( &fias, &foas ) )
            printf("Error initializing conversion!\n");
        if( !converter.ConvertAll( &g_PD ) )
            printf("Converter error!\n");
    }

    {
        printf("Testing Scheme 3 Conversion (bitness, channel mixer and resampling)...\n");
        AudioEngine::CFileOutputAudioStream foas;
        if( !foas.Open("Test-scheme3.wav", false, VT::kFP32, 48000, 2 ) ){
            printf("Failed to create output file!\n");
            return;
        }
        fias.Seek( 0 );
        if( !converter.Init( &fias, &foas, kQualityHigh, // kQualityBest
            kDitheringShaping, kAutoWith64BitDSP ) )
            printf("Error initializing conversion!\n");
        if( !converter.ConvertAll( &g_PD ) )
            printf("Converter error!\n");
    }

}

void StreamWrapperTest()
{
    printf("********************** StreamWrapperTest ******************************\n");
    AudioEngine::CFileInputAudioStream fias;
    AudioEngine::CFileOutputAudioStream foas;
    fias.Open("Test.wav");
    foas.Open("Test-WrapperOut.wav", false, fias.GetSampleType(), fias.GetSampleRate(), fias.GetNChannels() );
    AudioEngine::CAudioStreamWrapper inwrap( &fias, true ), outwrap( &foas, true );
    printf("Pumping...\n");
    if( !outwrap.PumpFrom(&inwrap) )
        printf("Error :(\n");
    else
        printf("Success!\n");
}

void ClipStretchingTest()
{
    printf("********************** ClipStretchingTest ******************************\n");
    try{
        AudioEngine::CFileInputAudioStream fias;
        AudioEngine::CFileOutputAudioStream foas;
        // const LPCVTCHAR in="Test_Out_32bitIEEEFloat.raw", out="Test_Out_32bitIEEEFloat-FADES.wav";
        // const LPCVTCHAR in="Sinus 60sec.wav", out="Sinus 60sec FADES.wav";
        const LPCVTCHAR in="Test.wav", out="Test-CLIP.wav";
        printf("In: %s\nOut: %s\n", in, out);
        if( !fias.Open(in, true, kFP32, 44100, 2 ) ){
            printf("Failed to open input file: %s\n", in);
            return;
        };
        if( !foas.Open(out, false, fias.GetSampleType(),
                fias.GetSampleRate(), fias.GetNChannels() ) ){
                printf("Failed to open output file: %s\n", out);
            return;
        };
        AudioEngine::CClipStretcher cropper;
        cropper.SetQuality( kQualityLow );
        cropper.SetMaxMemoryFileSize( 50 * 1024*1024 );
        cropper.SetProgressDisplay( &g_PD );
        cropper.GetCropTransform().m_fFadeInMs = 10.0;
        cropper.GetCropTransform().m_fFadeOutMs = 20.0;
        //cropper.GetCropTransform().m_fVolume = 0.7;
        cropper.GetCropTransform().m_bEnabled = true;
        cropper.GetCropTransform().m_fStartingPosMs = 20000.0;
        cropper.GetCropTransform().m_fLengthMs = 60000.0;
        cropper.GetStretchTransform().m_bEnabled = true;
        cropper.GetStretchTransform().m_fPitchFactor = Notes::SemitonesToPitch(2.0);
        cropper.GetStretchTransform().m_fTimeFactor = 1.0/cropper.GetStretchTransform().m_fPitchFactor;
        if( !cropper.Process( &fias, &foas ) )
            printf("Cropper error :(\n");
        else
            printf("Cropper success!\n");
    }catch( std::exception exc ){
        printf("Caught an exception: %s\n", exc.what());
    }catch(...){
        printf("Unknown exception.\n");
    }
    printf("Fader test done.\n");
}



void ClipConveyorTest()
{
    printf("********************** Clip Conveyor Test ******************************\n");
    AudioEngine::TClipJob job;

    AudioEngine::CClipConveyor m_Conveyor("TestConveyor", false);
    m_Conveyor.SetProgressDisplay( &g_PD );

    m_Conveyor.SetBitMode( kForce64 );

    printf("Feeding the conveyor with jobs...\n");

    #if 1
        job.Default();
        VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
        VTSTRNCPY( job.m_OutFileName, "Test-Conveyor-1.wav", MAX_PATH );
        job.m_CropTransform.m_bEnabled = true;
        job.m_CropTransform.m_fStartingPosMs = 10000.0;
        job.m_CropTransform.m_fLengthMs = 5000.0;
        job.m_OutFormat.m_uChannels = 2;
        job.m_OutFormat.m_SampleType = kInt24S;
        //job.m_StretchTransform.m_bEnabled = true;
        //job.m_StretchTransform.m_fPitchFactor = Notes::SemitonesToPitch(1.0);
        m_Conveyor.AddJob( job );
    #endif

    job.Default();
    VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
    VTSTRNCPY( job.m_OutFileName, "Test-Conveyor-2.wav", MAX_PATH );
    job.m_CropTransform.m_bEnabled = true;
    job.m_CropTransform.m_fStartingPosMs = 30000.0;
    job.m_CropTransform.m_fLengthMs = 5000.0;
    job.m_OutFormat.m_uChannels = 2;
    job.m_OutFormat.m_SampleType = kInt8U;
    job.m_OutFormat.m_fSampleRate = 48000.0;
    //job.m_StretchTransform.m_bEnabled = true;
    //job.m_StretchTransform.m_fPitchFactor = Notes::SemitonesToPitch(-1.0);
    m_Conveyor.AddJob( job );

    printf("!!!!!!!!!!!!!!!!!!!!!!!!! 2 jobs added, now sleep...\n");
    VT::Sleep( 5000 );

    #if 1
        job.Default();
        VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
        VTSTRNCPY( job.m_OutFileName, "Test-Conveyor-3rev.wav", MAX_PATH );
        job.m_CropTransform.m_bEnabled = true;
        job.m_CropTransform.m_bReverseClip = true;
        job.m_CropTransform.m_fStartingPosMs = 50000.0;
        job.m_CropTransform.m_fLengthMs = 5000.0;
        job.m_OutFormat.m_uChannels = 2;
        job.m_OutFormat.m_SampleType = kInt16S;
        m_Conveyor.AddJob( job );

        job.Default();
        VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
        VTSTRNCPY( job.m_OutFileName, "Test-Conveyor-3.wav", MAX_PATH );
        job.m_CropTransform.m_bEnabled = true;
        job.m_CropTransform.m_bReverseClip = false;
        job.m_CropTransform.m_fStartingPosMs = 50000.0;
        job.m_CropTransform.m_fLengthMs = 5000.0;
        job.m_OutFormat.m_uChannels = 2;
        job.m_OutFormat.m_SampleType = kInt16S;
        m_Conveyor.AddJob( job );
    #endif

    printf("!!!!!!!!!!!!!!!!!!!!!!!!! One more added, now wait...\n");
    m_Conveyor.WaitForIdle();
    printf("!!!!!!!!!!!!!!!!!!!!!!!!! Waiting complete.\n");
    // VT::Sleep( 10000 );
    printf("!!!!!!!!!!!!!!!!!!!!!!!!! stopping the thread...\n");
    m_Conveyor.TerminateAndWait();
    printf("Clip conveyor test done.\n");
}





void FactoryTest()
{
    try{
        printf("********************** Charlie & Clip Factory Test ******************************\n");
        AudioEngine::TClipJob job;

        AudioEngine::CClipFactory factory(0); // 0 "free" CPU's

        MkDir("temp");
        factory.SetTempFileDirectory("temp");
        factory.SetTempFilePrefix("HelloWorld_");

        printf("Factory uses %u CPU's.\n", unsigned(factory.GetNCPUs()));
        printf("Max total size of %d memory files in the Factory: %u (one file: %u bytes)\n",
            int(factory.GetNTempFiles()),
            unsigned(factory.GetTotalMaxMemoryFileSize()),
            unsigned(factory.GetMaxMemoryFileSize()));

        factory.SetCommonThreadProgressDisplayer( &g_PD );

        //m_Conveyor.SetProgressDisplay( &g_PD );
        //m_Conveyor.SetBitMode( kForce64 );

        printf("Feeding the conveyor with jobs...\n");

        #if 1
            job.Default();
            VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
            VTSTRNCPY( job.m_OutFileName, "Test-Factory-1.wav", MAX_PATH );
            job.m_CropTransform.m_bEnabled = true;
            job.m_CropTransform.m_fStartingPosMs = 10000.0;
            job.m_CropTransform.m_fLengthMs = 2000.0;
            job.m_OutFormat.m_uChannels = 2;
            job.m_OutFormat.m_SampleType = kInt24S;
            //.job.m_StretchTransform.m_bEnabled = true;
            job.m_StretchTransform.m_fPitchFactor = Notes::SemitonesToPitch(2.0);
            factory.AddJob( job );
        #endif

        VT::Sleep(300);

        job.Default();
        VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
        VTSTRNCPY( job.m_OutFileName, "Test-Factory-2.wav", MAX_PATH );
        job.m_CropTransform.m_bEnabled = true;
        job.m_CropTransform.m_fStartingPosMs = 30000.0;
        job.m_CropTransform.m_fLengthMs = 2000.0;
        job.m_OutFormat.m_uChannels = 2;
        job.m_OutFormat.m_SampleType = kInt8U;
        job.m_OutFormat.m_fSampleRate = 48000.0;
        //job.m_StretchTransform.m_bEnabled = true;
        job.m_StretchTransform.m_fPitchFactor = Notes::SemitonesToPitch(-2.0);
        factory.AddJob( job );

        printf("!!!!!!!!!!!!!!!!!!!!!!!!! 2 jobs added, now sleep...\n");
        VT::Sleep( 2000 );

        #if 1
            job.Default();
            VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
            VTSTRNCPY( job.m_OutFileName, "Test-Factory-3rev.wav", MAX_PATH );
            job.m_CropTransform.m_bEnabled = true;
            job.m_CropTransform.m_bReverseClip = true;
            job.m_CropTransform.m_fStartingPosMs = 50000.0;
            job.m_CropTransform.m_fLengthMs = 5000.0;
            job.m_OutFormat.m_uChannels = 2;
            job.m_OutFormat.m_SampleType = kInt16S;
            factory.AddJob( job );

            VT::Sleep(200);

            job.Default();
            VTSTRNCPY( job.m_InFileName, "Test.wav", MAX_PATH );
            VTSTRNCPY( job.m_OutFileName, "Test-Factory-3.wav", MAX_PATH );
            job.m_CropTransform.m_bEnabled = true;
            job.m_CropTransform.m_bReverseClip = false;
            job.m_CropTransform.m_fStartingPosMs = 50000.0;
            job.m_CropTransform.m_fLengthMs = 5000.0;
            job.m_OutFormat.m_uChannels = 2;
            job.m_OutFormat.m_SampleType = kInt16S;
            factory.AddJob( job );
        #endif

        //printf("!!!!!!!!!!!!!!!!!!!!!!!!! One more added, now wait...\n");
        //factory.WaitForIdle();
        //VT::Sleep( 15000 );
        //printf("!!!!!!!!!!!!!!!!!!!!!!!!! Waiting complete.\n");
        printf(
            "*********************************************************************\n"
            "*** HIT ENTER FOR A LITTLE INTERRUPTION! ****************************\n"
            "*** HIT ENTER FOR A LITTLE INTERRUPTION! ****************************\n"
            "*** HIT ENTER FOR A LITTLE INTERRUPTION! ****************************\n"
            "*** HIT ENTER FOR A LITTLE INTERRUPTION! ****************************\n"
            "*** HIT ENTER FOR A LITTLE INTERRUPTION! ****************************\n"
            "*********************************************************************\n");
        getchar();


        // Receiving completed tasks

        { CString dump;    factory.ConcatShortDump( &dump ); puts(dump); }

        const char *const files[]={
            "Test-Factory-1.wav",
            "Test-Factory-2.wav",
            "Test-Factory-3.wav",
            "Test-Factory-3rev.wav"
        };
        for( size_t i=0; i<4; i++ ){
            bool ishq;
            AudioEngine::TClipJobState state = factory.GetJobState( files[i], &ishq );
            printf("Job %s Status: %s, HQ: %s\n", files[i], AudioEngine::kClipJobStateDescription[state], LCYESNO(ishq));
            char lq[MAX_PATH+1]="";
            bool rcvlq = factory.ReceivePreviewResult( files[i], lq );
            VT::Sleep( 500 );
            printf("Received LQ result: %s, file name = %s\n", LCYESNO(rcvlq), lq);
            bool rcvhq = factory.ReceiveFinalResult( files[i] );
            printf("Received HQ result: %s\n", LCYESNO(rcvhq));
            printf("\n");
        }

        { CString dump;    factory.ConcatShortDump( &dump ); puts(dump); }


        printf("!!!!!!!!!!!!!!!!!!!!!!!!! stopping the thread...\n");
        //factory.TerminateAndWait();
        printf("Charlie & Clip Factory test done.\n");
    }catch(CException exc){
        printf("Whooops :( Exception: \"%s\"\n", exc.what());
    }catch(...){
        printf("Whooops :( Unknown exception.\n");
    }
}











int main( int argc, char *argv[] )
{
    printf("Hello cool audio world!\n");
    // printf("BTW, -3 dB is %.18lf\n", double(Math::Convert_dBTo01_NoClip(-3.0)));
    printf("Number of CPU's detected: %u\n", VT::CPU::GetNumberOfCPUs() );

    FactoryTest();


//    ChannelMixingTest();

//    ClipConveyorTest();

//    ClipStretchingTest();

//    StreamWrapperTest();

//    AudioStreamTest();

    return 0;
}





