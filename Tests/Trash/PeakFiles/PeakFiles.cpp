//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PeakFiles.h"
#include "vt_peakfiles.hpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
    UpdatePeakImage();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BitBtn1Click(TObject *Sender)
{
    if( OpenDialog1->Execute(this) ){
        FileEdit->Text = OpenDialog1->FileName;
        UpdatePeakImage();
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::UpdatePeakImage()
{
    VT::AudioEngine::CAudioChannelPeaks peaks;
    VT::CString fileName( FileEdit->Text.c_str() );

    peaks.SetBlockSizeMs( BlockSizeUD->Position );

    peaks.SetPeakType( VT::AudioEngine::TPeakType( ComboBox1->ItemIndex ) );

    if( !peaks.BuildFromWav( fileName ) ){
        AnsiString msg = "Ooops :`-( File couldn't be open: ";
        msg += FileEdit->Text;
        MessageDlg( msg, mtError, TMsgDlgButtons(mbOK), 0 );
        return;
    }

    peaks.SetMaximize( MaximizeCB->Checked );
    peaks.SetUseLogScale( CheckBox1->Checked );

    peaks.Render( PeakImage->Picture->Bitmap, 300, clWhite, clBlue );

}




void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{
    UpdatePeakImage();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MaximizeCBClick(TObject *Sender)
{
    UpdatePeakImage();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{
    UpdatePeakImage();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::BitBtn2Click(TObject *Sender)
{
    VT::CString fileName( FileEdit->Text.c_str() );
    VT::AudioEngine::CAudioChannelPeaks peaks;
    peaks.SetBlockSizeMs( BlockSizeUD->Position );
    peaks.SetPeakType( VT::AudioEngine::TPeakType( ComboBox1->ItemIndex ) );
    if( !peaks.PeakFileFor( fileName, "testpeak", false, ForceRecreateCB->Checked ) )
        MessageDlg( "Some error :(", mtError, TMsgDlgButtons(mbOK), 0 );
    else{
        peaks.SetMaximize( MaximizeCB->Checked );
        peaks.SetUseLogScale( CheckBox1->Checked );
        peaks.Render( PeakImage->Picture->Bitmap, 300, clWhite, clGreen );
    }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::BlockSizeEditChange(TObject *Sender)
{
    UpdatePeakImage();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox2Click(TObject *Sender)
{
    PeakImage->Stretch = CheckBox2->Checked;
}
//---------------------------------------------------------------------------

