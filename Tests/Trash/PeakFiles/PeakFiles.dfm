object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Peak File Test'
  ClientHeight = 566
  ClientWidth = 738
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  Scaled = False
  DesignSize = (
    738
    566)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 183
    Width = 722
    Height = 375
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = 2039583
    Padding.Left = 10
    Padding.Top = 10
    Padding.Right = 10
    Padding.Bottom = 10
    ParentBackground = False
    TabOrder = 0
    object PeakImage: TImage
      Left = 12
      Top = 12
      Width = 698
      Height = 351
      Align = alClient
      Center = True
      Stretch = True
      ExplicitLeft = 72
      ExplicitTop = 48
      ExplicitWidth = 105
      ExplicitHeight = 105
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 72
    Width = 209
    Height = 105
    Caption = 'Generation Options'
    TabOrder = 1
    object Label1: TLabel
      Left = 18
      Top = 31
      Width = 28
      Height = 13
      Caption = 'Type:'
    end
    object Label2: TLabel
      Left = 18
      Top = 64
      Width = 74
      Height = 13
      Caption = 'Block Size (ms):'
    end
    object ComboBox1: TComboBox
      Left = 52
      Top = 27
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 1
      TabOrder = 0
      Text = 'Average'
      OnChange = ComboBox1Change
      Items.Strings = (
        'Maximum'
        'Average'
        'RMS')
    end
    object BlockSizeEdit: TEdit
      Left = 124
      Top = 60
      Width = 53
      Height = 21
      TabOrder = 1
      Text = '3'
      OnChange = BlockSizeEditChange
    end
    object BlockSizeUD: TUpDown
      Left = 177
      Top = 60
      Width = 15
      Height = 21
      Associate = BlockSizeEdit
      Min = 1
      Position = 3
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 223
    Top = 72
    Width = 193
    Height = 105
    Caption = 'Rendering Options'
    TabOrder = 2
    object MaximizeCB: TCheckBox
      Left = 16
      Top = 27
      Width = 97
      Height = 17
      Caption = 'Maximize peaks'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = MaximizeCBClick
    end
    object CheckBox1: TCheckBox
      Left = 16
      Top = 50
      Width = 97
      Height = 17
      Caption = 'Log Scaling'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = CheckBox1Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 422
    Top = 72
    Width = 147
    Height = 105
    Caption = 'Peak File'
    TabOrder = 3
    object BitBtn2: TBitBtn
      Left = 17
      Top = 64
      Width = 113
      Height = 25
      Caption = 'Make Peak File'
      TabOrder = 0
      OnClick = BitBtn2Click
    end
    object ForceRecreateCB: TCheckBox
      Left = 17
      Top = 27
      Width = 97
      Height = 17
      Caption = 'Force recreate'
      TabOrder = 1
    end
  end
  object GroupBox4: TGroupBox
    Left = 8
    Top = 8
    Width = 721
    Height = 58
    Caption = 'Sound File'
    TabOrder = 4
    DesignSize = (
      721
      58)
    object FileEdit: TEdit
      Left = 18
      Top = 24
      Width = 603
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      Text = 'Test.wav'
    end
    object BitBtn1: TBitBtn
      Left = 627
      Top = 22
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Browse'
      TabOrder = 1
      OnClick = BitBtn1Click
    end
  end
  object GroupBox5: TGroupBox
    Left = 575
    Top = 72
    Width = 154
    Height = 105
    Caption = 'View'
    TabOrder = 5
    object CheckBox2: TCheckBox
      Left = 16
      Top = 27
      Width = 135
      Height = 17
      Caption = 'Stretch the image'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CheckBox2Click
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'wav'
    Filter = 'WAV files|*.wav'
    Left = 536
    Top = 24
  end
end
