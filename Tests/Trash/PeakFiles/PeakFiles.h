//---------------------------------------------------------------------------

#ifndef PeakFilesH
#define PeakFilesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:    // IDE-managed Components
    TPanel *Panel1;
    TImage *PeakImage;
    TGroupBox *GroupBox1;
    TComboBox *ComboBox1;
    TLabel *Label1;
    TGroupBox *GroupBox2;
    TGroupBox *GroupBox3;
    TCheckBox *MaximizeCB;
    TCheckBox *CheckBox1;
    TBitBtn *BitBtn2;
    TCheckBox *ForceRecreateCB;
    TGroupBox *GroupBox4;
    TEdit *FileEdit;
    TBitBtn *BitBtn1;
    TOpenDialog *OpenDialog1;
    TEdit *BlockSizeEdit;
    TUpDown *BlockSizeUD;
    TLabel *Label2;
    TGroupBox *GroupBox5;
    TCheckBox *CheckBox2;
    void __fastcall BitBtn1Click(TObject *Sender);
    void __fastcall ComboBox1Change(TObject *Sender);
    void __fastcall MaximizeCBClick(TObject *Sender);
    void __fastcall CheckBox1Click(TObject *Sender);
    void __fastcall BitBtn2Click(TObject *Sender);
    void __fastcall BlockSizeEditChange(TObject *Sender);
    void __fastcall CheckBox2Click(TObject *Sender);
private:    // User declarations
public:        // User declarations
    __fastcall TForm1(TComponent* Owner);
    __fastcall void UpdatePeakImage();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
