//-----------------------------------------------------------------------------
//
// Audio Engine VST Test
//
// Started: 2009/03
//
// Programmer: Sergey A. Galin
//
//-----------------------------------------------------------------------------

#ifndef AUDIOENGINETESTEFFECT_HPP
#define AUDIOENGINETESTEFFECT_HPP

#include "vt_portable.hpp"
#include <string.h>
#include "audioeffectx.h"
#include "AudioEngine/vt_audiostreamplayer.hpp"
#include "AudioEngine/vt_audiopool.hpp"

class CAudioEngineTestEffect:
    public AudioEffectX
{
public:
    CAudioEngineTestEffect(audioMasterCallback audioMaster);
    virtual ~CAudioEngineTestEffect();

    virtual bool getEffectName (char* name){ strcpy(name, "CAudioEngineTestEffect"); return true; }
    virtual bool getVendorString (char* text){ strcpy(text, "Virartech"); return true; }
    virtual bool getProductString (char* text){ strcpy(text, "VT Audio Engine Test"); return true; }
    virtual VstInt32 getVendorVersion () { return 1000; }
    virtual VstPlugCategory getPlugCategory () { return kPlugCategMastering; }

    int getParameter() {return testEffectParameter;}

    virtual bool string2parameter (VstInt32 index, char *text) { return sscanf(text, "%f", &m_fVolume)==1; }
    virtual void setParameter (VstInt32 index, float value) { m_fVolume = value; }
    virtual float getParameter (VstInt32 index){ return m_fVolume; }
    virtual void getParameterLabel (VstInt32 index, char *label){ strcpy(label, "x"); }
    virtual void getParameterDisplay (VstInt32 index, char *text) { sprintf(text, "%.3f", m_fVolume); }
    virtual void getParameterName (VstInt32 index, char *text){ strcpy(text, "Volume"); }

    virtual void process(float** inputs, float** outputs, VstInt32 sampleFrames);
    virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);

    void test1();
    void test2();
    void poolTest1();

private:
    //friend class VSTWidget;
    float m_fVolume;

    int testEffectParameter;
    static const int kNChannels = 2;

    VT::AudioEngine::CCachingFileInputAudioStream m_Stream1, m_Stream2;
    VT::AudioEngine::CAudioStreamPlayer m_Player;
    VT::AudioEngine::CAudioPool m_Pool;
    VT::CVector<float> m_Buffer;

};

#endif
