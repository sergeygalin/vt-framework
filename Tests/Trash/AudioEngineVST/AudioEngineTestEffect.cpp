//-----------------------------------------------------------------------------
//
// Audio Engine VST Test
//
// Started: 2009/03
//
// Programmer: Sergey A. Galin
//
//-----------------------------------------------------------------------------


#include <QMessageBox>
#include "AudioEngineTestEffect.hpp"
#include "VST-Qt/qeffecteditor.hpp"
#include "qaudioenginetestgui.h"
#include "vt_midi.hpp"


AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
	return new CAudioEngineTestEffect (audioMaster);
}

CAudioEngineTestEffect::CAudioEngineTestEffect(audioMasterCallback audioMaster)
	: AudioEffectX(audioMaster, 1, 1),
	m_fVolume( 1.0f ),
	m_Stream1( 1048576 * 10 ),
	m_Stream2( 1048576 * 10 )
{
	setNumInputs( kNChannels );
	setNumOutputs( kNChannels );
	canProcessReplacing(); 

	//canDoubleReplacing (); -- now set in CVTAudioEffectX		
	//canMono (); // makes sense to feed both inputs with the same signal
	//#if defined(VT_SUPPORT_VST23)
	//	cEffect.flags |= 1<<3; // See aeffect.h line 214
	//#endif	

	setUniqueID ('Vaet');	

	bool t1 = m_Stream1.Open( "c:\\1\\test1.wav" );
	bool t2 = m_Stream2.Open( "c:\\1\\test2.wav" );
	if( !t1 || !t2 )
		QMessageBox::warning(NULL, "Whoops", "Either test1.wav or test2.wav was not found (or both).");

	m_Player.SetSampleType( VT::kFP32 );
	m_Player.SetNChannels( kNChannels );

	m_Pool.SetDefaultGuestLocation( VT::AudioEngine::CAudioPool::kGuestInSourceDir );

	editor = new CQEffectEditorT<QAudioEngineTestGUI>( this, "Cleanlooks" );
}

CAudioEngineTestEffect::~CAudioEngineTestEffect(void)
{
}

void CAudioEngineTestEffect::process(float** inputs, float** outputs, VstInt32 sampleFrames)
{
	m_Buffer.resize( sampleFrames*m_Player.GetNChannels() );
	m_Buffer.zero();
	m_Player.ProcessAdding( m_Buffer.getDataPtr(), sampleFrames );
	float 
		*in[kNChannels],  // Input pointers
		*out[kNChannels]; // Output pointers
	for(int i=0; i<kNChannels; i++){
		in[i] = inputs[i];
		out[i] = outputs[i];
	}
	const float *frameptr = m_Buffer.getRODataPtr();
	for( VstInt32 i = 0; i < sampleFrames; i++ ){
		*(out[0]++) += (*(in[0]++) + *(frameptr++)) * m_fVolume;
		*(out[1]++) += (*(in[1]++) + *(frameptr++)) * m_fVolume;
	}
}

void CAudioEngineTestEffect::processReplacing(float** inputs, float** outputs, VstInt32 sampleFrames)
{
	m_Buffer.resize( sampleFrames*m_Player.GetNChannels() );
	m_Buffer.zero();
	m_Player.ProcessAdding( m_Buffer.getDataPtr(), sampleFrames );
	float 
		*in[kNChannels],  // Input pointers
		*out[kNChannels]; // Output pointers
	for(int i=0; i<kNChannels; i++){
		in[i] = inputs[i];
		out[i] = outputs[i];
	}
	const float *frameptr = m_Buffer.getRODataPtr();
	for( VstInt32 i = 0; i < sampleFrames; i++ ){
		*(out[0]++) = (*(in[0]++) + *(frameptr++)) * m_fVolume;
		*(out[1]++) = (*(in[1]++) + *(frameptr++)) * m_fVolume;
	}
}

void CAudioEngineTestEffect::test1()
{
	//m_Player.RemoveStream( &m_Stream1 );
	//m_Stream1.Seek( 0 );
	m_Player.PlayWrapped( &m_Stream1 );
}

void CAudioEngineTestEffect::test2()
{
	//m_Player.RemoveStream( &m_Stream2 );
	//m_Stream2.Seek( 0 );
	m_Player.PlayWrapped( &m_Stream2 );
}

void CAudioEngineTestEffect::poolTest1()
{
	VT::AudioEngine::TClipJob job;
	job.SetInFileName("c:\\1\\test1.wav");
	job.m_OutFormat.m_fSampleRate = getSampleRate();
	job.m_OutFormat.m_SampleType = VT::kFP32;
	job.m_OutFormat.m_uChannels = m_Player.GetNChannels();
	job.m_StretchTransform.m_bEnabled = true;
	job.m_StretchTransform.m_fPitchFactor = VT::Notes::SemitonesToPitch( -6.0 );
	VT::AudioEngine::CClipStreamWrapper *stream = m_Pool.Open( &job );
	m_Player.Play( stream, 0, 1.0, true );
}