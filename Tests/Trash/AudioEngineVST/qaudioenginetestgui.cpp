
#include "qaudioenginetestgui.h"
#include "AudioEngineTestEffect.hpp"

QAudioEngineTestGUI::QAudioEngineTestGUI(QWidget *parent, AudioEffectX* effect):
    QWidget(parent),
    m_pTestEffect( dynamic_cast<CAudioEngineTestEffect*>(effect) )
{
    ui.setupUi(this);
}

QAudioEngineTestGUI::~QAudioEngineTestGUI()
{

}

void QAudioEngineTestGUI::test1()
{
    if( m_pTestEffect!=NULL )
        m_pTestEffect->test1();
}

void QAudioEngineTestGUI::test2()
{
    if( m_pTestEffect!=NULL )
        m_pTestEffect->test2();
}

void QAudioEngineTestGUI::poolTest1()
{
    if( m_pTestEffect!=NULL )
        m_pTestEffect->poolTest1();
}
