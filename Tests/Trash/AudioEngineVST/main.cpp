//-----------------------------------------------------------------------------
// VST iCenter filter
// 2008
// Programmer: Ivan A. Chalov
//-----------------------------------------------------------------------------


#include "AudioEngineTestEffect.hpp"

bool oome = false;

#if MAC
#pragma export on
#endif

//------------------------------------------------------------------------
// Prototype of the export function main
//------------------------------------------------------------------------
#if BEOS
#define main main_plugin
extern "C" __declspec(dllexport) AEffect *main_plugin (audioMasterCallback audioMaster);

#elif MACX
#define main main_macho
extern "C" AEffect *main_macho (audioMasterCallback audioMaster);

#else
int main (audioMasterCallback audioMaster);
#endif

//------------------------------------------------------------------------
int main (audioMasterCallback audioMaster)
{
	// Get VST Version
	if (!audioMaster (0, audioMasterVersion, 0, 0, 0, 0))
		return 0;  // old version

	// Create the AudioEffect
	CAudioEngineTestEffect* effect = new CAudioEngineTestEffect (audioMaster);
	if (!effect)
		return 0;

	// Check if no problem in constructor of ACompressor
	if (oome)
	{
		delete effect;
		return 0;
	}
	return int(effect->getAeffect ());
}

#if MAC
#pragma export off
#endif


/*

Not needed for VST SDK 2.4

//------------------------------------------------------------------------
#if WIN32
#include <windows.h>
void* hInstance;
BOOL WINAPI DllMain (HINSTANCE hInst, DWORD dwReason, LPVOID lpvReserved)
{
hInstance = hInst;
return 1;
}
#endif

*/