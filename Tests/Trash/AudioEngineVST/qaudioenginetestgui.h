#ifndef QAUDIOENGINETESTGUI_H
#define QAUDIOENGINETESTGUI_H

#include <QWidget>
#include "ui_qaudioenginetestgui.h"

class AudioEffectX;
class CAudioEngineTestEffect;

class QAudioEngineTestGUI : public QWidget
{
    Q_OBJECT

public:
    QAudioEngineTestGUI(QWidget *parent, AudioEffectX* effect);
    ~QAudioEngineTestGUI();

public slots:
    void test1();
    void test2();
    void poolTest1();

private:
    Ui::QAudioEngineTestGUIClass ui;
    CAudioEngineTestEffect *m_pTestEffect;
};

#endif // QAUDIOENGINETESTGUI_H

