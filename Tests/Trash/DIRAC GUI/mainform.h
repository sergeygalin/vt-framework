//---------------------------------------------------------------------------

#ifndef mainformH
#define mainformH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

#include "vt_baseclasses.hpp"
#include <ExtCtrls.hpp>
#include <MPlayer.hpp>
#include <Dialogs.hpp>
#include <Graphics.hpp>

class TForm1 : public TForm
{
__published:    // IDE-managed Components
    TButton *Button1;
    TProgressBar *TheProgressBar;
    TStatusBar *TheStatusBar;
    TEdit *InFileEdit;
    TEdit *OutFileEdit;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *DIRACInfoLabel;
    TPanel *Panel1;
    TMediaPlayer *MediaPlayer1;
    TLabel *MediaPlayerLabel;
    TButton *Button2;
    TButton *Button3;
    TButton *Button4;
    TOpenDialog *OpenDialog1;
    TSaveDialog *SaveDialog1;
    TButton *Button5;
    TComboBox *LambdaCB;
    TComboBox *QualityCB;
    TEdit *PitchEdit;
    TEdit *LengthEdit;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TLabel *Label7;
    TLabel *Label8;
    TButton *AbortButton;
    TLabel *CPULabel;
    TComboBox *ChannelsCB;
    TLabel *Label9;
    TUpDown *UpDown1;
    TUpDown *UpDown2;
    TLabel *Label10;
    TLabel *StretchQualityLabel;
    TBevel *Bevel1;
    TPanel *Panel2;
    TImage *Image1;
    TBevel *Bevel2;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label13;
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall Button4Click(TObject *Sender);
    void __fastcall Button5Click(TObject *Sender);
    void __fastcall AbortButtonClick(TObject *Sender);
    void __fastcall PitchEditChange(TObject *Sender);
private:    // User declarations

    volatile bool m_bProcessRunning, m_bAbortFlag;

    class TMyProgressDisplay: public VT::CProgressDisplay
    {
    public:
        TMyProgressDisplay( TForm1 *owner ):
            m_pOwner(owner)
            {}

        // Show operation progress message and update progress bar / % display.
        // percentDone can be negative, which indicates that % done cannot be estimated.
        virtual void PDShowProgress( float percentDone, LPCVTCHAR statusMessage=NULL );

        volatile TForm1 *m_pOwner;
    } m_ProgressDisplay;

    void __fastcall PlayFile( AnsiString name );

public:        // User declarations
    __fastcall TForm1(TComponent* Owner);
    virtual __fastcall ~TForm1();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
