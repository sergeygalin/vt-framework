//---------------------------------------------------------------------------

#include <vcl.h>
#include <Dialogs.hpp>
#pragma hdrstop

#include "AudioEngine/vt_audiostretching_dirac.hpp"
#include "DiracWrapper.h"
#include "vt_time.hpp"
#include "vt_midi.hpp"
#include "vt_audiomath.hpp"

#include "mainform.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner),
    m_ProgressDisplay( this ),
    m_bProcessRunning(false),
    m_bAbortFlag(false)
{
    AnsiString str;
    str += "Engine: ";
    str += VT::AudioEngine::CDIRACInterface::GetDIRACName();
    str += " via ";
    str += DW_Info();
    DIRACInfoLabel->Caption = str;

    str = LIBSIMD_Info();

    str += "   //   CPUID OK: ";
    str += VT::LCYESNO( VT::CPU::g_CPUID_Success );

    str += ";   MMX: ";
    str += VT::LCYESNO( VT::CPU::g_MMX );
    str += ";   SSE: ";
    str += VT::LCYESNO( VT::CPU::g_SSE );
    str += ";   SSE2: ";
    str += VT::LCYESNO( VT::CPU::g_SSE2 );
    str += ";   SSE3: ";
    str += VT::LCYESNO( VT::CPU::g_SSE3 );
    str += ";   FXSR: ";
    str += VT::LCYESNO( VT::CPU::g_FXSR );
    str += ";   HTT: ";
    str += VT::LCYESNO( VT::CPU::g_HTT );
    CPULabel->Caption = str;

    PitchEditChange( this );
}

__fastcall TForm1::~TForm1()
{
    m_ProgressDisplay.m_pOwner = NULL;
}

// Show operation progress message and update progress bar / % display.
// percentDone can be negative, which indicates that % done cannot be estimated.
void TForm1::TMyProgressDisplay::PDShowProgress( float percentDone, LPCVTCHAR statusMessage )
{
    if( m_pOwner!=NULL ){
        m_pOwner->TheProgressBar->Position = VT::Math::URound( percentDone );
        if( statusMessage != NULL )
            m_pOwner->TheStatusBar->SimpleText = statusMessage;
        else
            m_pOwner->TheStatusBar->SimpleText = "Working...";
    }
    Application->ProcessMessages();
}




//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    if( m_bProcessRunning )
        return;

    m_bProcessRunning = true;
    m_bAbortFlag = false;
    AbortButton->Enabled = true;
    Button1->Enabled = false;
    MediaPlayer1->Enabled = false;

    MediaPlayer1->Close();
    using namespace VT;

    double timeFactor, pitchST = 0.0, timePC = 100.0;
    try{
        timePC = StrToFloat( LengthEdit->Text );
    }catch(...){}
    try{
        pitchST = StrToFloat( PitchEdit->Text );
    }catch(...){}
    timeFactor = Math::Clip( timePC/100.0, 0.5, 2.0 );
    pitchST = Math::Clip( pitchST, -12.0, 12.0 );
    LengthEdit->Text = timeFactor*100.0;
    PitchEdit->Text = pitchST;

    CString infile( InFileEdit->Text.c_str() );
    CString outfile( OutFileEdit->Text.c_str() );

    int lambda; long l = kDiracLambda3;
    switch( Math::Clip( LambdaCB->ItemIndex, 0, 5 ) )
    {
        case 0: l = kDiracLambdaPreview; break;
        case 1: l = kDiracLambda1; break;
        case 2: l = kDiracLambda2; break;
        case 3: l = kDiracLambda3; break;
        case 4: l = kDiracLambda4; break;
        case 5: l = kDiracLambda5; break;
    }

    int quality; long q = kDiracQualityGood;
    switch( Math::Clip( QualityCB->ItemIndex, 0, 3 ) )
    {
        case 0: q = kDiracQualityPreview; break;
        case 1: q = kDiracQualityGood; break;
        case 2: q = kDiracQualityBetter; break;
        case 3: q = kDiracQualityBest; break;
    }


    Application->ProcessMessages();

    // GO GO GO, rockanroll!
    {

    double pitchFactor = Notes::SemitonesToPitch( pitchST );
    double time1 = UNIXTimeD();

    AudioEngine::CFileInputAudioStream in;
    AudioEngine::CFileOutputAudioStream out;

    in.Open( infile );

    if( !in.IsOK() ){
        MessageDlg( "Failed to open input file!", mtError, TMsgDlgButtons(mbOK), 0 );
        AbortButton->Enabled = false;
        Button1->Enabled = true;
        m_bProcessRunning = false;
        MediaPlayer1->Enabled = true;
        return;
    }

    int nOutChannels = ChannelsCB->ItemIndex;
    if( nOutChannels<=0 || nOutChannels>2 )
        nOutChannels = in.GetNChannels();

    out.Open( outfile, false, kInt16S, in.GetSampleRate(), nOutChannels );

    if( !out.IsOK() ){
        MessageDlg( "Failed to open output file!", mtError, TMsgDlgButtons(mbOK), 0 );
        AbortButton->Enabled = false;
        Button1->Enabled = true;
        m_bProcessRunning = false;
        MediaPlayer1->Enabled = true;
        return;
    }

    AudioEngine::CDIRACInterface dirac;
    dirac.SetProcessAbortFlag( &m_bAbortFlag );
    dirac.SetProgressDisplay( &m_ProgressDisplay );
    dirac.SetDIRACLambda( l );
    dirac.SetDIRACQuality( q );

    if( !dirac.PrepareProcess( &in, &out ) ){
        if( m_bAbortFlag )
            MessageDlg( "Aborted.", mtInformation, TMsgDlgButtons(mbOK), 0 );
        else
            MessageDlg( "Initialization error.", mtInformation, TMsgDlgButtons(mbOK), 0 );
        AbortButton->Enabled = false;
        Button1->Enabled = true;
        m_bProcessRunning = false;
        MediaPlayer1->Enabled = true;
        return;
    }

    dirac.SetPitchFactor( pitchFactor );
    dirac.SetTimeFactor( timeFactor );

    double time2 = UNIXTimeD();

    if( !dirac.DoProcess() ){
        if( m_bAbortFlag )
            MessageDlg( "Aborted.", mtInformation, TMsgDlgButtons(mbOK), 0 );
        else
            MessageDlg( "Processing error.", mtInformation, TMsgDlgButtons(mbOK), 0 );
        AbortButton->Enabled = false;
        Button1->Enabled = true;
        m_bProcessRunning = false;
        MediaPlayer1->Enabled = true;
        return;
    }

    double end = UNIXTimeD();

    VTCHAR buf[256];
    VTSPRINTF( buf, VTT("Done, DoProcess() time: %.2f, total time: %.2f (%.3f X input, %.3f X output)\n"),
        float(end-time2), float(end-time1),
        float((end-time2)/in.GetLengthSec()),
        float((end-time2)/out.GetLengthSec()));
    TheStatusBar->SimpleText = buf;

    }

    PlayFile( outfile.Str() );

    AbortButton->Enabled = false;
    Button1->Enabled = true;
    m_bProcessRunning = false;
    MediaPlayer1->Enabled = true;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
    PlayFile( InFileEdit->Text );
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
    PlayFile( OutFileEdit->Text );
}
//---------------------------------------------------------------------------

void __fastcall TForm1::PlayFile( AnsiString name )
{
    MediaPlayerLabel->Caption = name;
    try{
        MediaPlayer1->AutoEnable = true;
        MediaPlayer1->AutoOpen = true;
        MediaPlayer1->FileName = name;
        MediaPlayer1->Open();
    }catch( Exception& ex ){
        MessageDlg( ex.Message, mtError, TMsgDlgButtons(mbOK), 0 );
    }
}
void __fastcall TForm1::Button4Click(TObject *Sender)
{
    if( OpenDialog1->Execute() ){
        InFileEdit->Text = OpenDialog1->FileName;
        PlayFile( OpenDialog1->FileName );
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
    if( SaveDialog1->Execute() ){
        OutFileEdit->Text = SaveDialog1->FileName;
        PlayFile( SaveDialog1->FileName );
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::AbortButtonClick(TObject *Sender)
{
    m_bAbortFlag = true;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::PitchEditChange(TObject *Sender)
{
    double timeFactor, pitchST = 0.0, timePC = 100.0;
    try{
        timePC = StrToFloat( LengthEdit->Text );
    }catch(...){}
    try{
        pitchST = StrToFloat( PitchEdit->Text );
    }catch(...){}
    timeFactor = VT::Math::Clip( timePC/100.0, 0.5, 2.0 );
    pitchST = VT::Math::Clip( pitchST, -12.0, 12.0 );
    double pitchFactor = VT::Notes::SemitonesToPitch( pitchST );
    double q = VT::AudioEngine::GetPitchConversionQuality(
        pitchFactor, timeFactor );

    char buf[32];
    sprintf( buf, "%.04f%%", float(q*100.0) );
    StretchQualityLabel->Caption = buf;
}
//---------------------------------------------------------------------------



