
#define _CRT_SECURE_NO_DEPRECATE

#include <string.h>
#include "vt_crypto.hpp"
#include "vt_cryptodir.hpp"
#include "vt_string.hpp"
#include "Registration/vt_pcid.hpp"
#include "Registration/vt_reginfosaver.hpp"
#include "Registration/vt_regkeys.hpp"

#if !defined(_DEBUG)
    #pragma comment(lib, "../../../../Lib/cryptlib.lib")
#else
    #pragma comment(lib, "../../../../Lib/Debug/cryptlib.lib")
#endif

using namespace VT;

const char xfile[]=
    "Mary had a little lamb,\n"
    "Its fleece was white as snow;\n"
    "And everywhere that Mary went,\n"
    "The lamb was sure to go.\n"
    "He followed her to school one day;\n"
    "Which was against the rule;\n"
    "It made the children laugh and play;\n"
    "To see a lamb at school.\n"
    "\"Why does the lamb love Mary so?\"\n"
    "The eager children cry;\n"
    "\"Why, Mary loves the lamb, you know,\"\n"
    "The teacher did reply.\n";

const char testmsg[]=
    "This is a test message 1234.\n"
    "It simply isn't fair to you or me. "
    "The worst crime I can think of would be to rip "
    "people off by faking it and pretending as if I'm having 100% fun.";

const char key[]="Hello!!! And what's going on here, actually? // Over 32 chars";

void PCIdTest()
{
    printf("*** PC Id Test ***\n");
    LPCVTCHAR id = Registration::GetPCId();
    printf("ID = \"%s\"\n", id);
    printf("PC Id Test done.\n");
}

void MachineStorage()
{
    printf("*** Machine associated file test ***\n");
    LPCVTCHAR fn = VTT("test_pcfile.dat");
    Registration::CMachineAssociatedFile file1, file2;
    file1.SetKey( (const BYTE*)key );
    file2.SetKey( (const BYTE*)key );
    if( !file1.write( xfile, strlen(xfile), 1 ) ){
        printf("File1 write error!");
        return;
    }
    if( !file1.SaveRegFile( fn ) ){
        printf("File1 SaveRegFile error!");
        return;
    }
    if( !file2.LoadRegFile( fn ) ){
        printf("File2 LoadRegFile error!");
        return;
    }
    CString str;
    str.CopyFrom( LPCVTCHAR(file2.getRODataPtr()), file2.getMFSize() );
    printf(
        "==================================\n"
        "%s\n"
        "==================================\n", str.Str() );

    // File name creation test
    CString xfn;

    file1.CreateFileName( &xfn, VTT("Virartech"), VTT("Reg Test App"),
        NULL, true, false );
    printf("Test FN1: \"%s\"\n", xfn.Str() );
    file1.CreateFileName( &xfn, VTT("Virartech"), VTT("Reg Test App"),
        VTT("test.key"), false, false );
    printf("Test FN2 \"%s\"\n", xfn.Str() );



    printf("Machine associated file test done.\n");
}


void RegKeyTest()
{
    CString tmp;
    printf("*** Registration Key test ***\n");
    Registration::CRegistrationKey_Scheme1 regkey;

    regkey.Generate("Max Payne");
    regkey.GetAsText( &tmp );
    printf("Key: %s\n", tmp.Str() );

    regkey.Generate("\t         M A X    P A Y N E       \t");
    regkey.GetAsText( &tmp );
    printf("Key: %s\n", tmp.Str() );

    regkey.SetKey( (const BYTE*)key, strlen(key) );
    regkey.Generate("Max Payne");
    regkey.GetAsText( &tmp );
    printf("Key: %s\n", tmp.Str() );

    regkey.Generate("Mona Sax");
    regkey.GetAsText( &tmp );
    printf("Key: %s\n", tmp.Str() );

    printf("Registration Key test done.\n");


}



int main(int argc, char *argv[])
{
    printf("Registration test!\n\n");
    PCIdTest();
    MachineStorage();
    RegKeyTest();
    return 0;
}


