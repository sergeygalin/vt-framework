// AudioReadAny.cpp : Defines the entry point for the console application.
//

#include "AudioEngine/vt_audiostreamconverter.hpp"

int main(int argc, char* argv[])
{
    size_t nf;
    unsigned channels, sr;
    VT::TSampleTypeIdx sampleType = VT::kFP32; //VT::kInt16S;

    BYTE* data = VT::AudioEngine::LoadSoundToBuffer( VT::CString("Test.wav"), &nf, &channels, &sr, sampleType );
    if( data==NULL ){
        printf("Error loading sound from Test.wav!\n");

    }else{
        printf(
            "Sound loaded successfully!\n"
            "Number of frames = %u\n"
            "Channels = %u\n"
            "Sampling Rate = %u\n", unsigned(nf), channels, sr );
        VT::CDiskFile outf( VT::CString("Test-out.raw"), VT::kOpenForWriting );
        outf.writeb( data, nf*channels*GetAudioTypeSize(sampleType) );
        delete[] data;
    }
    getchar();
    return 0;
}

