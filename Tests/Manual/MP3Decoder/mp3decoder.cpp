#include "../../vt_mp3decode.hpp"

#pragma comment(lib, "../../../../Lib/libmpg123-0.lib")

using namespace VT;

void callback(long nsamples, long currentFrame, long framesLeft,
        double posSeconds, double leftSeconds)
{
    printf("%5.1lf/%5.1lf Seconds * %5ld/%5ld Frames * %8ld Samples           \r",
        posSeconds, leftSeconds, currentFrame, framesLeft,
        nsamples);
}

#if !defined(_UNICODE)
int main(int argc, char* argv[])
{
    printf("Hello World.\n");


    int result = (int) MP3FileDecoder::Decode("1.mp3", "1.wav", callback);

    printf("\n");
    printf("Exited with result: %d\n", result);

    if( MP3FileDecoder::GetLastErrorMessage()!=NULL )
        printf("Returned error message: %s\n", MP3FileDecoder::GetLastErrorMessage());

    return 0;
}
#else
int _tmain(int argc, TCHAR* argv[])
{
    printf("Hello World.\n");

    int result = (int) VTMP3Dec::Decode(VTT("�������.mp3"), VTT("�������.wav"), callback);

    printf("\n");
    printf("Exited with result: %d\n", result);

    if( VTMP3Dec::GetLastErrorMessage()!=NULL )
        printf("Returned error message: %s\n", VTMP3Dec::GetLastErrorMessage());

    return 0;
}
#endif

