
#include <time.h>

#include "vt_audiomath.hpp"
#include "vt_localization.hpp"
#include "vt_simd.hpp"
#include "vt_buffers.hpp"

using namespace VT;

#include <sys/timeb.h>

#define FIRST_TEST


class TableSinus
{
public:
    TableSinus(){
        double step = Math::k2Pi/double(kTableSize);
        for( size_t i=0; i<kTableSize; i++ )
            m_fTable[i] = float(sin( step*double(i) ));
    }
    VTINLINE float Sinus( float arg ){
        return m_fTable[ int(fmod( arg, float(Math::k2Pi)) *kTableSize) ];
    }
private:
    static const size_t kTableSize = 100000;
    float m_fTable[ kTableSize ];

};
static TableSinus fsinus;


VTINLINE double TaylorSinus( double x )
{
    // Taylor sum for sinus:
    // sin(x) = SUM(0..INF)( (-1)^n * X^(2n+1) / (2n+1)! )
    double x2 = x*x, xpow, sum;

    xpow  = x;  sum =  xpow;
    xpow *= x2; sum -= xpow / 6.0;
    xpow *= x2;    sum += xpow / 120.0;
    xpow *= x2;    sum -= xpow / 5040.0;

    xpow *= x2;    sum += xpow / 362880.0;
    xpow *= x2;    sum -= xpow / 39916800.0;
//    xpow *= x2;    sum += xpow / 6227020800.0;
//    xpow *= x2;    sum -= xpow / 1307674368000.0;

//    xpow *= x2;    sum += xpow / 355687428096000.0;
//    xpow *= x2;    sum -= xpow / 1.21645100408832e+017;
//    xpow *= x2;    sum += xpow / 5.10909421717094e+019;
//    xpow *= x2;    sum -= xpow / 2.5852016738885e+022;

//    xpow *= x2;    sum += xpow / 1.5511210043331e+025;
//    xpow *= x2;    sum -= xpow / 1.08888694504184e+028;
//    xpow *= x2;    sum += xpow / 8.8417619937397e+030;

    return sum;

/*
    __declspec(align(16)) float load[4];

    const __declspec(align(16)) float
        mul1[4] = { 1.0f, -6.0f, 120.0f, -5040.0f },
        mul2[4] = { 362880.0f, -39916800.0f, 6227020800.0f, -1307674368000.0f },
        mul3[4] = { 355687428096000.0f, -1.21645100408832e+017f, 5.10909421717094e+019f, -2.5852016738885e+022f };

    float xpow = x, x2 = x*x;
               load[0] = xpow; // x        x^9
    xpow *=x2; load[1] = xpow; // x^3    x^11
    xpow *=x2; load[2] = xpow; // x^5    x^13
    xpow *=x2; load[3] = xpow; // x^7    x^15 ...

    __m128 mmxpow = _mm_load_ps( load );
    float x8 = x2*x2; x8*=x8;
    __m128 mm_x8 = _mm_set1_ps( x8 );
    __m128 sum = _mm_div_ps( mmxpow, _mm_load_ps( mul1 ) );
    mmxpow = _mm_mul_ps( mmxpow, mm_x8 );
    sum = _mm_add_ps( sum, _mm_div_ps( mmxpow, _mm_load_ps( mul2 ) ) );
    mmxpow = _mm_mul_ps( mmxpow, mm_x8 );
    sum = _mm_add_ps( sum, _mm_div_ps( mmxpow, _mm_load_ps( mul3 ) ) );

    _mm_store_ps( load, sum );

    return load[0]+load[1]+load[2]+load[3];
    */
}




inline double mmtime()
{
    struct timeb t;
    ftime( &t );
    return double(t.time) + double(t.millitm)/1000.0f;
}

void SinTest()
{
    printf("Sinus test!\n");
    double t;
    float sum;
    const long kLength = 1000000l;
    const int repeat = 5;
    static float table[kLength];
    for( long i=0; i<kLength; i++ )
        table[i] = 100.0f*float(rand())/float(RAND_MAX);

    for( int x=0; x<9; x++ ){
        double
            arg = double(x),
            s = sin(arg),
            ts = TaylorSinus(arg),
            err = Math::Abs( s-ts );
        printf("%6.3lf =>      Error = %.10f      sin = %13.10lf     TaylorSin = %13.10lf\n",
            arg,
            err,
            s,
            ts);
    }

    getchar();


    printf("Math sinus...");
    fflush(stdout);
    sum = 0;
    t = mmtime();
    for( int r=0; r<repeat; r++ )
        for( long i=0; i<kLength; i++ )
            sum+=sin(table[i]);
    t = mmtime()-t;
    printf("%f, %.3lf sec.\n", sum, t);

    printf("Table sinus...");
    fflush(stdout);
    sum = 0;
    t = mmtime();
    for( int r=0; r<repeat; r++ )
        for( long i=0; i<kLength; i++ )
            sum+=fsinus.Sinus(table[i]);
    t = mmtime()-t;
    printf("%f, %.3lf sec.\n", sum, t);

    printf("Taylor sinus...");
    fflush(stdout);
    sum = 0;
    t = mmtime();
    for( int r=0; r<repeat; r++ )
        for( long i=0; i<kLength; i++ )
            sum+=float(TaylorSinus(table[i]));
    t = mmtime()-t;
    printf("%f, %.3lf sec.\n", sum, t);
}


void ShowCPUInfo()
{
    // Detecting CPU
    printf("== CPU Information Gathered ==\n");

    printf("Chip Vendor: \"%s\"\n", CPU::GetVendor());
    printf("Number of CPU's detected: %u\n", CPU::GetNumberOfCPUs() );

    if( CPU::g_CPUID_Success ){
        printf(
            "FPU on chip......... %s\n"
            "MMX................. %s\n"
            "FXSR................ %s\n"
            "SSE................. %s\n"
            "SSE2................ %s\n"
            "SSE3................ %s\n"
            "Hyper Threading..... %s\n"
            "\n",
            LCYESNO(CPU::g_FPU),
            LCYESNO(CPU::g_MMX),
            LCYESNO(CPU::g_FXSR),
            LCYESNO(CPU::g_SSE),
            LCYESNO(CPU::g_SSE2),
            LCYESNO(CPU::g_SSE3),
            LCYESNO(CPU::g_HTT));
    }else
        printf("CPUID was not performed.\n\n");

}


#if defined(FIRST_TEST)

void FirstTest()
{
    printf("\nThe First SSE test!!\n\n");



    // Preparing data
    const int kDataSize = 603;
    static float data1[kDataSize], data2[kDataSize], data3[kDataSize], data4[kDataSize];
    static double ddata1[kDataSize], ddata2[kDataSize], ddata3[kDataSize], ddata4[kDataSize];
    static float data1s[kDataSize], data2s[kDataSize], data3s[kDataSize], data4s[kDataSize];
    static double ddata1s[kDataSize], ddata2s[kDataSize], ddata3s[kDataSize], ddata4s[kDataSize];
    long double lddata1[kDataSize], lddata2[kDataSize];
    for(int i=0; i<kDataSize; i++){
        ddata1[i] = ddata1s[i] = data1s[i] = data1[i] = float(rand());
        ddata2[i] = ddata2s[i] = data2s[i] = data2[i] = float(rand());
        ddata3[i] = ddata3s[i] = data3s[i] = data3[i] = float(rand());
        ddata4[i] = ddata4s[i] = data4s[i] = data4[i] = float(rand());
        lddata1[i] = (long double)(data1[i]);
        lddata2[i] = (long double)(data2[i]);
    }

    printf("CONVOLUTION TEST\n");
    printf("Float, C++:               %e\n", SIMD::Convolve_CPP(data1, data2, kDataSize));
    printf("Float, Accelerated:       %e\n", SIMD::Convolve(data1, data2, kDataSize));
    printf("Double, C++:              %le\n", SIMD::Convolve_CPP(ddata1, ddata2, kDataSize));
    printf("Double Accelerated:       %le\n", SIMD::Convolve(ddata1, ddata2, kDataSize));
    printf("Long Double, C++:         %le\n", double(SIMD::Convolve_CPP(lddata1, lddata2, kDataSize)));
    printf("Long Double, Accelerated: %le\n", double(SIMD::Convolve(lddata1, lddata2, kDataSize)));
    printf("\n");

    printf("Hit <Enter> to continue...\n");
    getchar();





    size_t n = Math::Min( 7, kDataSize );
    const float kMulti = float(Math::kPi);
    float out[kDataSize];
    double dout[kDataSize];
    float t1, t2, t3;

    #define TEST_MIXER( NAME, FNCALL1, FNCALL2, FNCALL3 )                   \
        {                                                                   \
            printf("**************************************************\n"); \
            printf("%s test\n", (const char*)(NAME));                       \
            printf("**************************************************\n"); \
            memcpy( out, data1, n*sizeof(float) );                          \
            FNCALL1;                                                        \
            printf("C++:      ");                                           \
            for( size_t i=0; i<n; i++ ) printf("%10.3f ", out[i]);          \
            printf("\n");                                                   \
            memcpy( out, data1, n*sizeof(float) );                          \
            FNCALL2;                                                        \
            printf("Accel:    ");                                           \
            for( size_t i=0; i<n; i++ ) printf("%10.3f ", out[i]);          \
            printf("\n");                                                   \
            memcpy( dout, ddata1, n*sizeof(double) );                       \
            FNCALL3;                                                        \
            printf("Accel/DP: ");                                           \
            for( size_t i=0; i<n; i++ ) printf("%10.3lf ", dout[i]);        \
            printf("\n\n");                                                 \
        }




    TEST_MIXER( "AddVector()",
        SIMD::AddVector_CPP( out, data2, n ),
        SIMD::AddVector( out, data2, n ),
        SIMD::AddVector( dout, ddata2, n ));

    TEST_MIXER( "AddVectorMultiplied()",
        SIMD::AddVectorMultiplied_CPP( out, data2, kMulti, n ),
        SIMD::AddVectorMultiplied( out, data2, kMulti, n ),
        SIMD::AddVectorMultiplied( dout, ddata2, double(kMulti), n ));

    TEST_MIXER( "MultiplyVector( vector, vector, n )",
        SIMD::MultiplyVector_CPP( out, data2, n ),
        SIMD::MultiplyVector( out, data2, n ),
        SIMD::MultiplyVector( dout, ddata2, n ));

    TEST_MIXER( "MultiplyVector( vector, vector, k, n )",
        SIMD::MultiplyVectorK_CPP( out, data2, kMulti, n ),
        SIMD::MultiplyVectorK( out, data2, kMulti, n ),
        SIMD::MultiplyVectorK( dout, ddata2, double(kMulti), n ) );

    TEST_MIXER( "MultiplyByScaledAndShiftedVector( vector, vector, scale, shift, n )",
        SIMD::MultiplyByScaledAndShiftedVector_CPP( out, data2, kMulti, 1.0f, n ),
        SIMD::MultiplyByScaledAndShiftedVector( out, data2, kMulti, 1.0f, n ),
        SIMD::MultiplyByScaledAndShiftedVector( dout, ddata2, double(kMulti), double(1.0), n ));

    TEST_MIXER( "MultiplyAndShift( vector, scale, shift, n )",
        SIMD::MultiplyAndShift_CPP( out, kMulti, 1.0f, n ),
        SIMD::MultiplyAndShift( out, kMulti, 1.0f, n ),
        SIMD::MultiplyAndShift( dout, double(kMulti), double(1.0), n ));

    TEST_MIXER( "SumScaledVectors2( result, x1, k1, x2, k2, n )",
        SIMD::SumScaledVectors2_CPP( out, data1, 0.2f, data2, -0.6f, n ),
        SIMD::SumScaledVectors2( out, data1, 0.2f, data2, -0.6f, n ),
        SIMD::SumScaledVectors2( dout, ddata1, double(0.2), ddata2, double(-0.6), n ));

    TEST_MIXER( "SumScaledVectors3( result, x1, k1, x2, k2, x3, k3, n )",
        SIMD::SumScaledVectors3_CPP( out, data1, 0.2f, data2, -0.6f, data3, 3.1f, n ),
        SIMD::SumScaledVectors3( out, data1, 0.2f, data2, -0.6f, data3, 3.1f , n ),
        SIMD::SumScaledVectors3( dout, ddata1, double(0.2), ddata2, double(-0.6), ddata3, double(3.1), n ));

    TEST_MIXER( "SumScaledVectors4( result, x1, k1, x2, k2, x3, k3, x4, k4, n )",
        SIMD::SumScaledVectors4_CPP( out, data1, 0.2f, data2, -0.6f, data3, 3.1f, data4, -2.2f, n ),
        SIMD::SumScaledVectors4( out, data1, 0.2f, data2, -0.6f, data3, 3.1f, data4, -2.2f, n ),
        SIMD::SumScaledVectors4( dout, ddata1, double(0.2), ddata2, double(-0.6), ddata3, double(3.1), ddata4, double(-2.2), n ));

    TEST_MIXER( "SumVector()",
        out[0] = SIMD::SumVector_CPP( out, n ),
        out[0] = SIMD::SumVector( out, n ),
        dout[0] = SIMD::SumVector( dout, n ));

    TEST_MIXER( "FillVector()",
        SIMD::FillVector_CPP( out, (float)Math::kPi, n ),
        SIMD::FillVector( out, (float)Math::kPi, n ),
        SIMD::FillVector( dout, (double)Math::kPi, n ) );

    TEST_MIXER( "FillLinear()",
        t1=SIMD::FillLinear_CPP( out, 3.14f, 1.0f, n ),
        t2=SIMD::FillLinear( out, 3.14f, 1.0f, n ),
        t3=float(SIMD::FillLinear( dout, 3.14, 1.0, n )) );
    printf("T1 = %f T2=%f T3 = %f\n", t1, t2, t3);



    TEST_MIXER( "SumVectorScaledVectors3()",
        SIMD::SumVectorScaledVectors3_CPP( out, data1, data2, data2, data3, data1, data3, n ),
        SIMD::SumVectorScaledVectors3( out, data1, data2, data2, data3, data1, data3, n ),
        SIMD::SumVectorScaledVectors3( dout, ddata1, ddata2, ddata2, ddata3, ddata1, ddata3, n ) );


    TEST_MIXER( "Multiply()",
        SIMD::Multiply_CPP( out, 3.14f, n ),
        SIMD::Multiply( out, 3.14f, n ),
        SIMD::Multiply( dout, 3.14, n ) );

    TEST_MIXER( "AddLinearCrossfade()",
        t1=SIMD::AddLinearCrossfade_CPP( out, data1, data2, 0.9f, 0.01f, n ),
        t2=SIMD::AddLinearCrossfade( out, data1, data2, 0.9f, 0.01f, n ),
        t3=float(SIMD::AddLinearCrossfade( dout, ddata1, ddata2, 0.9, 0.01, n )) );
    printf("T1 = %f T2=%f T3 = %f\n", t1, t2, t3);

    TEST_MIXER( "LinearFade()",
        t1=SIMD::LinearFade_CPP( out, 0.9f, 0.01f, n ),
        t2=SIMD::LinearFade( out, 0.9f, 0.01f, n ),
        t3=float(SIMD::LinearFade( dout, 0.9, 0.01, n ) ) );
    printf("T1 = %f T2=%f T3 = %f\n", t1, t2, t3);

    TEST_MIXER( "AddLinearFade()",
        t1=SIMD::AddLinearFade_CPP( out, data1, 0.9f, 0.01f, n ),
        t2=SIMD::AddLinearFade( out, data1, 0.9f, 0.01f, n ),
        t3=float(SIMD::AddLinearFade( dout, ddata1, 0.9, 0.01, n ) ));
    printf("T1 = %f T2=%f T3 = %f\n", t1, t2, t3);


    TEST_MIXER( "SumVectors3()",
        SIMD::SumVectors3_CPP( out, data1, data2, data3, n ),
        SIMD::SumVectors3( out, data1, data2, data3, n ),
        SIMD::SumVectors3( dout, ddata1, ddata2, ddata3, n ) );

    #if 1
        printf("Perform timing test? Type <y><Enter> to continue...\n");
        if( getchar()!='y' )
            return;


        const unsigned long kIter = 10000000lu;
        unsigned long iter;
        double time1;
        double sum;

        printf("Performing timing test, %lu iterations...\n", kIter);

        //
        // :-))))))))))))))))))))))))))))))
        //
        // NOTE: sum1 and sum MUST be counted during test, or compiler's optimization
        // will throw CPP versions of functions with const pointer arguments out.
        // These variables also MUST be printer to user, or compiler throws them
        // aways as well, then... see above!!
        //
        // :-))))))))))))))))))))))))))))))
        //

        #define TIMING_TEST( message, FNCALL )                                  \
            printf("%-36s ---> ", (const char*)(message));                      \
            memcpy( data1, data1s, sizeof(data1) );                             \
            memcpy( ddata1, ddata1s, sizeof(ddata1) );                          \
            fflush( stdout );                                                   \
            time1 = mmtime();                                                   \
            sum = 0;                                                            \
            for( iter=0; iter<kIter; iter++ ){                                  \
                FNCALL;                                                         \
            }                                                                   \
            time1 = mmtime()-time1;                                             \
            printf("%5.2lf sec, result=%le\n", time1, sum);

/*
        TIMING_TEST( "Linear Fill, Float, C++",             for(int j=0; j<2; j++){ sum+=SIMD::FillLinear_CPP(data1, 3.14f, 1.0f, kDataSize); } );
        TIMING_TEST( "Linear Fill, Float, Accelerated",     for(int j=0; j<2; j++){ sum+=SIMD::FillLinear(data1, 3.14f, 1.0f, kDataSize); } );
        TIMING_TEST( "Linear Fill, Double, Accelerated",    for(int j=0; j<2; j++){ sum+=SIMD::FillLinear(ddata1, 3.14, 1.0, kDataSize); } );

*/


        #if 1
            TIMING_TEST( "Clip, Float, C++", SIMD::Clip_CPP(data1, kDataSize, 25.0f, 10000.0f); sum += data1[kDataSize-1]+data1[kDataSize-10]; );
            TIMING_TEST( "Clip, Float, Accelerated", SIMD::Clip(data1, kDataSize, 25.0f, 10000.0f); sum += data1[kDataSize-1]+data1[kDataSize-10]; );
            TIMING_TEST( "Clip, Double, C++", SIMD::Clip_CPP<double>(ddata1, kDataSize, 25.0, 10000.0); sum += ddata1[kDataSize-1]+ddata1[kDataSize-10]; );
            TIMING_TEST( "Clip, Double, Accelerated", SIMD::Clip<double>(ddata1, kDataSize, 25.0, 10000.0); sum += ddata1[kDataSize-1]+ddata1[kDataSize-10]; );
        #endif

        #if 1
            TIMING_TEST( "Vector Sum of Squares, Float, C++",                sum+=SIMD::SumVectorSquares_CPP(data1, kDataSize) );
            TIMING_TEST( "Vector Sum of Squares, Float, Accelerated",        sum+=SIMD::SumVectorSquares(data1, kDataSize) );
            TIMING_TEST( "Vector Sum of Squares, Double, C++",               sum+=SIMD::SumVectorSquares_CPP(ddata1, kDataSize) );
            TIMING_TEST( "Vector Sum of Squares, Double, Accelerated",       sum+=SIMD::SumVectorSquares(ddata1, kDataSize) );
        #endif

        #if 1
            #if !defined(VTSIMD_SSEINTRINSICS_DISABLE_VECTORMIN_VECTORMAX_FLOAT)
                TIMING_TEST( "Vector Max, Float, C++",                sum+=SIMD::VectorMax_CPP(data1, kDataSize) );
                TIMING_TEST( "Vector Max, Float, Accelerated",        sum+=SIMD::VectorMax(data1, kDataSize) );
                TIMING_TEST( "Vector Min, Float, C++",                sum+=SIMD::VectorMin_CPP(data1, kDataSize) );
                TIMING_TEST( "Vector Min, Float, Accelerated",        sum+=SIMD::VectorMin(data1, kDataSize) );
            #endif
            #if !defined(VTSIMD_SSEINTRINSICS_DISABLE_VECTORMIN_VECTORMAX_DOUBLE)
                TIMING_TEST( "Vector Max, Double, C++",               sum+=SIMD::VectorMax_CPP(ddata1, kDataSize) );
                TIMING_TEST( "Vector Max, Double, Accelerated",       sum+=SIMD::VectorMax(ddata1, kDataSize) );
                TIMING_TEST( "Vector Min, Double, C++",               sum+=SIMD::VectorMin_CPP(ddata1, kDataSize) );
                TIMING_TEST( "Vector Min, Double, Accelerated",       sum+=SIMD::VectorMin(ddata1, kDataSize) );
            #endif
        #endif


        #if 1
            {
                float min32, max32;
                TIMING_TEST( "Vector Min/Max, Float, C++",                SIMD::VectorMinMax_CPP(data1, kDataSize, &min32, &max32) );
                printf("...........Min = %f, Max = %f\n", min32, max32 );
                TIMING_TEST( "Vector Min/Max, Float, Accelerated",        SIMD::VectorMinMax(data1, kDataSize, &min32, &max32) );
                printf("...........Min = %f, Max = %f\n", min32, max32 );
            }

            {
                double min64, max64;
                TIMING_TEST( "Vector Min/Max, Double, C++",                SIMD::VectorMinMax_CPP(ddata1, kDataSize, &min64, &max64) );
                printf("...........Min = %lf, Max = %lf\n", min64, max64 );
                TIMING_TEST( "Vector Min/Max, Double, Accelerated",        SIMD::VectorMinMax(ddata1, kDataSize, &min64, &max64) );
                printf("...........Min = %lf, Max = %lf\n", min64, max64 );
            }
        #endif


        #if 1
            TIMING_TEST( "Linear Fade, Float, C++",             for(int j=0; j<2; j++){ sum+=SIMD::LinearFade_CPP(data1, 3.14f, 1.0f, kDataSize); } );
            TIMING_TEST( "Linear Fade, Float, Accelerated",     for(int j=0; j<2; j++){ sum+=SIMD::LinearFade(data1, 3.14f, 1.0f, kDataSize); } )
            TIMING_TEST( "Linear Fade, Double, C++",            for(int j=0; j<2; j++){ sum+=SIMD::LinearFade_CPP(ddata1, 3.14, 1.0, kDataSize); } );;
            TIMING_TEST( "Linear Fade, Double, Accelerated",    for(int j=0; j<2; j++){ sum+=SIMD::LinearFade(ddata1, 3.14, 1.0, kDataSize); } );
        #endif

        #if 1 //defined(__GNUC__)
            TIMING_TEST( "Add Linear Fade, Float, C++",             for(int j=0; j<2; j++){ sum+=SIMD::AddLinearFade_CPP(data1, data2, 0.9f, 0.000001f, kDataSize); } );
            TIMING_TEST( "Add Linear Fade, Float, Accelerated",     for(int j=0; j<2; j++){ sum+=SIMD::AddLinearFade(data1, data2, 0.9f, 0.000001f, kDataSize); } );
            TIMING_TEST( "Add Linear Fade, Double, C++",            for(int j=0; j<2; j++){ sum+=SIMD::AddLinearFade_CPP(ddata1, ddata2, 0.9, 0.000001, kDataSize); } );
            TIMING_TEST( "Add Linear Fade, Double, Accelerated",    for(int j=0; j<2; j++){ sum+=SIMD::AddLinearFade(ddata1, ddata2, 0.9, 0.000001, kDataSize); } );
        #endif


        //TIMING_TEST( "Crossfade, Float, C++",                sum+=SIMD::AddLinearCrossfade_CPP(data1, data2, data3, 1.0f, 0.000001f, kDataSize) );
        //TIMING_TEST( "Crossfade, Float, Accelerated",        sum+=SIMD::AddLinearCrossfade(data1, data2, data3, 1.0f, 0.000001f, kDataSize) );



        #if 1
            TIMING_TEST( "Convolution, Float, C++",             sum+=SIMD::Convolve_CPP(data1, data2, kDataSize) );
            TIMING_TEST( "Convolution, Float, Accelerated",     sum+=SIMD::Convolve(data1, data2, kDataSize) );
            TIMING_TEST( "Convolution, Double, C++",            sum+=SIMD::Convolve_CPP(ddata1, ddata2, kDataSize) );
            TIMING_TEST( "Convolution, Double, Accelerated",    sum+=SIMD::Convolve(ddata1, ddata2, kDataSize) );
        #endif

/*
        TIMING_TEST( "Vector Sum, Float, C++",                sum+=SIMD::SumVector_CPP(data1, kDataSize) );
        TIMING_TEST( "Vector Sum, Float, Accelerated",        sum+=SIMD::SumVector(data1, kDataSize) );
        TIMING_TEST( "Vector Sum, Double, C++",                sum+=SIMD::SumVector_CPP(ddata1, kDataSize) );
        TIMING_TEST( "Vector Sum, Double, Accelerated",        sum+=SIMD::SumVector(ddata1, kDataSize) );
*/

        // ****************************************************************************
        // *** VectorFill() ***********************************************************
        // ****************************************************************************

        #if 1 //defined(__GNUC__)
            TIMING_TEST( "Vector Fill, Float, C++",
                for(int j=0; j<5; j++){
                    SIMD::FillVector_CPP(data1, float(rand()), kDataSize); sum += data1[kDataSize-1]; } );
            #if !defined(VTSIMD_MSCSSE_DISABLE_FILLVECTOR_FLOAT)
                TIMING_TEST( "Vector Fill, Float, Accelerated",
                    for(int j=0; j<5; j++){
                        SIMD::FillVector(data1, float(rand()), kDataSize); sum += data1[kDataSize-1]; } );
            #else
                printf("Vector Fill, Float - test disabled (no accelerated version).\n");
            #endif
            TIMING_TEST( "Vector Fill, Double, C++",
                for(int j=0; j<1; j++){
                    SIMD::FillVector_CPP(ddata1, 3.14, kDataSize); sum += ddata1[kDataSize-1]; } );
            TIMING_TEST( "Vector Fill, Double, Accelerated",
                for(int j=0; j<1; j++){
                    SIMD::FillVector(ddata1, 3.14, kDataSize); sum += ddata1[kDataSize-1]; } );
        #endif

    #endif

    getchar();
}
#endif


int main( int argc, char *argv[] )
{
    printf("Hello SIMD world!\n\n");


    printf("== SIMD Extension Configuration ===================================\n\n");

    #if defined(VTSIMD_ENABLE_SSE2_INTRINSICS)
        printf(" ** SSE/SSE2 compiler intrinsics ENABLED.\n");
    #else
        printf(" ** SSE/SSE2 compiler intrinsics DISABLED.\n");
    #endif

    #if defined(VTSIMD_ACCELERATION_POSSIBLE)
        printf(" ** SIMD acceleration is possible (recommeneded to use SIMD:: functions).\n");
    #else
        printf(" ** SIMD acceleration is NOT possible (optimized C++ can be used).\n");
    #endif

    #if defined(VTSIMD_LONGDOUBLE_IS_DOUBLE)
        printf(
            " ** Long double type (%d bits) is equal to double (%d bits).\n"
            " ** SIMD may work with long double.\n",
            int(sizeof(long double)*8), int(sizeof(double)*8));
    #else
        printf(
            " ** Long double type (%d bits) is NOT equal to double (%d bits).\n"
            " ** Long double calculations are not accelerated via SIMD.\n",
            int(sizeof(long double)*8), int(sizeof(double)*8));
    #endif

    #if defined(VT_ENABLE_MSCCPUIID)
        printf(" ** MSC __cpuid() intrinsic is available.\n");
    #else
        printf(" ** MSC __cpuid() intrinsic is NOT available.\n");
    #endif

    #if defined(VT_ENABLE_GCC_CPUID)
        printf(" ** GCC cpuid() macro available.\n");
    #else
        printf(" ** GCC cpuid() macro is NOT available.\n");
    #endif

    #if defined(VT_ENABLE_GCC_ASM_CPUID)
        printf(" ** GCC assembler cpuid available.\n");
    #else
        printf(" ** GCC assembler cpuid is NOT available.\n");
    #endif

    #if defined(VTSIMD_SSEINTRINSICS_WRAP_VECTORMINMAX_DOUBLE)
        printf("VTSIMD_SSEINTRINSICS_WRAP_VECTORMINMAX_DOUBLE is defined.\n");
    #endif

    printf("\n");
    ShowCPUInfo();

    printf(
        "\n"
        "== Performing Tests... ============================================\n");



    // SinTest();

    #if defined(FIRST_TEST)
        FirstTest();
    #endif



    return 0;
}

