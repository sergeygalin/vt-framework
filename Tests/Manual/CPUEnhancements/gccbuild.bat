
@set LANG=en_US

g++ -mtune=athlon64 -ffast-math -msse2 -msse3 -m3dnow -mfpmath=sse,387 -O3 -DVTSIMD_SSE_GCC_O3 -DCONSOLE -DWINDOWS -I../.. main.cpp ../../vt_portable.cpp ../../vt_simd.cpp ../../vt_localization.cpp -o cpuenhancements.exe


@rem g++ -mtune=athlon64 -ffast-math -msse2 -O2 -DCONSOLE -DWINDOWS -I../.. main.cpp ../../vt_portable.cpp ../../vt_simd.cpp ../../vt_localization.cpp -o cpuenhancements.exe


