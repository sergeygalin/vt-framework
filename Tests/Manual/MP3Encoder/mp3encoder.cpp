
#include "vt_mp3encode.hpp"

#if defined(_DEBUG)
    #pragma comment(lib, "../../../../Lib/Debug/lame_enc.lib")
#else
    #pragma comment(lib, "../../../../Lib/lame_enc.lib")
#endif

using namespace VT;

class CEncoder: public CPCM2MP3FileEncoder
{
public:
    CEncoder()
    {
        m_bVBR = true;
        m_bVBRHighQuality = false;
        m_bHighQuality = false;
        m_iMinBR = 8;
        m_iMaxBR = 24;
    }

    virtual void OnStart(){ printf(">>> Encoder started.\n"); }
    virtual void OnEnd(){ printf(">>> Encoder finished.\n"); }
    virtual void OnStatus(LPCVTCHAR message){ printf(">>> Encoder: %s\n", message); }
    virtual void OnProgress(int percentDone){ printf(">>> %d%%\r", percentDone); }
};


int main(int argc, char* argv[])
{

    printf("Hello world!\n");
    CEncoder encoder;

    printf("Engine Information:\n%s\n", encoder.GetEngineInfo());

    if(!encoder.Encode("in.wav", "out.mp3"))
        printf("Encoding failed.\n");
    else
        printf("Encoding successful.\n");
    printf("Encoder's message:\n%s\n", encoder.GetErrorMessage());

    return 0;
}



