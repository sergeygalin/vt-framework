

#include <math.h>

#if !defined(VT_CRTDEBUG)
    #define VT_CRTDEBUG
#endif

#include "vt_resampling.hpp"
#include "vt_riff.hpp"
#include "vt_dithering.hpp"

using namespace VT;


const int kBufSize = 1024*256;
int kOutRate;
const float kLevelChange = 1.0;

int main(int argc, char *argv[])
{

    //CVTVector<double> kaiser(11);
    //kaiser.fill(1.0);
    //KaiserWindow(kaiser);
    //return 0;

    /*
    double eps;
    eps = 1e-13; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-14; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-15; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-16; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-17; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-18; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-20; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-22; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-24; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-26; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-30; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    eps = 1e-40; printf("eps=%le KaiserI0=%.20lf\n", eps, FIR::KaiserI0(FIR::kKaiserWindowDefaultBeta, eps));
    return 0;
    */


    printf("Audio resampler test app. by Sergey A. Galin, 2008.\n");

    //CDecimateDuplicateResampler<float> res;
    //CAveragingResampler<float> res;
    //CSincResampler<float, double> res;

    typedef float SAMPLETYPE;
    typedef float PROCESSTYPE;

    CStdMetaResampler<SAMPLETYPE, PROCESSTYPE> res( true ); // Use FIR pools
    int quality=res.getBestQuality();

    char buf[256];
    res.getName(buf, 255);
    printf("RESAMPLER ENGINE: <<%s>> (%d bit stream, %d bit process)\n",
        buf, int(sizeof(SAMPLETYPE)*8), int(sizeof(PROCESSTYPE)*8));
    for(int i=0; i<=res.getBestQuality(); i++){
        res.getFullDescription(i, buf, 255);
        printf("    [%d]: %s\n", i, buf);
    }


    char *infile, *outfile;
    if( argc<4 || argc>5 ){
        printf("Usage: infile outfile output_rate [quality]\n");
        return 1;
    }else{
        infile = argv[1];
        outfile = argv[2];
        if( sscanf(argv[3], "%d", &kOutRate)!=1 ){
            printf("Output rate cannot be parsed.\n");
            return 1;
        }
        if( kOutRate<8000 || kOutRate>192000 ){
            printf("Bad output rate, must be withing 8000..192000 Hz.\n");
            return 1;
        }
        if( argc>=5 ){
            if( sscanf(argv[4], "%d", &quality)!=1 ){
                printf("Bad quality setting (not a number).\n");
                return 1;
            }
            if( quality<0 || quality>res.getBestQuality() ){
                printf("Bad quality setting (out of range).\n");
                return 1;
            }
        }
    }

    res.getFullDescription(quality, buf, 255);
    printf("*** Selected mode: %d (%s) ***\n", quality, buf);


    try{







        FILE *fp = fopen(infile, "rb");
        if(!fp){
            printf("Failed to open input file.\n");
            return 2;
        }
        FILE *fo = fopen(outfile, "wb");
        if(!fo){
            printf("Failed to open output file.\n");
            return 2;
        }

        TRIFFWavHeader hdr;
        fread(&hdr, sizeof(hdr), 1, fp);

        if( hdr.Channels!=1 ){
            printf("Sorry, I understand only mono files.\n");
            return 3;
        }

        typedef signed short PCM;

        static PCM readBuffer[kBufSize], writeBuffer[kBufSize*100];
        static SAMPLETYPE tmpBuffer[kBufSize], tmpWriteBuffer[kBufSize*100];

        if(sizeof(PCM)!=2){
            printf("Wrong PCM sample size, recompile me!!!\n");
            return 255;
        }




        int inrate = hdr.SamplesPerSec;
        printf("CONVERSION: %d Hz => %d Hz\n", int(hdr.SamplesPerSec), int(kOutRate));


        int samplesLeft = hdr.datalen/2;
        int input_samples = samplesLeft;
        printf("Input size: %d samples.\n", int(samplesLeft));
        int dataSizeOut = 0;

        // Set all fields we can before starting
        hdr.datalen = URound(floor(double(input_samples) * double(inrate) / double(kOutRate))) * 2;
        hdr.SamplesPerSec = kOutRate;
        hdr.AvgBytesPerSec = kOutRate*2;
        fwrite(&hdr, sizeof(hdr), 1, fo);

        res.init2(inrate, kOutRate, quality);
        printf("Resampler suggests min input block: %d samples\n", int(res.getMinMiddleInputSize()));
        printf("Min output block for %d sample input: %d samples\n", int(kBufSize), int(res.outBufferSize(kBufSize)));
        printf("Conversion ratio: %.20lf\n", res.getRatio());
        printf("Inverted ratio:   %.20lf\n", res.getInvRatio());

        printf("Starting resampling loop.\n");

        int blk = 0;
        int samples_read = 0;
        long clipCount=0;

        CDitherer<SAMPLETYPE, PCM, -32768, 32767> ditherer;
        ditherer.SetMethod( VT::kDitheringShaping );
        // ditherer.SetMethod( VT::kDitheringTriangular );
        // ditherer.SetMethod( VT::kDitheringNone );

        while( samplesLeft>0 ){
            printf("Block: %d | ", blk++);
            int toread = (samplesLeft>kBufSize)?kBufSize:samplesLeft;
            fread(readBuffer, toread*2, 1, fp);
            samples_read += toread;

            // Convert to floating-point
            for(int i=0; i<toread; i++){
                #if 0
                    tmpBuffer[i] = SAMPLETYPE((double(readBuffer[i])-0.5+
                        double(rand())/(double(RAND_MAX)))/
                        double(32768.0f));
                #else
                    tmpBuffer[i] = SAMPLETYPE(readBuffer[i])/SAMPLETYPE(32768.0f);
                #endif
            }

            // Doing resampling
            int outsamp = int(res.process(tmpBuffer, toread, tmpWriteBuffer, (samplesLeft==toread)));
            dataSizeOut+=outsamp;
            ditherer.Dither( tmpWriteBuffer, writeBuffer, outsamp, SAMPLETYPE(32768.0) );
            fwrite(writeBuffer, outsamp*2, 1, fo);
            samplesLeft -= toread;
            printf("Out: %d samples | Left: %d samples | %d %%            \r",
                int(dataSizeOut), int(samplesLeft), int(long(samples_read*100)/long(input_samples)));
            fflush(stdout);
        }
        printf("\n");

        // Update header. The rest of the fields are as in the input file.
        hdr.datalen = dataSizeOut*2;
        fseek(fo, 0, SEEK_SET);
        fwrite(&hdr, sizeof(hdr), 1, fo);

        printf("Input   samples: %d\n", input_samples);
        printf("Output  samples: %d\n", int(dataSizeOut));
        printf("Expected around: %d\n",
            int( double(input_samples)*double(kOutRate)/double(inrate) ));
        printf("Clipped samples: %ld\n", clipCount);
        printf("Required  ratio: %1.20lf\n", double(kOutRate)/double(inrate));
        printf("Resulting ratio: %1.20lf\n", double(dataSizeOut)/double(input_samples));


        fclose(fp);
        fclose(fo);
        printf("Done.\n");

    }catch(LPCVTCHAR msg){
        printf("\nEXCEPTION: %s\n", msg);
    }catch(...){
        printf("\nUNKNOWN EXCEPTION!!\n");
    }

}




