
#include "vt_inifile.hpp"

using namespace VT;

int main(int argc, char *argv[])
{
	int ret = 5;

	const char *fn = "iniFiles.ini";
	CDiskFile file(fn, "rb");
	if( !file.IsOK() ){
		printf("Failed to open input file '%s'.\n", fn);
		return 1;
	}

	CIniFile ini;
	if( !ini.Load(&file) ){
		printf("Failed to load ini data.\n", fn);
		return 1;
	}

	if( ini.GoToSection("one", false) ){
		printf("Section [one] located at line %d.\n", ini.GetCursor()+1);
		
		printf("Parameter one = '%s'\n", (char*)ini.GetStrDef("one", "NOT FOUND", false));
		printf("Parameter two = '%s'\n", (char*)ini.GetStrDef("two", "NOT FOUND", false));
		printf("Parameter one = '%s'\n", (char*)ini.GetStrDef("one", "NOT FOUND", false));
		printf("Parameter pi = '%f'\n", (float)ini.GetFloatDef("pi", -1, false));		
	}else{
		printf("Section one not found.\n");
		ret = 2;
	}

	if( ini.GoToSection("two", false) ){
		printf("Section [two] located at line %d.\n", ini.GetCursor()+1);
		
		printf("Parameter one = '%s'\n", (char*)ini.GetStrDef("one", "NOT FOUND", false));
		printf("Parameter two = '%s'\n", (char*)ini.GetStrDef("two", "NOT FOUND", false));
		printf("Parameter one = '%s'\n", (char*)ini.GetStrDef("one", "NOT FOUND", false));
		printf("Parameter pi = '%le'\n", (double)ini.GetDoubleDef("pi", -1, false));
		printf("Parameter pi2_100 = '%le'\n", (double)ini.GetDoubleDef("pi2_100", -1, false));
		
	}else{
		printf("Section one not found.\n");
		ret = 2;
	}

	// Ini writing test
	ini.InsertSection( "newSection1" );
	ini.InsertValue( "inNewSection1", "value1" );
	ini.InsertValue( "inNewSection2", "value2" );
	ini.InsertValue( "inNewSection3", "value3" );
	ini.InsertSection( "newSection2" );
	ini.InsertValue( "inNewSection1", "value1-2" );
	ini.InsertValue( "inNewSection2", "value2-2" );
	ini.InsertValue( "inNewSection3", "value3-2" );
	ini.InsertSection( "One" );
	ini.InsertValue( "addedToOne", "is a good value" );
	ini.InsertValue( "addedToOne", "is a good value - overwritten" );
	ini.InsertValue( "addedToOne2", "is another good value" );
	ini.InsertDouble( "pi2", double(1.5707963267948966192313216916398) );

	CString dump;
	dump.Implode( ini, '\n' );
	printf("Dump =======================\n%s\n============================\n", dump.Str());


	printf("\nResult: %d\n", ret);

	ini.Save( "iniFiles-out.ini" );

	return ret;
}




