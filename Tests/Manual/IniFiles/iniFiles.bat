
cls
del *.o
set LANG=en_US

set OPTS=-Wfatal-errors -DNDEBUG -I.. -I../..
rem -DVERBOSE

g++ %OPTS% -c ../../vt_fileutils.cpp 
g++ %OPTS% -c ../../vt_inifile.cpp 
g++ %OPTS% -c ../../vt_string.cpp 
g++ %OPTS% -c ../../vt_portable.cpp 
g++ %OPTS% -c ../../vt_threading.cpp 
g++ %OPTS% -c ../../vt_messageboxes.cpp 
g++ %OPTS% -c ../../vt_exceptions.cpp 
g++ %OPTS% -c ../../vt_file.cpp 
g++ %OPTS% -c ../../vt_time.cpp 
g++ %OPTS% iniFiles.cpp *.o -o iniFiles.exe




