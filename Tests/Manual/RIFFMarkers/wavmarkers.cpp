
#include "vt_riff.hpp"

using namespace VT;
using namespace VT::RIFF;

int main(int argc, char *argv[])
{
    const char *fn="test.wav";

    printf("Reading file: %s\n", fn);

    CRIFFReader reader;
    reader.Open( fn );

    if( reader.IsOK() ){
        printf("File size: %u\n", unsigned(reader.GetFileSize()));

        for(size_t i=0; i<reader.GetChunkCount(); i++){
            TChunkInfo hdr;
            hdr = *reader.GetChunk(i);
            printf("CHUNK %u: '%c%c%c%c', %u+8 bytes at %u..%u\n",
                unsigned(i),
                char(hdr.id&0xFF),
                char((hdr.id>>8)&0xFF),
                char((hdr.id>>16)&0xFF),
                char((hdr.id>>24)&0xFF),
                unsigned(hdr.size),
                unsigned(hdr.position),
                unsigned(hdr.position+hdr.size+8));
        }

        reader.LoadAllLists();

        for( size_t i=0; i<reader.GetCuePointCount(); i++ ){
            printf("CUE ENTRY %i: ID=%u, Playlist Position=%u\n"
                   "    Containing Chunk='%s', "
                   "Chunk Start=%u, Block Start=%u, Sample Offset=%u\n",
                   i,
                   unsigned(reader.GetCuePoint(i)->dwIdentifier),
                   unsigned(reader.GetCuePoint(i)->dwPosition),
                   VTFOURCC2Str(reader.GetCuePoint(i)->fccChunk),
                   unsigned(reader.GetCuePoint(i)->dwChunkStart),
                   unsigned(reader.GetCuePoint(i)->dwBlockStart),
                   unsigned(reader.GetCuePoint(i)->dwSampleOffset));
            printf("    Label: '%s'\n", reader.GetLabelById( reader.GetCuePoint(i)->dwIdentifier ));
        }

        for( size_t i=0; i<reader.GetLabelCount(); i++ )
                printf("LABEL %i: ID=%u, Value='%s'\n",
                       i,
                       unsigned(reader.GetLabel(i)->dwIdentifier),
                       reader.GetLabel(i)->dwText);

        printf("*** OUR SPECIAL MARKERS ***\n");
        const char *labels[]={
            "ABeg", "AEnd", "RBeg", "REnd", "L1Beg", "L1End", NULL
        };
        for( const char **ptr=labels; *ptr!=NULL; ptr++ ){
            const TCuePoint *point=reader.GetCueByLabel( *ptr );
            printf("SPECIAL LABEL '%s': ", *ptr);
            if( point==NULL )
                printf("Not found.\n");
            else
                printf("Sample Offset = %u\n", point->dwSampleOffset);
        }



        /*
        size_t sz;
        TCuePoint *points = reader.LoadCuePoints( &sz );
        if( points!=NULL ){
            printf("CUE sheet:\n");
            for(size_t i=0; i<sz; i++)
                printf("  ENTRY %i: ID=%u, Playlist Position=%u, Containing Chunk='%s', "
                       "Chunk Start=%u, Block Start=%u, Sample Offset=%u\n",
                       i,
                       unsigned(points[i].dwIdentifier),
                       unsigned(points[i].dwPosition),
                       VTFOURCC2Str(points[i].fccChunk),
                       unsigned(points[i].dwChunkStart),
                       unsigned(points[i].dwBlockStart),
                       unsigned(points[i].dwSampleOffset));

            delete points;
        }else
            printf("No CUE sheet found.\n");


        size_t lbs;
        TLabel **labels = reader.LoadLabels(&lbs);
        if( labels ){
            printf("Labels:\n");
            for(size_t i=0; i<lbs; i++){
                printf("  LABEL %i: ID=%u, Value='%s'\n",
                       i,
                       unsigned(labels[i]->dwIdentifier),
                       labels[i]->dwText);
                delete labels[i];
            }
            delete labels;
        }else
            printf("No labels found.\n");
    */


    }else
        printf("Reader init failed!\n");




    return 0;
}

