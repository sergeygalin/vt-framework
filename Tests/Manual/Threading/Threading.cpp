
#include "vt_threading.hpp"

using namespace VT;

class CMyThread: public CThread
{
public:
    CMyThread( bool suspended, LPCVTCHAR ID, int steps = 10,
        CThread::TPriority priority = CThread::kNormalPriority ):

        CThread(suspended, priority),
        m_Steps(steps)
    {
        SetThreadName( ID );
    }

    virtual ~CMyThread(){
        printf("Thread %s DESTROYED.\n", GetThreadName());
    }

    virtual bool Kill( long allowToFinish_ms = -1 ){
        printf("Thread %s GETS KILLED.\n", GetThreadName());
        return CThread::Kill( allowToFinish_ms );
    }

    virtual void Process(){
        printf("Thread %s STARTTING.\n", GetThreadName());
        for( int i=1; i<=m_Steps; i++ ){
            printf("Thread %s: %d\n", GetThreadName(), i);
            Sleep( 100 );
            if( IsTerminated() ){
                printf("Terminating %s :-(\n", GetThreadName());
                break;
            }
        }
        printf("Thread %s ENDED.\n", GetThreadName());
    }

protected:
    int m_Steps;
};

int main(int argc, char* argv[])
{
    printf("Hello threading world!\n");

    CMyThread
        thread1( false, "WAN", 100, CThread::kLowerPriority ),
        thread2( false, "OBI", 10, CThread::kLowPriority );
    thread1.SetThreadDestructorTimeoutMs( 1000 );
    thread2.SetThreadDestructorTimeoutMs( 1000 );

    #define SELFKILLER

    #ifdef SELFKILLER
        CMyThread *threadp;
        threadp = new CMyThread( true, "KENNY", 20, CThread::kIdlePriority );
        threadp->SetFreeOnFinish( true );
        threadp->Resume();
    #endif

    printf("======== FIRST SLEEP BEGIN ==========\n");
    VT::Sleep( 500 );
    printf("========= FIRST SLEEP END ===========\n\n");

    thread2.Terminate();

    #ifdef SELFKILLER
        threadp->Kill(-1);
    #endif

    printf("======== SECOND SLEEP BEGIN ==========\n");
    VT::Sleep( 500 );
    printf("========= SECOND SLEEP END ===========\n\n");

    #if defined(VT_ALLOW_THREAD_PAUSING)
        thread1.Suspend();
    #endif

    printf("======== THIRD SLEEP BEGIN ==========\n");
    VT::Sleep( 500 );
    printf("========= THIRD SLEEP END ===========\n\n");

    thread1.Resume();

    printf("======== FOURTH SLEEP BEGIN ==========\n");
    VT::Sleep( 1000 );
    printf("========= FOURTH SLEEP END ===========\n\n");

    // printf("Press any key to continue...\n");
    // getchar();

    printf("Goodbye threading world.\n");
    return 0;
}

