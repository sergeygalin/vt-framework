
#define _CRT_SECURE_NO_DEPRECATE

#include <string.h>
#include "vt_fileutils.hpp"

using namespace VT;

#define S VTPATHSLASH

int main(int argc, char *argv[])
{
    printf("*** vt_fileutils test ***\n");

    printf("MAX_PATH: %d\n", int(MAX_PATH));

    VTSetGlobalArgs(argc, argv);

#if 0

    static char out[MAX_PATH], out2[MAX_PATH], in[MAX_PATH], in2[MAX_PATH];

    printf("\nCutoffFileName()\n");
    strcpy(out, S "home" S "ktulhu" S); printf("Before:   %s\n", out);
    CutoffFileName(out);  printf("After:    %s\n", out);
    strcpy(out, S "home" S "ktulhu" S "something.good"); printf("Before:   %s\n", out);
    CutoffFileName(out);  printf("After:    %s\n", out);
    strcpy(out, "something.bad"); printf("Before:   %s\n", out);
    CutoffFileName(out);  printf("After:    %s\n", out);
    strcpy(out, S "something.bad"); printf("Before:   %s\n", out);
    CutoffFileName(out);  printf("After:    %s\n", out);
    strcpy(out, S); printf("Before:   %s\n", out);
    CutoffFileName(out);  printf("After:    %s\n", out);

    printf("\nCutoffFileExt()\n");
    strcpy(out, S "home" S "ktulhu" S "medved.p"); printf("Before:   %s\n", out);
    CutoffFileExt(out);  printf("After:    %s\n", out);
    strcpy(out, S "home" S "ktulhu" S "medved"); printf("Before:   %s\n", out);
    CutoffFileExt(out);  printf("After:    %s\n", out);
    strcpy(out, S "home" S "ktulhu.good" S "preved"); printf("Before:   %s\n", out);
    CutoffFileExt(out);  printf("After:    %s\n", out);
    strcpy(out, ".indeeps"); printf("Before:   %s\n", out);
    CutoffFileExt(out);  printf("After:    %s\n", out);
    strcpy(out, S "don't" S ".cut"); printf("Before:   %s\n", out);
    CutoffFileExt(out);  printf("After:    %s\n", out);


    printf("\nExtractFileName()\n");
    strcpy(in, S "home" S "ktulhu" S "medved.p"); printf("Before:   %s\n", in);
    ExtractFileName(in, out);  printf("After:    %s\n", out);
    strcpy(in, "fhtagn!"); printf("Before:   %s\n", in);
    ExtractFileName(in, out);  printf("After:    %s\n", out);
    strcpy(in, "abcde" S); printf("Before:   %s\n", in);
    ExtractFileName(in, out);  printf("After:    %s\n", out);
    strcpy(in, "abcde" S ".efg"); printf("Before:   %s\n", in);
    ExtractFileName(in, out);  printf("After:    %s\n", out);



    printf("\nExtractFileNameWOExt()\n");
    strcpy(in, S "home" S "ktulhu" S "medved.p"); printf("Before:   %s\n", in);
    ExtractFileNameWOExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, "fhtagn!"); printf("Before:   %s\n", in);
    ExtractFileNameWOExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, "abcde" S); printf("Before:   %s\n", in);
    ExtractFileNameWOExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, "virus.com"); printf("Before:   %s\n", in);
    ExtractFileNameWOExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, S "blabla" S ".hidden"); printf("Before:   %s\n", in);
    ExtractFileNameWOExt(in, out);  printf("After:    %s\n", out);



    printf("\nExtractFileExt()\n");
    strcpy(in, S "home" S "ktulhu" S "medved.p"); printf("Before:   %s\n", in);
    ExtractFileExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, ".notseethis"); printf("Before:   %s\n", in);
    ExtractFileExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, "abcde" S); printf("Before:   %s\n", in);
    ExtractFileExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, "virus.com"); printf("Before:   %s\n", in);
    ExtractFileExt(in, out);  printf("After:    %s\n", out);
    strcpy(in, "virus.com" S ".wow"); printf("Before:   %s\n", in);
    ExtractFileExt(in, out);  printf("After:    %s\n", out);



    printf("\nExtractDirectoryName()\n");
    strcpy(in, S "home" S "ktulhu" S "medved.p"); printf("Before:   %s\n", in);
    ExtractDirectoryName(in, out);  printf("After:    %s\n", out);
    strcpy(in, ".notseethis"); printf("Before:   %s\n", in);
    ExtractDirectoryName(in, out);  printf("After:    %s\n", out);
    strcpy(in, "abcde" S); printf("Before:   %s\n", in);
    ExtractDirectoryName(in, out);  printf("After:    %s\n", out);
    strcpy(in, "virus.com"); printf("Before:   %s\n", in);
    ExtractDirectoryName(in, out);  printf("After:    %s\n", out);
    strcpy(in, S "hello" S "world"); printf("Before:   %s\n", in);
    ExtractDirectoryName(in, out);  printf("After:    %s\n", out);
    strcpy(in, S "hello" S "world" S ".ofinvisible"); printf("Before:   %s\n", in);
    ExtractDirectoryName(in, out);  printf("After:    %s\n", out);


    printf("\nAbsolutePath()\n");
    strcpy(in, S "home" S "ktulhu" S "medved.p");
    strcpy(in2, S "abc" S "def" S "gjh");
    AbsolutePath(in, in2, out);
    printf("'%s'\t'%s'\t=>\t%s\n", in, in2, out);
    strcpy(in, S "home" S "ktulhu" S "medved.p");
    strcpy(in2, "c:\\abc" S "def" S "gjh");
    AbsolutePath(in, in2, out);
    printf("'%s'\t'%s'\t=>\t%s\n", in, in2, out);
    strcpy(in, "Z:\\home" S "ktulhu" S "medved.p");
    strcpy(in2, "..\\..\\abc" S "def" S "gjh");
    AbsolutePath(in, in2, out);
    printf("'%s'\t'%s'\t=>\t%s\n", in, in2, out);


    printf("\nRelativePathFromAbs()\n");
    strcpy(in, S "home" S "ktulhu" S "medved.p");
    strcpy(in2, S "abc" S "def" S "gjh");
    RelativePathFromAbs(in, in2, out); AbsolutePath(in, out, out2);
    printf("DIR='%s'\tFILE='%s'\t=>\t%s\t=>\t%s\n", in, in2, out, out2);
    strcpy(in, S "home" S "ktulhu" S "medved.p" S);
    strcpy(in2, S "abc" S "def" S "gjh");
    RelativePathFromAbs(in, in2, out); AbsolutePath(in, out, out2);
    printf("DIR='%s'\tFILE='%s'\t=>\t%s\t=>\t%s\n", in, in2, out, out2);
    strcpy(in, S "home" S "ktulhu" S "medved.p" S);
    strcpy(in2, S "home" S "ktulhu" S "medved.p" S "preved" S "hello" );
    RelativePathFromAbs(in, in2, out); AbsolutePath(in, out, out2);
    printf("DIR='%s'\tFILE='%s'\t=>\t%s\t=>\t%s\n", in, in2, out, out2);
    strcpy(in, S "home" S "ktulhu" S "medved.p" S);
    strcpy(in2, S "home" S "preved" S "hello" );
    RelativePathFromAbs(in, in2, out); AbsolutePath(in, out, out2);
    printf("DIR='%s'\tFILE='%s'\t=>\t%s\t=>\t%s\n", in, in2, out, out2);

#endif

    printf("\nCDirectoryReader\n");
    VT::CDirectoryReader dir("./*.*", "EXE,PDB");
    for(size_t i=0; i<dir.GetFileCount(); i++)
        printf("Directory Entry: %s\n", (const char*)dir.GetFile(i));

}
