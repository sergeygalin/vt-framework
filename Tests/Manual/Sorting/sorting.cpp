
#include "vt_string.hpp"

using namespace VT;

int main( int argc, char *argv[] )
{
    printf("Sorting test!\n");

    CVector<int> iarr(0);
    iarr.push( 12 );
    iarr.push( 121454 );
    iarr.push( 1 );
    iarr.push( 3252365 );
    iarr.push( 12234 );
    iarr.push( 9 );
    iarr.push( 789 );
    iarr.push( 4995 );


    printf("======== BEFORE ==========\n");
    for( size_t i=0; i<iarr.getSize(); i++ )
        printf( "[%d] => %d\n", int(i), iarr.getConst(i) );

    iarr.Sort();

    printf("======== AFTER ==========\n");
    for( size_t i=0; i<iarr.getSize(); i++ )
        printf( "[%d] => %d\n", int(i), iarr.getConst(i) );

    getchar();

/*    iarr.Sort(true);

    printf("======== AFTER2 ==========\n");
    for( size_t i=0; i<iarr.getSize(); i++ )
        printf( "[%d] => %d\n", int(i), iarr.getConst(i) );
*/

    CStringArray sarr;
    sarr.push( "dfdfsdf" );
    sarr.push( "lpopode" );
    sarr.push( "66[l[" );
    sarr.push( "FFFffff" );
    sarr.push( "wefwefw" );
    sarr.push( "efWfwef" );
    sarr.push( "Dedew" );
    sarr.push( "gtgrg" );
    sarr.push( "cvzv" );
    sarr.push( "65346" );
    sarr.push( "4t534" );

    printf("======== BEFORE ==========\n");
    for( size_t i=0; i<sarr.getSize(); i++ )
        printf( "[%d] => %s\n", int(i), sarr.getConst(i).Str() );


    sarr.Sort( true );
    printf("======== AFTER CASE-SENSITIVE ==========\n");
    for( size_t i=0; i<sarr.getSize(); i++ )
        printf( "[%d] => %s\n", int(i), sarr.getConst(i).Str() );


    sarr.Sort( false );
    printf("======== AFTER CASE-INSENSITIVE ==========\n");
    for( size_t i=0; i<sarr.getSize(); i++ )
        printf( "[%d] => %s\n", int(i), sarr.getConst(i).Str() );


    return 0;
}


