#if !defined( _CRT_SECURE_NO_DEPRECATE )
    #define _CRT_SECURE_NO_DEPRECATE
#endif

#include <math.h>
#include <time.h>
#include "vt_dithering.hpp"
#include "vt_fourier.hpp"
#include "vt_rawaudio.hpp"
#include "vt_buffers.hpp"
#include "vt_string.hpp"
#include "vt_csv.hpp"


#include "vt_audioprototypes.hpp"

using namespace VT;

#define DATATYPE double

// #define HIGHPASS 150

const DATATYPE k = DATATYPE(0.98);

const size_t FFTSize = 4096; // FFT size - bigger than the blocks
const size_t blockSize = FFTSize-1024; // Blocks (smaller than FFT)

int main(int argc, char* argv[])
{

    const char *fn = "test.wav", *fo = "test_out.wav";

    printf("Behold -- the FFT Test App 3 "__DATE__ " " __TIME__ " is here!\n");

    if( argc!=3 && argc!=1 ){
        printf("USAGE: fourier [<infile> <outfile>]\n");
        return 1;
    }

    if( argc==3 ){
        fn = argv[1];
        fo = argv[2];
    }

    printf("Transforming: %s -> %s\n", fn, fo);


    // ***************************************************************
    // *** Loading data
    // ***************************************************************
    CInMemoryAudio<DATATYPE, 2> audio, outaudio;
    if(!audio.LoadPCMWav(fn)){
        printf("Couldn't load test.wav! Error: '%s'\n", audio.GetLastErrorMessage());
        return 255;
    }
    printf("File loaded successfully: %.2f seconds, %d channels, %u samples, %d Hz.\n",
        audio.getTimeLength(), int(audio.getNChannels()),
        audio.getLength(), audio.GetSampleRate());


    audio.SavePCMWav16SI("IN_DUMP.wav");

    // ***************************************************************
    // *** Doing something weird
    // ***************************************************************

    printf("Splitting channels...\n");

    CInMemoryAudio<DATATYPE, 1> inchannels[2], outchannels[2];
    CInMemoryAudio<DATATYPE, 1> *inptrs[2],  *outptrs[2];
    inptrs[0] = &inchannels[0];
    inptrs[1] = &inchannels[1];
    outptrs[0] = &outchannels[0];
    outptrs[1] = &outchannels[1];

    audio.SplitChannelsTo(inptrs);

    // Processing data as blocks
    printf("Doing weird things...\n");

    const size_t fadeLength = (FFTSize-blockSize)/2;
    static DATATYPE rex[FFTSize], imx[FFTSize], outimx[2][FFTSize], outrex[2][FFTSize];

    printf("FFT=%u, block=%u, fade length=%u\n",
        unsigned(FFTSize), unsigned(blockSize), unsigned(fadeLength));

    DATATYPE spectrumK = DATATYPE(1.0/floor(DATATYPE(audio.getLength())/DATATYPE(blockSize)));
    printf("Number of spectrums: %.1f, weight=%.3f\n", float(1.0/spectrumK), float(spectrumK));


    outchannels[0].Allocate(audio.getLength());
    outchannels[1].Allocate(audio.getLength());
    outchannels[0].Zero();
    outchannels[1].Zero();

    size_t max = audio.getLength();
    printf("Samples to process: %u\n", unsigned(max));

    printf("Decomposing & averaging...\n");

    // inchannels[0].SaveRaw("inchannel0.raw");
    // inchannels[1].SaveRaw("inchannel1.raw");


    #if defined HIGHPASS
        float hpfreq=float(HIGHPASS);
        size_t hpidx = size_t((500.0/44100.0)*FFTSize);
        printf("HP filtering below %.0f Hz (indexes <=%u in %u FFT)...\n",
            float(hpfreq), unsigned(hpidx), unsigned(FFTSize));
    #endif

    for(size_t ch=0; ch<2; ch++){
        for(size_t i=0; i<FFTSize; i++){
            outrex[ch][i]=0;
            outimx[ch][i]=0;
        }

        // printf("Dec. channel %d...\n", ch);
        int bc=0;
        for(size_t ptr=0; ptr+blockSize<max; ptr+=blockSize){

            memset(rex, 0, sizeof(DATATYPE)*FFTSize);
            memset(imx, 0, sizeof(DATATYPE)*FFTSize);


            bc++;
            printf("\rIn Ch=%u Ptr=%u ", (unsigned)ch, (unsigned)ptr);

            // Calculating indexes
            size_t
                starting_idx, // Index of beginning of the block in audio
                offsetRex,      // Shift of the block
                ending_idx;   // Index of ending o  .l.lf the block in audio
            if( ptr>=fadeLength ){
                starting_idx = ptr-fadeLength;
                offsetRex = 0;
            }else{
                starting_idx = 0;
                offsetRex = fadeLength-ptr;
            }
            ending_idx = ptr + FFTSize - fadeLength;
            if( ending_idx >= outchannels[ch].getLength() )
                ending_idx = outchannels[ch].getLength();

             // Preparing input audio buffers
            // Copying data to buffer
            if( offsetRex ){
                memset(rex, 0, offsetRex);
            }
            size_t cpsz = (ending_idx-starting_idx)*sizeof(DATATYPE);
            void *ptr1=rex+offsetRex, *ptr2=((DATATYPE*)inchannels[ch].GetData())+starting_idx;
            memcpy(ptr1, ptr2, cpsz);

            // Doing the FFT decomposition
            RealFFT(rex, imx, FFTSize);

            #if defined(HIGHPASS)
                // High-pass
                for(size_t i=0; i<hpidx; i++){
                    outrex[ch][i]=0;
                    outimx[ch][i]=0;
                }
            #endif

            // Adding coefficients
            for(size_t i=0; i<FFTSize; i++){
                outrex[ch][i] += rex[i] * spectrumK;
                outimx[ch][i] += imx[i] * spectrumK;
            }
        }
        printf("\nBlocks: %d\n", bc);
    }

    printf("Composing...\n");

    for(size_t ch=0; ch<2; ch++){

        // Composing
        InverseRealFFT( outrex[ch], outimx[ch], FFTSize );

        // Creating fades
        for(size_t i=0, j=FFTSize-i-1, offset2=fadeLength*2; i<offset2; i++, j--){
            DATATYPE k=DATATYPE(i+1)/DATATYPE(offset2);
            outrex[ch][i] *= k;
            outrex[ch][j] *= k;
        }

        /*
        char fn[32];
        sprintf(fn, "dump%d.csv", int(ch));
        printf("\nDumping to '%s'...\n", fn);
        FILE *f=fopen(fn, "wt");
        for(size_t i=0; i<FFTSize; i++)
            fprintf(f, "%.0f\n", float(outrex[ch][i]*32767.0f));
        fclose(f);
        */

        for(size_t ptr=0; ptr+blockSize<max; ptr+=blockSize){
            printf("\rOut ch=%u Ptr=%u ", (unsigned)ch, (unsigned)ptr);
            // Calculating indexes
            size_t
                starting_idx, // Index of beginning of the block in audio
                offsetRex,      // Shift of the block
                ending_idx;   // Index of ending of the block in audio
            if( ptr>=fadeLength ){
                starting_idx = ptr-fadeLength;
                offsetRex = 0;
            }else{
                starting_idx = 0;
                offsetRex = fadeLength-ptr;
            }
            ending_idx = ptr + FFTSize - fadeLength;
            if( ending_idx >= outchannels[ch].getLength() )
                ending_idx = outchannels[ch].getLength();

            // Overlap-add
            for(size_t i=starting_idx, j=offsetRex; i<ending_idx; i++, j++)
                // outchannels are mono, so writing to their zero channel
                outchannels[ch].SetData(i, 0, outrex[ch][j]*k +outchannels[ch].GetData(i, 0) );

            /*// Reverse rex
            for(size_t i=0, j=FFTSize-1; i<j; i++, j--){
                DATATYPE t = outrex[ch][i];
                outrex[ch][i] = outrex[ch][j];
                outrex[ch][j] = t;
            }*/

        }
    }


    printf("\nMWA HA HA HA HA!!\n");


    printf("Combining channels...\n");
    outaudio.CombineChannelsFrom(outptrs);
    outaudio.ForceSampleRate( audio.GetSampleRate() );

    // ***************************************************************
    // *** Writing data
    // ***************************************************************

    printf("Saving data to file...\n");
    printf("%.2f seconds, %d channels, %u samples, %d Hz.\n",
        outaudio.getTimeLength(), int(outaudio.getNChannels()),
        outaudio.getLength(), outaudio.GetSampleRate());
    if( !outaudio.SavePCMWav16SI( fo ) ){
        printf("Error writing output file!\n%s\n", audio.GetLastErrorMessage());
        return 255;
    }
    printf("WAV file written successfully.\n");

    printf("I've done for now.\n");

    return 0;
}


