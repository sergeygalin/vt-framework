// fourier.cpp : Defines the entry point for the console application.
//
#if !defined( _CRT_SECURE_NO_DEPRECATE )
    #define _CRT_SECURE_NO_DEPRECATE
#endif

#include <math.h>
#include <time.h>
#include "vt_dithering.hpp"
#include "vt_fourier.hpp"
#include "vt_rawaudio.hpp"
#include "vt_buffers.hpp"

#define DATATYPE double
#define OVERLAPMIX

const DATATYPE k = DATATYPE(0.98);

const size_t FFTSize = 4096; // FFT size - bigger than the blocks
const size_t blockSize = FFTSize-1024; // Blocks (smaller than FFT)



// Create a sawtooth frequency filter
// basefreq is a fraction of sampling frequency: [0..0.5]
// vector size of input defined length of the spectrum.
// Returns index of basefreq element.
template<class T> size_t VTSawtoothFilter(CVTVector<T>* vector, T basefreq)
{
    const T slopewidthk = T(0.2); // In octaves

    size_t baseIdx = 0, leftIdx = 0, rightIdx = 0;
    T freq=basefreq;
    vector->zero();
    while( rightIdx<vector->getSize() ){
        baseIdx = size_t(freq * T(vector->getSize()));
        leftIdx = size_t(freq*(1.0-slopewidthk) * T(vector->getSize()));
        rightIdx = size_t(freq*(1.0+slopewidthk) * T(vector->getSize()));

        printf("Frequency: %.3f (%.2f Hz?) (base=%f) IDX: %u---%u---%u\n",
            float(freq), float(freq*22050.0), float(basefreq),
            unsigned(leftIdx), unsigned(baseIdx), unsigned(rightIdx));

        // VTASSERT( baseIdx<vector->getSize() && baseIdx!=0 );

        T step = T(1.0/T((baseIdx-1)-leftIdx)), val=0;
        VTASSERT(step>0);

        for(size_t i=leftIdx; i<baseIdx-1 && i<vector->getSize(); i++){
            vector->set( i, val );
            val+=step;
        }

        step = T(1.0/T(rightIdx-(baseIdx+1))), val=1.0;
        VTASSERT(step>0);

        for(size_t i=baseIdx+2; i<=rightIdx && i<vector->getSize(); i++){
            val-=step;
            vector->set( i, val );
        }

        if( baseIdx-1<vector->getSize() )
            vector->set( baseIdx-1, 1 );
        if( baseIdx<vector->getSize() )
            vector->set( baseIdx, 1 );
        if( baseIdx+1<vector->getSize() )
            vector->set( baseIdx+1, 1 );

        freq+=basefreq;
    }

    vector->write_txt("sawtooth.csv", true);

    return baseIdx = size_t(basefreq*2.0 * T(vector->getSize()));
}


template<class T> void VTSawtoothFilter2(CVTVector<T>* vector, T basefreq)
{
    const T KMUL = 0.71;
    const int FLATWIDTH2=6;

    size_t baseIdx = 0;
    T freq=basefreq;
    vector->zero();
    for(;;){
        baseIdx = size_t(freq * T(vector->getSize()));

        int leftIdx = int(baseIdx), rightIdx=int(baseIdx);

        for( int i=0; i<=FLATWIDTH2; i++){
            if( leftIdx>=0 && leftIdx<int(vector->getSize()) ){
                if( vector->get(leftIdx)<1 )
                    vector->set(leftIdx, 1);
            }
            if( rightIdx<int(vector->getSize()) )
                vector->set(rightIdx, 1);
            leftIdx--;
            rightIdx++;
        }

        for( T k = 1.0; k>0.1; k*=KMUL ){
            if( leftIdx>=0 && leftIdx<int(vector->getSize()) ){
                if( vector->get(leftIdx)<k )
                    vector->set(leftIdx, k);
            }
            if( rightIdx<int(vector->getSize()) )
                vector->set(rightIdx, k);

            leftIdx--;
            rightIdx++;
        }
        if( rightIdx>=int(vector->getSize()) )
            break;
        freq+=basefreq;
    }
}















int main(int argc, char* argv[])
{

    printf("Behold -- the FFT Test App "__DATE__ " " __TIME__ " is here!\n");

    // ***************************************************************
    // *** Loading data
    // ***************************************************************
    CInMemoryAudio<DATATYPE, 2> audio, audioB, outaudio;
    if(!audio.LoadPCMWav("test.wav")){
        printf("Couldn't load test.wav!\n");
        return 255;
    }
    printf("File loaded successfully: %.2f seconds, %d channels, %u samples, %d Hz.\n",
        audio.getTimeLength(), int(audio.getNChannels()),
        audio.getLength(), audio.m_SampleRate);

    if(!audioB.LoadPCMWav("test2.wav")){
        printf("Couldn't load test2.wav!\n");
        return 255;
    }
    printf("File 2 loaded successfully: %.2f seconds, %d channels, %u samples, %d Hz.\n",
        audioB.getTimeLength(), int(audioB.getNChannels()),
        audioB.getLength(), audioB.m_SampleRate);


    // ***************************************************************
    // *** Doing something weird
    // ***************************************************************

    printf("Splitting channels...\n");



    CInMemoryAudio<DATATYPE, 1> inchannels[2], inchannelsB[2], outchannels[2];
    CInMemoryAudio<DATATYPE, 1> *inptrs[2], *inptrsB[2], *outptrs[2];
    inptrs[0] = &inchannels[0];
    inptrs[1] = &inchannels[1];
    inptrsB[0] = &inchannelsB[0];
    inptrsB[1] = &inchannelsB[1];
    outptrs[0] = &outchannels[0];
    outptrs[1] = &outchannels[1];

    audio.SplitChannelsTo(inptrs);
    audioB.SplitChannelsTo(inptrsB);

    // Processing data as blocks
    printf("Doing weird things...\n");


    const size_t fadeLength = (FFTSize-blockSize)/2;
    static DATATYPE rex[FFTSize], imx[FFTSize], rexB[FFTSize], imxB[FFTSize], outimx[FFTSize], outrex[FFTSize];

    printf("FFT=%u, block=%u, fade length=%u\n",
        unsigned(FFTSize), unsigned(blockSize), unsigned(fadeLength));


    // FILTER

    CVTVector<DATATYPE> filter(FFTSize);
    //size_t basefreqi = VTSawtoothFilter(&filter, DATATYPE(523.25/44100.0) );
    //for(size_t i = 0; i<basefreqi; i++)    filter.set(i, 1);

    VTSawtoothFilter2(&filter, DATATYPE(523.25/44100.0) );

    outchannels[0].Allocate(audio.getLength());
    outchannels[1].Allocate(audio.getLength());
    outchannels[0].Zero();
    outchannels[1].Zero();

    size_t max = Min(audio.getLength(), audioB.getLength());
    printf("Samples to process: %u\n", unsigned(max));


    DATATYPE transition=0, transitionStep = DATATYPE(blockSize)/DATATYPE(max);

    for(size_t ptr=0; ptr+blockSize<max; ptr+=blockSize){
        printf("\rPtr: %u ", (unsigned)ptr);

        for(size_t ch=0; ch<2; ch++){

            // Calculating indexes
            size_t
                starting_idx, // Index of beginning of the block in audio
                offsetRex,      // Shift of the block
                ending_idx;   // Index of ending o  .l.lf the block in audio
            if( ptr>=fadeLength ){
                starting_idx = ptr-fadeLength;
                offsetRex = 0;
            }else{
                starting_idx = 0;
                offsetRex = fadeLength-ptr;
            }
            ending_idx = ptr + FFTSize - fadeLength;
            if( ending_idx >= outchannels[ch].getLength() )
                ending_idx = outchannels[ch].getLength();

             // Preparing input audio buffers
            #if defined(OVERLAPMIX)
                // Copying data to buffer
                if( offsetRex ){
                    memset(rex, 0, offsetRex);
                    memset(rexB, 0, offsetRex);
                }

                size_t cpsz = (ending_idx-starting_idx)*sizeof(DATATYPE);

                void *ptr1=rex+offsetRex, *ptr2=((DATATYPE*)inchannels[ch].GetData())+starting_idx;
                memcpy(ptr1, ptr2, cpsz);
                ptr1=rexB+offsetRex, ptr2=((DATATYPE*)inchannelsB[ch].GetData())+starting_idx;
                memcpy(ptr1, ptr2, cpsz);


            #else
                add b channel!

                // Zeroing the edges
                memset( rex, 0, fadeLength*sizeof(DATATYPE) );
                memset( rex+fadeLength+blockSize, 0, fadeLength*sizeof(DATATYPE) );
                // Copying block to the middle of the buffer
                memcpy( rex+fadeLength, ((DATATYPE*)inchannels[ch].GetData())+ptr, blockSize*sizeof(DATATYPE) );
                // No need to prepare imx - it will be ignored anyway.
            #endif

            // Doing the FFT decomposition
            RealFFT(rex, imx, FFTSize);
            RealFFT(rexB, imxB, FFTSize);


            // A place for real evil: we have the spectrum!

            DATATYPE pc = DATATYPE(1.0)-transition, pc2 = transition;
            //..DATATYPE pc = 0.5, pc2 = 0.5;

            for(size_t i=0; i<FFTSize; i++){
                outrex[i] = (rex[i]*pc + rexB[i]*pc2); //*filter.get(i);
                outimx[i] = (imx[i]*pc + imxB[i]*pc2); //*filter.get(i);
            }

            // FFT Compositon
            InverseRealFFT( outrex, outimx, FFTSize );


            // Returning result of the composition to the out buffer
            #if defined(OVERLAPMIX)
                // Creating fades
                for(size_t i=0, j=FFTSize-i-1, offset2=fadeLength*2; i<offset2; i++, j--){
                    DATATYPE k=DATATYPE(i+1)/DATATYPE(offset2);
                    outrex[i] *= k;
                    outrex[j] *= k;
                }
            #endif

            // Overlap-add
            for(size_t i=starting_idx, j=offsetRex; i<ending_idx; i++, j++)
                outchannels[ch].SetData(i, 0, outrex[j]*k +outchannels[ch].GetData(i, 0) );
        }

        transition+=transitionStep;

    }
    printf("MWA HA HA HA HA!!\n");


    printf("Combining channels...\n");
    outaudio.CombineChannelsFrom(outptrs);
    outaudio.m_SampleRate = audio.m_SampleRate;

    // ***************************************************************
    // *** Writing data
    // ***************************************************************

    printf("Saving data to file...\n");
    printf("%.2f seconds, %d channels, %u samples, %d Hz.\n",
        outaudio.getTimeLength(), int(outaudio.getNChannels()),
        outaudio.getLength(), outaudio.m_SampleRate);
    if( !outaudio.SavePCMWav16SI( "test_out.wav" ) ){
        printf("Error writing output file!\n%s\n", audio.GetLastErrorMessage());
        return 255;
    }
    printf("WAV file written successfully.\n");

    printf("I've done for now.\n");

    return 0;
}


