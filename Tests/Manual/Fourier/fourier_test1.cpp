//
// Fist Fourier test: split-process-combine chunks
//


#if !defined( _CRT_SECURE_NO_DEPRECATE )
    #define _CRT_SECURE_NO_DEPRECATE
#endif

#include <math.h>
#include <time.h>
#include "vt_dithering.hpp"
#include "vt_fourier.hpp"
#include "vt_rawaudio.hpp"

#define DATATYPE float
#define OVERLAPMIX
// #define INPUTFADE


const DATATYPE k = DATATYPE(0.98);


int test1_main(int argc, char* argv[])
{

    printf("Behold -- the FFT Test App "__DATE__ " " __TIME__ " is here!\n");

    // ***************************************************************
    // *** Loading data
    // ***************************************************************
    CInMemoryAudio<DATATYPE, 2> audio, outaudio;
    if(!audio.LoadPCMWav("test.wav")){
        printf("Couldn't load test.wav!\n");
        return 255;
    }
    printf("File loaded successfully: %.2f seconds, %d channels, %u samples, %d Hz.\n",
        audio.getTimeLength(), int(audio.getNChannels()),
        audio.getLength(), audio.m_SampleRate);

    // ***************************************************************
    // *** Doing something weird
    // ***************************************************************

    printf("Splitting channels...\n");



    CInMemoryAudio<DATATYPE, 1> inchannels[2], outchannels[2];
    CInMemoryAudio<DATATYPE, 1> *inptrs[2], *outptrs[2];
    inptrs[0] = &inchannels[0];
    inptrs[1] = &inchannels[1];
    outptrs[0] = &outchannels[0];
    outptrs[1] = &outchannels[1];
    audio.SplitChannelsTo(inptrs);

    //printf("Flushing channels to files...\n");
    //channeldata[0].SavePCMWav16SI("test-out-left.wav");
    //channeldata[1].SavePCMWav16SI("test-out-right.wav");


    // Processing data as blocks
    printf("Doing weird things...\n");



    const size_t FFTSize = 4096; // FFT size - bigger than the blocks
    const size_t blockSize = FFTSize-512; // Blocks (smaller than FFT)


    const size_t fadeLength = (FFTSize-blockSize)/2;
    static DATATYPE rex[FFTSize], imx[FFTSize], outimx[FFTSize], outrex[FFTSize];

    printf("FFT=%u, block=%u, fade length=%u\n",
        unsigned(FFTSize), unsigned(blockSize), unsigned(fadeLength));



    outchannels[0].Allocate(audio.getLength());
    outchannels[1].Allocate(audio.getLength());
    outchannels[0].Zero();
    outchannels[1].Zero();

    for(size_t ptr=0; ptr+blockSize<audio.getLength(); ptr+=blockSize){
        printf("\rPtr: %u ", (unsigned)ptr);

        for(size_t ch=0; ch<2; ch++){

            // Calculating indexes
            size_t
                starting_idx, // Index of beginning of the block in audio
                offsetRex,      // Shift of the block
                ending_idx;   // Index of ending o  .l.lf the block in audio
            if( ptr>=fadeLength ){
                starting_idx = ptr-fadeLength;
                offsetRex = 0;
            }else{
                starting_idx = 0;
                offsetRex = fadeLength-ptr;
            }
            ending_idx = ptr + FFTSize - fadeLength;
            if( ending_idx >= outchannels[ch].getLength() )
                ending_idx = outchannels[ch].getLength();

printf("A ");
            // Preparing input audio buffers
            #if defined(OVERLAPMIX)
                // Copying data to buffer
                if( offsetRex )
                    memset(rex, 0, offsetRex);
                //for(size_t i=starting_idx, j=offsetRex; i<ending_idx; i++, j++)
                //    rex[j] = *( ((DATATYPE*)inchannels[ch].GetData())+i );
                memcpy(rex+offsetRex, ((DATATYPE*)inchannels[ch].GetData())+starting_idx,
                    (ending_idx-starting_idx)*sizeof(DATATYPE));

                #if defined(INPUTFADE)
                    // Creating fades
                    for(size_t i=0, j=FFTSize-i-1; i<fadeLength*2; i++, j--){
                        DATATYPE k=DATATYPE(i+1)/DATATYPE(fadeLength*2);
                        rex[i] *= k;
                        rex[j] *= k;
                    }
                #endif
            #else
                // Zeroing the edges
                memset( rex, 0, fadeLength*sizeof(DATATYPE) );
                memset( rex+fadeLength+blockSize, 0, fadeLength*sizeof(DATATYPE) );
                // Copying block to the middle of the buffer
                memcpy( rex+fadeLength, ((DATATYPE*)inchannels[ch].GetData())+ptr, blockSize*sizeof(DATATYPE) );
                // No need to prepare imx - it will be ignored anyway.
            #endif

printf("B ");


            // Doing the FFT decomposition
            RealFFT(rex, imx, FFTSize);


printf("C ");

            // A place for real evil: we have the spectrum!

            #if 0
                memcpy( outrex, rex, sizeof(DATATYPE)*FFTSize );
                memcpy( outimx, imx, sizeof(DATATYPE)*FFTSize );
            #elif 1
                size_t i=0;
                const size_t cutoff1=size_t((200.0/44100.0)*double(FFTSize));
                const size_t cutoff2=size_t((2000.0/44100.0)*double(FFTSize));
                for(i=0; i<cutoff1; i++){
                    outrex[i]=0;
                    outimx[i]=0;
                }
                for(;i<cutoff2; i++){
                    outrex[i]=rex[i];
                    outimx[i]=imx[i];
                }
                for(; i<FFTSize; i++){
                    size_t j=i;
                    outrex[i]=0; //rex[j]/DATATYPE(16.0);
                    outimx[i]=0; //imx[j]/DATATYPE(16.0);
                }
            #elif 0
                for(size_t i=0; i<FFTSize; i++){
                    size_t j=(i*2)/3; // j<i !!
                    outrex[i]=rex[j];
                    outimx[i]=imx[j]; //!!
                }
            #endif

            //memcpy( outrex, rex, FFTSize*sizeof(DATATYPE) );
            //memcpy( outimx, imx, FFTSize*sizeof(DATATYPE) );


printf("D ");

            // FFT Compositon
            InverseRealFFT( outrex, outimx, FFTSize );

printf("E ");

            // Returning result of the composition to the out buffer
            #if defined(OVERLAPMIX)
                #if !defined(INPUTFADE)
                    // Creating fades
                    for(size_t i=0, j=FFTSize-i-1, offset2=fadeLength*2; i<offset2; i++, j--){
                        DATATYPE k=DATATYPE(i+1)/DATATYPE(offset2);
                        outrex[i] *= k;
                        outrex[j] *= k;
                    }
                #endif
            #endif

            // Overlap-add
            for(size_t i=starting_idx, j=offsetRex; i<ending_idx; i++, j++)
                outchannels[ch].SetData(i, 0, outrex[j]*k +outchannels[ch].GetData(i, 0) );
        }
    }
    printf("MWA HA HA HA HA!!\n");


    printf("Combining channels...\n");
    outaudio.CombineChannelsFrom(outptrs);
    outaudio.m_SampleRate = audio.m_SampleRate;

    // ***************************************************************
    // *** Writing data
    // ***************************************************************

    printf("Saving data to file...\n");
    printf("%.2f seconds, %d channels, %u samples, %d Hz.\n",
        outaudio.getTimeLength(), int(outaudio.getNChannels()),
        outaudio.getLength(), outaudio.m_SampleRate);
    //if( !outaudio.SaveRaw( "test_out.raw" ) ){
    //    printf("Error writing output file!\n%s\n", audio.GetLastErrorMessage());
    //    return 255;
    //}
    //printf("RAW file written successfully, now WAV...\n");
    if( !outaudio.SavePCMWav16SI( "test_out.wav" ) ){
        printf("Error writing output file!\n%s\n", audio.GetLastErrorMessage());
        return 255;
    }
    printf("WAV file written successfully.\n");

    printf("I've done for now.\n");

    return 0;
}


/*
void prow(const char* msg, DATATYPE *ptr, int n)
{
    printf("%-12s ", msg);
    for(int x=0; x<n; x++)
        printf("%10.5lf ", ptr[x]);
    printf("\n");
}

void prowe(const char* msg, DATATYPE *ptr, int n)
{
    printf("%-12s ", msg);
    for(int x=0; x<n; x++)
        printf("%10.2le ", ptr[x]);
    printf("\n");
}
*/


    /*
    FFT MATH TEST

    const int N=8;
    DATATYPE rex[N], imx[N], outre[N], outim[N];

    for(int x=0; x<N; x++){
        rex[x] = 10.0 * DATATYPE(rand())/DATATYPE(RAND_MAX);
        imx[x] = 0;
    }

    prow("ReX[]:", rex, N);
    prow("ImX[]:", imx, N);
    printf("\n");

    ComplexDFTCorrelation( rex, imx, outre, outim, N );
    prow("ReX[] DFT:", outre, N);
    prow("ImX[] DFT:", outim, N);
    printf("\n");

    ComplexFFT( rex, imx, N );
    prow("ReX[] FFT:", rex, N);
    prow("ImX[] FFT:", imx, N);
    printf("\n");

    InverseComplexFFT( rex, imx, N );
    prow("ReX[] IFFT:", rex, N);
    prowe("ImX[] IFFT:", imx, N);
    printf("\n");

    RealFFT( rex, imx, N );
    prow("ReX[] RFFT:", rex, N);
    prow("ImX[] RFFT:", imx, N);
    printf("\n");

    InverseRealFFT( rex, imx, N );
    prow("ReX[] IRFFT:", rex, N);
    prow("ImX[] IRFFT:", imx, N);
    printf("\n");
    */



    /*
    DITHERING TEST

    srand( (unsigned)time( NULL ) );
    printf("Hello Fourier world.\n");

    printf("1.3 fmod 1 => %lf\n", fmod(1.3, 1.0));
    printf("-1.3 fmod 1 => %lf\n", fmod(-1.3, 1.0));
    DATATYPE x=-1.3f;
    printf("int(-1.3) => %d\n", int(x));

    printf("floor(-1.3) => %f\n", floor(-1.3));
    printf("ceil(-1.3) => %f\n", ceil(-1.3));


    const int N=42;
    static DATATYPE data[N] = { 1.5, 1.1, 1.0, 0, -1, -2,
        0.3333, 0.3333, 0.3333, 0.3333,
        0.3333, 0.3333, 0.4444, 0.4444, 0.3333, 0.4444, 0.3333, 0.3333, 0.3333, 0.3333,
        -0.3333, -0.3333, -0.3333, -0.3333, -0.3333, -0.3333, -0.3333, -0.3333, -0.3333, -0.3333,

        0.1111, 0.2222, 0.3333, 0.4444, 0.4444, 0.4444,
        0.1111, 0.2222, 0.3333, 0.4444, 0.4444, 0.4444
    };
    static signed short int out[N];
    typedef CDitherer<DATATYPE, signed short int, -32767, 32767> TMyDitherer;
    TMyDitherer ditherer;
    // ditherer.SetMethod( TMyDitherer::kTriangular );
    ditherer.SetMethod( TMyDitherer::kShaping );
    ditherer.Dither( data, out, N, 11111 );

    for(int i=0; i<N; i++){
        printf("%12.6lf => %6d\n", data[i], int(out[i]));
    }*/



