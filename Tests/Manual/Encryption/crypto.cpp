
#define _CRT_SECURE_NO_DEPRECATE

#include <string.h>
#include "vt_crypto.hpp"
#include "vt_cryptodir.hpp"

#if !defined(_DEBUG)
    #pragma comment(lib, "../../../../Lib/cryptlib.lib")
#else
    #pragma comment(lib, "../../../../Lib/Debug/cryptlib.lib")
#endif

using namespace VT;
using namespace VT::Crypto;

const char xfile[]=
    "Mary had a little lamb,\n"
    "Its fleece was white as snow;\n"
    "And everywhere that Mary went,\n"
    "The lamb was sure to go.\n"
    "He followed her to school one day;\n"
    "Which was against the rule;\n"
    "It made the children laugh and play;\n"
    "To see a lamb at school.\n"
    "\"Why does the lamb love Mary so?\"\n"
    "The eager children cry;\n"
    "\"Why, Mary loves the lamb, you know,\"\n"
    "The teacher did reply.\n";

const char testmsg[]=
    "This is a test message 1234.\n"
    "It simply isn't fair to you or me. "
    "The worst crime I can think of would be to rip "
    "people off by faking it and pretending as if I'm having 100% fun.";

const char key[]="Hello!!! And what's going on here, actually? // Over 32 chars";


void Hashes()
{
    char buf[10240], out[10240];
    printf("Hash testing...\n");
    strcpy(buf, "Hello world"); Crypto::Hash(buf, strlen(buf), out);
    printf("%20s => %20s\n", buf, out);
    strcpy(buf, "Abcde"); Crypto::Hash(buf, strlen(buf), out);
    printf("%20s => %20s\n", buf, out);
    strcpy(buf, "Hello world"); Crypto::Hash(buf, strlen(buf), out);
    printf("%20s => %20s\n", buf, out);
    strcpy(buf, "SHA1 Test"); Crypto::SHA1(buf, strlen(buf), out);
    printf("%20s => %20s\n", buf, out);
    strcpy(buf, "SHA512 Test"); Crypto::SHA512(buf, strlen(buf), out);
    printf("%20s => %20s\n", buf, out);
    strcpy(buf, "Old good MD5 test."); Crypto::MD5(buf, strlen(buf), out);
    printf("%20s => %20s\n", buf, out);
    printf("Hash testing done.\n");
}

void AESSimple()
{
    printf("Now, AES encryption...\n");
    printf(
        "Source text length: %d, Key source length: %d\n"
        "AES Key Size: %d bytes, Block size: %d bytes\n",
        int(strlen(xfile)), int(strlen(key)),
        Crypto::AES::kStdKeySizeBytes,
        Crypto::AES::kBlockSizeBytes );
    size_t osz = 0;
    char *enc = (char*)Crypto::AES::Encrypt( (const BYTE *)xfile, strlen(xfile), NULL, &osz, (const BYTE *)key );
    if( enc!=NULL ){
        printf("Encryption successful, %u bytes.\n", unsigned(osz));
        FILE *fp = fopen("Test-AES-Encoded.dat", "wb");
        fwrite( enc, osz, 1, fp );
        fclose( fp );
        size_t dsz = 0;
        char *dec = (char*)Crypto::AES::Decrypt( (const BYTE *)enc, osz, NULL, &dsz, (const BYTE *)key );
        if( dec!=NULL ){
            printf("Decryption successful, %u bytes.\n", unsigned(dsz));
            FILE *fdec = fopen("Test-AES-Decoded.txt", "wb");
            fwrite( dec, dsz, 1, fdec );
            fclose( fdec );
        }else
            printf("Decryptor returned NULL!!");
    }else
        printf("Encryptor returned NULL!!");
}


bool CFS()
{
    const char
        *enf = "CryptoFS_Ecrypted.dat",
        *unenf = "CryptoFS_Unencrypted.dat",
        *f1 = "file1.txt",
        *f2 = "file2.txt";

    printf("*** Crypto storage (CFileArchive) test ***\n");
    CFileArchive archive;

    CFile *file1 = archive.OpenFileForWriting( f1 );
    if( file1==NULL ){
        printf("file1 didn't open for writing.\n");
        return false;
    }
    file1->write( testmsg, strlen(testmsg) );
    delete file1;

    CFile *file2 = archive.OpenFileForWriting( f2 );
    if( file2==NULL ){
        printf("file2 didn't open for writing.\n");
        return false;
    }
    file2->write( xfile, strlen(xfile) );
    delete file2;

    if( !archive.SaveToFile( unenf ) ){
        printf("Unencrypted file write error!\n");
        return false;
    }
    archive.SetWriteKey( (const BYTE *)key );
    if( !archive.SaveToFile( enf ) ){
        printf("Encrypted file write error!\n");
        return false;
    }


    CFileArchive readarchive1;
    if( !readarchive1.LoadFromFile( unenf ) ){
        printf("Unencrypted file read error!\n");
        return false;
    }
    CFile *rf2 = readarchive1.OpenFileForReading( f2 );
    if( rf2==NULL ){
        printf("Unencrypted storage file2 open error.\n");
        return false;
    }
    PumpFileContents( rf2, "CryptoFS_Unencrypted_read2.txt" );
    delete rf2;
    CFile *rf1 = readarchive1.OpenFileForReading( f1 );
    if( rf1==NULL ){
        printf("Unencrypted storage file1 open error.\n");
        return false;
    }
    PumpFileContents( rf1, "CryptoFS_Unencrypted_read1.txt" );
    delete rf1;



    CFileArchive readarchive2;
    readarchive2.SetReadKey( (const BYTE *)key );
    if( !readarchive2.LoadFromFile( enf ) ){
        printf("Encrypted file read error!\n");
        return false;
    }
    CFile *rfe2 = readarchive2.OpenFileForReading( f2 );
    if( rfe2==NULL ){
        printf("Encrypted storage file2 open error.\n");
        return false;
    }
    PumpFileContents( rfe2, "CryptoFS_Encrypted_read2.txt" );
    delete rfe2;
    CFile *rfe1 = readarchive2.OpenFileForReading( f1 );
    if( rfe1==NULL ){
        printf("Encrypted storage file1 open error.\n");
        return false;
    }
    PumpFileContents( rfe1, "CryptoFS_Encrypted_read1.txt" );
    delete rfe1;


    printf("*** Crypto storage test successful. ***\n");
    return true;
}


int main(int argc, char *argv[])
{
    printf("vt_crypto test!\n\n");
    Hashes();
    AESSimple();
    CFS();
    return 0;
}


