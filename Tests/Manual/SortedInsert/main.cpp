
#include <stdlib.h>
#include "vt_vectors.hpp"

const int kTestArraySize = 40;

void test( bool reverse )
{
    VT::CVector<int> v;
    for( int i = 0; i<kTestArraySize; i++ ){
        int x = int(double(rand())/double(RAND_MAX)*99.0);
        printf("%02d: [%02d] ", i, x );
        fflush(stdout);
        v.sortedInsert( x, reverse );
        for( size_t i = 0; i < v.getSize(); i++ )
            printf("%2d ", int(v[i]) );
        printf("\n");
    }
    printf("RESULT: ");
    for( size_t i = 0; i < v.getSize(); i++ )
        printf("%2d ", int(v[i]) );

    printf("\n");

    if( v.IsMonotonic() )
      printf("Monotonic ;-)\n");
    else
      printf("Not monotonic! :(\n");

}

int main( int argc, char* argv[] )
{
    test( false );
    test( true );
    return 0;
}





