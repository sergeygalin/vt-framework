//-----------------------------------------------------------------------------
//
// Basic Audio Types and Conversion Routines
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/


#if !defined(VT_AUDIOTYPES_HPP)
#define VT_AUDIOTYPES_HPP

#include "vt_audiomath.hpp"
#include "vt_vectors.hpp"
#include "vt_dithering.hpp"

namespace VT {

    //
    // Limits (where applied)
    //
    const int kMaxAudioSampleRate    = 192000;    // Max sampling rate we support (where it matters)
    const int kMaxAudioChannels      = 8;        // Max number of channels in audio we support (where it matters)

    //
    // Channel mapping constants
    //
    // Channel mapping in various file formats:
    //
    // 5.1 WAV  FL , FR , FC , LFE, SL , SR
    // 5.1 AC3  FL , FC , FR , SL , SR , LFE
    // 5.1 DTS  FC , FL , FR , SL , SR , LFE
    // 5.1 AAC  FC , FL , FR , SL , SR , LFE
    // 5.1 AIFF FL , SL , FC , FR , SR , LFE
    //
    // We here use WAV channel ordering by default.
    //
    static const unsigned
        kMonoChannel            = 0,
        kLeftChannel            = 0, // Also front left channel
        kRightChannel           = 1, // Also front right channel
        kCenterChannel          = 2,
        kLFEChannel             = 3,
        kSurroundLeftChannel    = 4,
        kSurroundRightChannel   = 5;


    //
    // Audio sample types definition.
    //
    // Meaning of the suffixes:
    //   F - floating point
    //   S - signed fixed point
    //   U - unsigned fixed point
    //
    typedef SWORD            TSample16S;
    typedef SDWORD           TSample32S;
    typedef float            TSample32F;
    typedef double           TSample64F;
    typedef signed char      TSample8S;
    typedef unsigned char    TSample8U;
    typedef TSample32S       TSample20S; // 24-bit data can be stored in DWORD's
    typedef TSample32S       TSample24S; // 20-bit data can be stored in DWORD's

    //
    // Type identifier constants
    //
    // Do not change the numeric constants.
    //
    enum TSampleTypeIdx {
        kInt8S       = 0,
        kInt8U       = 1,
        kInt16S      = 2,
        kInt20S      = 3,
        kInt24S      = 4,
        kInt32S      = 5,
        kFP32        = 6,
        kFP64        = 7,
        kUnknownSampleType
    };

    const size_t        kMaxSampleSizeBytes = 8;

    //
    // Max sample values
    //
    const SWORD         kInt16SMax = 32767;
    const SDWORD        kInt20SMax = 524287;
    const SDWORD        kInt24SMax = 8388607;
    const SDWORD        kInt32SMax = 2147483647;
    const TSample32F    kFP32Max = 1.0f;
    const TSample64F    kFP64Max = 1.0;

    //
    // Sample format properties
    //
    size_t GetAudioTypeSize( TSampleTypeIdx idx );
    LPCVTCHAR GetAudioTypeName( TSampleTypeIdx idx );
    LPCVTCHAR GetAudioTypeShortId( TSampleTypeIdx idx );
    inline bool IsFPFormat( TSampleTypeIdx idx ){ return idx==kFP32 || idx==kFP64; }
    inline bool IsIntFormat( TSampleTypeIdx idx ){ return !IsFPFormat(idx); }

    // Get floating point sample type _recommended_ for use
    // for operating on source/destination format 'idx'.
    // Returns kFP32 if 'idx' specifies a format which has
    // much fewer significant bits in mantiss than 32-bit float
    // or if idx==kFP32; otherwise, returns kFP64.
    TSampleTypeIdx GetIntermediateFPFormat( TSampleTypeIdx idx );

    enum TAudioProcessorBitBehaviour
    {
        kAutoFast,           // Automatically selects resolution (32/64 bit), doesn't
                             // take resampling into account (DEFAULT). The intermediate
                             // resolution is selected to be no worse than input and
                             // output resolution.
        kAutoWith64BitDSP,   // Automatically selects resolution (32/64 bit) like kAutoFast,
                             // but if resampling is involved switches to 64 bits to improve
                             // precision of FIR-based Sinc interpolation.
        kForce32,            // Always use 32 bits for intermediate data.
        kForce64             // Always use 64 bits for intermediate data.
    };

    TSampleTypeIdx GetIntermediateFPFormat( TSampleTypeIdx inType, TSampleTypeIdx outType,
        TAudioProcessorBitBehaviour bitBehaviour, bool heavyDSPUsed );



    // Convert 24 bit sample (as byte sequence) to 32 bit integer.
    VTINLINE SDWORD ConvertSample24to32( const unsigned char *data ){
        register SDWORD s = SDWORD( *(data++) )<<8;
        s |= SDWORD( *(data++) )<<16;
        s |= SDWORD( *(data++) )<<24;
        return s;
    }

    VTINLINE SDWORD ConvertSample20to32( const unsigned char *data ){ return ConvertSample24to32( data ); }

    // Convert 32 bit integer sample to 24 bit sequence without dithering.
    // For dithering, include vt_dithering.hpp, convert 32 bits to double
    // and then to 24 bits using VT_64F_24S_Ditherer, or implement
    // integer dithering... whatever.
    VTINLINE void ConvertSample32to24( TSample32S sample, unsigned char *out ){
        sample >>= 8; *(out++) = (unsigned char)sample;
        sample >>= 8; *(out++) = (unsigned char)sample;
        sample >>= 8; *out = (unsigned char)sample;
    }

    // Convert audio sample: 32 bit to 20 bit (as 3-byte sequence).
    VTINLINE void ConvertSample32to20( TSample32S sample, unsigned char *out ){ ConvertSample32to24( sample, out ); }

    // Convert audio sample: 32 bit to 24 bit sequence dropping higher byte.
    VTINLINE void ConvertSample32to24NoScaling( TSample32S sample, unsigned char *out ){
        *(out++) = (unsigned char)sample;
        sample >>= 8; *(out++) = (unsigned char)sample;
        sample >>= 8; *out = (unsigned char)sample;
    }

    // Convert audio sample: 32 bit to 20 bit sequence dropping higher byte.
    VTINLINE void ConvertSample32to20NoScaling( TSample32S sample, unsigned char *out ){
        ConvertSample32to24NoScaling( sample, out ); }

    // Unsigned 8 bit sample to signed and vice versa. This is useful
    // because 8-bit WAV files are unsigned, while everything good is signed.
    VTINLINE TSample8S ConvertSample8Uto8S( TSample8U src ){ return (TSample8S)(SWORD(src)-128); }
    VTINLINE TSample8U ConvertSample8Sto8U( TSample8S src ){ return (TSample8U)(SWORD(src)+128); }

    // Conversion of SIGNED integer samples to floating-point.
    // For quality reverse conversion, use CDithering.
    template<class T> VTINLINE T ConvertSample16toFP( TSample16S s ){ return T(s)/T(32768.0); }
    template<class T> VTINLINE T ConvertSample20toFP( TSample20S s ){ return T(s)/T(524288.0); }
    template<class T> VTINLINE T ConvertSample24toFP( TSample24S s ){ return T(s)/T(8388608.0); }
    template<class T> VTINLINE T ConvertSample32toFP( TSample32S s ){ return T(s)/T(2147483648.0); }
    template<class T> VTINLINE T ConvertSample8toFP( TSample8U s ){ return T(ConvertSample8Uto8S(s))/T(128.0); }


    //
    // Defining dithering classes for all standard audio types
    //
    typedef CDitherer<TSample32F, TSample16S, -32767, 32767> VT_32F_16S_Ditherer;
    typedef CDitherer<TSample64F, TSample16S, -32767, 32767> VT_64F_16S_Ditherer;
    typedef CDitherer<TSample32F, TSample32S, -2147483647, 2147483647> VT_32F_32S_Ditherer;
    typedef CDitherer<TSample64F, TSample32S, -2147483647, 2147483647> VT_64F_32S_Ditherer;
    typedef CDitherer<TSample32F, TSample8S, -127, 127> VT_32F_8S_Ditherer;
    typedef CDitherer<TSample64F, TSample8S, -127, 127> VT_64F_8S_Ditherer;

    // For 20 and 24 bits, use 32 bit integer for storage; then the highest bits
    // should be dropped if necessary.
    typedef CDitherer<TSample32F, TSample32S, -524287, 524287> VT_32F_20S_Ditherer;
    typedef CDitherer<TSample64F, TSample32S, -524287, 524287> VT_64F_20S_Ditherer;
    typedef CDitherer<TSample32F, TSample32S, -8388607, 8388607> VT_32F_24S_Ditherer;
    typedef CDitherer<TSample64F, TSample32S, -8388607, 8388607> VT_64F_24S_Ditherer;


    //
    // Block audio data converters
    //
    typedef void(*TPAudioConverterFunc)( const void*, void*, size_t );

    // Anything to 32-bit float
    void ConvertAudio8U_32F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio8S_32F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio16S_32F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio20S_32F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio24S_32F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32S_32F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64F_32F( const void* src, void *dest, size_t nSamples );

    // Anything to 64-bit float
    void ConvertAudio8U_64F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio8S_64F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio16S_64F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio20S_64F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio24S_64F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32S_64F( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32F_64F( const void* src, void *dest, size_t nSamples );

    // 32-bit float to integer (no dithering!)
    // For dithered conversion, use CDitherer template.
    void ConvertAudio32F_8U( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32F_8S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32F_16S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32F_20S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32F_24S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32F_32S( const void* src, void *dest, size_t nSamples );

    // 64-bit float to integer (no dithering!)
    // For dithered conversion, use CDitherer template.
    void ConvertAudio64F_8U( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64F_8S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64F_16S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64F_20S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64F_24S( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64F_32S( const void* src, void *dest, size_t nSamples );

    // Convert anything to floating point
    void ConvertAudioTo32F( const void* src, TSampleTypeIdx srcType, void *dest, size_t nSamples );
    void ConvertAudioTo64F( const void* src, TSampleTypeIdx srcType, void *dest, size_t nSamples );

    // Stub functions (do memcpy).
    void ConvertAudio8BitStub( const void* src, void *dest, size_t nSamples );
    void ConvertAudio16BitStub( const void* src, void *dest, size_t nSamples );
    void ConvertAudio20BitStub( const void* src, void *dest, size_t nSamples );
    void ConvertAudio24BitStub( const void* src, void *dest, size_t nSamples );
    void ConvertAudio32BitStub( const void* src, void *dest, size_t nSamples );
    void ConvertAudio64BitStub( const void* src, void *dest, size_t nSamples );

    // Get pointer to sample conversion function.
    // This can be used to avoid switch & function call in ConvertAudioToXXX()
    // and speed up conversion (very little though, unless audio blocks are
    // very small).
    TPAudioConverterFunc GetAudioConverterTo32F( TSampleTypeIdx srcType );
    TPAudioConverterFunc GetAudioConverterTo64F( TSampleTypeIdx srcType );
    TPAudioConverterFunc GetAudioConverterFrom32F( TSampleTypeIdx destType );
    TPAudioConverterFunc GetAudioConverterFrom64F( TSampleTypeIdx destType );
    TPAudioConverterFunc GetAudioConverterStub( TSampleTypeIdx thetype );

    //
    // Splitting/combining audio channels, to convert between
    // multi-channel and interlaced data.
    //
    void SplitAudioChannels( const void *in, void **out, unsigned nChannels, unsigned sampleSizeBytes, size_t nFrames );
    void CombineAudioChannels( const void *const *in, void *out, unsigned nChannels, unsigned sampleSizeBytes, size_t nFrames );

    //
    // A handy class which converts any sample type to any,
    // maintaining internal buffer for intermediate floating-point data and
    // performing dithering when converting floating formats to integer.
    //
    class CAudioBitConverter
    {
    public:
        CAudioBitConverter();
        CAudioBitConverter( TSampleTypeIdx srcType, TSampleTypeIdx destType, size_t bufferPrealloc = 0,
            TDitheringMethod dithering = kDitheringNone, unsigned nDitheringChannels = 1 );

        // Initialize source and destination conversion types.
        // 'dither' can be set to enable dithering (valid only if output
        // format is integer).
        void Init( TSampleTypeIdx srcType, TSampleTypeIdx destType,
            TDitheringMethod dither = kDitheringNone, unsigned nDitheringChannels = 1 );

        // If converter is working with dithering for multiple audio files,
        // it should be restarted between different files. This restarts all
        // ditherers for new data.
        void Restart();

        // Pre-allocate buffers for nSamples block size. Do it after Init().
        void PreallocBuffers( size_t nSamples );

        // Convert block
        void Convert( const void *src, void *dest, size_t nSamples );

        // Do we use 64 bit for conversion? That makes no sense if
        // input and output formats are low-precision.
        // 4-byte IEEE float has 8-bit exponent and 23-bit mantissa,
        // so calculation which involve less than 24 bits (8, 16, 20)
        // can be performed in 32 bit float. Well, let's have it for
        // 20 bits as well.
        // This function should be called after initialization, of course.
        bool Uses64Bit() const {
            return GetIntermediateFPFormat( m_SrcType, m_DestType, kAutoFast, false )==kFP64;
        }

        bool UsesDirectConversion() const;
        bool UsesDithering() const {
            return m_DitheringMethod!=kDitheringNone && IsIntFormat(m_DestType) && !UsesDirectConversion(); }

        bool IsInitialized() const { return m_pfInConverter!=NULL; }

        TSampleTypeIdx GetSrcType() const { return m_SrcType; }
        TSampleTypeIdx GetDestType() const { return m_DestType; }
    private:
        TSampleTypeIdx m_SrcType, m_DestType;
        TPAudioConverterFunc m_pfInConverter, m_pfOutConverter;
        CVector<TSample32F> m_Buffer32;
        CVector<TSample64F> m_Buffer64;
        unsigned m_uChannels;
        TDitheringMethod m_DitheringMethod;
        // That looks bulky, but in fact these objects are tiny (like 12 bytes)
        // and initialized quicky. 10 types by 8 channels = 80 objects or 960 bytes.
        VT_32F_16S_Ditherer       m_Dither32F_16S[kMaxAudioChannels];
        VT_64F_16S_Ditherer       m_Dither64F_16S[kMaxAudioChannels];
        VT_32F_32S_Ditherer       m_Dither32F_32S[kMaxAudioChannels];
        VT_64F_32S_Ditherer       m_Dither64F_32S[kMaxAudioChannels];
        VT_32F_8S_Ditherer        m_Dither32F_8S[kMaxAudioChannels];
        VT_64F_8S_Ditherer        m_Dither64F_8S[kMaxAudioChannels];
        VT_32F_20S_Ditherer       m_Dither32F_20S[kMaxAudioChannels];
        VT_64F_20S_Ditherer       m_Dither64F_20S[kMaxAudioChannels];
        VT_32F_24S_Ditherer       m_Dither32F_24S[kMaxAudioChannels];
        VT_64F_24S_Ditherer       m_Dither64F_24S[kMaxAudioChannels];
    };




    //
    // Audio frame template (for interlaced multi-channel audio)
    //
    // T - sample type
    // NCH - number of channels
    //
    template<class T, int NCH=2> struct TAudioFrame
    {
    private:
        T channels[NCH];
    public:

        VTINLINE TAudioFrame(){} // Default constructor does nothing
        VTINLINE TAudioFrame(T x){ Set(x); }
        template<class U> VTINLINE TAudioFrame( const TAudioFrame<U, NCH>& src ){ CopyFrom( src ); }

        VTINLINE operator T*(){ return channels; }

        VTINLINE operator T() const { T sum = channels[0]; for(register int i=1; i<NCH; i++) sum+=channels[i]; return sum/T(NCH); }

        VTINLINE T channel( int ch ) const { return channels[ch]; }
        VTINLINE void setChannel( int ch, T value ){ channels[ch] = value; }
        VTINLINE void addChannel( int ch, T value ){ channels[ch] += value; }
        VTINLINE void mulChannel( int ch, T value ){ channels[ch] *= value; }

        template<class U> VTINLINE void CopyFrom( const TAudioFrame<U, NCH>& src ){
            for(register int i=0; i<NCH; i++) channels[i] = T( src.channels[i] ); }

        template<class U> VTINLINE TAudioFrame<T, NCH>& operator=( const TAudioFrame<U, NCH>& src ){
            CopyFrom( src ); return *this; }

        template<class U> VTINLINE void Zero(){
            for(register int i=0; i<NCH; i++) channels[i]=T(0); }

        template<class U> VTINLINE void Set(U x){
            for(register int i=0; i<NCH; i++) channels[i]=T(x); }

        template<class U> VTINLINE void Add( const TAudioFrame<U, NCH>& add ){
            for(register int i=0; i<NCH; i++) channels[i] = T( channels[i]+add.channels[i] ); }

        VTINLINE void AddScalar( T add ){
            for(register int i=0; i<NCH; i++) channels[i] += add; }

        template<class U> VTINLINE TAudioFrame<T, NCH>& operator+=( const TAudioFrame<U, NCH>& add ){
            Add( add ); return *this; }

        template<class U> VTINLINE void Mul( U mul ){
            for(register int i=0; i<NCH; i++) channels[i] = T( channels[i]*mul ); }

        template<class U> VTINLINE void Mul( const TAudioFrame<U, NCH> mul ){
            for(register int i=0; i<NCH; i++) channels[i] = T( channels[i]*mul.channels[i] ); }

        template<class U> VTINLINE TAudioFrame<T, NCH>& operator*=( U mul ){
            Mul( mul ); return *this; }

        template<class U, class R> VTINLINE void AddMultiplied( const TAudioFrame<U, NCH>& add, R k ){
            for(register int i=0; i<NCH; i++) channels[i]+=T(add.channels[i]*k); }

        template<class U, class R> VTINLINE void SetMultiplied( const TAudioFrame<U, NCH>& add, R k ){
            for(register int i=0; i<NCH; i++) channels[i]=T(add.channels[i]*k); }

        template<class U, class R> VTINLINE void MultiplyAndAddMultiplied( R kmine, const TAudioFrame<U, NCH>& add, R k ){
            for(register int i=0; i<NCH; i++)
                channels[i] = T( channels[i]*kmine + add.channels[i]*k ); }

        template<class U, class R> VTINLINE void MultiplyAndAdd( U kmine, const R add ){
            for(register int i=0; i<NCH; i++) channels[i] = T( channels[i]*kmine + add ); }

        // Strict comparison
        bool operator==(const TAudioFrame<T, NCH>& other){
            for(register int i=0; i<NCH; i++)
                if( channels[i] != other.channels[i] )
                    return false;
            return true;
        }
    };


    template<class T> struct TAudioFrame<T, 1>
    {
    private:
        T value;
    public:
        VTINLINE TAudioFrame(){} // Default constructor does nothing
        VTINLINE TAudioFrame(T x): value(x){}
        template<class U> VTINLINE TAudioFrame( const TAudioFrame<U, 1>& src ){ value = src.value; }
        VTINLINE operator T*(){ return &value; }
        VTINLINE operator T() const { return value; }
        VTINLINE T channel( int VT_UNUSED(ch) ) const { return value; }
        VTINLINE void setChannel( int VT_UNUSED(ch), T val ){ value = val; }
        VTINLINE void addChannel( int VT_UNUSED(ch), T val ){ value += val; }
        VTINLINE void mulChannel( int VT_UNUSED(ch), T val ){ value *= val; }
        template<class U> VTINLINE void CopyFrom( const TAudioFrame<U, 1>& src ){ value = T( src.value ); }
        template<class U> VTINLINE TAudioFrame<T, 1>& operator=( const TAudioFrame<U, 1>& src ){ CopyFrom( src ); return *this; }
        template<class U> VTINLINE void Zero(){ value=T(0); }
        template<class U> VTINLINE void Set(U x){ value=T(x); }
        template<class U> VTINLINE void Add( const TAudioFrame<U, 1>& add ){ value = T( value+add.value ); }
        VTINLINE void AddScalar( T add ){ value += add; }
        template<class U> VTINLINE TAudioFrame<T, 1>& operator+=( const TAudioFrame<U, 1>& add ){ Add( add ); return *this; }
        template<class U> VTINLINE void Mul( U mul ){ value = T( value*mul ); }
        template<class U> VTINLINE void Mul( const TAudioFrame<U, 1> mul ){ value = T( value*mul.value ); }
        template<class U> VTINLINE TAudioFrame<T, 1>& operator*=( U mul ){ Mul( mul ); return *this; }
        template<class U, class R> VTINLINE void AddMultiplied( const TAudioFrame<U, 1>& add, R k ){ value+=T(add.value*k); }
        template<class U, class R> VTINLINE void SetMultiplied( const TAudioFrame<U, 1>& add, R k ){ value=T(add.value*k); }
        template<class U, class R> VTINLINE void MultiplyAndAddMultiplied( R kmine, const TAudioFrame<U, 1>& add, R k ){
            value = T( value*kmine + add.value*k ); }
        template<class U, class R> VTINLINE void MultiplyAndAdd( U kmine, const R add ){ value = T( value*kmine + add ); }
        bool operator==(const TAudioFrame<T, 1>& other){ return value == other.value; }
    };

    template<class T> struct TAudioFrame<T, 2>
    {
    private:
        T l, r;
    public:
        VTINLINE TAudioFrame(){} // Default constructor does nothing
        VTINLINE TAudioFrame(T x): l(x), r(x){}
        T left() const { return l; }
        T right() const { return r; }
        template<class U> VTINLINE TAudioFrame( const TAudioFrame<U, 1>& src ){ l = src.l; r = src.r; }
        VTINLINE operator T*(){ return &l; }
        VTINLINE operator T() const { return T((l+r)/2.0); }
        VTINLINE T channel( int ch ) const { return (&l)[ch]; }
        VTINLINE void setChannel( int ch, T val ){ (&l)[ch] = val; }
        VTINLINE void addChannel( int ch, T val ){ (&l)[ch] += val; }
        VTINLINE void mulChannel( int ch, T val ){ (&l)[ch] *= val; }
        template<class U> VTINLINE void CopyFrom( const TAudioFrame<U, 2>& src ){ l = T( src.left() ); r = T( src.right() ); }
        template<class U> VTINLINE TAudioFrame<T, 2>& operator=( const TAudioFrame<U, 1>& src ){ CopyFrom( src ); return *this; }
        template<class U> VTINLINE void Zero(){ l=r=T(0); }
        template<class U> VTINLINE void Set(U x){ r=l=T(x); }
        template<class U> VTINLINE void Add( const TAudioFrame<U, 2>& add ){ l = T( l+add.left() ); r = T( r+add.right() ); }
        VTINLINE void AddScalar( T add ){ l += add; r += add; }
        template<class U> VTINLINE TAudioFrame<T, 2>& operator+=( const TAudioFrame<U, 2>& add ){ Add( add ); return *this; }
        template<class U> VTINLINE void Mul( U mul ){ l = T( l*mul ); r = T( r*mul ); }
        template<class U> VTINLINE void Mul( const TAudioFrame<U, 2> mul ){ l = T( l*mul.left() ); r = T( r*mul.right() ); }
        template<class U> VTINLINE TAudioFrame<T, 2>& operator*=( U mul ){ Mul( mul ); return *this; }
        template<class U, class R> VTINLINE void AddMultiplied( const TAudioFrame<U, 2>& add, R k ){ l+=T(add.left()*k); r+=T(add.right()*k); }
        template<class U, class R> VTINLINE void SetMultiplied( const TAudioFrame<U, 2>& add, R k ){ l=T(add.left()*k); r=T(add.right()*k); }
        template<class U, class R> VTINLINE void MultiplyAndAddMultiplied( R kmine, const TAudioFrame<U, 2>& add, R k ){
            l = T( l*kmine + add.left()*k ); r = T( r*kmine + add.right()*k ); }
        template<class U, class R> VTINLINE void MultiplyAndAdd( U kmine, const R add ){ l = T( l*kmine + add ); r = T( r*kmine + add ); }
        bool operator==(const TAudioFrame<T, 1>& other){ return l == other.left() && r == other.right(); }
    };

} // namespace

#endif


