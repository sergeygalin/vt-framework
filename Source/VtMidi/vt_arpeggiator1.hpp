//-----------------------------------------------------------------------------
//
// MIDI Processing Kit
//
// Arpeggiator-1.
//
// Started: 2010/02
//
// Programmer: Sergey A. Galin
//
//-----------------------------------------------------------------------------

#ifndef VT_ARPEGGIATOR1_HPP
#define VT_ARPEGGIATOR1_HPP

#include "vt_midi_processor.hpp"
#include "vt_piano.hpp"
#include "vt_arrays_sortable.hpp"

namespace VT {

    namespace MIDI {

        class CArpeggiator1: public CMIDIProcessor
        {
        public:
            enum TDirection {
                kDirectionMin = 0,
                kUp = kDirectionMin,    // From lowest to highest and back to lowest

                kDown,                    // From highest to lowest and back to highest
                kUpDown,                // Highest to lowest to highest to lowest... See also: UpDownRepeatEdge
                kDownUp,                // Lowest to highest to lowest to highest... See also: UpDownRepeatEdge
                kRandom,                // Pick random notes
                kLowest,                // Play only lowest note
                kHighest,                // Play only highest note
                kFirst,                    // Play only the note which was pressed first;
                                        // it that key is released the arpeggiator is silent
                                        // until all sequence is released and new one started.
                                        // !!!!!!!!!!! to do: add depress mode
                kLast,                    // Play only the note which was pressed last;
                                        // it that key is released the arpeggiator is silent
                                        // until all sequence is released and new one started.
                                        // !!!!!!!!!!! to do: add depress mode

                kDirectionMax = kLast
            };

            enum TExtension {
                kExtensionMin = 0,

                kNoExtension = kExtensionMin,            // Normal mode - arpeggiating pressed keys
                kExtend2Octaves,                        // Add keys from 1 octave up into the sequence
                kExtend4Octaves,                        // Add keys from 1 octave down and 1, 2 up into the sequence
                kExtend3Octaves,                        // Add keys from 1 octave down and 1 up into the sequence

                kExtensionMax = kExtend3Octaves
            };

            enum TMultiplication {
                kMultiplicationMin = 0,

                kSingleNote = kMultiplicationMin,        // Normal mode - play only notes of the sequence
                kDoubleNote,                            // Add notes from 1 octave up
                kQuadrupleNote,                            // Add notes from 1 octave down and 1, 2 up
                kTripleNote,                            // Add notes from 1 octave down and 1 up

                kMultiplicationMax = kTripleNote
            };

            enum TVelocityMode {
                kVelocityModeMin = 0,

                kFirstInSequence = kVelocityModeMin,    // Use velocity of the first key which was
                                                        // pressed in the current sequence (after silence)
                                                        // for all output notes.
                kLastVelocity,                            // Use last input velocity for all output notes.
                kAsPressed,                                // Use input velocities
                kFixedVelocity,                            // Fixed velocity (default = 100)

                kDefaultVelocityMode = kAsPressed,
                kVelocityModeMax = kFixedVelocity
            };

            CArpeggiator1();
            virtual ~CArpeggiator1();

            virtual void ResetSettings();
            virtual void Reset();
            virtual void ResetTimer( const TTrackPosition* pos  = NULL );

            virtual bool SaveConfiguration( CIniFile* file, LPCVTCHAR sectionNamePrefix ) const;
            virtual bool LoadConfiguration( CIniFile* file, LPCVTCHAR sectionNamePrefix );

            virtual void Process( TMIDIMessage* input, size_t in_count, const TTrackPosition* pos, CMIDIOutputBase* out );

            //
            // Apreggiator properties
            //
            int GetInChannel() const { return m_InChannel; } // 1..16, <0: all channels
            void SetInChannel( int ch ); // 1..16, <0: all channels

            int GetOutChannel() const { return m_OutChannel; } // 1..16, <0: last input channel
            void SetOutChannel( int ch ); // 1..16, <0: last input channel

            double GetNoteLength128th() const { return m_NoteLength128th; }
            void SetNoteLength128th( double len );
            double GetNoteLengthStepPc() const;
            void SetNoteLengthStepPc( double percent );

            int GetNoteStep768th() const { return m_NoteStep768th; }
            void SetNoteStep768th( int x );

            void SetNoteStepAndLengthPc( int step768th, double lengthPc );

            // Sequence direction: up, down, up-down, down-up, random, and etc. (See TDirection.)
            TDirection GetDirection() const { return m_Direction; }
            void SetDirection( TDirection dir );

            bool GetUpDownRepeatEdge() const { return m_UpDownRepeatEdge; }
            void SetUpDownRepeatEdge( bool rep );

            // Note multiplication: single notes, double notes, quadruple notes
            // (allow to make "thicker" sounding).
            TMultiplication GetMultiplicatiobn() const { return m_Multiplication; }
            void SetMultiplication( TMultiplication m );

            TExtension GetExtension() const { return m_Extension; }
            void SetExtension( TExtension ex );

            // How velocities of the output notes are determined. (See TVelocityMode.)
            TVelocityMode GetVelocityMode() const { return m_VelocityMode; }
            void SetVelocityMode( TVelocityMode mode );

            // Set velocity for kFixedVelocity mode.
            BYTE GetFixedVelocity() const { return m_FixedVelocity; }
            void SetFixedVelocity( BYTE vel );




        private:
            //
            // Arpeggiator Properties
            //
            TDirection m_Direction;
            TMultiplication m_Multiplication;
            TExtension m_Extension;

            int m_InChannel,        // -1 means "any channel"
                m_OutChannel;        // -1 means "use last input channel"
            double m_NoteLength128th;
            int m_NoteStep768th;
            TVelocityMode m_VelocityMode;
            BYTE m_FixedVelocity;
            bool m_UpDownRepeatEdge;

            //
            // Temporary (play state) variables
            //
            Piano::CKeyboardStateBase m_Keyboard; // "Keyboard" state reflector
            TTime m_PreviousOutputPos768th; // Playing position
            BYTE m_Velocities[128], m_FirstNote, m_LastNote, m_FirstVelocity, m_LastVelocity; // Input velocities
            VT::CSortableArray<TMIDIMessage, true> m_Sequence; // Prepared sequence notes
            int m_LastInChannel; // Last seen input channel
            int m_SequencePos; // Sequence pointer
            bool m_bUp; // Current sequence direction (up/down)

            //
            // Private functions
            //
            BYTE GetActualOutChannel() const;
            void SetRandomPos();
            void PushUnique( const TMIDIMessage& msg );
            void UpdateSequence( bool soft = false );
            void ResetSequencePos();
            void OutNote( const TTrackPosition* pos, const TMIDIMessage& msg, CMIDIOutputBase* out );
        };



    } // namespace MIDI
} // namespace VT

#endif


