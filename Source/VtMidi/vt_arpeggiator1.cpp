//-----------------------------------------------------------------------------
//
// MIDI Processing Kit
//
// Arpeggiator-1.
//
// Started: 2010/02
//
// Programmer: Sergey A. Galin
//
// See the corresponding header for more information.
//
//-----------------------------------------------------------------------------

#include "vt_arpeggiator1.hpp"
#include "vt_assert.hpp"

using namespace VT;
using namespace MIDI;

CArpeggiator1::CArpeggiator1():
    m_Direction( kUp ),
    m_Multiplication( kSingleNote ),
    m_Extension( kNoExtension ),
    m_InChannel( -1 ),
    m_OutChannel( -1 ),
    m_LastInChannel( -1 ),
    m_Sequence( 0, 64 ),
    m_NoteLength128th( 32.0 ),
    m_NoteStep768th( 96 ),
    m_bUp( true ),
    m_PreviousOutputPos768th( -1000000 ),
    m_VelocityMode( kDefaultVelocityMode ),
    m_FirstNote( MIDI::kErrorNote ),
    m_LastNote( MIDI::kErrorNote ),
    m_FirstVelocity( 0 ),
    m_LastVelocity( 0 ),
    m_FixedVelocity( 100 ),
    m_UpDownRepeatEdge( false )
{
    ResetSettings();
}

CArpeggiator1::~CArpeggiator1()
{
}

void CArpeggiator1::ResetSettings()
{
    m_Direction = kUp;
    m_Multiplication = kSingleNote;
    m_InChannel = -1;
    m_OutChannel = -1;
    m_LastInChannel = -1;
    m_NoteLength128th = 32.0;
    m_NoteStep768th = 96;
    m_VelocityMode = kDefaultVelocityMode;
    m_FirstVelocity = 0;
    m_LastVelocity = 0;
    m_FirstNote = MIDI::kErrorNote;
    m_LastNote = MIDI::kErrorNote;
    m_FixedVelocity = 100;
    m_UpDownRepeatEdge = false;

    Reset();
}

void CArpeggiator1::Reset()
{
    ResetTimer();

    m_LastInChannel = 0;
    m_Keyboard.Clear();
    UpdateSequence();
    memset( m_Velocities, 100, sizeof(m_Velocities) );
}

void CArpeggiator1::ResetTimer( const TTrackPosition* pos )
{
    if( pos == NULL )
        m_PreviousOutputPos768th = -10000000;
    else
        m_PreviousOutputPos768th = pos->PseudoAbs768th();
}

bool CArpeggiator1::SaveConfiguration( CIniFile* file, LPCVTCHAR sectionNamePrefix ) const
{
    file->InsertSection( sectionNamePrefix );
    file->InsertBool( VTT("Bypass"), IsBypass() );
    file->InsertInt( VTT("InChannel"), GetInChannel() );
    file->InsertInt( VTT("OutChannel"), GetOutChannel() );
    file->InsertDouble( VTT("NoteLength"), GetNoteLength128th() );
    file->InsertInt( VTT("NoteStep"), GetNoteStep768th() );
    file->InsertInt( VTT("Direction"), int(GetDirection()) );
    file->InsertBool( VTT("Repeat"), GetUpDownRepeatEdge() );
    file->InsertInt( VTT("Multiplication"), GetMultiplicatiobn() );
    file->InsertInt( VTT("Extension"), GetExtension() );
    file->InsertInt( VTT("VelocityMode"), int(GetVelocityMode()) );
    file->InsertInt( VTT("Velocity"), GetFixedVelocity() );
    return true;
}

bool CArpeggiator1::LoadConfiguration( CIniFile* file, LPCVTCHAR sectionNamePrefix )
{
    ResetSettings();
    if( !file->GoToSection( sectionNamePrefix ) )
        return false;

    try{
        SetBypass( file->GetBoolDef( VTT("Bypass"), IsBypass() ) );
        SetInChannel( file->GetIntDef( VTT("InChannel"), GetInChannel() ) );
        SetOutChannel( file->GetIntDef( VTT("OutChannel"), GetOutChannel() ) );
        SetNoteLength128th( file->GetDoubleDef( VTT("NoteLength"), GetNoteLength128th() ) );
        SetNoteStep768th( file->GetIntDef( VTT("NoteStep"), GetNoteStep768th() ) );
        SetDirection( TDirection( file->GetIntDef( VTT("Direction"), int(GetDirection()) ) ) );
        SetUpDownRepeatEdge( file->GetBoolDef( VTT("Repeat"), GetUpDownRepeatEdge() ) );
        SetMultiplication( TMultiplication( file->GetIntDef( VTT("Multiplication"), GetMultiplicatiobn() ) ) );
        SetExtension( TExtension( file->GetIntDef( VTT("Extension"), GetExtension() ) ) );
        SetVelocityMode( TVelocityMode( file->GetIntDef( VTT("VelocityMode"), int(GetVelocityMode()) ) ) );
        SetFixedVelocity( file->GetIntDef( VTT("Velocity"), GetFixedVelocity() ) );
    }catch(...){
        ResetSettings();
        VTDEBUGMSGT( "An exception occured while loading arpeggiator1 data from INI file." );
        return false;
    }

    return true;
}


void CArpeggiator1::Process( TMIDIMessage* input, size_t in_count, const TTrackPosition* pos, CMIDIOutputBase* out )
{
    if( in_count ){
        // Update keyboard state
        m_Keyboard.FlushStateChanged();

        bool needReset = false, softSequenceReset = true;

        if( m_Sequence.size()==0 ){
            m_FirstVelocity = 0;
            m_FirstNote = MIDI::kErrorNote;
        }

        for( size_t i = 0; i < in_count; i++ ){
            TMIDIMessage& msg = input[i];
            bool isPolyPressure = (msg.command == MIDI::MIDI_POLYPRESSURE);

            // Filter out non-notes
            if( !msg.IsNote() && !isPolyPressure )
                continue;

            // Filter out notes from wrong channels
            if( m_InChannel > 0 )
                if( int(msg.channel) != m_InChannel )
                    continue;

            // We've found a note - sequence must be re-calculated
            needReset = true;

            // Keep an eye on input message channel
            m_LastInChannel = msg.channel;

            // Track which keys are pressed/released using m_Keyboard
            m_Keyboard.MIDIMessage( msg );

            //
            // Register velocity
            //
            if( msg.IsNoteOn() || isPolyPressure ){
                BYTE vel = Math::Clip<BYTE>( msg.GetNoteVelocity(), 1, 127 );
                m_Velocities[ Math::Clip<size_t>( msg.GetNote(), 0, 127 ) ] = vel;

                m_LastVelocity = vel;

                if( !isPolyPressure ){
                    m_LastNote = msg.GetNote();
                    if( m_FirstVelocity==0 && m_Sequence.size()==0 ){ // First note pressed
                        m_FirstVelocity = vel;
                        m_FirstNote = msg.GetNote();
                    }
                    softSequenceReset = false;
                }else{

                    int x = 1;

                }
            }

            // Kill the message - note that after-touch messages are killed as well
            if( !IsBypass() )
                msg.ZeroMessage();
        }

        //if( m_Keyboard.GetStateChanged() )
        if( needReset )
            UpdateSequence( softSequenceReset );
    }

    TTime pos768th = pos->PseudoAbs768th();

    TTime previousOutputGridPos768th = pos768th - ( pos768th % m_NoteStep768th );

    if( m_Sequence.getSize() ){    // We have some notes to arpeggiate

        if( previousOutputGridPos768th > m_PreviousOutputPos768th ){

            //
            // Verify sequence index
            //
            if(    m_SequencePos >= int(m_Sequence.getSize()) )
                m_SequencePos = int(m_Sequence.getSize())-1;
            else if( m_SequencePos < 0 )
                m_SequencePos = 0;

            //
            // Get the message to output
            //
            VTASSERT( m_SequencePos>=0 );
            VTASSERT( m_SequencePos<m_Sequence.getSize() );
            TMIDIMessage msg( m_Sequence[m_SequencePos] );

            //
            // Calculate next sequence position
            //
            switch( m_Direction ){
                case kUp:
                    if( m_SequencePos >= int(m_Sequence.getSize())-1 )
                        m_SequencePos = 0;
                    else
                        m_SequencePos++;
                    break;

                case kDown:
                    if( m_SequencePos <= 0 )
                        m_SequencePos = int(m_Sequence.getSize())-1;
                    else
                        m_SequencePos--;
                    break;

                case kUpDown:
                case kDownUp:
                    if( m_bUp ){
                        if( m_SequencePos >= int(m_Sequence.getSize())-1 ){
                            if( m_UpDownRepeatEdge )
                                m_SequencePos = int(m_Sequence.getSize())-1;
                            else
                                m_SequencePos = int(m_Sequence.getSize())-2;
                            m_bUp = false; // Reverse inertial stabilizers
                        }else
                            m_SequencePos++;
                    }else{
                        if( m_SequencePos <= 0 ){
                            if( m_UpDownRepeatEdge )
                                m_SequencePos = 0;
                            else
                                m_SequencePos = 1;
                            m_bUp = true; // Reverse inertial stabilizers
                        }else
                            m_SequencePos--;
                    }
                    break;

                case kRandom:
                    SetRandomPos();
                    break;

                case kLowest:
                case kHighest:
                case kFirst:
                case kLast:
                    m_SequencePos = 0;
                    break;

                default:
                    VTDEBUGMSGT( "Unknown sequence direction code." );
            }

            //
            // Output the note
            //
            if( !IsBypass() ){
                OutNote( pos, msg, out );
                if( m_Multiplication==kDoubleNote ){
                    //
                    // Output same note but 1 octave higher
                    //
                    if( msg.GetNote() <= 127-12 ){
                        msg.SetNote( msg.GetNote()+12 );
                        OutNote( pos, msg, out );
                    }
                }else if( m_Multiplication==kQuadrupleNote || m_Multiplication==kTripleNote ){
                    // Output same note but 1 and 2 octaves higher and 1 octave lower
                    TMIDIMessage tempMsg( msg );
                    if( msg.GetNote() <= 127-12 ){
                        tempMsg.SetNote( msg.GetNote()+12 );
                        OutNote( pos, tempMsg, out );
                        if( m_Multiplication==kQuadrupleNote && msg.GetNote() <= 127-24 ){
                            tempMsg.SetNote( msg.GetNote()+24 );
                            OutNote( pos, tempMsg, out );
                        }
                    }
                    if( msg.GetNote() >= 12 ){
                        tempMsg.SetNote( msg.GetNote()-12 );
                        OutNote( pos, tempMsg, out );
                    }
                }
            }

            m_PreviousOutputPos768th = previousOutputGridPos768th;

        }

    }else{

        m_PreviousOutputPos768th = previousOutputGridPos768th;
    }

}


void CArpeggiator1::SetInChannel( int ch )
{
    VTASSERT( ch<0 || (ch>=1 && ch<=16) );
    if( !(ch<0 || (ch>=1 && ch<=16)) )
        throw CInvalidMIDIChannelException();
    m_InChannel = ch;
}

void CArpeggiator1::SetOutChannel( int ch )
{
    VTASSERT( ch<0 || (ch>=1 && ch<=16) );
    if( !(ch<0 || (ch>=1 && ch<=16)) )
        throw CInvalidMIDIChannelException();
    m_OutChannel = ch;
}

void CArpeggiator1::SetNoteLength128th( double len )
{
    if( len<0.0 )
        throw CInvalidParameterException( VTT("Note length cannot be negative.") );
    m_NoteLength128th = len;
}

double CArpeggiator1::GetNoteLengthStepPc() const
{
    double noteLength128th = GetNoteLength128th();
    double noteLength768th = noteLength128th * 6.0;
    double pcF = 100.0 * noteLength768th / double(GetNoteStep768th());
    return pcF;
}

void CArpeggiator1::SetNoteLengthStepPc( double percent )
{
    double noteLength768th = GetNoteStep768th() * percent / 100.0;
    double noteLength128thF = noteLength768th / 6.0;
    SetNoteLength128th( noteLength128thF );
}

void CArpeggiator1::SetNoteStepAndLengthPc( int step768th, double lengthPc )
{
    SetNoteStep768th( step768th );
    SetNoteLengthStepPc( lengthPc );
}

void CArpeggiator1::SetNoteStep768th( int x )
{
    if( x<1 )
        throw CInvalidParameterException( VTT("Note step must be positive.") );
    m_NoteStep768th = x;
}

void CArpeggiator1::SetDirection( TDirection dir )
{
    if( dir < kDirectionMin || dir > kDirectionMax )
        throw CInvalidParameterException( VTT("Invalid arpeggiator direction.") );
    m_Direction = dir;
}

void CArpeggiator1::SetUpDownRepeatEdge( bool rep )
{
    m_UpDownRepeatEdge = rep;
}

void CArpeggiator1::SetFixedVelocity( BYTE vel )
{
    if( vel<1 || vel>127 )
        throw CInvalidParameterException( VTT("Note velocity must be 1..127.") );
    m_FixedVelocity = vel;
}

void CArpeggiator1::SetMultiplication( TMultiplication m )
{
    if( m < kMultiplicationMin || m > kMultiplicationMax )
        throw CInvalidParameterException( VTT("Invalid arpeggiator multiplication mode.") );
    m_Multiplication = m;
}

void CArpeggiator1::SetExtension( TExtension ex )
{
    if( ex < kExtensionMin || ex > kExtensionMax )
        throw CInvalidParameterException( VTT("Invalid arpeggiator range extension mode.") );
    m_Extension = ex;
    UpdateSequence();
}

void CArpeggiator1::SetVelocityMode( TVelocityMode mode )
{
    if( mode < kVelocityModeMin || mode > kVelocityModeMax )
        throw CInvalidParameterException( VTT("Invalid arpeggiator velocity mode.") );
    m_VelocityMode = mode;
}

void CArpeggiator1::OutNote( const TTrackPosition* pos, const TMIDIMessage& msg, CMIDIOutputBase* out )
{
    // Constants for minimal note separation
    const double kMaxLengthFraction = 0.98;
    const TTime kMinNoteSeparation = 101; // Ticks, typically ms

    VTASSERT( !IsBypass() );
    if( out != NULL ){
        TTime noteLength = Math::URoundT<double, TTime>( ceil( pos->Get128thLengthTicks() * m_NoteLength128th ) );

        //
        // Prevent note overlapping

        double noteRepeatIntervalF = floor( pos->Get768thLengthTicks() * double( GetNoteStep768th() ) * m_Sequence.getSize() );
        TTime noteRepeatInterval = Math::URoundT<double, TTime>( noteRepeatIntervalF );

        TTime maxLength = Math::URoundT<double, TTime>( kMaxLengthFraction * noteRepeatInterval );

        if( noteLength > kMinNoteSeparation && noteRepeatInterval > kMinNoteSeparation ){
            TTime maxSepLength = noteRepeatInterval - kMinNoteSeparation;
            maxLength = Math::Min( maxLength, maxSepLength );
        }

        out->OutNote( // Defined in VT::MIDI::CMIDIOutputBase
            msg,
            Math::Min( noteLength, maxLength ), // Length
            0 // Delay
        );
    }
}

BYTE CArpeggiator1::GetActualOutChannel() const
{
    if( m_OutChannel > 0 )
        return BYTE( m_OutChannel );
    if( m_LastInChannel > 0 )
        return BYTE( m_LastInChannel );
    return 0;
}

void CArpeggiator1::PushUnique( const TMIDIMessage& msg )
{
    for( size_t i = 0; i < m_Sequence.size(); i++ )
        if( msg.GetNote() == m_Sequence.at(i).GetNote() )
            return;
    m_Sequence.push( msg );
}

void CArpeggiator1::UpdateSequence( bool soft )
{
    //
    // Clean old sequence
    //
    m_Sequence.resize( 0 );

    BYTE vel;
    if( m_VelocityMode==kFirstInSequence )
        vel = m_FirstVelocity;
    else if( m_VelocityMode==kLastVelocity )
        vel = m_LastVelocity;
    else
        vel = m_FixedVelocity;

    // Temporary message
    TMIDIMessage message;
    message.SetNoteOn(
        0,
        vel,
        GetActualOutChannel() );

    switch( m_Direction ){
        case kLowest:
            for( BYTE i=0; i<m_Keyboard.kMaxKeys; i++ )
                if( m_Keyboard.IsKeyPressed( i ) ){
                    message.SetNote( i );
                    if( m_VelocityMode==kAsPressed ) // Otherwise, leave velocity as is
                        message.SetNoteVelocity( m_Velocities[i] );
                    m_Sequence.push( message );
                    break; // The lowest note pushed, job's done
                }
            break;

        case kHighest:
            for( int i = m_Keyboard.kMaxKeys-1; i>=0; i-- ){
                if( m_Keyboard.IsKeyPressed( BYTE(i) ) ){
                    message.SetNote( BYTE(i) );
                    if( m_VelocityMode==kAsPressed ) // Otherwise, leave velocity as is
                        message.SetNoteVelocity( m_Velocities[i] );
                    m_Sequence.push( message );
                    break; // The highest note pushed, job's done
                }
            }
            break;

        case kFirst:
            if( m_FirstVelocity > 0 && m_FirstNote != MIDI::kErrorNote && m_FirstNote < m_Keyboard.kMaxKeys )
                if( m_Keyboard.IsKeyPressed( m_FirstNote ) ){ // !!!!!!!!!!!! add depress mode
                    message.SetNote( m_FirstNote );
                    if( m_VelocityMode==kAsPressed )
                        message.SetNoteVelocity( m_FirstVelocity );
                    m_Sequence.push( message );
                }
            break;

        case kLast:
            if( m_LastVelocity > 0 && m_LastNote != MIDI::kErrorNote && m_LastNote < m_Keyboard.kMaxKeys )
                if( m_Keyboard.IsKeyPressed( m_LastNote ) ){ // !!!!!!!!!!!! add depress mode
                    message.SetNote( m_LastNote );
                    if( m_VelocityMode==kAsPressed )
                        message.SetNoteVelocity( m_LastVelocity );
                    m_Sequence.push( message );
                }
            break;


        default:
            for( BYTE i=0; i<m_Keyboard.kMaxKeys; i++ )
                if( m_Keyboard.IsKeyPressed( i ) ){
                    message.SetNote( i );
                    if( m_VelocityMode==kAsPressed ) // Otherwise, leave velocity as is
                        message.SetNoteVelocity( m_Velocities[i] );
                    m_Sequence.push( message );
                }
    }


    //
    // Extending the range
    //

    if( GetExtension() != kNoExtension ){

        size_t szBeforeExtension = m_Sequence.getSize();

        if( GetExtension()==kExtend2Octaves ){
            //
            // Extend the chord by adding keys 1 octave above nominal
            //
            for( size_t i = 0; i < szBeforeExtension; i++ ){
                TMIDIMessage toExtend( m_Sequence.at( i ) );
                BYTE note = toExtend.GetNote();
                if( note<=127-12 ){
                    toExtend.SetNote( note + 12 );
                    PushUnique( toExtend );
                }
            }


        }else if( GetExtension()==kExtend3Octaves ){
            //
            // Extend the chord by adding keys 1 octave below and 1 and 2 octaves above nominal
            //
            for( size_t i = 0; i < szBeforeExtension; i++ ){
                TMIDIMessage toExtend( m_Sequence.at( i ) );
                BYTE note = toExtend.GetNote();
                if( note >= 12 ){
                    toExtend.SetNote( note - 12 );
                    PushUnique( toExtend );
                }
                if( note <= 127-12 ){
                    toExtend.SetNote( note + 12 );
                    PushUnique( toExtend );
                }
            }

        }else if( GetExtension()==kExtend4Octaves ){
            //
            // Extend the chord by adding keys 1 octave below and 1 and 2 octaves above nominal
            //
            for( size_t i = 0; i < szBeforeExtension; i++ ){
                TMIDIMessage toExtend( m_Sequence.at( i ) );
                BYTE note = toExtend.GetNote();
                if( note >= 12 ){
                    toExtend.SetNote( note - 12 );
                    PushUnique( toExtend );
                }
                if( note <= 127-12 ){
                    toExtend.SetNote( note + 12 );
                    PushUnique( toExtend );
                }
                if( note <= 127-24 ){
                    toExtend.SetNote( note + 24 );
                    PushUnique( toExtend );
                }
            }
        }else{
            VTDEBUGMSGT( "Unknown range extension code." );
        }

        //
        // Keep notes in low to high order to have arpegiating
        // direction modes working properly (they assume that the sequence
        // is ordered high to low).
        //
        TMIDIMessage::CNoteSortingComparator comp;
        m_Sequence.Sort( comp );
    }

    if( !soft )
        ResetSequencePos();
}

void CArpeggiator1::ResetSequencePos()
{
    if( m_Direction == kDown || m_Direction == kDownUp )
        m_SequencePos = int(m_Sequence.getSize())-1;

    else if( m_Direction == kRandom )
        SetRandomPos();

    else
        // All other modes start from first note
        m_SequencePos = 0;

    // m_Direction is used only in kUpDown and kDownUp modes
    if( m_Direction == kDownUp )
        m_bUp = false; // Go down first
    else
        m_bUp = true; // Go up first
}

void CArpeggiator1::SetRandomPos()
{
    m_SequencePos = Math::Clip<int>(
        Math::URound( double(rand())*double(m_Sequence.getSize()-1)/double(RAND_MAX) ),
        0,
        int( m_Sequence.getSize() )-1 );
}



