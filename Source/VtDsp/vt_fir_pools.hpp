//-----------------------------------------------------------------------------
//
// FIR filter pool allows many audio processors to share their pre-calculated
// filters. This is especially useful for real-time resamplers, speed changers
// and etc., when multiple objects can use the same polyphase sinc filter
// arrays which take pretty much memory and some time to be created.
// Filters are calculated first they are requested, and stay in memory until
// end of program execution. If it is not within ideology of your application,
// do not use this module.
// More types of filters to be added later.
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2008 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/


#ifndef VT_FIR_POOLS_H
#define VT_FIR_POOLS_H

#include "vt_messageboxes.hpp" // First include!
#include "vt_fir_polyphase.hpp"
#include "vt_handystuff.hpp"
#include "vt_threading.hpp"

namespace VT {
    namespace FIR {

        // Default Polyphase Sinc interpolation filter settings.
        // The defaults provide good enough quality to make distortion
        // unaudible, but still to keep CPU load reasonably low.
        // FIR size imporves quality, but dramatically decreases performance
        // and also introduces delay by (FIR size)/2 samples.
        // Increasing number of phases decreases jitter and consumes mostly
        // RAM resource. CPU is really loaded only to precalculate all phases
        // during initialization of the first synth using a filter.
        // BTW, for filter lengths over approximately 32 it becomes more
        // efficient to use Fourier transformation instead of FIR convolution.
        //
        // NOTES ABOUT FIR LENGTHS
        //
        // - FIR filter length for interpolation is usually an odd number.
        // - Single precision SSE acceleration functions work best is length
        //   is a multiply of 4 (the remaining 0..3 elements are calculated
        //   via FPU) so it's good if we had float filters of N*4 or N*4+1 length.
        //     Unfortunately that won't give much to our circular buffers
        //   (CBuffer) as they split convolution to a sum of two convolutions
        //   when FIR filter overlaps "physical" end of the buffer. This problem
        //   can be reduced by increasing size of circular buffer to many times
        //   longer than FIR filter, so probability of the overlap decreases.
        // - Check CSincResampler constants. It's better to have the same
        //   filter types here and there, so they could share their data in pools.
        //
        const size_t
            kDefaultRealTimePhases      = 4096, // Default real-time polyphase filter phases.
                                                // Increase to fight interpolator's jitter.
                                                // Changing this value has no direct effect on
                                                // processing performance, however, bigger value may
                                                // reduce CPU's memory cache hits. And of course
                                                // program's initialization time and memory consumption
                                                // increases.
                                                // 4096 seems to be about an optimal value.

            kLowQualityRTFIRSize        =   25,    // Lowest quality we accept ;-)
            kNormalQualityRTFIRSize     =   45,    // No audible artefacts
            kHighQualityRTFIRSize       =   65,    // But this setting is better
            kVeryHighQualityRTFIRSize   =  257,    // And this one is already good
            kInsaneQualityRTFIRSize     =  513,    // "Production quality"
            kKillerQualityRTFIRSize     = 2047,    // "Insane quality"

            kDefaultRealTimeFIRSize    = kNormalQualityRTFIRSize; // For use as default by real-time classes


        //
        // Low-pass Sinc filter pool: returns Sinc filter sets for given filter
        // length / phases / frequency / frequency tolerance. There are pools
        // for float, double and long double filters, constructed using templates.
        //
        // NOTE: As this object is intended for application-wide useage, it has its
        // own thread locking, so multiple threads can call GetFilter() without
        // multithreading collisions.
        //
        template<class T, Windowing::TWindowType WINTYPE = Windowing::kLanczos>
            class CPolyphaseSincFilterPool: protected CArray<CPolyphaseSincFilterSet<T>*>
        {
        public:
            CPolyphaseSincFilterPool<T, WINTYPE>(){}

            ~CPolyphaseSincFilterPool<T, WINTYPE>(){
                CCSLock lock(&m_CS);
                for( size_t i=0; i<this->getSize(); i++ )
                    FreeAndNull( this->m_pData[i] );
                this->resize(0);
            }

            CPolyphaseSincFilterSet<T>* GetFilter( size_t oneFilterLength, size_t nPhases, T fc, T fctolerance )
            {
                CCSLock lock(&m_CS);
                // Searching existing filters for required filter
                for( size_t i=0; i<this->getSize(); i++ ){
                    VTASSERT( this->m_pData[i] != NULL );
                    if( oneFilterLength==this->m_pData[i]->GetFilterSize() &&
                        nPhases==this->m_pData[i]->GetNPhases() &&
                        Math::Abs( this->m_pData[i]->GetFc()-fc )<=fctolerance ){
                        return this->m_pData[i];
                    }
                }
                // Add space for new filter and create it
                size_t newFilterIdx = this->getSize();
                this->resize( newFilterIdx+1, true );
                return this->m_pData[ newFilterIdx ] =
                    new CPolyphaseSincFilterSet<T>( oneFilterLength, nPhases, fc, WINTYPE );
            }
        protected:
            CCriticalSection m_CS;
        };


        //
        // Definition of standard polyphase sinc filter pools.
        //
        extern CPolyphaseSincFilterPool<float> g_StdPolyphaseSincFilterPoolSP;
        extern CPolyphaseSincFilterPool<double> g_StdPolyphaseSincFilterPoolDP;
        extern CPolyphaseSincFilterPool<long double> g_StdPolyphaseSincFilterPoolLDP;

        //
        // A set of specialized templates to assist using std filters in class templates.
        //
        // Stub "protoype" template (should never be actually used).
        template<class T> VTINLINE const CPolyphaseSincFilterSet<T>* GetStdPolyphaseSincFilters(
            size_t oneFilterLength, size_t nPhases, T fc, T fctolerance ){
            VTTHROWT("Generic GetStdPolyphaseSincFilters() called!");
            return NULL;
        }

        // Specialized template for float
        template<> VTINLINE const CPolyphaseSincFilterSet<float>* GetStdPolyphaseSincFilters(
            size_t oneFilterLength, size_t nPhases, float fc, float fctolerance  ){
            return g_StdPolyphaseSincFilterPoolSP.GetFilter( oneFilterLength, nPhases, fc, fctolerance  );
        }

        // Specialized template for double
        template<> VTINLINE const CPolyphaseSincFilterSet<double>* GetStdPolyphaseSincFilters(
            size_t oneFilterLength, size_t nPhases, double fc, double fctolerance  ){
            return g_StdPolyphaseSincFilterPoolDP.GetFilter( oneFilterLength, nPhases, fc, fctolerance  );
        }

        // Specialized template for long double (BTW, in MSC 'long double' is the same as 'double'. Fun!!
        // But let's not rely on that. Some other compiler may use 80 bit long double, or even bigger.)
        template<> VTINLINE const CPolyphaseSincFilterSet<long double>* GetStdPolyphaseSincFilters(
            size_t oneFilterLength, size_t nPhases, long double fc, long double fctolerance  ){
            return g_StdPolyphaseSincFilterPoolLDP.GetFilter( oneFilterLength, nPhases, fc, fctolerance  );
        }

    } // namespace
} // namespace

#endif






