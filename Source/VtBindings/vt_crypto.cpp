//-----------------------------------------------------------------------------
//
// Encryption wrappers
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include "vt_crypto.hpp"
#include "vt_string.hpp"
#include "vt_exceptions.hpp"
// #include "vt_assert.hpp" - included later

// #define CRYPTOPP_INCLUDE_VT_LIB

#if defined(USE_CRYPTOPP)
    #define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

    #if defined(USE_CRYPTOPP_STATIC)

        #define CRYPTOPP_EXPORTS
        #if defined(CRYPTOPP_INCLUDE_VT_LIB)
            #pragma message("Using statically linked Crypto++ and headers in Lib directory.")
            #include "../../Lib/include/sha.h"
            #include "../../Lib/include/aes.h"
            #include "../../Lib/include/modes.h"
            #include "../../Lib/include/filters.h"
            #include "../../Lib/include/cbcmac.h"
        #else
            #pragma message("Using statically linked Crypto++ and headers in search path.")
            #include "sha.h"
            #include "aes.h"
            #include "modes.h"
            #include "filters.h"
            #include "cbcmac.h"
        #endif

        #if defined(VTCRYPT_WEAK_ALGORITHMS_ENABLED)
            #if defined(CRYPTOPP_INCLUDE_VT_LIB)
                #include "../../Lib/include/md5.h"
            #else
                #include "md5.h"
            #endif
            #define VTCRYPTO_WEAK CryptoPP::Weak
        #endif

    #else
        #if defined(CRYPTOPP_INCLUDE_VT_LIB)
            #pragma message("Using Crypto++ DLL and headers Lib directory.")
            #include "../../Lib/include/dll.h"
        #else
            #pragma message("Using Crypto++ DLL and header in search path.")
            #include "dll.h"
        #endif

        #if defined(VTCRYPT_WEAK_ALGORITHMS_ENABLED)
            #define VTCRYPTO_WEAK CryptoPP
        #endif
    #endif
#else
    #error "Only CRYPTOPP511 is supported!"
#endif

#if defined(VT_CONSOLEDEBUG)
    #include "vt_string.hpp"
#endif

#include "vt_assert.hpp" // Last include!

using namespace VT;

size_t Crypto::AlignToBlocks( size_t sz, size_t blocksz )
{
    size_t ret = ((sz+blocksz-1)/blocksz) * blocksz;
    VTASSERT( ret>=sz );
    VTASSERT( ret-sz < blocksz );
    return ret;
}

void Crypto::Base16(const unsigned char *data, size_t inbytes, VTCHAR *out)
{
    static const VTCHAR kHexDigits[]=VTT("0123456789abcdef");
    for(size_t x=0; x<inbytes; x++){
        *(out++) = kHexDigits[data[x]>>4];
        *(out++) = kHexDigits[data[x]&0x0F];
    }
    *out=0;
}

inline unsigned char VTCryptXDigit2Number( VTCHAR ch )
{
    ch = VTTOLOWER( ch );
    if( ch>=VTT('0') && ch<='9' )
        return (unsigned char)(ch - VTT('0'));
    VTASSERT( ch>='a' && ch<='f' );
    return (unsigned char)((ch-VTT('a')) + 10);
}

bool Crypto::Base16Decode( LPCVTCHAR src, size_t inchars, unsigned char *out )
{
    size_t outsize = inchars/2;
    for( size_t i=0; i<outsize; i++ ){
        VTCHAR ch1, ch2;
        while( VTISSPACE(*src) && *src )
            src++;
        ch1 = *(src++);
        if( !ch1 )
            return false;
        while( VTISSPACE(*src) && *src )
            src++;
        ch2 = *(src++);
        if( !ch2 )
            return false;
        if( !VTISXDIGIT(ch1) || !VTISXDIGIT(ch2) )
            return false;
        *(out++) = (unsigned char)((VTCryptXDigit2Number(ch1)<<4) + VTCryptXDigit2Number(ch2));
    }
    return true;
}

size_t Crypto::SHA1Digest(const char *data, size_t dataSize, BYTE *digestOut)
{
    #if defined(USE_CRYPTOPP)
        VTASSERT( kDigestBufferSize>=CryptoPP::SHA1::DIGESTSIZE );
        CryptoPP::SHA1 sha;
        memset(digestOut, 0, kDigestBufferSize);
        sha.Restart();
        sha.Update((const byte*)data, dataSize);
        sha.Final((byte*)digestOut);
        return size_t(CryptoPP::SHA1::DIGESTSIZE);
    #else
        #error "Only CRYPTOPP511 is supported!"
    #endif
}

void Crypto::SHA1(const char *data, size_t dataSize, VTCHAR *out)
{
    BYTE digest[ kDigestBufferSize ];
    size_t digestSize = SHA1Digest( data, dataSize, digest );
    Crypto::Base16(digest, digestSize, out);
}

size_t Crypto::SHA512Digest(const char *data, size_t dataSize, BYTE *digestOut)
{
    #if defined(USE_CRYPTOPP)
        VTASSERT( kDigestBufferSize>=CryptoPP::SHA512::DIGESTSIZE );
        CryptoPP::SHA512 sha;
        memset(digestOut, 0, kDigestBufferSize);
        sha.Restart();
        sha.Update((const byte*)data, dataSize);
        sha.Final((byte*)digestOut);
        return size_t(CryptoPP::SHA512::DIGESTSIZE);
    #else
        #error "Only CRYPTOPP511 is supported!"
    #endif
}


void Crypto::SHA512(const char *data, size_t dataSize, VTCHAR *out)
{
    BYTE digest[ kDigestBufferSize ];
    size_t digestSize = SHA512Digest( data, dataSize, digest );
    Crypto::Base16(digest, digestSize, out);
}


#if defined(VTCRYPT_WEAK_ALGORITHMS_ENABLED)

    size_t Crypto::MD5Digest(const char *data, size_t dataSize, BYTE *digestOut)
    {
        #if defined(USE_CRYPTOPP)
            VTASSERT( kDigestBufferSize>=VTCRYPTO_WEAK::MD5::DIGESTSIZE );
            VTCRYPTO_WEAK::MD5 hash;
            memset(digestOut, 0, kDigestBufferSize);
            hash.Restart();
            hash.Update((const byte*)data, dataSize);
            hash.Final((byte*)digestOut);
            return size_t(VTCRYPTO_WEAK::MD5::DIGESTSIZE);
        #else
            #error "Only CRYPTOPP511 is supported!"
        #endif
    }

    void Crypto::MD5(const char *data, size_t dataSize, VTCHAR *out)
    {
        BYTE digest[ kDigestBufferSize ];
        size_t digestSize = MD5Digest( data, dataSize, digest );
        Crypto::Base16(digest, digestSize, out);
    }

#endif



void Crypto::Hash(const char *data, size_t dataSize, VTCHAR *out, int trim)
{
    Crypto::VTCRYPT_HASH_FN(data, dataSize, out);
    if( trim>=0 )
        out[trim]=0;
}




BYTE *Crypto::AES::Encrypt(const BYTE *src, size_t insz, BYTE *out, size_t *out_size,
                            const BYTE *key, int keySize )
{
    #if defined(USE_CRYPTOPP)
        // VTASSERT2T( CryptoPP::AES::DEFAULT_KEYLENGTH==kStdKeySizeBytes,
        //     CString(VTT("CryptoPP::AES::DEFAULT_KEYLENGTH=="))<<long(CryptoPP::AES::DEFAULT_KEYLENGTH));
        VTASSERT( CryptoPP::AES::BLOCKSIZE==kBlockSizeBytes );
        if( keySize < CryptoPP::AES::MIN_KEYLENGTH || keySize > CryptoPP::AES::MAX_KEYLENGTH ){
            #if defined(VT_CONSOLEDEBUG)
            printf(__FUNCTION__ ": Wrong key length for AES: %d\n", keySize );
            #endif
            *out_size = 0;
            return NULL;
        }
        // Allocate memory for output
        size_t outputSize = EncryptedSize( insz );
        if( out==NULL ){
            out = new BYTE[ outputSize ];
            if( !out ){
                #if defined(VT_CONSOLEDEBUG)
                    printf(__FUNCTION__ ": Failed to allocate %u bytes.\n", unsigned(outputSize) );
                #endif
                *out_size = 0;
                return NULL;
            }
        }else{
            if( *out_size < outputSize ){
                #if defined(VT_CONSOLEDEBUG)
                    printf(__FUNCTION__ ": Not enough space for output; "
                        "input=%u bytes, output=%u bytes.\n",
                        unsigned(outputSize), unsigned(*out_size) );
                #endif
                return NULL;
            }
        }
        *out_size = outputSize;
        memset( out, 0, *out_size );
        BYTE iv[CryptoPP::AES::BLOCKSIZE];
        memset( iv, 1, sizeof(iv) );
        try{
            // Create and initialize cipher
            CryptoPP::AES::Encryption aesEncryption(key, keySize );
            CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );
            CryptoPP::ArraySource( src, insz, true,
                new CryptoPP::StreamTransformationFilter( cbcEncryption,
                    new CryptoPP::ArraySink( out, *out_size ) ) );
        }catch(...){
            VTDEBUGMSGT("Exception in AES encryptor!");
            #if defined(VT_CONSOLEDEBUG)
                printf(__FUNCTION__ "Exception in AES encryptor!\n" );
            #endif
            delete out;
            *out_size = 0;
            return NULL;
        }
        return out;
    #else
        #error "Only CRYPTOPP511 is supported!"
    #endif
}

BYTE *Crypto::AES::Decrypt(const BYTE *src, size_t insz, BYTE *out, size_t *out_size,
                            const BYTE *key, int keySize )
{
    if( (insz % kBlockSizeBytes)!=0 ){
        #if defined(VT_CONSOLEDEBUG)
            printf(__FUNCTION__ ": Input size is not aligned to block size: %d\n", int(insz) );
        #endif
        *out_size = 0;
        return NULL;
    }

    // size_t encSz = EncryptedSize( insz );
    #if defined(USE_CRYPTOPP)
        // VTASSERT( CryptoPP::AES::DEFAULT_KEYLENGTH==kStdKeySizeBytes );
        VTASSERT( CryptoPP::AES::BLOCKSIZE==kBlockSizeBytes );
        if( keySize < CryptoPP::AES::MIN_KEYLENGTH || keySize > CryptoPP::AES::MAX_KEYLENGTH ){
            #if defined(VT_CONSOLEDEBUG)
                printf(__FUNCTION__ ": Bad key length for AES: %d\n", keySize );
            #endif
            *out_size = 0;
            return NULL;
        }
        // Allocate memory for output
        if( out==NULL ){
            out = new BYTE[ insz ];
            if( !out ){
                *out_size = 0;
                VTDEBUGMSG( CString( __FUNCTION__ )<< VTT(": Failed to allocate ")<<long(insz)<<VTT(" bytes.") );
                throw CMemoryException();
            }
        }else{
            if( *out_size<insz ){
                VTDEBUGMSG( CString( __FUNCTION__ )<<
                    VTT(": Not enough space for output; input=")<<long(insz)<<
                    VTT(" bytes, output=")<<long(*out_size)<<
                    VTT(" bytes.") );
                throw CMemoryException();
            }
        }
        *out_size = insz;
        memset( out, 0, insz );
        // Create and initialize cipher
        BYTE iv[CryptoPP::AES::BLOCKSIZE];
        memset( iv, 1, sizeof(iv) );
        try{
            CryptoPP::AES::Decryption aesDecryption(key, keySize );
            CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );
            CryptoPP::ArraySource( src, insz, true,
                new CryptoPP::StreamTransformationFilter( cbcDecryption,
                    new CryptoPP::ArraySink( out, insz ) ) );
        }catch(...){
            VTDEBUGMSGT("Exception in AES decryptor!");
            #if defined(VT_CONSOLEDEBUG)
                printf(__FUNCTION__ "Exception in AES decryptor!\n" );
            #endif
            delete out;
            *out_size = 0;
            return NULL;
        }
        return out;
    #else
        #error "Only CRYPTOPP511 is supported!"
    #endif
}



VT::CString Crypto::Base16(const unsigned char *data, size_t inbytes)
{
    VTCHAR *buf = new VTCHAR[inbytes*2+8];
    if( buf==NULL )
        throw CMemoryException();
    Crypto::Base16(data, inbytes, buf);
    VT::CString ret(buf);
    delete buf;
    return ret;
}

VT::CString Crypto::SHA512(const char *data, size_t dataSize)
{
    VTCHAR buf[Crypto::kHashStringSize+1];
    Crypto::SHA512(data, dataSize, buf);
    return VT::CString(buf);
}

VT::CString Crypto::SHA512(const VT::CString& str)
{
    return Crypto::SHA512( (const char*)(str.Str()), str.Length()*sizeof(VTCHAR) );
}

VT::CString Crypto::SHA1(const char *data, size_t dataSize)
{
    VTCHAR buf[Crypto::kHashStringSize+1];
    Crypto::SHA1(data, dataSize, buf);
    return VT::CString(buf);
}

VT::CString Crypto::SHA1(const VT::CString& str)
{
    return Crypto::SHA1( (const char*)(str.Str()), str.Length()*sizeof(VTCHAR) );
}

VT::CString Crypto::Hash(const char *data, size_t dataSize)
{
    return Crypto::VTCRYPT_HASH_FN(data, dataSize);
}

VT::CString Crypto::Hash(const VT::CString& str)
{
    return Crypto::VTCRYPT_HASH_FN(str);
}



#if defined(USE_LSSTRING)

    LString Crypto::Base16L(const unsigned char *data, size_t inbytes)
    {
        VTCHAR *buf = new VTCHAR[inbytes*2+8];
        if( buf==NULL )
            throw CMemoryException();
        Crypto::Base16(data, inbytes, buf);
        LString ret(buf);
        delete buf;
        return ret;
    }

    LString Crypto::SHA512L(const char *data, size_t dataSize)
    {
        VTCHAR buf[Crypto::kHashStringSize+1];
        Crypto::SHA512(data, dataSize, buf);
        return LString(buf);
    }

    LString Crypto::SHA512L(const LString& str)
    {
        if( str.IsEmpty() || !str.GetCurrentBuffer())
            return Crypto::SHA512("", 0);
        else
            return Crypto::SHA512((const char*)str.GetCurrentBuffer(), size_t(str.GetLength()*sizeof(TCHAR)));
    }

    LString Crypto::SHA1L(const char *data, size_t dataSize)
    {
        VTCHAR buf[Crypto::kHashStringSize+1];
        Crypto::SHA1(data, dataSize, buf);
        return LString(buf);
    }

    LString Crypto::SHA1L(const LString& str)
    {
        if( str.IsEmpty() || !str.GetCurrentBuffer())
            return Crypto::SHA1L("", 0);
        else
            return Crypto::SHA1L((const char*)str.GetCurrentBuffer(), size_t(str.GetLength()*sizeof(TCHAR)));
    }

    LString Crypto::HashL(const char *data, size_t dataSize)
    {
        return Crypto::VTCRYPT_HASH_FN_L(data, dataSize);
    }

    LString Crypto::HashL(const LString& str)
    {
        return Crypto::VTCRYPT_HASH_FN_L(str);
    }

#endif // if defined USE_LSSTRING






