//-----------------------------------------------------------------------------
//
// MP3 decoder wrapper, currently uses libmpg123.
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2007 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#if defined(_MSC_VER)
    #define _CRT_SECURE_NO_DEPRECATE
    #define ssize_t size_t
#endif

#include <windows.h>
#include "../../Lib/include/mpg123.h"
#include "vt_portable.hpp"
#include "vt_fileutils.hpp"
#include "vt_threading.hpp"
#include "vt_localization.hpp"
#include "vt_mp3decode.hpp"
#include "vt_riff.hpp"

using namespace VT;
using namespace VT::MP3FileDecoder;

volatile static bool bDecoderAborted = false;
static LPCVTCHAR pszLastErrorMessage = NULL;

static void cleanup(mpg123_handle *mp3handle)
{
    // It's really too late for error checks here ;-)
    try{
        if( mp3handle!=NULL ){
            mpg123_close(mp3handle);
            mpg123_delete(mp3handle);
        }
        mpg123_exit();
    }catch(...){};
}

static LPCVTCHAR formatmpg123msg(const char *msg)
{
    const int kBufSize = 256;
    static VTCHAR ret[kBufSize];
    #if defined(_UNICODE)
        VTCHAR buffer[kBufSize];
        MultiByteToWideChar(CP_ACP, 0, msg, -1, buffer, kBufSize-1);
        wcsncpy(ret, LCT("libmpg123 message"), kBufSize);
        wcsncat(ret, VTT("\r\n"), kBufSize);
        wcsncat(ret, buffer, kBufSize);
    #else
        strcpy(ret, LCT("libmpg123 message"));
        strcat(ret, "\r\n");
        strncat(ret, msg, kBufSize-32);
    #endif
    return ret;
}

LPCVTCHAR MP3FileDecoder::GetLastErrorMessage()
{
    return pszLastErrorMessage;
}

MP3FileDecoder::TDecoderResult MP3FileDecoder::Decode(char *infile, char *outfile, MP3FileDecoder::PTCallback callback)
{
    VT_LOCK_SECTION;

    if( infile==NULL ){
        pszLastErrorMessage = LCT("Input file name was not specified.");
        return kInfileError;
    }
    if( !infile[0] ){
        pszLastErrorMessage = LCT("Input file name was not specified.");
        return kInfileError;
    }
    if( outfile==NULL ){
        pszLastErrorMessage = LCT("Output file name was not specified.");
        return kInfileError;
    }
    if( !outfile[0] ){
        pszLastErrorMessage = LCT("Output file name was not specified.");
        return kInfileError;
    }
    try{
        bool bFormatSet = false;
        bDecoderAborted = false;
        MP3FileDecoder::TDecoderResult ret = kOK;
        pszLastErrorMessage = NULL;

        struct TRIFFWavHeader RIFF;
        RIFF.Init();
        FILE *wavhandle = NULL;
        mpg123_handle *mp3handle = NULL;
        unsigned char* buffer = NULL;
        size_t buffer_size = 0;
        size_t done = 0;
        int channels = 0, encoding = 0;
        long rate = 0;
        int err = MPG123_OK;
        DWORD bytes_out = 0;

        // Initialize the thing

        err = mpg123_init();
        if( err != MPG123_OK ){
            pszLastErrorMessage = LCT("Failed to initialize mpg123 library.");
            return kEngineError;
        }
        if( (mp3handle = mpg123_new(NULL, &err)) == NULL ){
            pszLastErrorMessage = formatmpg123msg(mpg123_plain_strerror(err));
            cleanup(mp3handle);
            return kEngineError;
        }
        if( mpg123_open(mp3handle, infile) != MPG123_OK ){
            pszLastErrorMessage = formatmpg123msg(mpg123_strerror(mp3handle));
            cleanup(mp3handle);
            return kInfileError;
        }


        // Try to setup file format before starting decoding process,
        // as it was done in the sample code

        if( mpg123_getformat(mp3handle, &rate, &channels, &encoding) != MPG123_OK ){
            pszLastErrorMessage = formatmpg123msg(mpg123_strerror(mp3handle));
            cleanup(mp3handle);
            return kInfileError;
        }
        //printf("Rate = %d, Channels = %d, Encoding = %x\n", rate, channels, encoding);
        if( rate==0 || channels==0 || encoding==0 ){
            rate=44100;
            channels=2;
            encoding=MPG123_ENC_SIGNED_16;
            //printf("Corrected format!\n");
        }else{
            if(encoding != MPG123_ENC_SIGNED_16){
                pszLastErrorMessage = LCT("Bad encoding.");
                cleanup(mp3handle);
                return kEngineError;
            }
            mpg123_format_none(mp3handle);
            mpg123_format(mp3handle, rate, channels, encoding);
            bFormatSet = true;
        }

        RIFF.SamplesPerSec = rate;
        RIFF.Channels = channels;
        RIFF.BitsPerSample = 16;
        RIFF.AvgBytesPerSec = rate * channels * 2;
        RIFF.BlockAlign = channels * 2;

        wavhandle = fopen(outfile, "wb");
        if( !wavhandle ){
            pszLastErrorMessage = LCT("Failed to open output file for writing.");
            cleanup(mp3handle);
            return kOutfileError;
        }
        fwrite( &RIFF, sizeof(RIFF), 1, wavhandle);
        buffer_size = mpg123_outblock( mp3handle );
        buffer = new unsigned char[ buffer_size+1024 ]; // Added some space for bugs

        if( callback!=NULL )
            callback(0, 0, 0, 0, 0);

        long callbackCounter = 0;
        do{
            if( bDecoderAborted ){
                pszLastErrorMessage = LCT("Decoding aborted.");
                ret = kAborted;
                break;
            }

            try{
                // !!!!! TP: this may throw an exception ("parameters defined")
                err = mpg123_read( mp3handle, buffer, buffer_size, &done );
            }catch(...){
                err = MPG123_ERR_READER;
                done = 0;
            }

            if( done>0 ){
                fwrite(buffer,  done, 1, wavhandle);
                bytes_out += (DWORD)done;
            }

            if( callback!=NULL ){
                if( (++callbackCounter) == kCallbackPeriod ){
                    callbackCounter = 0;
                    double current_seconds=0, seconds_left=0;
                    off_t current_frame=0, frames_left=0;
                    mpg123_position(mp3handle, 0, 0, &current_frame,
                        &frames_left, &current_seconds, &seconds_left);
                    callback(bytes_out / (2*channels), long(current_frame),
                        long(frames_left), current_seconds, seconds_left);
                }
            }

            // Update output file format, if neccessary. This should not happen twice!
            if( err== MPG123_NEW_FORMAT ){
                if( mpg123_getformat(mp3handle, &rate, &channels, &encoding) != MPG123_OK ){
                    pszLastErrorMessage = formatmpg123msg(mpg123_strerror(mp3handle));
                    break;
                    return kEngineError;
                }
                if(encoding != MPG123_ENC_SIGNED_16){
                    pszLastErrorMessage = LCT("Bad encoding.");
                    ret = kEngineError;
                    break;
                }
                if( bFormatSet && (
                        rate*channels*2!=RIFF.AvgBytesPerSec ||
                        RIFF.Channels!=channels )){
                    pszLastErrorMessage = LCT("Stream format changed during the decoding process.");
                    ret = kEngineError;
                    break;
                }
                //printf("New format! %ld/%d/%x\n", rate, channels, encoding);
                mpg123_format_none(mp3handle);
                mpg123_format(mp3handle, rate, channels, encoding);
                 RIFF.SamplesPerSec = rate;
                RIFF.Channels = channels;
                RIFF.BitsPerSample = 16;
                RIFF.AvgBytesPerSec = rate * channels * 2;
                RIFF.BlockAlign = channels * 2;
                bFormatSet = true;
            }

        }while(err==MPG123_OK || err==MPG123_NEED_MORE || err==MPG123_NEW_FORMAT);

        if( callback!=NULL ){
            double current_seconds=0, seconds_left=0;
            off_t current_frame=0, frames_left=0;
            mpg123_position(mp3handle, 0, 0, &current_frame,
                &frames_left, &current_seconds, &seconds_left);
            callback(bytes_out / (2*channels), long(current_frame),
                long(frames_left), current_seconds, seconds_left);
        }

        if(err != MPG123_DONE && ret != kAborted){
            pszLastErrorMessage = (err == MPG123_ERR ?
                formatmpg123msg(mpg123_strerror(mp3handle)) :
                formatmpg123msg(mpg123_plain_strerror(err)));
            ret = kPrematureEOF;
        }

        // Finalize WAV file
        RIFF.datalen = bytes_out;
        RIFF.WAVElen = bytes_out+sizeof(RIFF)-8;
        fseek(wavhandle, 0, SEEK_SET);
        fwrite(&RIFF, sizeof(RIFF), 1, wavhandle);
        fclose(wavhandle);

        delete buffer;
        cleanup(mp3handle);
        fclose(wavhandle);

        if( err==MPG123_DONE && !bFormatSet ){
            pszLastErrorMessage = LCT("Output format was never set by the decoder.");
            ret = kUnknownFormat;
        }

        if( ret!=kOK )
            _unlink(outfile);

        return ret;
    }catch(...){};
    pszLastErrorMessage = LCT("Unhandled exception in decoding process.");
    return kUnknownError;
}

MP3FileDecoder::TDecoderResult MP3FileDecoder::GetLength(char *file, double *sec, int *chan)
{
    VT_LOCK_SECTION;

    if( file==NULL ){
        pszLastErrorMessage = LCT("File name was not specified.");
        return kInfileError;
    }
    if( !file[0] ){
        pszLastErrorMessage = LCT("File name was not specified.");
        return kInfileError;
    }
    try{
        MP3FileDecoder::TDecoderResult ret = kOK;
        pszLastErrorMessage = NULL;
        mpg123_handle *mp3handle = NULL;
        int channels = 0, encoding = 0;
        long rate = 0;
        int err = MPG123_OK;
        // Initialize the thing
        err = mpg123_init();
        if( err != MPG123_OK ){
            pszLastErrorMessage = LCT("Failed to initialize mpg123 library.");
            return kEngineError;
        }
        if( (mp3handle = mpg123_new(NULL, &err)) == NULL ){
            pszLastErrorMessage = formatmpg123msg(mpg123_plain_strerror(err));
            cleanup(mp3handle);
            return kEngineError;
        }
        if( mpg123_open(mp3handle, file) != MPG123_OK ){
            pszLastErrorMessage = formatmpg123msg(mpg123_strerror(mp3handle));
            cleanup(mp3handle);
            return kInfileError;
        }
        if( mpg123_getformat(mp3handle, &rate, &channels, &encoding) != MPG123_OK ){
            pszLastErrorMessage = formatmpg123msg(mpg123_strerror(mp3handle));
            cleanup(mp3handle);
            return kInfileError;
        }
        off_t current_frame, frames_left;
        double current_seconds, seconds_left;
        if (mpg123_position(mp3handle, 0, 0, &current_frame, &frames_left, &current_seconds, &seconds_left) != MPG123_OK){
            pszLastErrorMessage = formatmpg123msg(mpg123_strerror(mp3handle));
            cleanup(mp3handle);
            return kInfileError;
        }
        if (sec != NULL)
            *sec = seconds_left;
        if (chan != NULL)
            *chan = channels;
        cleanup(mp3handle);
        return ret;
    }catch(...){};
    pszLastErrorMessage = LCT("Unhandled exception in getting parameters process.");
    return kUnknownError;
}

#if defined(_UNICODE) && defined(_WIN32)
MP3FileDecoder::TDecoderResult MP3FileDecoder::Decode(LPCTSTR infile, LPCTSTR outfile, MP3FileDecoder::PTCallback callback)
{
    if( infile==NULL ){
        pszLastErrorMessage = LCT("Input file name was not specified.");
        return kInfileError;
    }
    if( !infile[0] ){
        pszLastErrorMessage = LCT("Input file name was not specified.");
        return kInfileError;
    }
    if( outfile==NULL ){
        pszLastErrorMessage = LCT("Output file name was not specified.");
        return kOutfileError;
    }
    if( !outfile[0] ){
        pszLastErrorMessage = LCT("Output file name was not specified.");
        return kOutfileError;
    }
    if( !VT::FileExists(infile) ){
        pszLastErrorMessage = LCT("Input file does not exist.");
        return kInfileError;
    }

    // Create output file with full, VT_UNICODE name.
    // Note that it should be done BEFORE using GetShortPathName() on it.
    FILE *fp = _wfopen(outfile, VTT("wb"));
    if( fp==NULL ){
        pszLastErrorMessage = LCT("Failed to open output file for writing.");
        return kOutfileError;
    }
    fclose(fp);
    char infileshortch[MAX_PATH], outfileshortch[MAX_PATH];
    if( !VT::GetShortName(infile, infileshortch, true) ){
        pszLastErrorMessage = LCT("Failed to convert input file name to ANSI charset.");
        return kSystemError;
    }
    if( !VT::GetShortName(outfile, outfileshortch, true) ){
        pszLastErrorMessage = LCT("Failed to convert output file name to ANSI charset.");
        return kSystemError;
    }

    return MP3FileDecoder::Decode(infileshortch, outfileshortch, callback);
}

MP3FileDecoder::TDecoderResult MP3FileDecoder::GetLength(LPCTSTR file, double *sec, int *chan)
{
    char fileshortch[MAX_PATH];
    if( !VT::GetShortName(file, fileshortch, true) ){
        pszLastErrorMessage = LCT("Failed to convert input file name to ANSI charset.");
        return kSystemError;
    }

    return MP3FileDecoder::GetLength(fileshortch, sec, chan);
}

#endif


LPCVTCHAR MP3FileDecoder::GetErrorMessageHeader(MP3FileDecoder::TDecoderResult code)
{
    switch(code){
    case kOK:               return LCT("No Error");
    case kUnknownError:     return LCT("Unknown Error");
    case kAborted:          return LCT("Aborted");
    case kEngineError:      return LCT("MP3 Decoder Error");
    case kInfileError:      return LCT("Input File Error");
    case kOutfileError:     return LCT("Output File Error");
    case kPrematureEOF:     return LCT("Broken Input Stream");
    case kUnknownFormat:    return LCT("Unknown Stream Format");
    case kSystemError:      return LCT("System Error");
    };
    return LCT("Unknown Code");
}

void MP3FileDecoder::AbortDecoding()
{
    bDecoderAborted = true;
}

bool MP3FileDecoder::DecodingAborted()
{
    return bDecoderAborted;
}

