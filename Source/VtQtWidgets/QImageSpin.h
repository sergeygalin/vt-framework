//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// Image Spin - a spin-edit like control, but instead of displaying
// numbers it shows pixmaps. All images to display are combined
// in one pixmap as a vertically oriented "film".
// Almost everything is inherited from QSpinBox, which is cool.
//
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QIMAGESPIN_H
#define QIMAGESPIN_H

#include <QtGui/QSpinBox>
#include <QtGui/QPixmap>
#include "QPixmapReferencer.h"


class QImageSpin: public QSpinBox, protected QPixmapReferencer
{
    Q_OBJECT

public:
    explicit QImageSpin( QWidget *parent = 0 );
    explicit QImageSpin( QWidget *parent, const QString& pictureSrc, int nImages );
    explicit QImageSpin( QWidget *parent, const QPixmap& pixmap, int nImages );

    void init( const QString& pictureSrc, int nImages );
    void init( const QPixmap& pixmap, int nImages );

    // Show/hide text edit. By default it's hidden, so only the pixmap
    // is displayed. It it's make visible, standard QSpinBox's text editor
    // pops up over the pixmap.
    Q_PROPERTY( bool textEditVisible READ isTextEditVisible WRITE setTextEditVisible );
    bool isTextEditVisible() const;
    void setTextEditVisible( bool vis );

protected:
    bool m_bShowTextEdit;
    virtual void paintEvent(QPaintEvent* event);

private:
    int m_iImages;
    void initImageSpin();
};

#endif

