//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPainter>
#include <QtCore/QRectF>
#include <QtCore/QCoreApplication>
#include "vt_audiomath.hpp"
#include "vt_midi.hpp"
#include "QPianoPCKeyHelpBar.h"

QPianoPCKeyHelpBar::QPianoPCKeyHelpBar( QWidget* parent ):
    QWidget( parent ),
    m_FontColor( 255, 255, 255, 255 ),
    m_StartingKey( 36 ),
    m_NKeys( 61 ),
    m_Transposing( 0 ),
    m_EnableTranslation( true )
{
    init( 36, 61 );
    createLabels();
}

QPianoPCKeyHelpBar::~QPianoPCKeyHelpBar()
{
}


void QPianoPCKeyHelpBar::init( BYTE start, BYTE nkeys )
{
    m_StartingKey = start;
    m_NKeys = nkeys;
    update();
}

void QPianoPCKeyHelpBar::init()
{
    init( m_StartingKey, m_NKeys );
}

void QPianoPCKeyHelpBar::createLabels()
{
    if( m_EnableTranslation ){
        for( BYTE note=0; note<128; note++ ){
            BYTE trans = VT::Notes::Transpose( note, -getTransposing() );
            m_Labels[note] = translateKeyName( VT::MIDI::FindPCkeyboardCharForNote( trans ) );
        }
    }else{
        char buf[2] = {0,0};
        for( BYTE note=0; note<128; note++ ){
            BYTE trans = VT::Notes::Transpose( note, -getTransposing() );
            buf[0] = VT::MIDI::FindPCkeyboardCharForNote( trans );
            m_Labels[note] = buf;
        }
    }
    update();
}

void QPianoPCKeyHelpBar::paintEvent( QPaintEvent* )
{
    qreal step = qreal(width()) / qreal(m_NKeys);
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );
    qreal sz = VT::Math::Max<qreal>( VT::Math::Min<qreal>( step-4.0, qreal(height())-4.0 ), 3.0 );
    QFont font( "Helvetica", sz, false, false );
    painter.setFont( font );
    painter.setPen( m_FontColor );

    for( int k = int(m_StartingKey), end = int(m_NKeys)+int(m_StartingKey); k<end; k++ ){
        if( m_Labels[k].length() ){
            qreal l = step * qreal(k-m_StartingKey);
            QRectF bounds( l, 0, step, height() );
            painter.drawText( bounds, Qt::AlignCenter, m_Labels[k] );
        }
    }
}

QString QPianoPCKeyHelpBar::translateKeyName( char k )
{
    switch( k ){
        case 'Q':    return QCoreApplication::translate( "Keyboard map",  "Q",    "Letter on the same key on localized keyboard" );
        case 'W':    return QCoreApplication::translate( "Keyboard map",  "W",    "Letter on the same key on localized keyboard" );
        case 'E':    return QCoreApplication::translate( "Keyboard map",  "E",    "Letter on the same key on localized keyboard" );
        case 'R':    return QCoreApplication::translate( "Keyboard map",  "R",    "Letter on the same key on localized keyboard" );
        case 'T':    return QCoreApplication::translate( "Keyboard map",  "T",    "Letter on the same key on localized keyboard" );
        case 'Y':    return QCoreApplication::translate( "Keyboard map",  "Y",    "Letter on the same key on localized keyboard" );
        case 'U':    return QCoreApplication::translate( "Keyboard map",  "U",    "Letter on the same key on localized keyboard" );
        case 'I':    return QCoreApplication::translate( "Keyboard map",  "I",    "Letter on the same key on localized keyboard" );
        case 'O':    return QCoreApplication::translate( "Keyboard map",  "O",    "Letter on the same key on localized keyboard" );
        case 'P':    return QCoreApplication::translate( "Keyboard map",  "P",    "Letter on the same key on localized keyboard" );
        case '[':    return QCoreApplication::translate( "Keyboard map",  "[",    "Letter on the same key on localized keyboard" );
        case ']':    return QCoreApplication::translate( "Keyboard map",  "]",    "Letter on the same key on localized keyboard" );
        case 'A':    return QCoreApplication::translate( "Keyboard map",  "A",    "Letter on the same key on localized keyboard" );
        case 'S':    return QCoreApplication::translate( "Keyboard map",  "S",    "Letter on the same key on localized keyboard" );
        case 'D':    return QCoreApplication::translate( "Keyboard map",  "D",    "Letter on the same key on localized keyboard" );
        case 'F':    return QCoreApplication::translate( "Keyboard map",  "F",    "Letter on the same key on localized keyboard" );
        case 'G':    return QCoreApplication::translate( "Keyboard map",  "G",    "Letter on the same key on localized keyboard" );
        case 'H':    return QCoreApplication::translate( "Keyboard map",  "H",    "Letter on the same key on localized keyboard" );
        case 'J':    return QCoreApplication::translate( "Keyboard map",  "J",    "Letter on the same key on localized keyboard" );
        case 'K':    return QCoreApplication::translate( "Keyboard map",  "K",    "Letter on the same key on localized keyboard" );
        case 'L':    return QCoreApplication::translate( "Keyboard map",  "L",    "Letter on the same key on localized keyboard" );
        case ':':    return QCoreApplication::translate( "Keyboard map",  ";",    "Letter on the same key on localized keyboard" );
        case '\'':    return QCoreApplication::translate( "Keyboard map",  "'",    "Letter on the same key on localized keyboard" );
        case 'Z':    return QCoreApplication::translate( "Keyboard map",  "Z",    "Letter on the same key on localized keyboard" );
        case 'X':    return QCoreApplication::translate( "Keyboard map",  "X",    "Letter on the same key on localized keyboard" );
        case 'C':    return QCoreApplication::translate( "Keyboard map",  "C",    "Letter on the same key on localized keyboard" );
        case 'V':    return QCoreApplication::translate( "Keyboard map",  "V",    "Letter on the same key on localized keyboard" );
        case 'B':    return QCoreApplication::translate( "Keyboard map",  "B",    "Letter on the same key on localized keyboard" );
        case 'N':    return QCoreApplication::translate( "Keyboard map",  "N",    "Letter on the same key on localized keyboard" );
        case 'M':    return QCoreApplication::translate( "Keyboard map",  "M",    "Letter on the same key on localized keyboard" );
        case ',':    return QCoreApplication::translate( "Keyboard map",  ",",    "Letter on the same key on localized keyboard" );
        case '.':    return QCoreApplication::translate( "Keyboard map",  ".",    "Letter on the same key on localized keyboard" );
        case '/':    return QCoreApplication::translate( "Keyboard map",  "/",    "Letter on the same key on localized keyboard" );
        case '\\':    return QCoreApplication::translate( "Keyboard map",  "\\",    "Letter on the same key on localized keyboard" );
        case '`':    return QCoreApplication::translate( "Keyboard map",  "`",    "Letter on the same key on localized keyboard" );
    }
    char buf[2] = { 0, 0 };
    buf[0] = k;
    return buf;
}


