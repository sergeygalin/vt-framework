//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QMouseEvent>
#include <QtGui/QScrollBar>
#include "QCurveEditor.h"
#include "vt_audiomath.hpp"

QPolylineNode::QPolylineNode():
    m_bSpecialColor( false ),
    m_Style( kDefaultStyle ),
    m_Color( 255, 0, 0 ),
    m_X( 0 ),
    m_Y( 0 ),
    m_fXMin( 0 ), m_fXMax( 1.0 ), m_fYMin( 0 ), m_fYMax( 1.0 ), // updated by setDefaultMinMax();
    m_bEditableX( true ),
    m_bEditableY( true )
{
    setDefaultMinMax();
}

QPolylineNode::QPolylineNode( const QPolylineNode& src ):
    QObject()
{
    copyFrom( src );
}

void QPolylineNode::copyFrom( const QPolylineNode& src )
{
    m_X = src.m_X;
    m_Y = src.m_Y;
    m_bEditableX = src.m_bEditableX;
    m_bEditableY = src.m_bEditableY;
    m_fXMin = src.m_fXMin;
    m_fXMax = src.m_fXMax;
    m_fYMin = src.m_fYMin;
    m_fYMax = src.m_fYMax;
    m_bSpecialColor = src.m_bSpecialColor;
    m_Color = src.m_Color;
    m_Style = src.m_Style;
}

QPolylineNode::QPolylineNode( double x, double y, bool ex, bool ey, bool specColor, QColor clr, QPolylineNode::TNodeStyle style ):
    m_bSpecialColor( specColor ),
    m_Style( style ),
    m_Color( clr ),
    m_X( x ),
    m_Y( y ),
    m_fXMin( 0 ), m_fXMax( 1.0 ), m_fYMin( 0 ), m_fYMax( 1.0 ), // updated by setDefaultMinMax();
    m_bEditableX( ex ),
    m_bEditableY( ey )
{
    setDefaultMinMax();
}

QPolylineNode::QPolylineNode( qreal x, qreal minx, qreal maxx, qreal y, qreal miny, qreal maxy,
                             bool ex, bool ey, bool specColor, QColor clr, QPolylineNode::TNodeStyle style ):
    m_bSpecialColor( specColor ),
    m_Style( style ),
    m_Color( clr ),
    m_X( x ),
    m_Y( y ),
    m_fXMin( minx ),
    m_fXMax( maxx ),
    m_fYMin( miny ),
    m_fYMax( maxy ),
    m_bEditableX( ex ),
    m_bEditableY( ey )
{
    VTASSERT( x>=minx && x<=maxx );
    VTASSERT( y>=miny && y<=maxy );
}

QPolylineNode::QPolylineNode( TStdCurveTypes stdType, qreal x, qreal y, bool ex, bool ey,
                             bool specColor, QColor clr, QPolylineNode::TNodeStyle style ):
    m_bSpecialColor( specColor ),
    m_Style( style ),
    m_Color( clr ),
    m_X( x ),
    m_Y( y ),
    m_fXMin( 0 ), m_fXMax( 1.0 ), m_fYMin( 0 ), m_fYMax( 1.0 ), // updated later
    m_bEditableX( ex ),
    m_bEditableY( ey )
{
    switch( stdType )
    {
    case kUnknownCurveType:
        setDefaultMinMax();
        return;
    case kAmplitudeEG:
        setXRange( 0.0, 10000.0 );
        setYRange( 0.0, 100.0 );
        VTASSERT2T( !isBipolar(), "kAmplitudeEG point turned ought to be bipolar" );
        return;
    case kBipolarEG:
        setXRange( 0.0, 10000.0 );
        setYRange( -100.0, 100.0 );
        VTASSERT2T( isBipolar(), "kBipolarEG point turned ought to be non-biploar" );
        return;
    default:
        throw VT::CException(VTT("Unknown curve type in node constructor."));
    }
}

Qt::CursorShape QPolylineNode::getCursorShape() const
{
    if( m_bEditableX ){
        if( m_bEditableY )
            return Qt::SizeAllCursor;
        else
            return Qt::SizeHorCursor;
    }else{
        if( m_bEditableY )
            return Qt::SizeVerCursor;
        else
            return Qt::ForbiddenCursor;
    }

}

void QPolylineNode::setDefaultMinMax()
{
    m_fXMin = 0.0;
    m_fXMax = 100.0;
    m_fYMin = 0.0;
    m_fYMax = 1.0;
}

void QPolylineNode::setXRange( double minX, double maxX )
{
    m_fXMin = minX;
    m_fXMax = maxX;
    m_X = VT::Math::Clip( m_X, m_fXMin, m_fXMax );
}

void QPolylineNode::setYRange( double minY, double maxY )
{
    m_fYMin = minY;
    m_fYMax = maxY;
    m_Y = VT::Math::Clip( m_Y, m_fYMin, m_fYMax );
}

bool QPolylineNode::isBipolar() const
{
    qreal i = getYMin(), a = getYMax();
    if( (i>=0.0 && a>=0.0) ||
        (i<=0.0 && a<=0.0) )
        return false;
    return true;
}

void QPolylineNode::setXY( double x, double y )
{
    m_X = VT::Math::Clip( x, m_fXMin, m_fXMax );
    m_Y = VT::Math::Clip( y, m_fYMin, m_fYMax );
}

void QPolylineNode::setX( double x )
{
    m_X = VT::Math::Clip( x, m_fXMin, m_fXMax );
}

void QPolylineNode::setY( double y )
{
    m_Y = VT::Math::Clip( y, m_fYMin, m_fYMax );
}

bool QPolylineNode::move( double x, double y )
{
    bool ret = false;
    if( m_bEditableX ){
        qreal newx = VT::Math::Clip( x, m_fXMin, m_fXMax );
        if( newx != m_X ){
            m_X = newx;
            ret = true;
        }
    }
    if( m_bEditableY ){
        qreal newy = VT::Math::Clip( y, m_fYMin, m_fYMax );
        if( newy != m_Y ){
            m_Y = newy;
            ret = true;
        }
    }
    if( ret )
        emit moved();
    return ret;
}







QEditablePolyline::QEditablePolyline( TStdCurveTypes type, qreal stdAreaRectWidth, qreal stdVRectWidth ):
    m_Map(),
    m_bVisible( true ),
    m_bEditEnabled( true ),
    m_iTag( 0 ),
    m_ForwardOnly( true ),
    m_Additive( true ),
    m_Nodes(),
    m_iDownX( 0 ),
    m_iDownY( 0 ),
    m_iNode( -1 ),
    m_iHoverNode( -1 ),
    m_sName( "Unknown" ),
    m_bDragging( false )
{
    setDefaultColors();
    switch( type ){
        case kUnknownCurveType:
            if( stdAreaRectWidth>0 ){
                QRectF r( 0, 0, stdAreaRectWidth, 1.0 );
                map().setAreaRect( r );
                map().setVRect( r );
            }
            if( stdVRectWidth )
                map().setVRect( QRectF( 0, 0, stdVRectWidth, 1.0 ) );
            break;
        case kAmplitudeEG:
            if( stdAreaRectWidth<=0 )
                stdAreaRectWidth = 60000.0;
            if( stdVRectWidth<=0 )
                stdVRectWidth = VT::Math::Min( stdAreaRectWidth, 5000.0 );
            map().setAreaRect(QRectF( 0, 0, stdAreaRectWidth, 100.0 ));
            map().setVRect(QRectF( 0, 0, stdVRectWidth, 100.0 ));
            break;
        case kBipolarEG:
            if( stdAreaRectWidth<=0 )
                stdAreaRectWidth = 60000.0;
            if( stdVRectWidth<=0 )
                stdVRectWidth = VT::Math::Min( stdAreaRectWidth, 5000.0 );
            map().setAreaRect(QRectF( 0, -100.0, stdAreaRectWidth, 200.0 ));
            map().setVRect(QRectF( 0, -100.0, stdVRectWidth, 200.0 ));
            break;
    }

    // After all stuff is set, connect signals
    connectInternalSignals();
}

QEditablePolyline::QEditablePolyline( const QEditablePolyline& src ):
    QObject(),
    m_Map( src.mapConst() ),
    m_bVisible( src.m_bVisible ),
    m_bEditEnabled( src.m_bEditEnabled ),
    m_iTag( src.m_iTag ),
    m_LineColor( src.m_LineColor ),
    m_LineSelectedColor( src.m_LineSelectedColor ),
    m_NodeColor( src.m_NodeColor ),
    m_NodeSelectedColor( src.m_NodeSelectedColor ),
    m_ForwardOnly( src.m_ForwardOnly ),
    m_Additive( src.m_Additive ),
    m_Nodes( src.m_Nodes ),
    m_iDownX( 0 ),
    m_iDownY( 0 ),
    m_iNode( -1 ),
    m_iHoverNode( -1 ),
    m_sName( src.m_sName ),
    m_bDragging( false )
{
    connectInternalSignals();
}

void QEditablePolyline::connectInternalSignals()
{
    connect( &(map()), SIGNAL(mapChanged()), SLOT(mapChanged()) );
}

void QEditablePolyline::setDefaultColors()
{
    /*m_LineColor.setRgb( 20, 180, 20 );
    m_LineSelectedColor.setRgb( 80, 255, 80 );
    m_NodeColor.setRgb( 60, 255, 60 );
    m_NodeSelectedColor.setRgb( 230, 255, 60 );
    emit updated();*/

    setColor( QColor(20, 180, 20) );
}

void QEditablePolyline::copyFrom( const QEditablePolyline& src )
{
    endDrag();
    deselectNode();
    map().copyFrom( src.mapConst() );
    m_sName = src.m_sName;
    m_bDragging = false;

    // Copying nodes
    deleteAll();
    for( int i=0; i<src.m_Nodes.length(); i++ )
        m_Nodes.append( new QPolylineNode( *(src.m_Nodes.at(i)) ) );

    m_ForwardOnly = src.m_ForwardOnly;
    m_Additive = src.m_Additive;
    m_iTag = src.m_iTag;
    m_LineColor = src.m_LineColor;
    m_LineSelectedColor = src.m_LineSelectedColor;
    m_NodeColor = src.m_NodeColor;
    m_NodeSelectedColor = src.m_NodeSelectedColor;
    m_bEditEnabled = src.m_bEditEnabled;
    m_bVisible = src.m_bVisible;
    emit changed( this );
    emit updated();
}

QEditablePolyline* QEditablePolyline::createopy() const
{
    QEditablePolyline* obj = createObject();
    obj->copyFrom( *this );
    return obj;
}

QEditablePolyline::~QEditablePolyline()
{
    deleteAll();
}

// Draw a curve on some surface.
// guiRect - rectangle on the surface (pixels).
// vRect - virtual rectangle on the surface with coordinates mapped
//    to the space of the points.
// painter - a QPainter to use
void QEditablePolyline::draw( QPainter& painter ) const
{
    if( m_Nodes.length() ){
        // Draw lines
        painter.setPen( (m_iNode<0)? m_LineColor: m_LineSelectedColor );
        for( int i=1; i<m_Nodes.length(); i++ )
            painter.drawLine( getNodeX( i-1 ), getNodeY( i-1 ), getNodeX( i ), getNodeY( i ) );
        // Draw nodes
        if( getEditEnabled() )
            for( int i=0; i<m_Nodes.length(); i++ )
                drawNode( painter, i );
    }
}

void QEditablePolyline::drawNode( QPainter& painter, int nodeIdx ) const
{
    qreal x = getNodeX( nodeIdx );
    qreal y = getNodeY( nodeIdx );
    bool isSelected = m_iNode==nodeIdx;
    qreal size = getNodeSize(), sizem1 = size-1.0;
    qreal half = size/2.0;
    QRectF rect( x-half, y-half, sizem1, sizem1 );
    QColor color;
    if( m_Nodes.at(nodeIdx)->useSpecialColor() ){
        if( isSelected )
            color = m_Nodes.at(nodeIdx)->getSpecialColor().lighter( 200 );
        else
            color = m_Nodes.at(nodeIdx)->getSpecialColor();
    }else{
        if( isSelected )
            color = m_NodeSelectedColor;
        else
            color = m_NodeColor;
    }
    switch( m_Nodes.at(nodeIdx)->getStyle() ){
        case QPolylineNode::kFrameOnly:
            painter.setPen( color );
            painter.drawRect( rect );
            break;
        case QPolylineNode::kSolid:
            painter.fillRect( rect, QBrush( color ) );
            break;
        default:
            VTTHROWT( "Curve node has unknown style." );
    }
}

// Check that mouse at (x, y) hits the curve.
// The check may be done twice for some cuves, first for nodes, then for lines.
// If secondCheck is set, then "secondary" check should be performed.
// If the curve ignores clicks on lines, then it should return false when
// secondCheck is set.
bool QEditablePolyline::mouseCheck( int x, int y, bool click, bool secondCheck )
{
    if( !getEditEnabled() )
        return false;

    qreal nodeRectHalfSize = getNodeSize()/2.0;

    m_iDownX = x;
    m_iDownY = y;
    if( secondCheck )
        return false;
    for( size_t i=0; int(i)<m_Nodes.length(); i++ ){
        // const QPolylineNode* node = &(m_Nodes.at(i));
        // qreal nx = VXtoScreenX( node->x() ), ny = VYtoScreenY( node->y() );
        qreal nx = getNodeX( i ), ny = getNodeY( i );
        if(
            VT::Math::Abs( nx-x )<=nodeRectHalfSize &&
            VT::Math::Abs( ny-y )<=nodeRectHalfSize
          ){
            m_iHoverNode = i;
            if( click && m_iNode!=int(i) ){
                deselectNode();
                selectNode( i );
            }
            return true;
        }
    }
    return false;
}

// Get cursor for current state. mouseCheck() should be called
// prior to this function if mouse has been moved.
Qt::CursorShape QEditablePolyline::getCursorShape() const
{
    if( !getEditEnabled() )
        return Qt::ArrowCursor;

    if( m_iHoverNode>=0 && m_iHoverNode<m_Nodes.length() )
        return m_Nodes.at( m_iHoverNode )->getCursorShape();

    return Qt::ArrowCursor;
}

bool QEditablePolyline::startDrag() // Start dragging last checked or selected notes
{
    if( m_bDragging )
        endDrag();
    if( !getEditEnabled() )
        return false;
    if( isSelected() ){
        // Node is already selected
        m_bDragging = true;
        return true;
    }
    return false;
}

bool QEditablePolyline::doDrag( int x, int y ) // Continue dragging (to the mouse coordinates)
{
    if( m_bDragging && getEditEnabled() ){
        // Calculating mouse shift
        int intXShift = x - m_iDownX, intYShift = y - m_iDownY;
        if( intXShift!=0 || intYShift!=0 ){
            // Convert the shift to virtual coordinates
            qreal virtXShift = map().screenXToVX( intXShift )-map().screenXToVX( 0 );
            qreal virtYShift = map().screenYToVY( intYShift )-map().screenYToVY( 0 );

            // Estimate new position of the point
            qreal newx = m_Nodes.at( m_iNode )->x() + virtXShift;
            qreal newy = m_Nodes.at( m_iNode )->y() + virtYShift;

            validateNodeMoveAndDoIt( m_iNode, newx, newy, true );

            m_iDownX = x;
            m_iDownY = y;
        }
        return true;
    }
    return false;
}

bool QEditablePolyline::validateNodeMoveAndDoIt( int node, qreal& newx, qreal& newy, bool emitEvents )
{
    Q_UNUSED(node);
    if( getForwardOnly() && !getAdditive() ){
        // Do not allow point to be moved before previous / after next point
        if( m_iNode>0 )
            if( newx < m_Nodes.at( m_iNode-1 )->x() )
                newx = m_Nodes.at( m_iNode-1 )->x();
        if( m_iNode<m_Nodes.length()-1 )
            if( newx > m_Nodes.at( m_iNode+1 )->x() )
                newx = m_Nodes.at( m_iNode+1 )->x();
    }
    // I'd like to move it move it
    bool moved = m_Nodes[ m_iNode ]->move( newx, newy );
    if( emitEvents && moved ){
        emit changed( this );
        emit updated();
    }
    return moved;
}


qreal QEditablePolyline::getNodeVX( int node ) const
{
    if( getAdditive() ){
        qreal sum = 0;
        for( int i=0; i<=node; i++ )
            sum += m_Nodes.at( i )->x();
        return sum;
    }
    qreal ret = m_Nodes.at( node )->x();
    return ret;
}

qreal QEditablePolyline::getNodeVY( int node ) const
{
    qreal ret = m_Nodes.at( node )->y();
    return ret;
}

qreal QEditablePolyline::getNodeX( int node ) const
{
    qreal vx = getNodeVX( node );
    return mapConst().VXtoScreenX( vx );
}

qreal QEditablePolyline::getNodeY( int node ) const
{
    qreal vy = getNodeVY( node );
    return mapConst().VYtoScreenY( vy );
}

qreal QEditablePolyline::getLeftmostNodeVX() const
{
    if( m_Nodes.length()<1 )
        throw VT::CException(VTT("Cannot find leftmost node, the curve has no nodes."));
    if( getForwardOnly() )
        return getNodeVX( 0 );
    qreal minx = m_Nodes.first()->x();
    for( int i=1; i<m_Nodes.length(); i++ )
        if( m_Nodes.at(i)->x() < minx )
            minx = m_Nodes.at(i)->x();
    return minx;
}

qreal QEditablePolyline::getRightmostNodeVX() const
{
    if( m_Nodes.length()<1 )
        throw VT::CException(VTT("Cannot find rightmost node, the curve has no nodes."));
    if( getForwardOnly() )
        return getNodeVX( m_Nodes.length()-1 );
    qreal maxx = m_Nodes.first()->x();
    for( int i=1; i<m_Nodes.length(); i++ )
        if( m_Nodes.at(i)->x() > maxx )
            maxx = m_Nodes.at(i)->x();
    return maxx;
}

qreal QEditablePolyline::getLeftmostNodeVXPc() const
{
    return mapConst().VXtoAreaPc( getLeftmostNodeVX() );
}

qreal QEditablePolyline::getRightmostNodeVXPc() const
{
    return mapConst().VXtoAreaPc( getRightmostNodeVX() );
}

bool QEditablePolyline::isBipolar() const
{
    for( int i=0; i<m_Nodes.length(); i++ )
        if( m_Nodes.at(i)->isBipolar() )
            return true;
    return false;
}

void QEditablePolyline::reverse(double center)
{
    const int n = m_Nodes.count()-1;
    for(int i = 0;i < (n+1)/2;i++){
        m_Nodes[i]->setX(2*center - m_Nodes[i]->x());
        m_Nodes[n-i]->setX(2*center - m_Nodes[n-i]->x());
        m_Nodes.swap(i, n-i);
    }
    if( m_iNode >= 0 )
        m_iNode = n - m_iNode;
    emit changed(this);
    emit updated();
}

void QEditablePolyline::reverse_LineMiddle()
{
    const int n = m_Nodes.count()-1;
    double center = (m_Nodes[0]->x() + m_Nodes[n]->x())/2.0;
    for(int i = 0;i < (n+1)/2;i++){
        m_Nodes[i]->setX(2*center - m_Nodes[i]->x());
        m_Nodes[n-i]->setX(2*center - m_Nodes[n-i]->x());
        m_Nodes.swap(i, n-i);
    }
    if( m_iNode >= 0 )
        m_iNode = n - m_iNode;
    emit changed(this);
    emit updated();
}

void QEditablePolyline::flip(double center)
{
    for(int i = 0;i < m_Nodes.count();i++){
        m_Nodes[i]->setY(2*center-m_Nodes[i]->y());
    }
    emit changed(this);
    emit updated();
}

void QEditablePolyline::mapChanged()
{
    endDrag();
    emit updated();
}

bool QEditablePolyline::endDrag() // End dragging points
{
    if( m_bDragging ){
        m_bDragging = false;
        emit updated();
        return true;
    }
    return false;
}

bool QEditablePolyline::selectNode( int nodeIdx ) // Select a node
{
    if( !getEditEnabled() )
        return false;
    endDrag();
    deselectNode();
    if( nodeIdx>=0 && nodeIdx<m_Nodes.length() ){
        m_iNode = nodeIdx;
        emit selected( this, m_iNode );
        emit updated();
        return true;
    }
    return false;
}

bool QEditablePolyline::selectNode()
{
    if( m_iHoverNode>=0 && m_iHoverNode<m_Nodes.length() )
        return selectNode( m_iHoverNode );
    deselectNode();
    return false;
}

bool QEditablePolyline::deselectNode() // Deselect a node
{
    endDrag();
    if( m_iNode>=0 ){
        emit unselected( this, m_iNode );
        m_iNode = -1;
        emit updated();
        return true;
    }
    return false;
}

bool QEditablePolyline::isSelected() const
{
    VTASSERT( m_iNode < m_Nodes.length() );
    return m_iNode>=0;
}

bool QEditablePolyline::nudge( QEditablePolyline::TNudge nudge ) // Nudge selected note (via keyboard)
{
    endDrag();
    if( isNodeSelected() ){
        qreal nx = m_Nodes.at(m_iNode)->x(), ny = m_Nodes.at(m_iNode)->y();
        switch( nudge ){
            case kNudgeLeft:
            case kNudgeRight:
                {
                    qreal w = VT::Math::Abs( mapConst().screenXToVX( 1 )-mapConst().screenXToVX( 0 ) );
                    if( nudge == kNudgeLeft )
                        nx -= w;
                    else
                        nx += w;
                    break;
                }

            case kNudgeUp:
            case kNudgeDown:
                {
                    qreal h = VT::Math::Abs( mapConst().screenYToVY( 1 )-mapConst().screenYToVY( 0 ) );
                    if( mapConst().flip() )
                        h = -h;
                    if( nudge == kNudgeUp )
                        ny -= h;
                    else
                        ny += h;
                    break;
                }
        }
        validateNodeMoveAndDoIt( m_iNode, nx, ny, true );
        return true;
    }
    return false;
}

void QEditablePolyline::addNode( QPolylineNode* src )
{
    m_Nodes.append( src );
    emit updated();
}

void QEditablePolyline::addNode( const QPolylineNode& src )
{
    m_Nodes.append( new QPolylineNode(src) ); // emits updated()
}

void QEditablePolyline::addNode( const QPolylineNode& src, int atIndex, bool preserveCurveLength )
{
    if( atIndex<0 || atIndex>m_Nodes.length() )
        throw VT::CException(VTT("Bad node insert position."));
    if(getAdditive() && preserveCurveLength)
        if(atIndex < m_Nodes.size())
            m_Nodes[atIndex]->setX(m_Nodes[atIndex]->x() - src.x());
    m_Nodes.insert( atIndex, new QPolylineNode(src) );
    emit updated();
}

void QEditablePolyline::deleteNode( int atIndex )
{
    if( atIndex<0 || atIndex>m_Nodes.length() )
        throw VT::CException(VTT("Bad node delete position."));
    endDrag();
    if( atIndex==m_iNode )
        deselectNode();
    delete m_Nodes.at( atIndex );
    m_Nodes.removeAt( atIndex );
    emit updated();
}

void QEditablePolyline::deleteAll()
{
    endDrag();
    deselectNode();
    for( int i=0; i<m_Nodes.length(); i++ )
        delete m_Nodes.at(i);
    m_Nodes.clear();
    emit updated();
}

void QEditablePolyline::setColor( QColor baseColor )
{
    m_LineColor = baseColor.darker(160);
    m_LineSelectedColor = baseColor;
    m_NodeColor = baseColor;
    m_NodeSelectedColor = m_NodeColor.lighter(200);
    emit updated();
}







QCurveEditorField::QCurveEditorField( QWidget* parent ):
    QRectangularMapEditor( parent ),
    m_bSameMaps( true ),
    m_Curves(),
    m_BGFixed( false ),
    m_iCurve( -1 ),
    m_BGVirtualPos( 0, 0, 1.0, 1.0 )
{
    setGUIRect( rect() );
    setVRect( QRectF( 0, 0, 1.0, 1.0 ) );
    setMouseTracking( true );
    setFocusPolicy( Qt::StrongFocus );

    bool res = connect( &(map()), SIGNAL(mapChanged()), this, SLOT(fieldMapChanged()) );
    VTASSERT2T( res, "Signal not connected" );
    Q_UNUSED(res);
}

QCurveEditorField::~QCurveEditorField()
{
    deleteAll();
}

void QCurveEditorField::curveUpdated()
{
    update();
}

void QCurveEditorField::curveNodesChanged( QEditablePolyline *curve )
{
    emit curveChange( curve );
    update();
}

void QCurveEditorField::setGUIRect( const QRect& src )
{
    map().setGUIRect( src );
    fieldMapChanged(); // Copies map to all sub-curves
    update();
}

void QCurveEditorField::setVRect( const QRectF& src )
{
    map().setVRect( src );
    fieldMapChanged(); // Copies map to all sub-curves
    update();
}

void QCurveEditorField::setFlip( bool fl )
{
    map().setFlip( fl );
    fieldMapChanged(); // Copies map to all sub-curves
    update();
}

void QCurveEditorField::addCurve( QEditablePolyline* curve )
{
    m_Curves.append( curve );
    // Copying maps
    if( getSameMaps() )
        m_Curves.last()->map().copyFrom( mapConst() );
    else
        m_Curves.last()->map().copyGuiGeometryFrom( mapConst() );
    bool conn1 = connect( curve, SIGNAL(updated()), this, SLOT(curveUpdated()) );
    bool conn2 = connect( curve, SIGNAL(changed(QEditablePolyline*)), this, SLOT(curveNodesChanged(QEditablePolyline*)) );
    bool conn3 = connect( curve, SIGNAL(goneFromEditing(QEditablePolyline*)), this, SLOT(goneFromEditing(QEditablePolyline*)) );
    VTASSERT2T( conn1, "Signal connection failed" );
    VTASSERT2T( conn2, "Signal connection failed" );
    VTASSERT2T( conn3, "Signal connection failed" );
    Q_UNUSED(conn1);
    Q_UNUSED(conn2);
    Q_UNUSED(conn3);
    update();
}

void QCurveEditorField::addCurve( const QEditablePolyline& curve )
{
    addCurve( curve.createopy() ); // Calls update();
}

void QCurveEditorField::deleteCurve( int idx )
{
    if( idx<0 || idx>=m_Curves.length() )
        throw VT::CException(VTT("The specified curve index does not exist."));
    endDrag();
    deselectAll();
    delete m_Curves.at(idx);
    m_Curves.removeAt(idx);
    update();
}

void QCurveEditorField::deleteAll()
{
    endDrag();
    deselectAll();
    for( int i=0; i<m_Curves.length(); i++ )
        delete m_Curves[i];
    m_Curves.clear();
    update();
}

void QCurveEditorField::setEnabledCurves( bool enableedit, int except )
{
    for( int i=0; i<m_Curves.length(); i++ )
        if( i==except )
            m_Curves[i]->setEditEnabled( !enableedit );
        else
            m_Curves[i]->setEditEnabled( enableedit );
}

void QCurveEditorField::paintEvent( QPaintEvent * )
{
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );

    // Drawing background
    if( pixmap()!=NULL ){
        if( m_BGFixed )
            painter.drawPixmap( rect(), *(pixmap()), pixmap()->rect() );
        else{
            QRectF bgRect;
            const QRectF& f = getBGVirtualPosition();
            qreal l = mapConst().VXtoScreenX( f.left() ), t = mapConst().VYtoScreenY( f.top() ),
                r = mapConst().VXtoScreenX( f.right() ), b = mapConst().VYtoScreenY( f.bottom() );
            VT::Math::Order( &l, &r );
            VT::Math::Order( &t, &b );
            bgRect.setCoords( l, t, r, b );
            painter.fillRect( rect(), QBrush( QColor( 0, 0, 0 ) ) );
            painter.drawPixmap( bgRect, *(pixmap()), pixmap()->rect() );
        }
    }

    // Drawing markers (inherited from QRectangularMapEditor)
    drawMarkers( painter );

    for( int i=0; i<m_Curves.length(); i++ )
        if( i!=m_iCurve && m_Curves.at(i)->isVisible() )
            m_Curves.at(i)->draw( painter );
    // Selected curve is drawn on top of everything else
    if( m_iCurve>=0 && m_iCurve<m_Curves.length() )
        m_Curves.at(m_iCurve)->draw( painter );
}

void QCurveEditorField::resizeEvent( QResizeEvent * event )
{
    QWidget::resizeEvent( event );
    setGUIRect( rect() );
}

void QCurveEditorField::mousePressEvent(QMouseEvent *me)
{
    endDrag( false ); // Do not restore arrow cursor
    deselectAll();
    if( updateOnMouseMove( me ) ){ // A curve has been clicked
        if( m_iCurve>=0 && m_iCurve<m_Curves.length() ){
            m_Curves[ m_iCurve ]->selectNode();
            emit selected();
            m_Curves[ m_iCurve ]->startDrag();
            emit startedDragging();
        }
    }else
        setCursor( Qt::ArrowCursor );
}

void QCurveEditorField::mouseReleaseEvent(QMouseEvent *)
{
    endDrag();
}

bool QCurveEditorField::updateOnMouseMove( QMouseEvent *me )
{
    bool ret = false;
    if( !isDragging() ){
        // Not dragging nodes - have to check all curves & nodes to
        // detect proper cursor shape and possible actions.
        bool hovering = false;
        // Check currently selected node first
        if( m_iCurve>=0 && m_iCurve<m_Curves.length() )
            if( m_Curves[m_iCurve]->mouseCheck( me->x(), me->y(), false ) ){
                setCursor( m_Curves[m_iCurve]->getCursorShape() );
                hovering = true;
                ret = true;
            }
        if( !ret )
            // Testing curves from top to bottom
            for( int i=m_Curves.length()-1; i>=0; i-- ){
                if( i != m_iCurve && m_Curves[i]->isVisible() ){
                    if( m_Curves[i]->mouseCheck( me->x(), me->y(), false ) ){
                        setCursor( m_Curves[i]->getCursorShape() );
                        hovering = true;
                        ret = true;
                        m_iCurve = i;
                        break;
                    }
                }
            }
        if( !hovering ){
            setCursor( Qt::ArrowCursor );
        }
    }else{
        // Dragging mode
        m_Curves[ m_iCurve ]->doDrag( me->x(), me->y() );
    }
    return ret;
}

void QCurveEditorField::mouseMoveEvent( QMouseEvent *me )
{
    updateOnMouseMove( me );
}

void QCurveEditorField::keyPressEvent( QKeyEvent *event )
{
    if( m_iCurve>=0 && m_iCurve<m_Curves.length() ){
        if( m_Curves.at(m_iCurve)->isSelected() ){
            if( event->key()==Qt::Key_Up ){
                m_Curves[m_iCurve]->nudge( QEditablePolyline::kNudgeUp );
                emit nudged();
            }else if( event->key()==Qt::Key_Down ){
                m_Curves[m_iCurve]->nudge( QEditablePolyline::kNudgeDown );
                emit nudged();
            }else if( event->key()==Qt::Key_Left ){
                m_Curves[m_iCurve]->nudge( QEditablePolyline::kNudgeLeft );
                emit nudged();
            }else if( event->key()==Qt::Key_Right ){
                m_Curves[m_iCurve]->nudge( QEditablePolyline::kNudgeRight );
                emit nudged();
            }
        }
    }
}


bool QCurveEditorField::isDragging() const
{
    if( m_iCurve<0 )
        return false;
    if( m_Curves.at(m_iCurve)->isDragging() )
        return true;
    return false;
}

void QCurveEditorField::fieldMapChanged()
{
    // Update maps on all curves
    if( getSameMaps() ){
        for( int i=0; i<m_Curves.length(); i++ )
            m_Curves[i]->map().copyFrom( mapConst() );
    }else{
        qreal
            scrollx = map().scrollXPos(),
            scrolly = map().scrollYPos(),
            sx = map().zoomXArea(),
            sy = map().zoomYArea();
        for( int i=0; i<m_Curves.length(); i++ ){
            m_Curves[i]->map().copyGuiGeometryFrom( mapConst() );
            m_Curves[i]->map().scrollXToPos( scrollx );
            m_Curves[i]->map().scrollYToPos( scrolly );
            m_Curves[i]->map().zoomXToPc( sx );
            m_Curves[i]->map().zoomYToPc( sy );
        }
    }
}

void QCurveEditorField::goneFromEditing( QEditablePolyline *curve )
{
    if( hasSelection() ){
        if( curve==m_Curves[m_iCurve] )
            deselectAll();
    }
    update();
}

bool QCurveEditorField::endDrag( bool restoreCursor )
{
    if( restoreCursor )
        setCursor( Qt::ArrowCursor );
    if( m_iCurve<0 )
        return false;
    if( m_Curves.at(m_iCurve)->isDragging() ){
        bool ret = m_Curves[m_iCurve]->endDrag();
        if( ret )
            emit finishedDragging();
        return ret;
    }
    return false;
}

bool QCurveEditorField::selectCurve( int index, int selectNode, bool reenableCurveEdit )
{
    if( index<0 || index>=m_Curves.length() )
        throw VT::CException( VTT("Bad curve index.") );
    if( index != m_iCurve ){
        endDrag();
        deselectAll();
        m_Curves[index]->setVisible( true );
        if( reenableCurveEdit )
            m_Curves[index]->setEditEnabled( true );
        m_Curves[index]->selectNode( selectNode );
        m_iCurve = index;
        return true;
    }
    return false;
}

void QCurveEditorField::deselectAll()
{
    bool em = false;
    for( int i=0; i<m_Curves.length(); i++ ){
        if( m_Curves[i]->isSelected() ){
            m_Curves[i]->deselectNode();
            em = true;
        }
    }
    if( em )
        emit unselected();
}

void QCurveEditorField::setSameMaps( bool same )
{
    m_bSameMaps = same;
    if( same )
        for( int i=0; i<m_Curves.length(); i++ )
            m_Curves[i]->map().copyFrom( map() );
}

qreal QCurveEditorField::getLeftmostNodeVXPc() const
{
    if( !m_Curves.length() )
        return 0;
    qreal l;
    bool l_set = false;
    for( int i=0; i<m_Curves.length(); i++ )
        if( m_Curves.at(i)->hasNodes() && m_Curves.at(i)->isVisible() ){
            qreal l2 = m_Curves.at(i)->getLeftmostNodeVXPc();
            if( !l_set ){
                l = l2;
                l_set = true;
            }else{
                if( l2<l )
                    l = l2;
            }
        }
    if( !l_set )
        return 0;
    return l;
}

qreal QCurveEditorField::getRightmostNodeVXPc() const
{
    if( !m_Curves.length() )
        return 0;
    qreal r;
    bool r_set = false;
    for( int i=0; i<m_Curves.length(); i++ )
        if( m_Curves.at(i)->hasNodes() && m_Curves.at(i)->isVisible() ){
            qreal r2 = m_Curves.at(i)->getRightmostNodeVXPc();
            if( !r_set ){
                r = r2;
                r_set = true;
            }else{
                if( r2>r )
                    r = r2;
            }
        }
    if( !r_set )
        return 0;
    return r;
}

bool QCurveEditorField::zoomXToObjects()
{
    qreal l = getLeftmostNodeVXPc();
    qreal r = getRightmostNodeVXPc();
    VTASSERT( r>=l );
    if( l==0 && r==0 )
        return false;
    map().zoomXToRangePc( l, r );
    return true;
}













