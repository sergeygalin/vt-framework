//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A QLabel upgraded with some additional signals.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QCLICKABLELABEL_H
#define QCLICKABLELABEL_H

#include <QtGui/QLabel>
#include <QtCore/QPoint>

//
// This is an extension of standard Qt's QLabel widget which allows
// to connect signals to some of its mouse events - as I used to have in VCL ;-)
//
class QClickableLabel: public QLabel
{
    Q_OBJECT

public:
    QClickableLabel( QWidget * parent = 0, Qt::WindowFlags f = 0 );
    QClickableLabel( const QString & text, QWidget * parent = 0, Qt::WindowFlags f = 0 );
    virtual ~QClickableLabel();

protected:
    virtual void mouseReleaseEvent( QMouseEvent* ev );

signals:
    void clicked();
    void clicked(bool);
    void clicked( const QPoint& );
    void rightClicked();
    void rightClicked(bool);
    void rightClicked( const QPoint& );
};

#endif // QCLICKABLELABEL_H
