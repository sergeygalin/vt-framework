//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QMessageBox>
#include "QPixmapRadioBar.h"


QPixmapRadioBar::QPixmapRadioBar( QWidget * parent ):
    QPixmapButtonBar( parent ),
    m_SelectedButton( 0 )
{
}

QPixmapRadioBar::QPixmapRadioBar( QWidget * parent, const QPixmap& pixmap,
    int nButtons, int nStates,
    bool buttonsAreInVertical, bool onScreenVertical):
    QPixmapButtonBar( parent, pixmap ),
    m_SelectedButton( 0 )
{
    initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
}

QPixmapRadioBar::QPixmapRadioBar( QWidget * parent, const QString& file,
    int nButtons, int nStates,
    bool buttonsAreInVertical, bool onScreenVertical ):
    QPixmapButtonBar( parent, file ),
    m_SelectedButton( 0 )
{
    initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
}

// Called when button is clicked by user.
void QPixmapRadioBar::myRadioButtonClicked()
{
    // QMessageBox::warning( this, "myRadioButtonClicked", "myRadioButtonClicked" );
    for( int children=0, button=0; children < QWidget::children().length(); children++ ){
        QObject* child = QWidget::children().at( children );
        if( child->isWidgetType() ){
            QPixmapButton* btn = qobject_cast<QPixmapButton*>( child );
            if( btn!=NULL ){
                if( btn->isChecked() ){
                    m_SelectedButton = button;
                    emit valueChanged( m_SelectedButton );
                    return;
                }
                button++;
            }
        }
    }
}

// Called when value is set externally.
void QPixmapRadioBar::setValue( int value )
{
    if( value==m_SelectedButton )
        return;
    QPixmapButton* btn = getButton( value );
    if( btn!=NULL )
        btn->click(); // This calls myRadioButtonClicked() which will, in turn,
                       // will update m_SelectedButtona and emit valueChanged() signal.
}


void QPixmapRadioBar::initPixmapButtonBar( bool buttonsAreInVertical, bool onScreenVertical, int nButtons, int nStates )
{
    QPixmapButtonBar::initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
    if( m_SelectedButton>=nButtons )
        setValue( 0 );
}

void QPixmapRadioBar::setupButton( QPixmapButton* button, int idx )
{
    QPixmapButtonBar::setupButton( button, idx );
    button->setCheckable( true );
    button->setAutoExclusive( true );
    connect( button, SIGNAL(clicked()), this, SLOT(myRadioButtonClicked()) );
    if( idx==m_SelectedButton )
        button->setChecked( true );
}


