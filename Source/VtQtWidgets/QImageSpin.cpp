//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/qabstractspinbox.h>
#include <QtGui/QPainter>
#include <QtGui/QLineEdit>
#include "QImageSpin.h"

QImageSpin::QImageSpin( QWidget *parent ):
    QSpinBox( parent ),
    QPixmapReferencer(),
    m_iImages( 0 )
{
    setTextEditVisible( false );
}

QImageSpin::QImageSpin( QWidget *parent, const QString& pictureSrc, int nImages ):
    QSpinBox( parent ),
    QPixmapReferencer( pictureSrc ),
    m_iImages( (nImages>0)? nImages: 1 )
{
    initImageSpin();
    setTextEditVisible( false );
}

QImageSpin::QImageSpin( QWidget *parent, const QPixmap& pixmap, int nImages ):
    QSpinBox( parent ),
    QPixmapReferencer( pixmap ),
    m_iImages( (nImages>0)? nImages: 1 )
{
    initImageSpin();
    setTextEditVisible( false );
}

void QImageSpin::init( const QString& pictureSrc, int nImages )
{
    m_iImages = nImages;
    setPixmap( pictureSrc );
    initImageSpin();
}

void QImageSpin::init( const QPixmap& pixmap, int nImages )
{
    m_iImages = nImages;
    setPixmap( pixmap );
    initImageSpin();
}


void QImageSpin::initImageSpin()
{
    QSpinBox::setMinimum( 0 );
    QSpinBox::setMaximum( m_iImages-1 );
    QSpinBox::setSingleStep( 1.0 );
    if( pixmap()!=NULL ){
        // Setting initial spin edit size to match image
        int iheight = pixmap()->height() / m_iImages,
            iwidth = pixmap()->width(),
            spinswidth = width() - lineEdit()->width() + 2;

        resize( iwidth + spinswidth, iheight + (height()-lineEdit()->height())/2 + 4 );
    }
    update();
}

void QImageSpin::paintEvent(QPaintEvent* event)
{
    lineEdit()->setVisible( m_bShowTextEdit );
    QSpinBox::paintEvent( event );
    if( pixmap()!=NULL ){
        QPainter painter( this );
        painter.setRenderHint( QPainter::Antialiasing );
        painter.setRenderHint( QPainter::SmoothPixmapTransform );
        painter.setRenderHint( QPainter::HighQualityAntialiasing );
        int iheight = pixmap()->height() / m_iImages,
            item = value();
            //areaheight = lineEdit()->height();
        QRect targetRect( lineEdit()->pos().x(), lineEdit()->pos().y(), lineEdit()->width(), lineEdit()->height() );
        //QRect sourceRect( 0, iheight*item, lineEdit()->width(), lineEdit()->height() );
        QRect sourceRect( 0, iheight*item, pixmap()->width(), iheight );
        painter.drawPixmap( targetRect, *(pixmap()), sourceRect );
    }
}

bool QImageSpin::isTextEditVisible() const
{
    return m_bShowTextEdit;
}

void QImageSpin::setTextEditVisible( bool vis )
{
    m_bShowTextEdit = vis;
    lineEdit()->setVisible( vis );
}

