//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// VT::MIDI::CMIDIMap editor dialog
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPalette>
#include <QtGui/QColor>

#include "ui_QMIDIMapDialog.h"

#include "QMIDIMapDialog.h"
#include "QMIDIMapWidget.h"


QMIDIMapDialog::QMIDIMapDialog(QWidget *parent):
    QDialog( parent, Qt::Dialog | Qt::WindowCloseButtonHint | Qt::WindowSystemMenuHint | Qt::WindowTitleHint ),
    ui( new Ui::QMIDIMapDialogClass )
{
    ui->setupUi(this);
    ui->mapTree->buildTable(); // setupUi() trashes the table

    connect( ui->btnCancel, SIGNAL(clicked()), this, SLOT(reject()) );
    connect( ui->btnOk, SIGNAL(clicked()), this, SLOT(accept()) );
    connect( ui->btnLoad, SIGNAL(clicked()), ui->mapTree, SLOT(openDialog()) );
    connect( ui->btnSave, SIGNAL(clicked()), ui->mapTree, SLOT(saveDialog()) );
    connect( ui->btnClearAll, SIGNAL(clicked()), ui->mapTree, SLOT(clearAllConfirm()) );
    connect( ui->btnClearMapping, SIGNAL(clicked()), ui->mapTree, SLOT(clearMapConfirm()) );
    connect( ui->btnInDrums, SIGNAL(clicked()), ui->mapTree, SLOT(setInToGMDrumsConfirm()) );
    connect( ui->btnOutDrums, SIGNAL(clicked()), ui->mapTree, SLOT(setOutToGMDrumsConfirm()) );
    connect( ui->mapTree, SIGNAL(mapAssigned()), this, SLOT(onMapAssigned()) );
    connect( ui->mapTree, SIGNAL(modFlagChanged()), this, SLOT(onMapModifiedFlag()) );
    connect( ui->edtDescription, SIGNAL(textEdited(const QString&)), this, SLOT(descriptionEdited(const QString&)) );

    onMapModifiedFlag();
    onMapAssigned();
}

QMIDIMapDialog::~QMIDIMapDialog()
{
    delete ui;
    ui = NULL;
}

int QMIDIMapDialog::getStartingOctave() const
{
    return ui->mapTree->getStartingOctave();
}

void QMIDIMapDialog::setStartingOctave( int oct )
{
    ui->mapTree->setStartingOctave( oct );
}

const VT::CString& QMIDIMapDialog::GetMapDirectory() const
{
    return ui->mapTree->GetMapDirectory();
}

void QMIDIMapDialog::SetMapDirectory( LPCVTCHAR dir )
{
    ui->mapTree->SetMapDirectory( dir );
}

void QMIDIMapDialog::setMap( const VT::MIDI::CMIDIMap& map )
{
    ui->mapTree->setMap( map );
}

const VT::MIDI::CMIDIMap& QMIDIMapDialog::getMap()
{
    return ui->mapTree->getMap();
}

void QMIDIMapDialog::onMapAssigned()
{
    ui->edtDescription->setText( ui->mapTree->getMapDescription().QStr() );
}

void QMIDIMapDialog::descriptionEdited( const QString& text )
{
    ui->mapTree->setMapDescription( VT::CString( text ) );
}

void QMIDIMapDialog::onMapModifiedFlag()
{
    QString cap = tr("MIDI Map");
    if( ui->mapTree->isModified() ){
        cap += " *";
    }
    setWindowTitle( cap );
}

