//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <QtCore/QRect>
#include <QtGui/QPainter>
#include "QBlinkingLed.h"

QBlinkingLed::QBlinkingLed( QWidget* parent ):
    QWidget( parent ),
    m_BlinkTime( kDefaultBlinkTimeMs ),
    m_On( false )
{
    connect( &m_Timer, SIGNAL(timeout()), this, SLOT(timer()) );
}

QBlinkingLed::~QBlinkingLed()
{
}

void QBlinkingLed::paintEvent( QPaintEvent* )
{
    if( pixmap()==NULL )
        return;
    QRect srcRect;
    if( m_On )
        srcRect.setRect( 0, pixmap()->height()/2, pixmap()->width(), pixmap()->height()/2 );
    else
        srcRect.setRect( 0, 0, pixmap()->width(), pixmap()->height()/2 );
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );
    painter.drawPixmap( rect(), *(pixmap()), srcRect );
}

void QBlinkingLed::blink()
{
    m_On = true;
    update();
    m_Timer.start( m_BlinkTime );
}

void QBlinkingLed::timer()
{
    m_Timer.stop();
    m_On = false;
    update();
}


