//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QRECTANGULARMAPEDITORCONTAINER_H
#define QRECTANGULARMAPEDITORCONTAINER_H

#include <QtGui/QWidget>
#include <QtGui/QPainter>
#include <QtGui/QScrollBar>
#include <QtGui/QPushButton>
#include "QRectangularMapEditor.h"
#include "QPixmapReferencer.h"

//
// A base class for a GUI editor based on QRectangularMapEditor descendant
// which adds standard set of scroll bars and zooming buttons around the
// editor.
//
class QRectangularMapEditorContainer: public QWidget, public QPixmapReferencer
{
    Q_OBJECT

protected:
    QRectangularMapEditorContainer( QRectangularMapEditor *editr, QWidget* parent = NULL );

public:
    virtual ~QRectangularMapEditorContainer();

    Q_PROPERTY( bool enableHZoom READ getEnableHZoom WRITE setEnableHZoom );
    bool getEnableHZoom() const { return m_EnableHZoom; }
    void setEnableHZoom( bool z ){ m_EnableHZoom = z; resizeEvent(NULL); }

    Q_PROPERTY( bool enableVZoom READ getEnableVZoom WRITE setEnableVZoom );
    bool getEnableVZoom() const { return m_EnableVZoom; }
    void setEnableVZoom( bool z ){ m_EnableVZoom = z; resizeEvent(NULL); }

    Q_PROPERTY( bool enableHScroll READ getEnableHScroll WRITE setEnableHScroll );
    bool getEnableHScroll() const { return m_EnableHScroll; }
    void setEnableHScroll( bool z ){ m_EnableHScroll = z; resizeEvent(NULL); }

    Q_PROPERTY( bool enableVScroll READ getEnableVScroll WRITE setEnableVScroll );
    bool getEnableVScroll() const { return m_EnableVScroll; }
    void setEnableVScroll( bool z ){ m_EnableVScroll = z; resizeEvent(NULL); }

protected:
    QRectangularMapEditor* mapEditor(){ return m_Editor; }
    const QRectangularMapEditor* mapEditorConst() const { return m_Editor; }

    virtual void resizeEvent( QResizeEvent *event );
    virtual void paintEvent( QPaintEvent *event );

protected slots:
    void updateControlsByMap();

    void VZoomInClick();
    void VZoomOutClick();
    void HZoomInClick();
    void HZoomOutClick();
    void HZoomResetClick();
    void VZoomResetClick();
    void ZoomResetClick();
    void HScrollbarChange( int value );
    void VScrollbarChange( int value );

private:
    bool m_EnableHZoom, m_EnableVZoom, m_EnableHScroll, m_EnableVScroll;
    QRectangularMapEditor *m_Editor;
    QScrollBar *m_HScrollBar, *m_VScrollBar;
    QPushButton *m_VZoomIn, *m_VZoomOut, *m_HZoomIn, *m_HZoomOut, *m_ZoomReset, *m_VZoomReset, *m_HZoomReset;
    bool m_bIgnoreScrolling;
};

#endif // QRECTANGULARMAPEDITORCONTAINER_H
