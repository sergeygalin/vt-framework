//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPainter>
#include "QPixmapButton.h"

QPixmapButton::QPixmapButton( QWidget * parent ):
    QAbstractButton( parent ),
    QMultiPixmapReferencer(),
    m_ShowCaption( false ),
    m_Tag( 0 ),
    m_CaptionPressedXOffset( 0 ),
    m_CaptionPressedYOffset( 0 ),
    m_pTagOut( NULL ),
    m_useMousedState(false),
    m_isMouseOver(false)
{
}

QPixmapButton::QPixmapButton( QWidget * parent, const QPixmap& pixmap, int nStates, const QRect& buttonRect, bool vertical ):
    QAbstractButton( parent ),
    QMultiPixmapReferencer( pixmap ),
    m_Tag( 0 ),
    m_CaptionPressedXOffset( 0 ),
    m_CaptionPressedYOffset( 0 ),
    m_pTagOut( NULL ),
    m_useMousedState(false),
    m_isMouseOver(false)
{
    initPixmapButton( buttonRect, vertical, nStates );
}


QPixmapButton::QPixmapButton( QWidget * parent, const QString& file, int nStates, const QRect& buttonRect, bool vertical ):
    QAbstractButton( parent ),
    QMultiPixmapReferencer( file ),
    m_Tag( 0 ),
    m_CaptionPressedXOffset( 0 ),
    m_CaptionPressedYOffset( 0 ),
    m_pTagOut( NULL ),
    m_useMousedState(false),
    m_isMouseOver(false)
{
    initPixmapButton( buttonRect, vertical, nStates );
}





void QPixmapButton::init( const QPixmap& pixmap, int nStates,
    const QRect& buttonRect, bool vertical )
{
    m_useMousedState = false;
    setPixmap( pixmap );
    initPixmapButton( buttonRect, vertical, nStates );
}

void QPixmapButton::init( const QString& file, int nStates,
    const QRect& buttonRect, bool vertical )
{
    m_useMousedState = false;
    setPixmap( file );
    initPixmapButton( buttonRect, vertical, nStates );
}

void QPixmapButton::init( const QString& file1, const QString& file2,
    const QString& file3, const QString& file4 )
{
    m_useMousedState = false;
    releasePixmap();
    int ns = 2;
    setPixmap( 0, file1 );
    setPixmap( 1, file2 );
    if( file3.length() ){ setPixmap( 2, file3 ); ns = 3; }
    if( file4.length() ){ setPixmap( 3, file4 ); ns = 4; }
    initPixmapButton( QRect(), false, ns );
}

void QPixmapButton::init( const QPixmap* img1, const QPixmap* img2,
    const QPixmap* img3, QPixmap* img4 )
{
    m_useMousedState = false;
    releasePixmap();
    int ns = 2;
    setPixmap( 0, *img1 );
    setPixmap( 1, *img2 );
    if( img3!=NULL ){ setPixmap( 2, *img3 ); ns = 3; }
    if( img4!=NULL ){ setPixmap( 3, *img4 ); ns = 4; }
    initPixmapButton( QRect(), false, ns );
}

void QPixmapButton::initMoused( const QString& file1, const QString& file2, const QString& file5,
                const QString& file3, const QString& file4 )
{
    m_useMousedState = true;
    releasePixmap();
    int ns = 2;
    setPixmap( 0, file1 );
    setPixmap( 1, file2 );
    if( file5.length() ){ setPixmap( 2, file5 ); ns = 3; }
    if( file3.length() ){ setPixmap( 3, file3 ); ns = 4; }
    if( file4.length() ){ setPixmap( 4, file4 ); ns = 5; }
    initPixmapButton( QRect(), false, ns );
}

void QPixmapButton::initMoused( const QPixmap* img1, const QPixmap* img2, const QPixmap* img5,
                const QPixmap* img3, QPixmap* img4 )
{
    m_useMousedState = true;
    releasePixmap();
    int ns = 2;
    setPixmap( 0, *img1 );
    setPixmap( 1, *img2 );
    if( img5!=NULL ){ setPixmap( 2, *img5 ); ns = 3; }
    if( img3!=NULL ){ setPixmap( 3, *img3 ); ns = 4; }
    if( img4!=NULL ){ setPixmap( 4, *img4 ); ns = 5; }
    initPixmapButton( QRect(), false, ns );
}

void QPixmapButton::setCaptionPressedOffset( int dx, int dy )
{
    m_CaptionPressedXOffset = dx;
    m_CaptionPressedYOffset = dy;
    update();
}

void QPixmapButton::setShowCaption( bool show, int dx, int dy )
{
    setShowCaption( show );
    setCaptionPressedOffset( dx, dy );
}

void QPixmapButton::paintEvent( QPaintEvent* )
{
    // NOTE: QAbstractButton has no paintEvent() so we cannot fallback painting.
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );
    painter.setRenderHint( QPainter::TextAntialiasing );

    bool downed;
    if( isCheckable() )
        downed = isChecked();
    else
        downed = isDown();

    if( !IsMulti() ){
        //
        // Single-pixmap mode
        //
        if( pixmap()==NULL )
            return;
        QRect *srcRect; // Pointer to area of the pixmap which will be drawn
        if( downed ){
            if( isEnabled() )
                srcRect = &m_SrcRectOn;
            else
                srcRect = &m_SrcRectOnDisabled;
        }else{
            if( isEnabled() )
                srcRect = &m_SrcRectOff;
            else
                srcRect = &m_SrcRectOffDisabled;
        }
        painter.drawPixmap( rect(), *(pixmap()), *srcRect );
    }else{
        //
        // Multi-pixmap mode
        //
        // State order:
        //
        //    1. Released
        //    2. Pressed (default: same as 1)
        //    3. Released & disabled (default: same as 1)
        //    4. Pressed & disabled (default: same as 3, or as 1 if less than 3 states)
        //
        //    if MousedState is used ( initMoused(...) )
        //    1. Released
        //    2. Pressed (default: same as 1)
        //    3. Over ( Mouse is over ) & Released
        //    4. Released & disabled (default: same as 1)
        //    5. Pressed & disabled (default: same as 3, or as 1 if less than 3 states)
        //
        int idx;
        int np = NPixmaps();
        if( !m_useMousedState ) {
            if( downed ){
                if( isEnabled() ){ // Down, enabled
                    if( np>1 )
                        idx = 1; // Std down, enabled
                    else
                        idx = 0; // Fallback
                }else{
                    // Down, disabled
                    if( np>3 )
                        idx = 3; // Special down+disabled state
                    else if( np>2 )
                        idx = 2; // Released+disabled state
                    else if( np>1 )
                        idx = 1; // Pressed state
                    else
                        idx = 0; // Released state
                }
            }else{
                if( isEnabled() ){ // Up, enabled
                    idx = 0;
                }else{ // Up, disabled
                    if( np>2 )
                        idx = 2;
                    else
                        idx = 0;
                }
            }
        }
        else {
            if( downed ){
                if( isEnabled() ){ // Down, enabled
                    if( np>1 )
                        idx = 1; // Std down, enabled
                    else
                        idx = 0; // Fallback
                }else{
                    // Down, disabled
                    if( np>4 )
                        idx = 4; // Special down+disabled state
                    else if( np>3 )
                        idx = 3; // Released+disabled state
                    else if( np>1 )
                        idx = 1; // Pressed state
                    else
                        idx = 0; // Released state
                }
            }else{
                if( isEnabled() ){ // Up, enabled
                    if( m_isMouseOver )
                        idx = 2;
                    else
                        idx = 0;
                }else{ // Up, disabled
                    if( np>3 )
                        idx = 3;
                    else
                        idx = 0;
                }
            }
        }
        const QPixmap* pm = pixmap(idx);
        if( pm!=NULL )
            painter.drawPixmap( rect(), *pm, pm->rect() );
    }

    if( getShowCaption() ){

        // Font family and options
        painter.setFont( font() );

        // Font color
        QColor fontColor = palette().color(
            (isEnabled())?
                QPalette::Normal:
                QPalette::Disabled,
            QPalette::ButtonText );

        painter.setPen( fontColor );
        painter.setBrush( fontColor );

        // Text bounds
        QRect bounds;
        if( downed )
            bounds.setRect( 0+m_CaptionPressedXOffset, 0+m_CaptionPressedYOffset, width(), height() );
        else
            bounds.setRect( 0, 0, width(), height() );

        // Draw it!
        painter.drawText( bounds, Qt::AlignCenter | Qt::AlignVCenter, text() );
    }
}

void QPixmapButton::enterEvent(QEvent *)
{
    if( !m_useMousedState )
        return;
    m_isMouseOver = true;
    update();
}

void QPixmapButton::leaveEvent(QEvent *)
{
    if( !m_useMousedState )
        return;
    m_isMouseOver = false;
    update();
}

void QPixmapButton::initPixmapButton( const QRect& buttonRect, bool vertical, int nStates )
{
    //
    // Multi-pixmap mode
    //

    if( IsMulti() ){
        const QPixmap* pm = pixmap(0);
        if( pm!=NULL )
            resize( pm->width(), pm->height() );
        return;
    }


    //
    // Single-pixmap mode
    //

    if( pixmap()==NULL )
        return;
    QRect srcRect; // A rectangle around area of the pixmap occupiled by all button states
    if( buttonRect.width()<1 || buttonRect.height()<1 )
        srcRect = pixmap()->rect(); // All of the pixmap area is used by the button
    else
        srcRect = buttonRect; // Use only specified area of the pixmap
    // Determining locations of source images
    if( vertical ){
        //
        // Button states are in a column
        //
        int h2 = srcRect.height()/nStates; // Height of a sub-image
        m_SrcRectOff.setRect( srcRect.x(), srcRect.y(), srcRect.width(), h2 );
        if( nStates>1 ){
            m_SrcRectOn.setRect( srcRect.x(), srcRect.y()+h2, srcRect.width(), h2 );
            if( nStates>2 ){
                m_SrcRectOffDisabled.setRect( srcRect.x(), srcRect.y()+h2*2, srcRect.width(), h2 );
                if( nStates>3 )
                    m_SrcRectOnDisabled.setRect( srcRect.x(), srcRect.y()+h2*3, srcRect.width(), h2 );
                else
                    m_SrcRectOnDisabled = m_SrcRectOffDisabled;
            }else{
                m_SrcRectOnDisabled = m_SrcRectOn;
                m_SrcRectOffDisabled = m_SrcRectOff;
            }
        }else{
            m_SrcRectOn = m_SrcRectOff;
            m_SrcRectOnDisabled = m_SrcRectOff;
            m_SrcRectOffDisabled = m_SrcRectOff;
        }
    }else{
        //
        // Button states are in a row
        //
        int w2 = srcRect.width()/nStates; // Width of a sub-image
        m_SrcRectOff.setRect( srcRect.x(), srcRect.y(), w2, srcRect.height() );
        if( nStates>1 ){
            m_SrcRectOn.setRect( srcRect.x()+w2, srcRect.y(), w2, srcRect.height() );
            if( nStates>2 ){
                m_SrcRectOffDisabled.setRect( srcRect.x()+w2*2, srcRect.y(), w2, srcRect.height() );
                if( nStates>3 )
                    m_SrcRectOnDisabled.setRect( srcRect.x()+w2*3, srcRect.y(), w2, srcRect.height() );
                else
                    m_SrcRectOnDisabled = m_SrcRectOffDisabled;
            }else{
                m_SrcRectOnDisabled = m_SrcRectOn;
                m_SrcRectOffDisabled = m_SrcRectOff;
            }
        }else{
            m_SrcRectOn = m_SrcRectOff;
            m_SrcRectOnDisabled = m_SrcRectOff;
            m_SrcRectOffDisabled = m_SrcRectOff;
        }
    }
    resize( m_SrcRectOn.width(), m_SrcRectOn.height() );
}
