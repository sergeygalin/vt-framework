//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "vt_string.hpp"
#include "vt_messageboxes.hpp"
#include "QPixmapReferencer.h"

QPixmapReferencer::QPixmapReferencer():
    m_ExtPixmap( NULL ),
    m_MyPixmap( NULL )
{
}

QPixmapReferencer::QPixmapReferencer( const QPixmap& pm ):
    m_ExtPixmap( &pm ),
    m_MyPixmap( NULL )
{
}

QPixmapReferencer::QPixmapReferencer( const QString& fileName,
    const char * format, Qt::ImageConversionFlags flags ):
    m_ExtPixmap( NULL ),
    m_MyPixmap( new QPixmap( fileName, format, flags ) )
{
}

QPixmapReferencer::QPixmapReferencer( const QPixmapReferencer& src ):
    m_ExtPixmap( NULL ),
    m_MyPixmap( NULL )
{
    CopyFrom( src );
}

QPixmapReferencer& QPixmapReferencer::operator=( const QPixmapReferencer& src )
{
    CopyFrom( src );
    return *this;
}

void QPixmapReferencer::CopyFrom( const QPixmapReferencer& src )
{
    releasePixmap();
    if( src.m_ExtPixmap )
        m_ExtPixmap = src.m_ExtPixmap; // Make the same reference
    else{
        if( src.m_MyPixmap )
            // Create a copy of internal pixmap
            m_MyPixmap = new QPixmap( *(src.m_MyPixmap) );
    }
}

QPixmapReferencer::~QPixmapReferencer()
{
    releasePixmap();
}

void QPixmapReferencer::releasePixmap()
{
    if( m_MyPixmap!=NULL ){
        delete m_MyPixmap;
        m_MyPixmap = NULL;
    }
    m_ExtPixmap = NULL;
}

void QPixmapReferencer::setPixmap( const QPixmap& pm )
{
    VTASSERT( !pm.isNull() );
    releasePixmap();
    m_ExtPixmap = &pm;
}

void QPixmapReferencer::setPixmap( const QString& fileName, const char *format,
                                  Qt::ImageConversionFlags flags )
{
    releasePixmap();
    m_MyPixmap = new QPixmap( fileName, format, flags );
    VTASSERT2( !m_MyPixmap->isNull(), VT::CString().CopyFromQStr(fileName) );
}

const QPixmap* QPixmapReferencer::pixmap() const
{
    if( m_MyPixmap == NULL )
        return m_ExtPixmap;
    else
        return m_MyPixmap;
}















QMultiPixmapReferencer::QMultiPixmapReferencer():
    QPixmapReferencer(),
    m_Pixmaps()
{
}

QMultiPixmapReferencer::QMultiPixmapReferencer( const QPixmap& pm ):
    QPixmapReferencer( pm ),
    m_Pixmaps()
{
}

QMultiPixmapReferencer::QMultiPixmapReferencer( const QString& fileName, const char *format,
                                               Qt::ImageConversionFlags flags ):
    QPixmapReferencer( fileName, format, flags ),
    m_Pixmaps()
{
}

QMultiPixmapReferencer::~QMultiPixmapReferencer()
{
    releasePixmap();
}

bool QMultiPixmapReferencer::IsMulti() const
{
    if( m_Pixmaps.size() )
        return true;
    return false;
}

int QMultiPixmapReferencer::NPixmaps() const
{
    if( m_Pixmaps.size() )
        return m_Pixmaps.size();
    if( pixmap()!=NULL )
        return 1;
    return 0;
}

void QMultiPixmapReferencer::releasePixmap()
{
    QPixmapReferencer::releasePixmap();
    m_Pixmaps.clear();
}

void QMultiPixmapReferencer::setPixmap( const QPixmap& pm )
{
    // releasePixmap(); - called virtually by the inherited setPixmap().
    QPixmapReferencer::setPixmap( pm );
}

void QMultiPixmapReferencer::setPixmap( const QString& fileName, const char *format,
    Qt::ImageConversionFlags flags )
{
    // releasePixmap(); - called virtually by the inherited setPixmap().
    QPixmapReferencer::setPixmap( fileName, format, flags );
}

void QMultiPixmapReferencer::ResizePixmaps( int idx )
{
    if( idx<0 ){
        m_Pixmaps.clear();
        return;
    }
    QPixmapReferencer::releasePixmap();
    QPixmapReferencer stub;
    for( int i=m_Pixmaps.size(); i<=idx; i++ )
        m_Pixmaps.append( stub );
}

void QMultiPixmapReferencer::setPixmap( int idx, const QPixmap& pm )
{
    ResizePixmaps( idx );
    m_Pixmaps[idx].setPixmap( pm );
}

void QMultiPixmapReferencer::setPixmap( int idx, const QString& fileName, const char *format,
    Qt::ImageConversionFlags flags )
{
    ResizePixmaps( idx );
    m_Pixmaps[idx].setPixmap( fileName, format, flags );
}

void QMultiPixmapReferencer::setPixmaps( const QString& fileName1, const QString& fileName2 )
{
    releasePixmap();
    ResizePixmaps(1);
    m_Pixmaps[0].setPixmap( fileName1 );
    m_Pixmaps[1].setPixmap( fileName2 );
}

const QPixmap* QMultiPixmapReferencer::pixmap() const
{
    if( m_Pixmaps.size() )
        return m_Pixmaps.at(0).pixmap();
    return QPixmapReferencer::pixmap();
}

const QPixmap* QMultiPixmapReferencer::pixmap( int idx ) const
{
    if( m_Pixmaps.size()>idx )
        return m_Pixmaps.at(idx).pixmap();
    if( idx==0 )
        QPixmapReferencer::pixmap();
    return NULL;
}





