//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QMessageBox>
#include "QPixmapButtonBar.h"

QPixmapButtonBar::QPixmapButtonBar( QWidget *parent ):
    QGroupBox( parent ),
    QPixmapReferencer(),
    m_iButtons( 0 ),
    m_bDrawBox( false )
{
    initGroupBox();
}

QPixmapButtonBar::QPixmapButtonBar( QWidget * parent, const QPixmap& pixmap, int nButtons,
                                     int nStates, bool buttonsAreInVertical, bool onScreenVertical ):
    QGroupBox( parent ),
    QPixmapReferencer( pixmap ),
    m_iButtons( 0 ),
    m_bDrawBox( false )
{
    initGroupBox();
    initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
}


QPixmapButtonBar::QPixmapButtonBar( QWidget * parent, const QString& file, int nButtons,
                                     int nStates, bool buttonsAreInVertical, bool onScreenVertical ):
    QGroupBox( parent ),
    QPixmapReferencer( file ),
    m_iButtons( 0 ),
    m_bDrawBox( false )
{
    initGroupBox();
    initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
}

void QPixmapButtonBar::initGroupBox()
{
    setCheckable( false );
    setFlat( true ); // No border
    setTitle( "" );
    setAlignment( Qt::AlignLeft|Qt::AlignTop );
    setAutoFillBackground( false );
    setContentsMargins( 0, 0, 0, 0 );
}

void QPixmapButtonBar::init( const QPixmap& pixmap,
    int nButtons, int nStates,
    bool buttonsAreInVertical, bool onScreenVertical )
{
    setPixmap( pixmap );
    initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
}

void QPixmapButtonBar::init( const QString& file,
    int nButtons, int nStates,
    bool buttonsAreInVertical, bool onScreenVertical )
{
    setPixmap( file );
    initPixmapButtonBar( buttonsAreInVertical, onScreenVertical, nButtons, nStates );
}

QPixmapButtonBar::QPixmapButtonBar( QWidget * parent, const QPixmap& pixmap ):
    QGroupBox( parent ),
    QPixmapReferencer( pixmap ),
    m_iButtons( 0 ),
    m_bDrawBox( false )
{
    initGroupBox();
}

QPixmapButtonBar::QPixmapButtonBar( QWidget * parent, const QString& file ):
    QGroupBox( parent ),
    QPixmapReferencer( file ),
    m_iButtons( 0 ),
    m_bDrawBox( false )
{
    initGroupBox();
}

QPixmapButtonBar::~QPixmapButtonBar()
{
    // All sub elements deleted automatically
}

void QPixmapButtonBar::initPixmapButtonBar( bool buttonsAreInVertical, bool onScreenVertical, int nButtons, int nStates )
{
    int screendx, screendy, pixdx, pixdy, buttonwidth, buttonheight, buttonviswidth, buttonvisheight;
    m_iButtons = nButtons;
    if( buttonsAreInVertical ){
        buttonwidth = pixmap()->width();
        buttonheight = pixmap()->height() / nButtons;
        buttonviswidth = buttonwidth / nStates;
        buttonvisheight = buttonheight;
        pixdx = 0;
        pixdy = buttonheight;
    }else{
        buttonwidth = pixmap()->width() / nButtons;
        buttonheight = pixmap()->height();
        buttonviswidth = buttonwidth;
        buttonvisheight = buttonheight / nStates;
        pixdx = buttonwidth;
        pixdy = 0;
    }
    QRect screenRect( 0, 0, buttonviswidth, buttonvisheight ), pixRect( 0, 0, buttonwidth, buttonheight );
    if( onScreenVertical ){
        screendx = 0;
        screendy = buttonvisheight;
    }else{
        screendx = buttonviswidth;
        screendy = 0;
    }
    bool buttonStatesVertical = !buttonsAreInVertical;
    // Create and place the buttons
    for( int btn = 0; btn<nButtons; btn++ )
    {
        QPixmapButton* button = new QPixmapButton( this, *(pixmap()), nStates, pixRect, buttonStatesVertical );
        button->move( screenRect.x(), screenRect.y() );
        setupButton( button, btn );
        // Next button positions in pixmap / on screen
        if( btn+1<nButtons ){
            screenRect.moveTo( screenRect.x()+screendx, screenRect.y()+screendy );
            pixRect.moveTo( pixRect.x()+pixdx, pixRect.y()+pixdy );
        }
    }
    resize( screenRect.right(), screenRect.bottom() );
    update();
}

void QPixmapButtonBar::setupButton( QPixmapButton* button, int idx )
{
    button->setTag( idx );
    connect( button, SIGNAL(clicked()), this, SLOT(myRadioButtonClicked()) );
}

QPixmapButton* QPixmapButtonBar::getButton( int idx )
{
    if( idx<0 || idx>=QWidget::children().length() )
        return NULL;

    // Trying the child right under the index
    QObject* child = QWidget::children().at( idx );
    if( child->isWidgetType() ){
        QPixmapButton* btn = qobject_cast<QPixmapButton*>( child );
        if( btn!=NULL )
            if( btn->getTag()==idx ){
                // QMessageBox::warning( this, __FUNCTION__, "Got it right there!" );
                return btn;
            }
    }

    // Iterating through all children to find the one
    for( int children=0, button=0; children < QWidget::children().length(); children++ ){
        QObject* child = QWidget::children().at( children );
        if( child->isWidgetType() ){
            QPixmapButton* btn = qobject_cast<QPixmapButton*>( child );
            if( btn!=NULL ){
                if( idx==button ){
                    return btn;
                }
                button++;
            }
        }
    }
    return NULL;
}

const QPixmapButton* QPixmapButtonBar::getConstButton( int idx ) const
{
    return (const_cast<QPixmapButtonBar*>(this))->getButton( idx );
}


bool QPixmapButtonBar::isButtonChecked( int idx ) const
{
    const QPixmapButton* btn = getConstButton(idx);
    if( btn==NULL )
        return false;
    else
        return btn->isChecked();
}

bool QPixmapButtonBar::isButtonCheckable( int idx ) const
{
    const QPixmapButton* btn = getConstButton(idx);
    if( btn==NULL )
        return false;
    else
        return btn->isCheckable();
}

void QPixmapButtonBar::setButtonChecked( int idx, bool checked  )
{
    QPixmapButton* btn = getButton( idx );
    if( btn==NULL )
        throw;
    if( btn->isCheckable() )
        btn->setChecked( checked );
    else
        throw;
}

bool QPixmapButtonBar::isButtonAutoExclusive( int idx ) const
{
    const QPixmapButton* btn = getConstButton(idx);
    if( btn==NULL )
        return false;
    else
        return btn->autoExclusive();
}

void QPixmapButtonBar::setButtonAutoExclusive( int idx, bool aexc )
{
    QPixmapButton* btn = getButton( idx );
    if( btn==NULL )
        throw;
    btn->setAutoExclusive( aexc );
}


bool QPixmapButtonBar::isButtonEnabled( int idx ) const
{
    const QPixmapButton* btn = getConstButton(idx);
    if( btn==NULL )
        return false;
    else
        return btn->isEnabled();
}

void QPixmapButtonBar::setButtonEnabled( int idx, bool enable )
{
    QPixmapButton* btn = getButton( idx );
    if( btn==NULL )
        throw;
    else
        btn->setEnabled( enable );
}

void QPixmapButtonBar::connectButtonClicked( int idx, const QObject* receiver, const char* member )
{
    QPixmapButton* btn = getButton( idx );
    if( btn==NULL )
        throw;
    connect( btn, SIGNAL(clicked()), receiver, member );
}

void QPixmapButtonBar::paintEvent( QPaintEvent* event )
{
    if( m_bDrawBox )
        QGroupBox::paintEvent( event );
}
