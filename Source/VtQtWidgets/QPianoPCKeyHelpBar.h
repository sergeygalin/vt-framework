//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A control designed to be used together with QPiano, typically placed
// above QPiano and containing PC keyboard key hints.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QPIANOPCKEYHELPBAR_H
#define QPIANOPCKEYHELPBAR_H

#include <QtGui/QWidget>
#include <QtGui/QColor>
#include <QtCore/QString>
#include "vt_portable.hpp"

class QPianoPCKeyHelpBar: public QWidget
{
    Q_OBJECT
public:
    QPianoPCKeyHelpBar( QWidget* parent = NULL );
    virtual ~QPianoPCKeyHelpBar();

    void init( BYTE start, BYTE nkeys );

    Q_PROPERTY( QColor fontColor READ getFontColor WRITE setFontColor );
    QColor getFontColor() const { return m_FontColor; }
    void setFontColor( QColor c ){ m_FontColor = c; }

    Q_PROPERTY( BYTE startingKey READ getStartingKey WRITE setStartingKey );
    BYTE getStartingKey() const { return m_StartingKey; }
    void setStartingKey( BYTE k ){ init( k, getNKeys() ); }

    Q_PROPERTY( BYTE nKeys READ getNKeys WRITE setNkeys );
    BYTE getNKeys() const { return m_NKeys; }
    void setNkeys( BYTE n ){ init( getStartingKey(), n ); }

    Q_PROPERTY( bool translateLabels READ getTranslateLabels WRITE setTranslateLabels );
    bool getTranslateLabels() const { return m_EnableTranslation; }
    void setTranslateLabels( bool t ){ m_EnableTranslation = t; createLabels(); }

    Q_PROPERTY( int transposing READ getTransposing WRITE setTransposing );
    int getTransposing() const { return m_Transposing; }
    void setTransposing( int t ){ m_Transposing = t; createLabels(); }


protected:
    virtual void paintEvent( QPaintEvent* ev );
    static QString translateKeyName( char k );

private:
    QColor m_FontColor;
    BYTE m_StartingKey, m_NKeys;
    QString m_Labels[128];
    int m_Transposing;

    bool m_EnableTranslation;
    void init();
    void createLabels();
};

#endif

