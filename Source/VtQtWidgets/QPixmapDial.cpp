//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include "QPixmapDial.h"

QPixmapDial::QPixmapDial( QWidget *parent ):
    QDial( parent ),
    QPixmapReferencer(),
    m_PixType( kDefault ),
    m_ZeroAngle( -150.0 ),
    m_MaxAngle( 150.0 ),
    m_VirtualSlider( true ),
    m_VirtualVertical( true ),
    m_Tracking( false ),
    m_DownPos( 0 ),
    m_VirtualHeight( 100 ),
    m_NFrames( 64 )
{
    setWrapping( false );
}


QPixmapDial::~QPixmapDial()
{
}

void QPixmapDial::init( const QString& pictureSrc, TDialPixType type )
{
    setPixmap( pictureSrc );
    m_PixType = type;
    setAutoNFrames();
    update();
}

void QPixmapDial::init( const QPixmap& pixmap, TDialPixType type )
{
    setPixmap( pixmap );
    m_PixType = type;
    setAutoNFrames();
    update();
}

void QPixmapDial::setZeroAngle( qreal angle )
{
    m_ZeroAngle = angle;
    update();
}

void QPixmapDial::setMaxAngle( qreal angle )
{
    m_MaxAngle = angle;
    update();
}

void QPixmapDial::setAngles( qreal startAngle, qreal maxAngle )
{
    m_ZeroAngle = startAngle;
    m_MaxAngle = maxAngle;
    update();
}

void QPixmapDial::setNFrames( int nf )
{
    m_NFrames = nf;
    update();
}

void QPixmapDial::setAutoNFrames()
{
    if( pixmap()==NULL )
        return;
    if( pixmap()->height()<1 || pixmap()->width()<1 )
        return;
    switch( m_PixType ){
        case kAutoFilm:
            {
                int min, max;
                if( pixmap()->height() > pixmap()->width() ){
                    max = pixmap()->height();
                    min = pixmap()->width();
                }else{
                    min = pixmap()->height();
                    max = pixmap()->width();
                }
                m_NFrames = max/min;
                return;
            }
        case kVerticalFilm:
            m_NFrames = pixmap()->height() / pixmap()->width();
            return;
        case kHorizontalFilm:
            m_NFrames = pixmap()->width() / pixmap()->height();
            return;
        default:
            break;
    }
}

void QPixmapDial::paintEvent(QPaintEvent* event)
{
    if( pixmap()!=NULL ){
        QPainter painter( this );
        painter.setRenderHint( QPainter::Antialiasing );
        painter.setRenderHint( QPainter::SmoothPixmapTransform );
        painter.setRenderHint( QPainter::HighQualityAntialiasing );

        QRect destRect( rect() );

        // Convert value to [0..1] range
        qreal fvalue = qreal(value()-minimum())/qreal(maximum()-minimum());

        // Calculate handle position
        qreal angle = m_ZeroAngle + fvalue*(m_MaxAngle-m_ZeroAngle);

        // Detect dial drawing type, if that's set to "auto"
        TDialPixType typ;
        if( m_PixType==kAutoFilm ){
            if( pixmap()->height() > pixmap()->width() )
                typ = kVerticalFilm;
            else
                typ = kHorizontalFilm;
        }else
            typ = m_PixType;

        // Draw it
        switch( typ ){
            case kSimple:
                {
                    painter.translate( width()/2, height()/2 );
                    painter.rotate( qreal(angle) );
                    destRect.moveTopLeft( QPoint(-width()/2, -height()/2) );
                    painter.drawPixmap( destRect, *(pixmap()), pixmap()->rect() );
                    return;
                }
            case kVerticalFilm:
                {
                    int frameH = pixmap()->height()/m_NFrames;
                    float fframe = qreal( value()-minimum() ) / qreal( maximum()-minimum() ) * qreal(m_NFrames-1);
                    int frame = int( fframe + 0.4999999999999999999999 );
                    if( frame<0 )
                        frame = 0;
                    else if( frame>=m_NFrames )
                        frame = m_NFrames-1;
                    QRect srcRect( 0, frameH*frame, pixmap()->width(), frameH );
                    painter.drawPixmap( destRect, *(pixmap()), srcRect );
                    return;
                }
            case kHorizontalFilm:
                {
                    int frameW = pixmap()->width()/m_NFrames;
                    float fframe = qreal( value()-minimum() ) / qreal( maximum()-minimum() ) * qreal(m_NFrames-1);
                    int frame = int( fframe + 0.4999999999999999999999 );
                    if( frame<0 )
                        frame = 0;
                    else if( frame>=m_NFrames )
                        frame = m_NFrames-1;
                    QRect srcRect( frameW*frame, 0, frameW, pixmap()->height() );
                    painter.drawPixmap( destRect, *(pixmap()), srcRect );
                    return;
                }

            case kBackAndArrow:
                {
                    QRect srcRect( 0, 0, pixmap()->width(), pixmap()->height()/2 );
                    painter.drawPixmap( destRect, *(pixmap()), srcRect );
                    srcRect.moveTop( pixmap()->height()/2 );
                    // Transforming target coordinates
                    painter.translate( width()/2, height()/2 );
                    painter.rotate( qreal(angle) );
                    destRect.moveTopLeft( QPoint(-width()/2, -height()/2) );
                    painter.drawPixmap( destRect, *(pixmap()), srcRect );
                    return;
                }
            default:
                break;
        } // By default, falls back to QDial's painter.
    }
    QDial::paintEvent( event );
}

void QPixmapDial::mousePressEvent(QMouseEvent *me)
{
    if( m_VirtualSlider ){
        if( me->button()==Qt::LeftButton ){
            /*
                From Qt manual:

                It is almost never necessary to grab the mouse when using Qt,
                as Qt grabs and releases it sensibly. In particular, Qt grabs the mouse
                when a mouse button is pressed and keeps it until the last
                button is released.
            */
            if( m_VirtualVertical )
                setCursor( Qt::SizeVerCursor );
            else
                setCursor( Qt::SizeHorCursor );
            m_DownPos = (m_VirtualVertical)? me->pos().y(): me->pos().x();
            m_DownValue = value();
            m_Tracking = true;
        }
    }else
        QDial::mousePressEvent( me );
}

void QPixmapDial::mouseReleaseEvent(QMouseEvent *me)
{
    if( m_VirtualSlider ){
        if( me->button()==Qt::LeftButton || me->buttons()==0 ){
            setCursor( Qt::ArrowCursor );
            m_Tracking = false;
        }
    }else
        QDial::mouseReleaseEvent( me );
}

void QPixmapDial::mouseMoveEvent(QMouseEvent *me)
{
    if( m_VirtualSlider ){
        if( m_Tracking ){
            int pos = (m_VirtualVertical)? me->pos().y(): me->pos().x();
            int mshift = pos - m_DownPos;
            qreal fmshift = qreal( mshift ) / qreal( m_VirtualHeight );
            int newvalue = m_DownValue - fmshift * (maximum()-minimum());
            setValue( newvalue );
        }
    }else
        QDial::mouseMoveEvent( me );
}


