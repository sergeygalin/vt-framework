//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    An extremely powerful vector-graphic UI piano keyboard control.

    Some features to note:

    MOUSE PLAYING

    The keyboard emits Note On, Note Off and Poly Pressure messages as mouse
    clicks it and moves over it.


    STAND-ALONE MODE

    In this mode, QPiano does not need to be linked to a synth which would reflect
    which keys are down or up. Keys which are pressed are rendered as down
    for a short period.


    CURSOR CONTROL MODE

    To enable cursor mode:
    1) Change widget's focus policy to make it focuseable, e.g.:
    ui.piano->setFocusPolicy( Qt::StrongFocus );
    2) Call QPiano::setEnableCursor( true ).


    COLORED KEY MODE

    Keys can be colored using VT::Piano::CPianoColors.


*/

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#if !defined(QPIANO_H)
#define QPIANO_H

#include <QtGui/QApplication>
#include <QtGui/QPaintEvent>
#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include <QtGui/QFont>
#include "vt_piano.hpp"



class QPiano: public QWidget, protected VT::Piano::CKeyboardPosCalc, public VT::Piano::CKeyboardStatePoll
{
    Q_OBJECT
public:

    static const QColor
        kDefaultWhiteColor,
        kDefaultBlackColor,
        kDefaultBassColor,
        kDefaultWhiteCursorColor,
        kDefaultBlackCursorColor;

    QPiano( QWidget* parent = 0 );
    virtual ~QPiano();

    virtual void setKeyRange( BYTE startingNote, BYTE nNotes ); // See also: SetNoteRange() !!!
    virtual void SetBlackKeyWidthK( double k );
    virtual void copyState( const VT::Piano::CKeyboardStatePoll& state );

    //
    // Piano orientation - horizontal or vertical
    //
    Q_PROPERTY( bool vertical READ isVertical WRITE setVertical );
    bool isVertical() const { return m_bVertical; }
    virtual void setVertical( bool vert );

    // ********************************************************************************
    // PIANO COLORS
    // ********************************************************************************

    // Base white key color
    Q_PROPERTY( QColor whiteColor READ getWhiteColor WRITE setWhiteColor );
    QColor getWhiteColor() const { return m_WhiteColor; }
    void setWhiteColor( QColor c ){ if( m_WhiteColor != c ){ m_WhiteColor = c; update(); } }

    // In split mode, base white key color for left (bass) side keys;
    // whiteColor is used for right (melody) side keys.
    Q_PROPERTY( QColor bassColor READ getBassColor WRITE setBassColor );
    QColor getBassColor() const { return m_BassColor; }
    void setBassColor( QColor c ){ if( m_BassColor != c){ m_BassColor = c; update(); } }

    // Base black key color
    Q_PROPERTY( QColor BlackColor READ getBlackColor WRITE setBlackColor );
    QColor getBlackColor() const { return m_BlackColor; }
    void setBlackColor( QColor c ){ if( m_BlackColor != c ){ m_BlackColor = c; update(); } }

    // In cursor mode, color of a pointed black key
    Q_PROPERTY( QColor BlackCursorColor READ getBlackCursorColor WRITE setBlackCursorColor );
    QColor getBlackCursorColor() const { return m_BlackCursorColor; }
    void setBlackCursorColor( QColor c ){ if( m_BlackCursorColor != c ){ m_BlackCursorColor = c; update(); } }

    // In cursor mode, color of a pointed white key
    Q_PROPERTY( QColor WhiteCursorColor READ getWhiteCursorColor WRITE setWhiteCursorColor );
    QColor getWhiteCursorColor() const { return m_WhiteCursorColor; }
    void setWhiteCursorColor( QColor c ){ if( m_WhiteCursorColor != c ){ m_WhiteCursorColor = c; update(); } }

    // A coefficient which multiplies darkening of pressed keys.
    // By default, it is 1.0.
    // Zero will cause pressed keys to have the same brightness as depressed.
    // Values greater than 1.0 will cause them to be darker than by default.
    Q_PROPERTY( qreal PressedKeyDarkener READ getPressedKeyDarkener WRITE setPressedKeyDarkener );
    qreal getPressedKeyDarkener() const { return m_PressedKeyDarkener; }
    void setPressedKeyDarkener( qreal d ){ if( m_PressedKeyDarkener != d ){ m_PressedKeyDarkener = d; update(); } }

    //
    // Key coloring palette
    //
    // NOTE: colors from palette are not used "as is" - they are combined
    // with some some tinting, darkening for black keys, shadowing, and so on.
    //
    const VT::Piano::CPianoColors* GetPianoColors() const { return m_pColors; }
    void SetPianoColors( const VT::Piano::CPianoColors* colors );
    void SetRainbowColors();    // Set coloring using rainbow colors; black keys are not colored.
    void SetScriabinColors();    // Set coloring using standard Scriabin's palette (colors all keys).
    void SetNoColors();            // Remove coloring (essentially the same as SetPianoColors(NULL)).

    // Get resulting base colors of keys (do not take cursor state/position into account).
    // Uses control's coloring properties and current key coloring scheme.
    QColor getWhiteKeyColor( BYTE key ) const;
    QColor getBlackKeyColor( BYTE key ) const;

    // Set default key colors.
    // NOTE: this doesn't remove coloring palette - use SetNoColors() for that.
    void setDefaultKeyColors();


    // ********************************************************************************
    // NOTE NAME DISPLAY
    // ********************************************************************************

    // Show note names?
    Q_PROPERTY( bool noteNames READ getNoteNames WRITE setNoteNames );
    bool getNoteNames() const { return m_NoteNames; }
    void setNoteNames( bool visible ){ m_NoteNames = visible; update(); }

    // In dual mode, it is possible to show secondary instrument's notes separately
    // (in case they are transposed relatively to primary instrument's notes).
    Q_PROPERTY( bool secondaryNoteNames READ getSecondaryNoteNames WRITE setSecondaryNoteNames );
    bool getSecondaryNoteNames() const { return m_SecondaryNoteNames; }
    void setSecondaryNoteNames( bool visible ){ if( m_SecondaryNoteNames != visible ){ m_SecondaryNoteNames = visible; update(); } }

    // When note names are displayed, octave numbers can be displayed as 0..11
    // (Sonar-style). By default, octaves are -1..10 as in General MIDI standard.
    Q_PROPERTY( bool octaves_0_11 READ getOctaves_0_11 WRITE setOctaves_0_11 );
    bool getOctaves_0_11() const { return m_Octaves_0_11; }
    void setOctaves_0_11( bool o011 ){ if( m_Octaves_0_11 != o011 ){ m_Octaves_0_11 = o011; update(); } }

    // Customize the font used to display note names.
    // NOTE: size of the font is ignored (it is adjusted automatically to fit the key).
    Q_PROPERTY( QFont noteNameFont READ getNoteNameFont WRITE setNoteNameFont );
    QFont getNoteNameFont() const { return m_NoteNameFont; }
    void setNoteNameFont( QFont font ){ m_NoteNameFont = font; if( getNoteNames() ) update(); }

    // How to display note text.
    enum TNoteText {
        kNamesOnly,            // Default: keys are marked with note names, like: "C4", "D4", "E4" and so on.
        kNumbersOnly,        // Keys are marked with note numbers: "64", "65", "66"...
        kNamesAndNumbers    // Names and numbers below them. NOTE: this function may not work as expected
                            // if secondaryNoteNames is set to true!
    };
    Q_PROPERTY( TNoteText noteText READ getNoteText WRITE setNoteText );
    TNoteText getNoteText() const { return m_NoteText; }
    void setNoteText( TNoteText tx ){ if( tx != m_NoteText ){ m_NoteText = tx; update(); } }



    //
    // Transposing note names
    //
    // !!!!!!!!!!!!!! At the moment, only the displayed labels are transposed.
    // it is not possible to transpose key numbers in events sent or received by the keyboard.
    Q_PROPERTY( int keyNamesTransposing READ getKeyNamesTransposing WRITE setKeyNamesTransposing );
    int getKeyNamesTransposing() const { return m_Transpose; }
    void setKeyNamesTransposing( int t ){ if( m_Transpose != t){ m_Transpose = t; update(); } }
    Q_PROPERTY( int bassKeyNamesTransposing READ getBassKeyNamesTransposing WRITE setBassKeyNamesTransposing );
    int getBassKeyNamesTransposing() const { return m_BassTranspose; }
    void setBassKeyNamesTransposing( int t ){ if( m_BassTranspose != t ){ m_BassTranspose = t; update(); } }
    Q_PROPERTY( int secondaryKeyNamesTransposing READ getSecondaryKeyNamesTransposing WRITE setSecondaryKeyNamesTransposing );
    int getSecondaryKeyNamesTransposing() const { return m_SecondaryTranspose; }
    void setSecondaryKeyNamesTransposing( int t ){ if( m_SecondaryTranspose != t ){ m_SecondaryTranspose = t; update(); } }



    // ********************************************************************************
    // FINE PRINT DISPLAY
    // ********************************************************************************

    // Enable / disable use of fine print
    Q_PROPERTY( bool finePrint READ getFinePrint WRITE setFinePrint );
    bool getFinePrint() const { return m_FinePrint; }
    void setFinePrint( bool fp );

    Q_PROPERTY( bool finePrintVertically READ getFinePrintVertically WRITE setFinePrintVertically );
    bool getFinePrintVertically() const { return m_FinePrintVertically; }
    void setFinePrintVertically( bool vertical );

    // Customize the font used to display fine print.
    Q_PROPERTY( QFont finePrintFont READ getFinePrintFont WRITE setFinePrintFont );
    QFont getFinePrintFont() const { return m_FinePrintFont; }
    void setFinePrintFont( QFont font ){ m_FinePrintFont = font; if( getFinePrint() ) update(); }

    // Get/set fine print
    const QString& finePrintText( int idx ) const { return m_FinePrintTexts[idx]; }
    void setFinePrintText( int idx, const QString& text ){ m_FinePrintTexts[idx] = text; if( getFinePrint() ) update(); }
    void clearAllFinePrintText();


    // ********************************************************************************
    // SPLIT KEYBOARD (LEFT SIDE FOR BASS, RIGHT SIDE FOR SOLO)
    // ********************************************************************************

    // Enable split mode by setting the number from which primary
    // part of keyboard starts. By default it's 0, i.e. all keyboard
    // is the primary part. Any keys on left to the key are "bass part".
    Q_PROPERTY( BYTE splitMainPartStartKey READ getSplitMainPartStartKey WRITE setSplitMainPartStartKey );
    BYTE getSplitMainPartStartKey() const { return m_SplitMainPartStartKey; }
    void setSplitMainPartStartKey( BYTE key );

    // Does the key belong to "bass" part of the keyboard?
    bool IsBassKey( BYTE key ) const { return key < m_SplitMainPartStartKey; }






    //
    // Update key state using synth poll.
    // These functions are inherited from VT::Piano::CKeyboardStatePoll.
    //
    virtual void Clear();
    virtual void Press( BYTE note );
    virtual void Release( BYTE note );
    virtual void EndSynthStatePoll();

    //
    // Wrapping CKeyboardPosCalc functions
    //
    // Note: width parameter is ignored - taken from QWidget size
    virtual void Init( BYTE startingNote, BYTE nNotes, int width=-1 );
    virtual void CopyFrom( const CKeyboardPosCalc& src );
    //virtual void SetNoteRange( BYTE minNote, BYTE maxNote ); // See also: SetKeyRange() !!!

    // Light direction control
    Q_PROPERTY( bool leftLight READ LeftLight WRITE SetLeftLight );
    bool LeftLight() const { return m_LeftLight; }
    void SetLeftLight( bool ll ){ m_LeftLight = ll; update(); }

    // Draw shadows or not?
    Q_PROPERTY( bool shadows READ getShadows WRITE setShadows );
    bool getShadows() const { return m_Shadows; }
    void setShadows( bool drawShadows ){ m_Shadows = drawShadows; update(); }

    Q_PROPERTY( bool standalone READ getStandalone WRITE setStandalone );
    bool getStandalone() const { return m_Standalone; }
    void setStandalone( bool sa );

    Q_PROPERTY( qreal blackKeyLengthK READ getBlackKeyLengthK WRITE setBlackKeyLengthK );
    qreal getBlackKeyLengthK() const { return m_fBlackKeyLengthK; }
    void setBlackKeyLengthK( qreal k ){ m_fBlackKeyLengthK = k; update(); }

    // Find which key is located under the given GUI coordinates (x, y).
    // If no key found, returns VT::MIDI::kErrorNote.
    BYTE testKey( qreal x, qreal y ) const;
    BYTE getLastKey() const { return m_LastPressedKey; }
    void flushLastKey(){ m_LastPressedKey = VT::MIDI::kErrorNote; }

    bool cursorVisible() const;
    BYTE getCursor() const { return m_Cursor; }
    void setCursor( BYTE pos );

    Q_PROPERTY( bool enableCursor READ getEnableCursor WRITE setEnableCursor );
    bool getEnableCursor() const { return m_bEnableCursor; }
    void setEnableCursor( bool e ){ m_bEnableCursor = e; update(); }

signals:
    void geometryChanged();

    // Mouse playing.
    void keyPressed( BYTE key ); // For old code compatibility
    void keyPressed( BYTE key, BYTE velocity ); // Takes into account click position (as velocity)
    void keyAfterTouch( BYTE key, BYTE velocity ); // When mouse moves while pressed down, generate after-touch messages
    void keyReleased( BYTE key );
    void cursorChanged( BYTE key );

protected:
    QFont m_NoteNameFont, m_FinePrintFont;

    virtual void paintEvent( QPaintEvent *ev );
    virtual void resizeEvent( QResizeEvent *ev );
    virtual void mousePressEvent( QMouseEvent *me );
    virtual void mouseReleaseEvent( QMouseEvent *me );
    virtual void mouseMoveEvent( QMouseEvent *me );
    virtual void leaveEvent( QEvent *ev );
    virtual void keyPressEvent( QKeyEvent *ev );
    virtual void keyReleaseEvent( QKeyEvent *ev );
    virtual void showEvent( QShowEvent *ev );
    virtual void hideEvent( QHideEvent *ev );
    virtual void closeEvent( QCloseEvent *ev );
    virtual void focusInEvent( QFocusEvent *ev );
    virtual void focusOutEvent( QFocusEvent *ev );

    void ReleaseKey();

    qreal getWhiteKeyLength() const;
    qreal getBlackKeyLength() const;
    qreal getKeyboardLength() const;

    // NOTE: All drawing functions work as if keyboard was oriented
    // horizontally from left to right. If the keyboard is placed
    // in another direction then paintEvent() does the necessary
    // coordinate system transformation in 'painter' prior
    // to drawing the keys.
    virtual void drawWhiteKey( QPainter& painter, BYTE key );
    virtual void drawBlackKey( QPainter& painter, BYTE key );
    virtual void drawNoteName( QPainter& painter, BYTE keyNumber, BYTE note,
        double labelHeightFraction, double labelPosFraction, bool asNumber );
    virtual void drawFinePrint( QPainter& painter, BYTE key );

    // Re-calculate key positions (necessary after
    // widget geometry or key range has been changed).
    // A descendant class may override this function to catch
    // such situations (don't forget to pass the call
    // to the inherited function).
    virtual void CalcKeyRects();
    virtual void CalcFinePrintRects();

    qreal getNoteNameLabelYK() const { return 0.78; }
    qreal getNoteNameLabel2YK() const { return 0.89; }


    // Access to pre-calculated key positions from descendant classes.
    const QRectF& getKeyRect( BYTE idx ) const { return m_Rects[idx]; }

    // Velocity of notes can depend on place where user clicked
    // on screen keys.
    BYTE VelocityFromClickPos( int x, int y, BYTE key ) const;


    void updateUI(){
        update();
        //QApplication::instance()->postEvent( this, new QPaintEvent( rect() ) );
    }

private:
    QTimer m_Timer;
    bool m_bVertical, m_LeftLight, m_Shadows, m_NoteNames, m_FinePrint, m_FinePrintVertically,
        m_SecondaryNoteNames, m_Octaves_0_11, m_Standalone;
    int m_Transpose, m_BassTranspose, m_SecondaryTranspose;
    TNoteText m_NoteText;

    qreal m_PressedKeyDarkener;        // Makes pressed keys darker than normally
    QColor
        m_WhiteColor,                // Base color of white keys
        m_BlackColor,                // Base color of black keys
        m_BassColor,                // In split mode, white keys at left side of the keyboard
        m_BlackCursorColor,            // When QPiano is focuseable, that's current black key color
        m_WhiteCursorColor;            // When QPiano is focuseable, that's current white key color

    QRectF m_Rects[128];            // Cache of key positions
    QRectF m_FinePrintRects[128];    // Cache of fine print label positions

    BYTE m_LastPressedKey;            // Used by mouse playing
    BYTE m_LastSentVelocity;        // Used by mouse after-touch system to avoid extra MIDI events
    BYTE m_SplitMainPartStartKey;    // Main part of keyboard starting position (default: 0)
    BYTE m_Cursor;
    qreal m_fBlackKeyLengthK;
    bool m_bEnableCursor;            // Enable cursor mode, i.e. when one key is selected and
                                    // user can move the selection and trigger they key vis Space.

    const VT::Piano::CPianoColors* m_pColors; // Key coloring system

    int m_KeyDownCounter[128];        // Used in stand-alone mode to track for how long the keys
                                    // were shown as pressed.

    QString m_FinePrintTexts[128];  // Fine print texts ;-)

    void ResetKeyCounters();


private slots:
    void onTimer();

};

#endif


