//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// An extension of QTableWidget which adds some functionality, like additional
// signals.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QVTTABLEWIDGET_H
#define QVTTABLEWIDGET_H

#include <QtGui/QTableWidget>
#include <QtGui/QKeyEvent>

/*

    Additional functionality:

      + Event on arrow key movements
      + Additional hooks

*/
class QVTTableWidget: public QTableWidget
{
    Q_OBJECT
public:

    QVTTableWidget( QWidget* parent = 0 );
    virtual ~QVTTableWidget();

    Q_PROPERTY( bool hookCopyPasteShortcuts READ getHookCopyPasteShortcuts WRITE setHookCopyPasteShortcuts );
    bool getHookCopyPasteShortcuts() const { return m_HookCopyPasteShortcuts; }
    void setHookCopyPasteShortcuts( bool hook ){ m_HookCopyPasteShortcuts = hook; }

protected:
    bool m_HookCopyPasteShortcuts;

    virtual void keyPressEvent( QKeyEvent* evt );

signals:
    // Emitted when user uses keyboard keys to change selection
    void keyboardMovement();

    // Emitted when hookCopyPasteShortcuts is true and user presses
    // standard clipboard shortcuts for these actions.
    void copy();
    void copy(bool);
    void cut();
    void cut(bool);
    void paste();
    void paste(bool);


};






#endif


