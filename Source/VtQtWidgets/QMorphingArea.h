//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef QMORPHINGAREA_H
#define QMORPHINGAREA_H

#include <QtGui/QWidget>
#include <QtGui/QPainter>
#include <QtGui/QPixmap>
#include <QtGui/QCursor>
#include "QPixmapReferencer.h"
#include "vt_arrays.hpp"

class QMorphingArea: public QWidget
{
    Q_OBJECT
public:

    enum TDragMode
    {
        kDragNone,
        kDragMove,
        kDragSzLeft,
        kDragSzRight,
        kDragSzTop,
        kDragSzBottom,
        kDragSzX1Y1,
        kDragSzX1Y2,
        kDragSzX2Y1,
        kDragSzX2Y2,
        kDragHandle
    };

    class TFlashPoint
    {
    public:
        TFlashPoint(): m_X(-1.0), m_Y(-1.0){} // Invalid point
        TFlashPoint( qreal x, qreal y ): m_X(x), m_Y(y){}
        TFlashPoint( const TFlashPoint& src ): m_X(src.m_X), m_Y(src.m_Y){}
        TFlashPoint& operator=( const TFlashPoint& src ){ m_X = src.m_X; m_Y = src.m_Y; return *this; }
        qreal x() const { return m_X; }
        qreal y() const { return m_Y; }
        bool isValid() const { return m_X>=0.0 && m_X<=1.0 && m_Y>=0 && m_Y<=1.0; }
    protected:
        qreal m_X, m_Y;
    };

    QMorphingArea( QWidget* parent=0 );
    virtual ~QMorphingArea();

    // Area functions
    qreal getRawLeft() const { return m_Left; } // Get left side of area (maybe be out of range)
    qreal getRawRight() const { return m_Right; } // Get right side of area (maybe be out of range)
    qreal getRawTop() const { return m_Top; } // Get top side of area (maybe be out of range)
    qreal getRawBottom() const { return m_Bottom; } // Get bottom side of area (maybe be out of range)

    qreal getLeft() const;        // Get left side of area (clipped to [0..1])
    qreal getRight() const;        // Get right side of area (clipped to [0..1])
    qreal getTop() const;        // Get top side of area (clipped to [0..1])
    qreal getBottom() const;    // Get bottom side of area (clipped to [0..1])
    qreal getAreaX() const;        // Get X of center of the area
    qreal getAreaY() const;        // Get Y of center of the area

    // Moving the area. If emitIt is true, the function will
    // emit areaChanged() event after validating the coordinates.
    void setArea( qreal left, qreal top, qreal right, qreal bottom, bool emitIt = true );

    // Handle functions
    qreal getX() const { return m_X; }
    qreal getY() const { return m_Y; }
    void setXY( qreal x, qreal y, bool emitIt = true );

    // Get current dragging state
    TDragMode getCurrentDrag() const { return m_DragMode; }

    // Flash points
    void addFlashPoint( const TFlashPoint& point, bool update );
    void clearFlashPoints( bool update );

    // Abort current dragging
    virtual void stopDrag();

    // Colors
    Q_PROPERTY( QColor backgroundColor READ getBackgroundColor WRITE setBackgroundColor );
    QColor getBackgroundColor() const { return m_BgColor; }
    void setBackgroundColor( QColor color ){ m_BgColor=color; update(); }

    Q_PROPERTY( QColor areaColor READ getAreaColor WRITE setAreaColor );
    QColor getAreaColor() const { return m_AreaColor; }
    void setAreaColor( QColor color ){ m_AreaColor=color; update(); }

    Q_PROPERTY( QColor lineColor READ getLineColor WRITE setLineColor );
    QColor getLineColor() const { return m_LineColor; }
    void setLineColor( QColor color ){ m_LineColor=color; update(); }

    // Margins
    int getMarginLeft() const { return m_MarginLeft; }
    int getMarginRight() const { return m_MarginRight; }
    int getMarginTop() const { return m_MarginTop; }
    int getMarginBottom() const { return m_MarginBottom; }
    void setMargins( int left, int top, int right, int bottom );

    // Visible parts
    Q_PROPERTY( bool areaCenterVisible READ getAreaCenterVisible WRITE setAreaCenterVisible );
    bool getAreaCenterVisible() const { return m_AreaCenterVisible; }
    void setAreaCenterVisible( bool vis ){ m_AreaCenterVisible = vis; update(); }

    Q_PROPERTY( bool handleVisible READ getHandleVisible WRITE setHandleVisible );
    bool getHandleVisible() const { return m_HandleVisible; }
    void setHandleVisible( bool vis ){ m_HandleVisible = vis; update(); }


    //
    // Pixmaps
    //
    void setPixmapBackground( QString fileName, bool resizeSelf ); // Widget background
    void setPixmapBackground( const QPixmap& pixmap, bool resizeSelf ); // Widget background
    void setPixmapAreaCenter( QString fileName ); // Drawn in the middle of morphing area
    void setPixmapAreaCenter( const QPixmap& pixmap ); // Drawn in the middle of morphing area
    void setPixmapHandle( QString fileName ); // Morphing point handle
    void setPixmapHandle( const QPixmap& pixmap ); // Morphing point handle
    void setPixmapFlash( QString fileName ); // Flashing points (notes)
    void setPixmapFlash( const QPixmap& pixmap ); // Flashing points (notes)

signals:
    void areaStartDrag();
    void areaEndDrag();
    void areaChanged(); // Note: also may be emitted not in dragging mode (via mouse wheel and etc.)
    void handleStartDrag();
    void handleEndDrag();
    void handleChanged();

protected:
    QRect
        m_ActiveRect,    // Area which is actually used by the editor
        m_AreaRect;        // Rectangle of the area (cached)
    int m_MarginLeft, m_MarginRight, m_MarginTop, m_MarginBottom;
    qreal m_Left, m_Right, m_Top, m_Bottom; // Area coordinates (virtual, [0..1])
    qreal m_X, m_Y; // Handle coordinates (virtual, [0..1])
    QColor m_BgColor, m_AreaColor, m_LineColor;
    QPixmapReferencer m_BGPix, m_AreaCenterPix, m_HandlePix, m_FlashPix;
    bool m_HandleVisible, m_AreaCenterVisible;
    VT::CArray<TFlashPoint> m_FlashPoints;

    TDragMode m_DragMode;
    int m_DragDownX, m_DragDownY;
    qreal m_DragStartLeft, m_DragStartRight, m_DragStartTop, m_DragStartBottom, m_DragStartX, m_DragStartY;

    void updateActiveRect();

    int Xf2screen( qreal x ) const; // Convert internal floating-point X to screen X
    int Yf2screen( qreal y ) const; // Convert internal floating-point Y to screen Y

    int X2Active( int mouseX ) const;
    int Y2Active( int mouseY ) const;
    int XActive2Screen( int x ) const;
    int YActive2Screen( int y ) const;

    virtual void paintEvent(QPaintEvent* event);
    virtual void resizeEvent(QResizeEvent* event);
    virtual void mousePressEvent(QMouseEvent *me);
    virtual void mouseReleaseEvent(QMouseEvent *me);
    virtual void mouseMoveEvent(QMouseEvent *me);
    virtual void wheelEvent( QWheelEvent* event );

    // Drawing procedures
    virtual void drawBackground( QPainter& painter );
    virtual void drawArea( QPainter& painter );
    virtual void drawAreaCenter( QPainter& painter );
    virtual void drawHandle( QPainter& painter );
    virtual void drawFlashPoints( QPainter& painter );

    virtual void updateMorphingArea();
    virtual void finalizeAreaMovement();

    TDragMode getDragMode( int x, int y ) const;
    static Qt::CursorShape getCursorShape( TDragMode mode );

private:

};

#endif


