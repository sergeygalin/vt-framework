//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QPIXMAPRADIOGROUP_H
#define QPIXMAPRADIOGROUP_H

#include <QtGui/QGroupBox>
#include "QPixmapButton.h"

//
// A class which provides a set of pixmap-based radio buttons drawn all in one pixmap.
// The buttons are drawn in a vertical or horizontal order with button states in the
// other order. Like this ( buttonsAreInVertical==true ):
//
//     B1DOWN  B1UP
//     B2DOWN  B2UP
//     B3DOWN  B3UP
//     ...     ...
//
// Or ( buttonsAreInVertical==false ):
//
//     B1DOWN  B2DOWN  B3DOWN ...
//     B1UP    B2UP    B3UP   ...
//
// On screen, the radio button bar may be oriented either horizontally or vertically
// by changing onScreenVertical.
//
class QPixmapButtonBar: public QGroupBox, protected QPixmapReferencer
{
    Q_OBJECT
public:
    QPixmapButtonBar( QWidget *parent = NULL );

    QPixmapButtonBar( QWidget * parent, const QPixmap& pixmap,
        int nButtons, int nStates = 2,
        bool buttonsAreInVertical = true, bool onScreenVertical = true );

    QPixmapButtonBar( QWidget * parent, const QString& file,
        int nButtons, int nStates = 2,
        bool buttonsAreInVertical = true, bool onScreenVertical = true );
    virtual ~QPixmapButtonBar();

    void init( const QPixmap& pixmap,
        int nButtons, int nStates = 2,
        bool buttonsAreInVertical = true, bool onScreenVertical = true );

    void init( const QString& file,
        int nButtons, int nStates = 2,
        bool buttonsAreInVertical = true, bool onScreenVertical = true );

    // Access to some stuff of the buttons
    bool isButtonChecked( int idx ) const;
    bool isButtonCheckable( int idx ) const;
    void setButtonChecked( int idx, bool checked = true );
    bool isButtonAutoExclusive( int idx ) const;
    void setButtonAutoExclusive( int idx, bool aexc = true );
    bool isButtonEnabled( int idx ) const;
    void setButtonEnabled( int idx, bool enable );

    Q_PROPERTY( int buttonCount READ getButtonCount );
    int getButtonCount() const { return m_iButtons; }

    // Set QGroupBox drawing. By default it's off.
    Q_PROPERTY( bool drawBox READ getDrawBox WRITE setDrawBox );
    bool getDrawBox() const { return m_bDrawBox; }
    void setDrawBox( bool drawit ){ m_bDrawBox = drawit; update(); }

    void connectButtonClicked( int idx, const QObject* receiver, const char* member );

protected:
    int m_iButtons;
    bool m_bDrawBox;

    // Protected constructors - don't call initPixmapButtonBar().
    QPixmapButtonBar( QWidget * parent, const QPixmap& pixmap );
    QPixmapButtonBar( QWidget * parent, const QString& file );

    virtual void initGroupBox();
    virtual void initPixmapButtonBar( bool buttonsAreInVertical, bool onScreenVertical, int nButtons, int nStates );
    virtual void setupButton( QPixmapButton* button, int idx );

    QPixmapButton* getButton( int idx );
    const QPixmapButton* getConstButton( int idx ) const;

protected:
    virtual void paintEvent(QPaintEvent* event);

};



#endif

