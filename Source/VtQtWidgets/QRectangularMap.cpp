//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "QRectangularMap.h"
#include "vt_audiomath.hpp"
#include "vt_string.hpp"

QRectangularMap::QRectangularMap():
    m_iMargin( 0 ),
    m_GuiRect( 0, 0, 100, 100 ),
    m_VRect( 0, 0, 1.0, 1.0 ),
    m_AreaRect( 0, 0, 1.0, 1.0 ),
    m_Flip( true ),
    m_MinXZoom( 10.0 ),
    m_MinYZoom( 10.0 ),
    m_ClickZoomPc( 105.0 ),
    m_ClickZoomMultiplies( true )
{
}

void QRectangularMap::copyFrom( const QRectangularMap &src )
{
    m_GuiRect = src.m_GuiRect;
    m_VRect = src.m_VRect;
    m_AreaRect = src.m_AreaRect;
    m_Flip = src.m_Flip;
    m_MinXZoom  = src.m_MinXZoom;
    m_MinYZoom = src.m_MinYZoom;
    m_iMargin = src.m_iMargin;
    m_ClickZoomPc = src.m_ClickZoomPc;
    m_ClickZoomMultiplies = src.m_ClickZoomMultiplies;
    emit mapChanged();
}

void QRectangularMap::copyGuiGeometryFrom( const QRectangularMap &src )
{
    m_GuiRect = src.m_GuiRect;
    m_iMargin = src.m_iMargin;
    emit mapChanged();
}

void QRectangularMap::setGUIRect( const QRect& src )
{
    m_GuiRect = src;
    emit mapChanged();
}

void QRectangularMap::setVRect( const QRectF& src )
{
    m_VRect = src;
    emit mapChanged();
}

void QRectangularMap::setAreaRect( const QRectF& src )
{
    m_AreaRect = src;
    emit mapChanged();
}




// ************************************************************************
// Coordinate system conversions
// ************************************************************************

qreal QRectangularMap::screenXToVX( int x ) const
{
    qreal sx01 = qreal(x-qreal(getMargin())-m_GuiRect.left()) / qreal(getWorkingAreaWidth());
    return sx01 * m_VRect.width() + m_VRect.left();
}

void QRectangularMap::setFlip( bool fl )
{
    m_Flip = fl;
    emit mapChanged();
}

qreal QRectangularMap::screenYToVY( int y ) const
{
    if( flip() )
        y = m_GuiRect.height()-y;
    qreal sy01 = qreal(y-qreal(getMargin())-m_GuiRect.top()) / qreal(getWorkingAreaHeight());
    return sy01 * m_VRect.height() + m_VRect.top();
}

qreal QRectangularMap::VXtoScreenX( qreal x ) const
{
    if( m_VRect.width()<1e-10 ){
        VTDEBUGMSGT( "Virtual rectangle width is too small!" );
        return 0;
    }
    qreal vx01 = (x-m_VRect.left()) / m_VRect.width();
    /*VTASSERT2( vx01>=-0.001, VT::CString().ConcatDouble(vx01) );
    VTASSERT2( vx01<=1.001, VT::CString().ConcatDouble(vx01) );*/
    qreal screenX = vx01 * qreal(getWorkingAreaWidth()) + m_GuiRect.left();
    return screenX + qreal(getMargin());
}

qreal QRectangularMap::VYtoScreenY( qreal y ) const
{
    if( m_VRect.height()<1e-10 ){
        VTDEBUGMSGT( "Virtual rectangle height is too small!" );
        return 0;
    }
    qreal vy01 = (y-m_VRect.top()) / m_VRect.height();
    /*VTASSERT2( vy01>=-0.001 && vy01<=1.001, VT::CString().ConcatDouble(vy01).ConcatStr(VTT(", Y=")).
        ConcatDouble(y).ConcatStr(VTT(", top=")).ConcatDouble(m_VRect.top()).
        ConcatStr(VTT(", bottom=")).ConcatDouble(m_VRect.bottom()).
        ConcatStr(VTT(", Height=")).ConcatDouble(m_VRect.height()) );*/
    qreal ah = getWorkingAreaHeight();
    qreal guiy = vy01 * ah + m_GuiRect.top();
    if( flip() )
        guiy = ah-guiy;
    return guiy + qreal(getMargin());
}




// ************************************************************************
// Scrolling math utilites (protected functions)
// ************************************************************************

qreal QRectangularMap::VXCenterShift() const
{
    return m_AreaRect.center().x()-m_VRect.center().x();
}

qreal QRectangularMap::VYCenterShift() const
{
    return m_AreaRect.center().y()-m_VRect.center().y();
}

qreal QRectangularMap::VXMaxAbsShift() const
{
    qreal aw = m_AreaRect.width(), vw = m_VRect.width();
    if( vw>=aw ) // View window fits all area -> no scrolling
        return 0;
    // Area is wider than view => view can be shifted by their width difference
    qreal ret = (aw-vw)/2.0; // vw<aw => aw-vw>0
    VTASSERT( ret>0.0 );
    return ret;
}

qreal QRectangularMap::VYMaxAbsShift() const
{
    qreal ah = m_AreaRect.height(), vh = m_VRect.height();
    if( vh>=ah ) // View window fits all area -> no scrolling
        return 0;
    // Area is taller than view => view can be shifted by their height difference
    qreal ret = (ah-vh)/2.0; // vh<ah => ah-vh>0
    VTASSERT( ret>0.0 );
    return ret;
}



// ************************************************************************
// Scrolling functions
// ************************************************************************

bool QRectangularMap::canScrollX() const
{
    return VXMaxAbsShift()>0;
}

bool QRectangularMap::canScrollY() const
{
    return VYMaxAbsShift()>0;
}

qreal QRectangularMap::scrollXPos() const
{
    qreal sh = VXCenterShift();
    qreal max = VXMaxAbsShift();
    if( max==0 )
        return 50.0;
    VTASSERT( sh<=max+0.001 );
    qreal ret = -sh/max * 50.0 + 50.0;
    return VT::Math::Clip( ret, 0.0, 100.0 );
}

void QRectangularMap::scrollXToPos( qreal pcPos )
{
    VTASSERT( pcPos>=0.0 && pcPos<=100.0 );
    if( canScrollX() ){
        // Convert pcPos to [-1..1] system (with clipping)
        qreal pos11 = VT::Math::Clip( (pcPos-50.0)/50.0, -1.0, 1.0 );
        // Find X shift
        qreal shift = VXMaxAbsShift() * pos11;
        m_VRect.moveTo( m_AreaRect.center().x() - m_VRect.width()/2.0 + shift, m_VRect.y() );
    }else{
        // Cannot scroll - just center
        m_VRect.moveTo( m_AreaRect.center().x() - m_VRect.width()/2.0, m_VRect.y() );
    }
    emit mapChanged();
}


qreal QRectangularMap::scrollYPos() const
{
    qreal sh = VYCenterShift();
    qreal max = VYMaxAbsShift();
    if( max==0 )
        return 50.0;
    VTASSERT( sh<=max+0.001 );
    qreal ret = sh/max * 50.0;
    if( !flip() )
        ret = -ret;
    ret += 50.0;
    return VT::Math::Clip( ret, 0.0, 100.0 );
}



void QRectangularMap::scrollYToPos( qreal pcPos )
{
    VTASSERT( pcPos>=0.0 && pcPos<=100.0 );
    if( canScrollY() ){
        // Convert pcPos to [-1..1] system (with clipping)
        qreal pos11 = VT::Math::Clip( (pcPos-50.0)/50.0, -1.0, 1.0 );
        // Find Y shift
        qreal shift = VYMaxAbsShift() * pos11;
        if( flip() )
            shift = -shift;
        m_VRect.moveTo( m_VRect.x(), m_AreaRect.center().y() - m_VRect.height()/2.0 + shift );
    }else{
        // Cannot scroll - just center
        m_VRect.moveTo( m_VRect.x(), m_AreaRect.center().y() - m_VRect.height()/2.0 );
    }
    emit mapChanged();
}



// ************************************************************************
// Zooming functions
// ************************************************************************

qreal QRectangularMap::zoomXArea() const
{
    qreal ret = m_VRect.width() / m_AreaRect.width() * 100.0;
    return ret;
}

qreal QRectangularMap::zoomYArea() const
{
    qreal ret = m_VRect.height() / m_AreaRect.height() * 100.0;
    return ret;
}

void QRectangularMap::zoomXToPc( qreal pcPos )
{
    qreal oldScroll = scrollXPos();
    // zoom = m_VRect.width() / m_AreaRect.width() * 100.0 =>
    // m_VRect.width() = zoom * m_AreaRect.width() / 100.0
    qreal newWidth = pcPos / 100.0 * m_AreaRect.width();
    qreal cx = m_VRect.center().x();
    m_VRect.setRect( cx-newWidth/2.0, m_VRect.y(), newWidth, m_VRect.height() );
    scrollXToPos( oldScroll ); // emits mapChanged
}

void QRectangularMap::zoomYToPc( qreal pcPos )
{
    qreal oldScroll = scrollYPos();
    qreal newHeight = pcPos / 100.0 * m_AreaRect.height();
    qreal cy = m_VRect.center().y();
    m_VRect.setRect( m_VRect.x(), cy-newHeight/2.0, m_VRect.width(), newHeight );
    scrollYToPos( oldScroll ); // emits mapChanged
}


void QRectangularMap::zoomToArea()
{
    // zoomXToPc( 100.0 ); zoomYToPc( 100.0 );
    setVRect( getAreaRect() );
}

bool QRectangularMap::zoomXToRange( qreal x1, qreal x2 )
{
    x1 = VT::Math::Clip( x1, m_AreaRect.left(), m_AreaRect.right() );
    x2 = VT::Math::Clip( x2, m_AreaRect.left(), m_AreaRect.right() );
    VT::Math::Order( &x1, &x2 );
    if( x1==x2 )
        return false;
    m_VRect.setLeft( x1 );
    m_VRect.setRight( x2 );
    emit mapChanged();
    return true;
}

bool QRectangularMap::zoomYToRange( qreal y1, qreal y2 )
{
    y1 = VT::Math::Clip( y1, m_AreaRect.top(), m_AreaRect.bottom() );
    y2 = VT::Math::Clip( y2, m_AreaRect.top(), m_AreaRect.bottom() );
    VT::Math::Order( &y1, &y2 );
    if( y1==y2 )
        return false;
    m_VRect.setTop( y1 );
    m_VRect.setBottom( y2 );
    emit mapChanged();
    return true;
}

bool QRectangularMap::zoomXToRangePc( qreal x1pc, qreal x2pc )
{
    qreal x1 = x1pc/100.0 * m_AreaRect.width() + m_AreaRect.left();
    qreal x2 = x2pc/100.0 * m_AreaRect.width() + m_AreaRect.left();
    return zoomXToRange( x1, x2 );
}

bool QRectangularMap::zoomYToRangePc( qreal y1pc, qreal y2pc )
{
    qreal y1 = y1pc/100.0 * m_AreaRect.height() + m_AreaRect.top();
    qreal y2 = y2pc/100.0 * m_AreaRect.height() + m_AreaRect.top();
    return zoomXToRange( y1, y2 );
}

qreal QRectangularMap::VXtoAreaPc( qreal x ) const
{
    qreal ret = (x-m_AreaRect.left())/m_AreaRect.width() * 100.0;
    return ret;
}

qreal QRectangularMap::VYtoAreaPc( qreal y ) const
{
    qreal ret = (y-m_AreaRect.top())/m_AreaRect.height() * 100.0;
    return ret;
}

void QRectangularMap::zoomInX()
{
    qreal currentZoom = zoomXArea();
    qreal nz = (getClickZoomMultiplies())? currentZoom/(getClickZoomStep()/100.0): currentZoom-getClickZoomStep();
    qreal newZoom = VT::Math::Clip<qreal>( nz, getMinXZoom(), 100.0 );
    zoomXToPc( newZoom ); // emits mapChanged
}

void QRectangularMap::zoomInY()
{
    qreal currentZoom = zoomYArea();
    qreal nz = (getClickZoomMultiplies())? currentZoom/(getClickZoomStep()/100.0): currentZoom-getClickZoomStep();
    qreal newZoom = VT::Math::Clip<qreal>( nz, getMinYZoom(), 100.0 );
    zoomYToPc( newZoom ); // emits mapChanged
}

void QRectangularMap::zoomOutX()
{
    qreal currentZoom = zoomXArea();
    qreal nz = (getClickZoomMultiplies())? currentZoom*(getClickZoomStep()/100.0): currentZoom+getClickZoomStep();
    qreal newZoom = VT::Math::Clip<qreal>( nz, getMinXZoom(), 100.0 );
    zoomXToPc( newZoom ); // emits mapChanged
}

void QRectangularMap::zoomOutY()
{
    qreal currentZoom = zoomYArea();
    qreal nz = (getClickZoomMultiplies())? currentZoom*(getClickZoomStep()/100.0): currentZoom+getClickZoomStep();
    qreal newZoom = VT::Math::Clip<qreal>( nz, getMinYZoom(), 100.0 );
    zoomYToPc( newZoom ); // emits mapChanged
}













QSimpleOnmapGrid::QSimpleOnmapGrid():
    m_XStep( 1.0 ),
    m_YStep( 1.0 ),
    m_XBold( 10 ),
    m_YBold( 10 ),
    m_Color( 5, 100, 5 ),
    m_BoldColor( 7, 140, 7 ),
    m_AxisColor( 50, 180, 7 ),
    m_DrawArrows( true )
{
}


QSimpleOnmapGrid::QSimpleOnmapGrid( qreal xstep, int xbold, qreal ystep, int ybold,
                                   QColor color, QColor boldColor, QColor axisColor, bool drawArrows ):
    m_XStep( xstep ),
    m_YStep( ystep ),
    m_XBold( xbold ),
    m_YBold( ybold ),
    m_Color( color ),
    m_BoldColor( boldColor ),
    m_AxisColor( axisColor ),
    m_DrawArrows( drawArrows )
{
}

void QSimpleOnmapGrid::copyFrom( const QSimpleOnmapGrid& src )
{
    m_XStep = src.m_XStep;
    m_YStep = src.m_YStep;
    m_XBold = src.m_XBold;
    m_YBold = src.m_YBold;
    m_Color = src.m_Color;
    m_BoldColor = src.m_BoldColor;
    m_AxisColor = src.m_AxisColor;
    m_DrawArrows = src.m_DrawArrows;

    emit updated();
}

QSimpleOnmapGrid::~QSimpleOnmapGrid()
{
}

void QSimpleOnmapGrid::draw( const QRectangularMap& map, QPainter& painter )
{
    qreal
        x1 = map.VXtoScreenX( map.getVRect().left() ),
        x2 = map.VXtoScreenX( map.getVRect().right() ),
        y1 = map.VYtoScreenY( map.getVRect().top() ),
        y2 = map.VYtoScreenY( map.getVRect().bottom() );
    qreal
        vl = map.getVRect().left(),
        vr = map.getVRect().right(),
        vt = map.getVRect().top(),
        vb = map.getVRect().bottom();
    long
        xPosInBars = VT::Math::Round( ceil( vl / m_XStep ) )-1,
        xEndPosInBars = VT::Math::Round( floor( vr / m_XStep ) )+1,
        yPosInBars = VT::Math::Round( ceil( vt / m_YStep ) )-1,
        yEndPosInBars = VT::Math::Round( floor( vb / m_YStep ) )+1;
    for( long xpos = xPosInBars; xpos<=xEndPosInBars; xpos++ ){
        qreal x = map.VXtoScreenX( qreal(xpos) * m_XStep );
        painter.setPen( (xpos==0)? m_AxisColor: ((!( xpos % m_XBold ))? m_BoldColor: m_Color) );
        painter.drawLine( QPointF( x, y1 ), QPointF( x, y2 ) );
    }
    for( long ypos = yPosInBars; ypos<=yEndPosInBars; ypos++ ){
        qreal y = map.VYtoScreenY( qreal(ypos) * m_YStep );
        painter.setPen( (ypos==0)? m_AxisColor: ((!( ypos % m_YBold ))? m_BoldColor: m_Color) );
        painter.drawLine( QPointF( x1, y ), QPointF( x2, y ) );
    }

    // Draw arrows on axis lines
    if( m_DrawArrows ){
        const qreal kArrSize = 5.0, kArrSize2 = kArrSize/2.0, kCenterRadius = 2.0;
        painter.setPen( m_AxisColor );
        qreal x0 = map.VXtoScreenX( 0 ), y0 = map.VYtoScreenY( 0 );
        qreal ahbottom = y2;
        if( map.flip() )
            ahbottom += kArrSize;
        else
            ahbottom -= kArrSize;
        painter.drawLine( QPointF( x0, y2 ), QPointF( x0-kArrSize2, ahbottom ) );
        painter.drawLine( QPointF( x0, y2 ), QPointF( x0+kArrSize2, ahbottom ) );
        // Arrow head on X axis
        painter.drawLine( QPointF( x2, y0 ), QPointF( x2-kArrSize, y0+kArrSize2 ) );
        painter.drawLine( QPointF( x2, y0 ), QPointF( x2-kArrSize, y0-kArrSize2 ) );
        painter.drawEllipse( QPointF( x0, y0 ), kCenterRadius, kCenterRadius );
    }
}







