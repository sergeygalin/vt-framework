//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include "QMorphingArea.h"
#include "vt_audiomath.hpp"

QMorphingArea::QMorphingArea( QWidget* parent ):
    QWidget( parent ),
    m_ActiveRect( 0, 0, 0, 0 ),
    m_AreaRect( 0, 0, 0, 0 ),
    m_MarginLeft( 1 ),
    m_MarginRight( 1 ),
    m_MarginTop( 1 ),
    m_MarginBottom( 1 ),
    m_Left( 0.25 ),
    m_Right( 0.75 ),
    m_Top( 0.25 ),
    m_Bottom( 0.75 ),
    m_X( 0.5 ),
    m_Y( 0.5 ),
    m_BgColor( 62, 65, 60 ),
    m_AreaColor( 70, 150, 70 ),
    m_LineColor( 80, 255, 80 ),
    m_HandleVisible( true ),
    m_AreaCenterVisible( true ),
    m_FlashPoints( 0, 128 ),
    m_DragMode( kDragNone )
{
    setMouseTracking( true );
    updateActiveRect();
    updateMorphingArea();
}

void QMorphingArea::updateActiveRect()
{
    // Error check
    if( m_MarginLeft+m_MarginRight>=width() ||
        m_MarginTop+m_MarginBottom>=height() ||
        m_MarginLeft<0 ||
        m_MarginRight<0 ||
        m_MarginTop<0 ||
        m_MarginBottom<0 )
        m_ActiveRect = rect();
    else
        m_ActiveRect.setRect( m_MarginLeft, m_MarginTop, width()-m_MarginLeft-m_MarginRight, height()-m_MarginTop-m_MarginBottom );
}

QMorphingArea::~QMorphingArea()
{
}

qreal QMorphingArea::getLeft() const
{
    return VT::Math::Clip<qreal>( m_Left, 0.0, 1.0 );
}

qreal QMorphingArea::getRight() const
{
    return VT::Math::Clip<qreal>( m_Right, 0.0, 1.0 );
}

qreal QMorphingArea::getTop() const
{
    return VT::Math::Clip<qreal>( m_Top, 0.0, 1.0 );
}

qreal QMorphingArea::getBottom() const
{
    return VT::Math::Clip<qreal>( m_Bottom, 0.0, 1.0 );
}

qreal QMorphingArea::getAreaX() const
{
    return (m_Left + m_Right) / 2.0;
}

qreal QMorphingArea::getAreaY() const
{
    return (m_Top + m_Bottom) / 2.0;
}

void QMorphingArea::setArea( qreal left, qreal top, qreal right, qreal bottom, bool emitIt )
{
    m_Left = left;
    m_Right = right;
    m_Top = top;
    m_Bottom = bottom;
    updateMorphingArea();
    if( m_DragMode!=kDragNone && m_DragMode!=kDragHandle ){
        m_DragStartLeft = m_Left;
        m_DragStartRight = m_Right;
        m_DragStartTop = m_Top;
        m_DragStartBottom = m_Bottom;
    }
    if( emitIt )
        emit areaChanged();
}

void QMorphingArea::setXY( qreal x, qreal y, bool emitIt  )
{
    m_X = VT::Math::Clip( VT::Math::Clip( x, m_Left, m_Right ), 0.0, 1.0 );
    m_Y = VT::Math::Clip( VT::Math::Clip( y, m_Top, m_Bottom ), 0.0, 1.0 );
    if( m_DragMode==kDragHandle ){
        m_DragStartX = m_X;
        m_DragStartY = m_Y;
    }
    if( emitIt )
        emit handleChanged();
    update();
}

void QMorphingArea::updateMorphingArea()
{
    m_Left = VT::Math::Clip<qreal>( m_Left, -1.0, 2.0 );
    m_Right = VT::Math::Clip<qreal>( m_Right, -1.0, 2.0 );
    m_Top = VT::Math::Clip<qreal>( m_Top, -1.0, 2.0 );
    m_Bottom = VT::Math::Clip<qreal>( m_Bottom, -1.0, 2.0 );
    VT::Math::Order( &m_Left, &m_Right );
    VT::Math::Order( &m_Top, &m_Bottom );

    int
        x1 = Xf2screen( m_Left ),
        x2 = Xf2screen( m_Right ),
        y1 = Yf2screen( m_Top ),
        y2 = Yf2screen( m_Bottom );

    m_AreaRect.setRect( x1, y1, x2-x1, y2-y1 );

    update();
}

int QMorphingArea::Xf2screen( qreal x ) const
{
    return VT::Math::Round(x*m_ActiveRect.width()+m_ActiveRect.left());
}

int QMorphingArea::Yf2screen( qreal y ) const
{
    return VT::Math::Round(y*m_ActiveRect.height()+m_ActiveRect.top());
}

int QMorphingArea::X2Active( int mouseX ) const
{
    return mouseX - m_ActiveRect.left();
}

int QMorphingArea::Y2Active( int mouseY ) const
{
    return mouseY - m_ActiveRect.top();
}

int QMorphingArea::XActive2Screen( int x ) const
{
    return x + m_ActiveRect.left();
}

int QMorphingArea::YActive2Screen( int y ) const
{
    return y + m_ActiveRect.top();
}

void QMorphingArea::paintEvent( QPaintEvent* )
{
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );

    drawBackground( painter );
    drawArea( painter );
    if( m_AreaCenterVisible )
        drawAreaCenter( painter );
    if( m_HandleVisible )
        drawHandle( painter );
    if( !m_FlashPoints.isEmpty() )
        drawFlashPoints( painter );
}

void QMorphingArea::drawBackground( QPainter& painter )
{
    if( m_BGPix.pixmap()==NULL ){
        QColor darkerColor( m_BgColor.darker() );
        QBrush darkerBrush(darkerColor), bgBrush( m_BgColor );
        painter.fillRect( rect(), darkerColor );
        painter.fillRect( m_ActiveRect, bgBrush );
    }else{
        painter.drawPixmap( rect(), *(m_BGPix.pixmap()), m_BGPix.pixmap()->rect() );
    }
}

void QMorphingArea::drawArea( QPainter& painter )
{
    QColor borderColor( m_AreaColor.lighter( 110 ) );
    QRect realRect( m_AreaRect.intersect(m_ActiveRect) );
    QPainter::CompositionMode saveMode = painter.compositionMode();
    painter.setCompositionMode( QPainter::CompositionMode_HardLight );

    if( !realRect.width() || !realRect.height() ){
        painter.setPen( borderColor );
        painter.drawLine( realRect.left(), realRect.top(),
            realRect.right(), realRect.bottom() );
    }else{
        QBrush areaBrush( m_AreaColor );
        painter.fillRect( realRect, areaBrush );
        painter.setPen( borderColor );
        painter.drawRect( realRect );
    }
    painter.setCompositionMode( saveMode );
}


void QMorphingArea::drawAreaCenter( QPainter& painter )
{
    const int sz = 3, sz2 = 5;
    int cx = Xf2screen( (m_Left+m_Right)/2.0 ), cy = Yf2screen( (m_Top+m_Bottom)/2.0 );

    QPainter::CompositionMode saveMode = painter.compositionMode();
    painter.setCompositionMode( QPainter::CompositionMode_HardLight );

    if( m_AreaCenterPix.pixmap()==NULL ){
        painter.setPen( m_LineColor );
        painter.drawEllipse( cx-sz, cy-sz, sz*2, sz*2 );
        painter.drawLine( cx, cy-sz2, cx, cy+sz2 );
        painter.drawLine( cx-sz2, cy, cx+sz2, cy );
    }else{
        QRect drect(
            cx - m_AreaCenterPix.pixmap()->width()/2,
            cy - m_AreaCenterPix.pixmap()->height()/2,
            m_AreaCenterPix.pixmap()->width(),
            m_AreaCenterPix.pixmap()->height() );
        painter.drawPixmap( drect, *(m_AreaCenterPix.pixmap()), m_AreaCenterPix.pixmap()->rect() );
    }

    painter.setCompositionMode( saveMode );
}

void QMorphingArea::drawHandle( QPainter& painter )
{
    const int sz = 5, r = 3;
    int cx = Xf2screen( m_X ), cy = Yf2screen( m_Y );

    QPainter::CompositionMode saveMode = painter.compositionMode();
    painter.setCompositionMode( QPainter::CompositionMode_HardLight );

    if( m_HandlePix.pixmap()==NULL ){
        painter.setPen( m_LineColor );
        painter.drawEllipse( cx-r, cy-r, r*2, r*2 );
        painter.drawLine( cx-sz, cy-sz, cx+sz, cy+sz );
        painter.drawLine( cx+sz, cy-sz, cx-sz, cy+sz );
    }else{
        QRect drect(
            cx - m_HandlePix.pixmap()->width()/2,
            cy - m_HandlePix.pixmap()->height()/2,
            m_HandlePix.pixmap()->width(),
            m_HandlePix.pixmap()->height() );
        painter.drawPixmap( drect, *(m_HandlePix.pixmap()), m_HandlePix.pixmap()->rect() );
    }

    painter.setCompositionMode( saveMode );
}

void QMorphingArea::drawFlashPoints( QPainter& painter )
{
    const qreal r = 1.5;
    QPainter::CompositionMode saveMode = painter.compositionMode();
    painter.setCompositionMode( QPainter::CompositionMode_HardLight );
    for( size_t i=0; i<m_FlashPoints.getSize(); i++ ){
        int cx = Xf2screen(m_FlashPoints.getConst(i).x()),
            cy = Yf2screen(m_FlashPoints.getConst(i).y());
        if( m_FlashPix.pixmap()==NULL ){
            painter.setBrush( m_LineColor );
            painter.setPen( m_LineColor );
            painter.drawEllipse( QPointF(cx-r, cy-r), qreal(r*2.0), qreal(r*2.0) );
        }else{
            QRect drect(
                cx - m_FlashPix.pixmap()->width()/2,
                cy - m_FlashPix.pixmap()->height()/2,
                m_FlashPix.pixmap()->width(),
                m_FlashPix.pixmap()->height() );
            painter.drawPixmap( drect, *(m_FlashPix.pixmap()), m_FlashPix.pixmap()->rect() );
        }
    }
    painter.setCompositionMode( saveMode );
}

void QMorphingArea::mousePressEvent( QMouseEvent *me )
{
    QWidget::mousePressEvent( me );
    setFocus();
    if( me->button()==Qt::LeftButton ){
        m_DragMode = getDragMode( me->x(), me->y() );
        m_DragDownX = me->x();
        m_DragDownY = me->y();
        m_DragStartLeft = m_Left;
        m_DragStartRight = m_Right;
        m_DragStartTop = m_Top;
        m_DragStartBottom = m_Bottom;
        m_DragStartX = m_X;
        m_DragStartY = m_Y;
        // Setting cursor (probably mouseMoveEvent() didn't do that yet)
        Qt::CursorShape shape = getCursorShape( m_DragMode );
        setCursor( shape );
        if( m_DragMode!=kDragNone ){
            if( m_DragMode!=kDragHandle )
                emit areaStartDrag();
            else
                emit handleStartDrag();
        }
    }
}

void QMorphingArea::mouseReleaseEvent( QMouseEvent *me )
{
    QWidget::mouseReleaseEvent( me );
    if( (me->button()==Qt::LeftButton || me->buttons()==0 ) && m_DragMode!=kDragNone )
        stopDrag();
}

void QMorphingArea::mouseMoveEvent( QMouseEvent *me )
{
    QWidget::mouseMoveEvent( me );
    if( m_DragMode==kDragNone ){
        TDragMode dmode = getDragMode( me->x(), me->y() );
        Qt::CursorShape shape = getCursorShape( dmode );
        setCursor( shape );
    }else{
        // The area is being dragged


        // Screen movement
        int x = me->x(), y = me->y();
        x = VT::Math::Clip( x, 0, width() );
        y = VT::Math::Clip( y, 0, height() );
        int dx = x - m_DragDownX, dy = y - m_DragDownY;

        // FP movement
        qreal xstep = 1.0 / width(), ystep = 1.0 / height();
        qreal fdx = xstep * dx, fdy = ystep * dy;

        switch( m_DragMode ){
            case kDragMove:
                m_Left = m_DragStartLeft + fdx;
                m_Right = m_DragStartRight + fdx;
                m_Top = m_DragStartTop + fdy;
                m_Bottom = m_DragStartBottom + fdy;
                break;
            case kDragSzLeft:
                m_Left = m_DragStartLeft + fdx;
                m_Right = m_DragStartRight - fdx;
                break;
            case kDragSzRight:
                m_Left = m_DragStartLeft - fdx;
                m_Right = m_DragStartRight + fdx;
                break;
            case kDragSzTop:
                m_Top = m_DragStartTop + fdy;
                m_Bottom = m_DragStartBottom - fdy;
                break;
            case kDragSzBottom:
                m_Top = m_DragStartTop - fdy;
                m_Bottom = m_DragStartBottom + fdy;
                break;
            case kDragSzX1Y1: // Left top
                {
                    m_Left = m_DragStartLeft + fdx;
                    m_Top = m_DragStartTop + fdy;
                    m_Right = m_DragStartRight - fdx;
                    m_Bottom = m_DragStartBottom - fdy;
                }
                break;
            case kDragSzX1Y2: // Left bottom
                {
                    m_Left = m_DragStartLeft + fdx;
                    m_Top = m_DragStartTop - fdy;
                    m_Right = m_DragStartRight - fdx;
                    m_Bottom = m_DragStartBottom + fdy;
                }
                break;
            case kDragSzX2Y1: // Right top
                {
                    m_Left = m_DragStartLeft - fdx;
                    m_Top = m_DragStartTop + fdy;
                    m_Right = m_DragStartRight + fdx;
                    m_Bottom = m_DragStartBottom - fdy;
                }
                break;
            case kDragSzX2Y2: // Right bottom
                {
                    m_Left = m_DragStartLeft - fdx;
                    m_Top = m_DragStartTop - fdy;
                    m_Right = m_DragStartRight + fdx;
                    m_Bottom = m_DragStartBottom + fdy;
                }
                break;
            case kDragHandle:
                {
                    m_X = m_DragStartX + fdx;
                    m_Y = m_DragStartY + fdy;
                }
                break;
            case kDragNone:
                break;
        };

        // Validate the movement
        finalizeAreaMovement();
        if( m_DragMode!=kDragHandle )
            emit areaChanged();
        else
            emit handleChanged();
    }
}

void QMorphingArea::finalizeAreaMovement()
{
    // Check area position: center cannot be moved outside of the field
    {
        qreal cx = (m_Left + m_Right) / 2.0, cy = (m_Top + m_Bottom) / 2.0;
        if( cx<0 ){
            m_Left -= cx;
            m_Right -= cx;
        }else if( cx>1.0 ){
            cx -= 1.0;
            m_Left -=cx;
            m_Right -= cx;
        }
        if( cy<0 ){
            m_Top -= cy;
            m_Bottom -= cy;
        }else if( cy>1.0 ){
            cy -= 1.0;
            m_Top -=cy;
            m_Bottom -= cy;
        }
    }

    // Left cannot be moved over right, top over bottom, and etc.
    if( m_Left>m_Right )
        m_Left = m_Right = (m_Left+m_Right)/2.0;
    if( m_Top>m_Bottom )
        m_Top = m_Bottom = (m_Top+m_Bottom)/2.0;

    // Make sure the field didn't grow too much
    {
        qreal cx = (m_Left + m_Right) / 2.0, cy = (m_Top + m_Bottom) / 2.0,
            halfwidth = m_Right - cx, halfheight = m_Bottom - cy;
        if( halfwidth > 1.0 ){
            qreal over = halfwidth - 1.0;
            m_Left += over;
            m_Right -= over;
        }
        if( halfheight > 1.0 ){
            qreal over = halfheight - 1.0;
            m_Top += over;
            m_Bottom -= over;
        }
    }

    // Fixing handle position
    m_X = VT::Math::Clip( VT::Math::Clip( m_X, m_Left, m_Right ), 0.0, 1.0 );
    m_Y = VT::Math::Clip( VT::Math::Clip( m_Y, m_Top, m_Bottom ), 0.0, 1.0 );

    // Update visual look
    updateMorphingArea();
}

void QMorphingArea::wheelEvent( QWheelEvent* event )
{
    // QWidget::wheelEvent( event );
    if( m_DragMode==kDragNone ){
        const qreal k = 0.0001;
        qreal delta = k * event->delta();

        // qreal cx = (m_Left + m_Right) / 2.0, cy = (m_Top + m_Bottom) / 2.0;
        qreal w = m_Right-m_Left, h = m_Bottom-m_Top, nw, nh, fdx, fdy;

        if( w==h ){
            nw = w + delta;
            nh = h + delta;
        }else if( w>h ){
            nw = w + delta;
            nh = h * (nw/w);
        }else{ // w<h
            nh = h + delta;
            nw = w * (nh/h);
        }
        fdx = (nw-w) / 2.0;
        fdy = (nh-h) / 2.0;

        m_Left -= fdx;
        m_Right += fdx;
        m_Top -= fdy;
        m_Bottom += fdy;

        finalizeAreaMovement();
        emit areaChanged();
    }
}



void QMorphingArea::stopDrag()
{
    bool emitit = m_DragMode!=kDragNone;
    m_DragMode = kDragNone;
    setCursor( Qt::ArrowCursor );
    if( emitit ){
        if( m_DragMode!=kDragHandle )
            emit areaEndDrag();
        else
            emit handleEndDrag();
    }
}

void QMorphingArea::setMargins( int left, int top, int right, int bottom )
{
    m_MarginLeft = left;
    m_MarginTop = top;
    m_MarginRight = right;
    m_MarginBottom = bottom;
    updateMorphingArea();
}

void QMorphingArea::setPixmapBackground( QString fileName, bool resizeSelf  )
{
    m_BGPix.setPixmap( fileName );
    if( resizeSelf && m_BGPix.pixmap()!=NULL )
        resize( m_BGPix.pixmap()->width(), m_BGPix.pixmap()->height() );
    update();
}

void QMorphingArea::setPixmapBackground( const QPixmap& pixmap, bool resizeSelf  )
{
    m_BGPix.setPixmap( pixmap );
    if( resizeSelf && m_BGPix.pixmap()!=NULL )
        resize( m_BGPix.pixmap()->width(), m_BGPix.pixmap()->height() );
    update();
}

void QMorphingArea::setPixmapAreaCenter( QString fileName )
{
    m_AreaCenterPix.setPixmap( fileName );
    update();
}

void QMorphingArea::setPixmapAreaCenter( const QPixmap& pixmap )
{
    m_AreaCenterPix.setPixmap( pixmap );
    update();
}

void QMorphingArea::setPixmapHandle( QString fileName )
{
    m_HandlePix.setPixmap( fileName );
    update();
}

void QMorphingArea::setPixmapHandle( const QPixmap& pixmap )
{
    m_HandlePix.setPixmap( pixmap );
    update();
}

void QMorphingArea::setPixmapFlash( QString fileName )
{
    m_FlashPix.setPixmap( fileName );
    update();
}

void QMorphingArea::setPixmapFlash( const QPixmap& pixmap )
{
    m_FlashPix.setPixmap( pixmap );
    update();
}

void QMorphingArea::resizeEvent( QResizeEvent* event )
{
    QWidget::resizeEvent( event );
    updateActiveRect();
    updateMorphingArea();
}


QMorphingArea::TDragMode QMorphingArea::getDragMode( int x, int y ) const
{
    // !!!!!!!!!Copypaste block
    // const int sz = 3, sz2 = 5;
    int cx = Xf2screen( (m_Left+m_Right)/2.0 ), cy = Yf2screen( (m_Top+m_Bottom)/2.0 );

    const int kCenterDelta = 1, kCornerDelta = 3, kSideDelta = 4, kHandleDelta = 3;

    // Handle
    if( m_HandleVisible ){
        int deltax, deltay;
        if( m_HandlePix.pixmap()!=NULL ){
            deltax = m_HandlePix.pixmap()->width()/2;
            deltay = m_HandlePix.pixmap()->height()/2;
        }else{
            deltax = kHandleDelta;
            deltay = kHandleDelta;
        }
        if( VT::Math::Abs( x-Xf2screen( m_X ) )<=deltax &&
            VT::Math::Abs( y-Yf2screen( m_Y ) )<=deltay )
            return kDragHandle;
    }

    // Center
    if( VT::Math::Abs( x-cx )<=kCenterDelta && VT::Math::Abs( y-cy )<=kCenterDelta )
        return kDragMove;

    int dx1 = VT::Math::Abs( m_AreaRect.left()-x ),
        dx2 = VT::Math::Abs( m_AreaRect.right()-x ),
        dy1 = VT::Math::Abs( m_AreaRect.top()-y ),
        dy2 = VT::Math::Abs( m_AreaRect.bottom()-y );

    // Left top corner
    if( dx1<=kCornerDelta && dy1<=kCornerDelta && x<m_AreaRect.left() && y<m_AreaRect.top() )
        return kDragSzX1Y1;

    // Right top corner
    if( dx2<=kCornerDelta && dy1<=kCornerDelta && x>m_AreaRect.right() && y<m_AreaRect.top() )
        return kDragSzX2Y1;

    // Left bottom corner
    if( dx1<=kCornerDelta && dy2<=kCornerDelta && x<m_AreaRect.left() && y>m_AreaRect.bottom() )
        return kDragSzX1Y2;

    // Right bottom corner
    if( dx2<=kCornerDelta && dy2<=kCornerDelta && x>m_AreaRect.right() && y>m_AreaRect.bottom() )
        return kDragSzX2Y2;

    // Edges
    bool
        in_hor = x>=m_AreaRect.left()-kSideDelta && x<=m_AreaRect.right()+kSideDelta,
        in_vert = y>=m_AreaRect.top()-kSideDelta && y<=m_AreaRect.bottom()+kSideDelta;

    if( in_hor && in_vert ){
        if( dx1<=kSideDelta && x<m_AreaRect.left() ) return kDragSzLeft;
        if( dx2<=kSideDelta && x>m_AreaRect.right() ) return kDragSzRight;
        if( dy1<=kSideDelta && y<m_AreaRect.top() ) return kDragSzTop;
        if( dy2<=kSideDelta && y>m_AreaRect.bottom() ) return kDragSzBottom;


        // LOOSER CHECKS!
        if( dx1<=kCornerDelta && dy1<=kCornerDelta ) return kDragSzX1Y1;
        if( dx2<=kCornerDelta && dy1<=kCornerDelta ) return kDragSzX2Y1;
        if( dx1<=kCornerDelta && dy2<=kCornerDelta ) return kDragSzX1Y2;
        if( dx2<=kCornerDelta && dy2<=kCornerDelta ) return kDragSzX2Y2;
        if( dx1<=kSideDelta ) return kDragSzLeft;
        if( dx2<=kSideDelta ) return kDragSzRight;
        if( dy1<=kSideDelta ) return kDragSzTop;
        if( dy2<=kSideDelta ) return kDragSzBottom;

        // Not an edge, but inside the area
        return kDragMove;
    }

    return kDragNone;
}

Qt::CursorShape QMorphingArea::getCursorShape( QMorphingArea::TDragMode mode )
{
    switch( mode ){
        case kDragNone:                            return Qt::ArrowCursor;
        case kDragMove:                            return Qt::SizeAllCursor;
        case kDragSzLeft:    case kDragSzRight:    return Qt::SizeHorCursor;
        case kDragSzTop:    case kDragSzBottom: return Qt::SizeVerCursor;
        case kDragSzX1Y1:    case kDragSzX2Y2:    return Qt::SizeFDiagCursor;
        case kDragSzX2Y1:    case kDragSzX1Y2:    return Qt::SizeBDiagCursor;
        case kDragHandle:                        return Qt::CrossCursor;
    };
    return Qt::ArrowCursor;
}

void QMorphingArea::addFlashPoint( const QMorphingArea::TFlashPoint& point, bool upd  )
{
    Q_ASSERT( point.isValid() );
    if( point.isValid() ){
        m_FlashPoints.push( point );
        if( upd )
            update();
    }
}

void QMorphingArea::clearFlashPoints( bool upd )
{
    if( m_FlashPoints.getSize() ){
        m_FlashPoints.resize( 0 );
        if( upd )
            update();
    }
}


