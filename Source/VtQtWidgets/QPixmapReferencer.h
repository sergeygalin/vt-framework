//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A helper base class for all classes which use a read-only pixmap.
// It makes easier to write controls which either share pixmaps
// or have their own, private pixmap copies.
//
// NOTE: It is not a descendant of QObject because it is not possible
// to have multiple QObject ancestors for its descendants. So to
// get a notification about pixmap change one has to override or
// wrap the virtual functions.
//
// NOTE: !!!! In future, this class could somehow employ Qt's smart pointers
// (it doesn't do now).
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QPIXMAPREFERENCER_H
#define QPIXMAPREFERENCER_H

#include <QtGui/QPixmap>
#include <QtCore/QList>

class QPixmapReferencer
{
public:
    QPixmapReferencer();
    QPixmapReferencer( const QPixmap& pm ); // Shared pixmap (ownership is NOT transferred).
    QPixmapReferencer( const QString& fileName, const char *format = 0,
        Qt::ImageConversionFlags flags = Qt::AutoColor );

    QPixmapReferencer( const QPixmapReferencer& src );
    void CopyFrom( const QPixmapReferencer& src );
    QPixmapReferencer& operator=( const QPixmapReferencer& src );

    virtual ~QPixmapReferencer();

    virtual void releasePixmap(); // Unlinks from pixmap, if it was not shared it is deleted.
    virtual void setPixmap( const QPixmap& pm ); // Shared pixmap (ownership is NOT transferred).
    virtual void setPixmap( const QString& fileName, const char *format = 0,
        Qt::ImageConversionFlags flags = Qt::AutoColor );

    virtual const QPixmap* pixmap() const;
protected:
    const QPixmap *m_ExtPixmap;
    QPixmap *m_MyPixmap;
};


//
// Multi pixmap referencer
//
class QMultiPixmapReferencer: public QPixmapReferencer
{
public:
    QMultiPixmapReferencer();
    QMultiPixmapReferencer( const QPixmap& pm ); // Shared pixmap (ownership is NOT transferred).
    QMultiPixmapReferencer( const QString& fileName, const char *format = 0,
        Qt::ImageConversionFlags flags = Qt::AutoColor );
    virtual ~QMultiPixmapReferencer();

    virtual bool IsMulti() const;
    virtual int NPixmaps() const;

    virtual void releasePixmap();
    virtual void setPixmap( const QPixmap& pm ); // Shared pixmap (ownership is NOT transferred).
    virtual void setPixmap( const QString& fileName, const char *format = 0,
        Qt::ImageConversionFlags flags = Qt::AutoColor );

    virtual void setPixmap( int idx, const QPixmap& pm );
    virtual void setPixmap( int idx, const QString& fileName, const char *format = 0,
        Qt::ImageConversionFlags flags = Qt::AutoColor );

    //
    // Most frequent use-cases have special functions
    //
    virtual void setPixmaps( const QString& fileName1, const QString& fileName2 );

    virtual const QPixmap* pixmap() const;
    virtual const QPixmap* pixmap( int idx ) const;

protected:
    QList<QPixmapReferencer> m_Pixmaps;
    void ResizePixmaps( int idx );
};



#endif

