//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A dial control based on QDial which uses pixmaps.
// There are many ways how pixmaps can be combined to create resulting look
// of the dial (see QPixmapDial::TDialPixType).
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QPIXMAPDIAL_H
#define QPIXMAPDIAL_H


#include <QtGui/QDial>
#include "QPixmapReferencer.h"

//
// "Pixmap deal" is a deal (knob) control which uses pixmaps as source images.
// There are some methods of building an image of the knob from source pixmaps:
// see TDialPixType comments.
//
class QPixmapDial: public QDial, protected QPixmapReferencer
{
    Q_OBJECT
public:

    // Determines how dial's visual look is created from source pixmap.
    enum TDialPixType{
        kSimple,            // One image, rotated.
        kVerticalFilm,        // All possible knob positions are drawn in a vertical strip.
        kHorizontalFilm,    // All possible knob positions are drawn in a horizontal strip.
        kAutoFilm,            // Automatically detects vertical/horizontal strip.
        kBackAndArrow,        // Pixmap is split to two parts, first part is background (static),
                            // second is the same type as background, mostly transparent, with
                            // an arrow or dot which marks position of the knob, drawn up
                            // (12 o'clock).

        kDefault = kSimple
    };

    QPixmapDial( QWidget *parent=0 );
    virtual ~QPixmapDial();

    // Note: init() functions call setAutoNFrames().
    void init( const QString& pictureSrc, TDialPixType type=kDefault );
    void init( const QPixmap& pixmap, TDialPixType type=kDefault );

    // Position of the knob when it's set to zero, in degrees, measured
    // from 12 o'clock position.
    // E.g.: -90 => 9 o'clock, -150 => 7 o'clock.
    Q_PROPERTY( qreal zeroAngle READ getZeroAngle WRITE setZeroAngle );
    qreal getZeroAngle() const { return m_ZeroAngle; }
    virtual void setZeroAngle( qreal angle );

    // Position of the knob when it's set to max value.
    Q_PROPERTY( qreal maxAngle READ getMaxAngle WRITE setMaxAngle );
    qreal getMaxAngle() const { return m_MaxAngle; }
    virtual void setMaxAngle( qreal angle );

    virtual void setAngles( qreal startAngle, qreal maxAngle );

    Q_PROPERTY( bool virtualSlider READ getVirtualSlider WRITE setVirtualSlider );
    bool getVirtualSlider() const { return m_VirtualSlider; }
    virtual void setVirtualSlider( bool virt ){ m_VirtualSlider = virt; }

    // Virtual slider orientation (vertical is default)
    Q_PROPERTY( bool virtualSliderVertical READ getVirtualSliderVertical WRITE setVirtualSliderVertical );
    bool getVirtualSliderVertical() const { return m_VirtualVertical; }
    void setVirtualSliderVertical( bool virt ){ m_VirtualVertical = virt; }

    // Height of the virtual slider
    Q_PROPERTY( int virtualHeight READ getVirtualHeight WRITE setVirtualHeight );
    int getVirtualHeight() const { return m_VirtualHeight; }
    void setVirtualHeight( int vh ){ m_VirtualHeight = vh; }

    // Set number of frames for kVerticalFilm / kHorizontalFilm mode.
    // Note: init() functions call setAutoNFrames().
    Q_PROPERTY( int nFrames READ getNFrames WRITE setNFrames );
    int getNFrames() const { return m_NFrames; }
    void setNFrames( int nf );

    // Automatically detects number of frames for
    // kVerticalFilm / kHorizontalFilm mode by dividing image dimensions
    // (assuming that knob images are usually square).
    void setAutoNFrames();

protected:
    TDialPixType m_PixType;
    qreal m_ZeroAngle, m_MaxAngle;
    bool m_VirtualSlider, m_VirtualVertical, m_Tracking;
    int
        m_DownPos,            // Used in mouse tracking
        m_DownValue,        // Used in mouse tracking
        m_VirtualHeight,    // Used in mouse tracking (virtual slider size)
        m_NFrames;            // Number of frames in stupid mode

    virtual void paintEvent(QPaintEvent* event);
    virtual void mousePressEvent(QMouseEvent *me);
    virtual void mouseReleaseEvent(QMouseEvent *me);
    virtual void mouseMoveEvent(QMouseEvent *me);


private:
};














#endif


