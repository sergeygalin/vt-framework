//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <math.h>
#include "QRectangularMapEditorContainer.h"
#include "vt_audiomath.hpp"

QRectangularMapEditorContainer::QRectangularMapEditorContainer( QRectangularMapEditor *editr, QWidget* parent ):
    QWidget( parent ),
    m_EnableHZoom( true ),
    m_EnableVZoom( true ),
    m_EnableHScroll( true ),
    m_EnableVScroll( true ),
    m_Editor( NULL ),
    m_HScrollBar( NULL ),
    m_VScrollBar( NULL ),
    m_VZoomIn( NULL ),
    m_VZoomOut( NULL ),
    m_HZoomIn( NULL ),
    m_HZoomOut( NULL ),
    m_ZoomReset( NULL ),
    m_bIgnoreScrolling( false )
{
    //
    // Creating sub-controls (in tab order)
    //
    m_Editor = editr;
    m_Editor->setParent( this );
    m_VZoomIn = new QPushButton( "+", this );
    m_VZoomOut = new QPushButton( "-", this );
    m_VZoomReset = new QPushButton( "*", this );
    m_VScrollBar = new QScrollBar( Qt::Vertical, this );
    m_HZoomIn = new QPushButton( "+", this );
    m_HZoomOut = new QPushButton( "-", this );
    m_HZoomReset = new QPushButton( "*", this );
    m_HScrollBar = new QScrollBar( Qt::Horizontal, this );
    m_ZoomReset = new QPushButton( "*", this );

    //
    // Connecting events
    //
    // Buttons and scrollbars
    connect( m_VZoomIn, SIGNAL(clicked()), this, SLOT(VZoomInClick()) );
    connect( m_VZoomOut, SIGNAL(clicked()), this, SLOT(VZoomOutClick()) );
    connect( m_VZoomReset, SIGNAL(clicked()), this, SLOT(VZoomResetClick()) );
    connect( m_HZoomIn, SIGNAL(clicked()), this, SLOT(HZoomInClick()) );
    connect( m_HZoomOut, SIGNAL(clicked()), this, SLOT(HZoomOutClick()) );
    connect( m_HZoomReset, SIGNAL(clicked()), this, SLOT(HZoomResetClick()) );
    connect( m_ZoomReset, SIGNAL(clicked()), this, SLOT(ZoomResetClick()) );
    connect( m_HScrollBar, SIGNAL(valueChanged(int)), this, SLOT(HScrollbarChange(int)) );
    connect( m_VScrollBar, SIGNAL(valueChanged(int)), this, SLOT(VScrollbarChange(int)) );

    // Refreshing signals
    connect( &(mapEditor()->map()), SIGNAL(mapChanged()), this, SLOT(updateControlsByMap()) );

    // Setting control positions
    resizeEvent( NULL );
    // updateControlsByMap() is called by resizeEvent().
}

QRectangularMapEditorContainer::~QRectangularMapEditorContainer()
{
}

void QRectangularMapEditorContainer::resizeEvent( QResizeEvent * )
{
    // !!!!!!!!!!!
    const int SZ = 18, intMargin = 4, extMargin = 6;


    m_Editor->move( extMargin, extMargin );
    int editorWidth = width()-extMargin*2, editorHeight = height()-extMargin*2;
    if( getEnableHScroll() || getEnableHZoom() )
        editorHeight -= SZ+intMargin;
    if( getEnableVScroll() || getEnableVZoom() )
        editorWidth -= SZ+intMargin;
    m_Editor->resize( editorWidth, editorHeight );

    int r = m_Editor->x() + m_Editor->width() + intMargin,
        b = m_Editor->y() + m_Editor->height() + intMargin;

    bool zoomResetVisible = getEnableHZoom() && getEnableVZoom();
    bool verPartVisible = zoomResetVisible || getEnableVZoom() || getEnableVScroll();
    bool horPartVisible = zoomResetVisible || getEnableHZoom() || getEnableHScroll();

    m_VScrollBar->move( r, SZ*(getEnableVZoom()? 3: 0)+extMargin );
    m_VScrollBar->resize( SZ, height()+SZ*
        ( (horPartVisible? -1: 0) + (getEnableVZoom()? -3: 0))-extMargin*2 );
    m_VScrollBar->setVisible( getEnableVScroll() );

    m_HScrollBar->move( SZ*(getEnableHZoom()? 3: 0)+extMargin, b );
    m_HScrollBar->resize( width()+SZ*
        ( (verPartVisible? -1: 0) + (getEnableHZoom()? -3: 0))-extMargin*2, SZ );
    m_HScrollBar->setVisible( getEnableHScroll() );

    m_VZoomIn->resize( SZ, SZ );
    m_VZoomIn->move( r, extMargin );
    m_VZoomIn->setVisible( getEnableVZoom() );

    m_VZoomOut->resize( SZ, SZ );
    m_VZoomOut->move( r, SZ+extMargin );
    m_VZoomOut->setVisible( getEnableVZoom() );

    m_VZoomReset->resize( SZ, SZ );
    m_VZoomReset->move( r, SZ*2+extMargin );
    m_VZoomReset->setVisible( getEnableVZoom() );

    m_HZoomIn->resize( SZ, SZ );
    m_HZoomIn->move( extMargin, b );
    m_HZoomIn->setVisible( getEnableHZoom() );

    m_HZoomOut->resize( SZ, SZ );
    m_HZoomOut->move( SZ+extMargin, b );
    m_HZoomOut->setVisible( getEnableHZoom() );

    m_HZoomReset->resize( SZ, SZ );
    m_HZoomReset->move( SZ*2+extMargin, b );
    m_HZoomReset->setVisible( getEnableHZoom() );

    m_ZoomReset->resize( SZ, SZ );
    m_ZoomReset->move( r, b );
    m_ZoomReset->setVisible( zoomResetVisible );

    // Set control ranges and etc.
    updateControlsByMap();
}

void QRectangularMapEditorContainer::paintEvent( QPaintEvent *event )
{
    // Drawing background
    if( pixmap()!=NULL ){
        QPainter painter( this );
        painter.setRenderHint( QPainter::Antialiasing );
        painter.setRenderHint( QPainter::SmoothPixmapTransform );
        painter.setRenderHint( QPainter::HighQualityAntialiasing );
        painter.drawPixmap( rect(), *(pixmap()) );
    }
    // Drawing sub-widgets
    QWidget::paintEvent( event );
}

void QRectangularMapEditorContainer::VZoomInClick()
{
    mapEditor()->map().zoomInY();
}

void QRectangularMapEditorContainer::VZoomOutClick()
{
    mapEditor()->map().zoomOutY();
}

void QRectangularMapEditorContainer::HZoomInClick()
{
    mapEditor()->map().zoomInX();
}

void QRectangularMapEditorContainer::HZoomOutClick()
{
    mapEditor()->map().zoomOutX();
}

void QRectangularMapEditorContainer::HZoomResetClick()
{
    mapEditor()->zoomXReset();
}

void QRectangularMapEditorContainer::VZoomResetClick()
{
    mapEditor()->zoomYReset();
}

void QRectangularMapEditorContainer::ZoomResetClick()
{
    mapEditor()->zoomReset();
}

void QRectangularMapEditorContainer::HScrollbarChange( int value )
{
    if( !m_bIgnoreScrolling ){
        if( m_HScrollBar->maximum()<=0 ){
            VTDEBUGMSGT( "H. scroll bar maximum must be positive." );
            return;
        }
        qreal posPc = qreal(value)/qreal(m_HScrollBar->maximum()) * 100.0;
        VTASSERT( posPc>=0.0 && posPc<=100.0 );
        mapEditor()->map().scrollXToPos( posPc );
    }
}

void QRectangularMapEditorContainer::VScrollbarChange( int value )
{
    if( !m_bIgnoreScrolling ){
        if( m_VScrollBar->maximum()<=0 ){
            VTDEBUGMSGT( "V. scroll bar maximum must be positive." );
            return;
        }
        qreal posPc = qreal(value)/qreal(m_VScrollBar->maximum()) * 100.0;
        VTASSERT( posPc>=0.0 && posPc<=100.0 );
        mapEditor()->map().scrollYToPos( posPc );
    }
}

void QRectangularMapEditorContainer::updateControlsByMap()
{
    m_bIgnoreScrolling = true;

    // Scrollbar formula:
    // document length = maximum() - minimum() + pageStep().
    // Therefore maximum should be reduced so the sum is 100.
    qreal zoomX = mapEditor()->map().zoomXArea();
    int pagew = VT::Math::Clip( VT::Math::Round( zoomX ), 0, 100 );
    int scaleScrollPosX = 100-pagew;
    int scrollx = VT::Math::Clip( VT::Math::Round( mapEditor()->map().scrollXPos() * scaleScrollPosX/100.0  ), 0, 100 );
    m_HScrollBar->setMinimum( 0 );
    m_HScrollBar->setMaximum( scaleScrollPosX );
    m_HScrollBar->setValue( scrollx );
    m_HScrollBar->setPageStep( pagew );

    qreal zoomY = mapEditor()->map().zoomYArea();
    int pageh = VT::Math::Clip( VT::Math::Round( zoomY ), 0, 100 );
    int scaleScrollPosY = 100-pageh;
    int scrolly = VT::Math::Clip( VT::Math::Round( mapEditor()->map().scrollYPos() * scaleScrollPosY/100.0  ), 0, 100 );
    m_VScrollBar->setMinimum( 0 );
    m_VScrollBar->setMaximum( scaleScrollPosY );
    m_VScrollBar->setValue( scrolly );
    m_VScrollBar->setPageStep( pageh );

    // Enabling / disabling zooming buttons

    m_HZoomIn->setEnabled( zoomX > mapEditor()->map().getMinXZoom() );
    m_VZoomIn->setEnabled( zoomY > mapEditor()->map().getMinYZoom() );
    m_HZoomOut->setEnabled( zoomX < 100.0 );
    m_VZoomOut->setEnabled( zoomY < 100.0 );

    m_HZoomReset->setEnabled( pagew<100 );
    m_VZoomReset->setEnabled( pageh<100 );
    m_ZoomReset->setEnabled( pagew<100 || pageh<100 );

    m_bIgnoreScrolling = false;

}







