//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A Qt widget which is primarily intended to imitate blinking LED's.
// It uses a pixmap which contains 2 states of the "LED" (on and off).
// Simply call blink() to and the LED will blink.
// Of course the pixmap may also contain not a "LED" but anyhing else
// which "flashes".
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QBLINKINGLED_H
#define QBLINKINGLED_H

#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include "QPixmapReferencer.h"

class QBlinkingLed: public QWidget, public QPixmapReferencer
{
    Q_OBJECT
public:
    static const int kDefaultBlinkTimeMs = 50;

    QBlinkingLed( QWidget* parent = NULL );
    virtual ~QBlinkingLed();

    Q_PROPERTY( int blinkTime READ getBlinkTime WRITE setBlinkTime );
    int getBlinkTime() const { return m_BlinkTime; }
    void setBlinkTime( int t ){ m_BlinkTime = t; }

public slots:
    virtual void blink();

protected:
    QTimer m_Timer;
    virtual void paintEvent( QPaintEvent* ev );

protected slots:
    virtual void timer();

private:
    int m_BlinkTime;
    bool m_On;
};



#endif
