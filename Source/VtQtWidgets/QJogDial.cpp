//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <math.h>
#include <QtGui/QMouseEvent>
#include <QtGui/QWheelEvent>
#include "QJogDial.h"
#include "vt_audiomath.hpp"
#include "vt_assert.hpp"

using namespace VT::Math;

QJogDial::QJogDial( QWidget* parent ):
    QPixmapDial( parent ),
    m_Rotating( false ),
    m_ClickAngleDeg( 5 ),
    m_DownAngleRad( 0 ),
    m_Test( 0 ),
    m_MouseWheelDelta( 0 )
{
    QPixmapDial::setVirtualSlider( false );
    QPixmapDial::setZeroAngle( -180.0 );
    QPixmapDial::setMaxAngle( 180.0 );
    QPixmapDial::setMinimum( 0 );
    QPixmapDial::setMaximum( 2000000000 );
    QPixmapDial::setValue( 0 );
}

QJogDial::~QJogDial()
{
}

void QJogDial::mousePressEvent(QMouseEvent *me)
{
    m_DownAngleRad = GetAngleRad( me->x(), me->y() );
    m_Rotating = true;

    //m_Test = m_DownAngleRad; emit jogTest(); //!!!
}

void QJogDial::mouseReleaseEvent(QMouseEvent *)
{
    m_Rotating = false;
}

void QJogDial::mouseMoveEvent(QMouseEvent *me)
{
    m_MouseWheelDelta = 0; // !!! not sure it's the best

    if( m_Rotating ){
        // Current mouse angle
        qreal posRad = GetAngleRad( me->x(), me->y() );

        // Change of angle since the last time
        qreal diffRad = posRad - m_DownAngleRad;

        //
        // The line between NE and SE quadrants is an anomaly where angle wraps
        // from Pi to -Pi, so it requires some additional heuristics to determine
        // the real change of the angle.
        //

        // Moved from SE to NE
        if( m_DownAngleRad > kPiD2 && posRad < -kPiD2 )
            diffRad += k2Pi;
        // Moved from NE to SE
        else if( m_DownAngleRad < -kPiD2 && posRad > kPiD2 )
            diffRad -= k2Pi;

        //m_Test = posRad; emit jogTest();
        // m_Test =diffRad; emit jogTest();

        // Click angle is th angle between wheel's "clicks" (steps)
        qreal clickRad = getClickAngle()*(kPi/180.0);
        VTASSERT( clickRad > 0.0 );

        int nClicks = 0;
        if( diffRad>=clickRad ){
            // Emit required number of click events
            for( qreal j = diffRad; j>=clickRad; j-=clickRad ){
                nClicks++;
                emit jogLeft();
                emit jog( false );
            }

        }else if( diffRad<=-clickRad ){
            // Emit required number of click events
            for( qreal j = -diffRad; j>=clickRad; j-=clickRad ){
                nClicks--;
                emit jogRight();
                emit jog( true );
            }
        }

        if( nClicks != 0 ){
            // Emit "cumulative" event
            emit jogClicks( -nClicks );

            // Display wheel rotation
            qreal angleChangeRad = qreal(nClicks) * clickRad;
            RotateWheel( angleChangeRad );

            // Change wheel down angle to avoid error when mouse moved
            // over a whole quadrant.
            m_DownAngleRad += angleChangeRad;

            // Normalize angle to make it fit the range we use: -Pi..Pi
            while( m_DownAngleRad > kPi )
                m_DownAngleRad -= k2Pi;
            while( m_DownAngleRad < -kPi )
                m_DownAngleRad += k2Pi;
        }
    }
}

void QJogDial::RotateWheel( qreal angleChangeRad )
{
    // m_Test = angleChangeRad * 180 / kPi; emit jogTest();
    qreal oldValueF = qreal(value());
    qreal dialRangeF = qreal(maximum()), dialRangeF2 = dialRangeF / 2.0;
    qreal oldValue01 = (oldValueF-dialRangeF2) / dialRangeF2; // [-1..1]
    m_Test = oldValue01 * 180; emit jogTest();
    qreal angleChange01 = angleChangeRad  / kPi;
    // m_Test = angleChange01 * 180; emit jogTest();
    qreal newValue01 = oldValue01 - angleChange01;

    while( newValue01 > 1.0 )
        newValue01 -= 2.0;
    while( newValue01 < -1.0 )
        newValue01 += 2.0;

    qreal newValueF = (newValue01 + 1.0) * dialRangeF2;
    qreal newValueFClip = Clip<qreal>( newValueF, qreal(minimum()), qreal(maximum()) );
    int newValue = SRound( newValueFClip );
    QPixmapDial::setValue( newValue );
}

void QJogDial::showEvent( QShowEvent * )
{
    m_MouseWheelDelta = 0;
}

void QJogDial::wheelEvent( QWheelEvent *we )
{
    m_MouseWheelDelta -= we->delta();
    const int kDeltaPerStep = 8*15; // eights of degree; 15 degrees per click
    int numSteps = m_MouseWheelDelta / kDeltaPerStep;
    if( numSteps>0 ){
        for( int i=0; i<numSteps; i++ ){
            emit jogRight();
            emit jog( true );
        }

    }else if( numSteps<0 ){
        for( int i=0; i<-numSteps; i++ ){
            emit jogLeft();
            emit jog( false );
        }
    }
    emit jogClicks( numSteps );
    if( numSteps!=0 ){
        RotateWheel( -numSteps*getClickAngle()*(kPi/180.0) );
        m_MouseWheelDelta -= numSteps*kDeltaPerStep;
    }
    we->accept();
}

qreal QJogDial::GetAngleRad( qreal x, qreal y ) const
{
    qreal cx = qreal(width())/2.0, cy = qreal(height())/2.0;
    qreal dx = x-cx, dy=cy-y;
    return atan2( dy, dx );
}



void QJogDial::setVirtualSlider( bool )
{
    VTDEBUGMSGT("This function should not be called for QJogDial");
}

void QJogDial::setZeroAngle( qreal )
{
    VTDEBUGMSGT("This function should not be called for QJogDial");
}

void QJogDial::setMaxAngle( qreal )
{
    VTDEBUGMSGT("This function should not be called for QJogDial");
}

void QJogDial::setMinimum( int )
{
    VTDEBUGMSGT("This function should not be called for QJogDial");
}

void QJogDial::setMaximum( int )
{
    VTDEBUGMSGT("This function should not be called for QJogDial");
}

void QJogDial::setValue( int )
{
    VTDEBUGMSGT("This function should not be called for QJogDial");
}







