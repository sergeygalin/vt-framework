//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPainter>
#include <QtGui/QFont>
#include <QtGui/QMouseEvent>
#include "QPiano.h"

static const int
    kKeyDownTimer  = 50, // Period of standalone mode timer (msec)
    kKeyDownPeriods = 3; // Number of periods a key stays pressed

static const BYTE
    kMaxPossibleNote = 127;

const QColor
    QPiano::kDefaultWhiteColor( 250, 250, 230, 255 ), // Slightly yellowish white
    QPiano::kDefaultBlackColor( 65, 65, 65, 255 ), // Very dark gray ("shining black")
    QPiano::kDefaultBassColor( 250, 230, 230, 255 ), // Slightly pink
    QPiano::kDefaultWhiteCursorColor( 250, 100, 100 ), // Red
    QPiano::kDefaultBlackCursorColor( 100, 30, 30 ); // Dark red / brown


QPiano::QPiano( QWidget* parent ):
    QWidget( parent ),
    VT::Piano::CKeyboardPosCalc(),
    VT::Piano::CKeyboardStatePoll(),
    m_NoteNameFont( "Helvetica", 10, false, false ),
    m_FinePrintFont( "Helvetica", 5, false, false ),
    m_Timer( this ),
    m_bVertical( false ),
    m_LeftLight( true ),
    m_Shadows( true ),
    m_NoteNames( false ),
    m_FinePrint( false ),
    m_FinePrintVertically( true ),
    m_SecondaryNoteNames( false ),
    m_Octaves_0_11( false ),
    m_Standalone( false ),
    m_Transpose( 0 ),
    m_BassTranspose( 0 ),
    m_SecondaryTranspose( 0 ),
    m_NoteText( kNamesOnly ),
    m_PressedKeyDarkener( 1.0 ),
    m_WhiteColor( kDefaultWhiteColor ),
    m_BlackColor( kDefaultBlackColor ),
    m_BassColor( kDefaultBassColor ),
    m_BlackCursorColor( kDefaultBlackCursorColor ),
    m_WhiteCursorColor( kDefaultWhiteCursorColor ),
    m_LastPressedKey( VT::MIDI::kErrorNote ),
    m_LastSentVelocity( 0 ),
    m_SplitMainPartStartKey( 0 ),
    m_Cursor( VT::MIDI::kErrorNote ),
    m_fBlackKeyLengthK( 0.67 ),
    m_bEnableCursor( false ),
    m_pColors( NULL )
{
    resizeEvent( NULL );
    //bool c =
    connect( &m_Timer, SIGNAL(timeout()), this, SLOT(onTimer()) );
    //VTASSERT2T( c, "Timer signal connection failed." );
    ResetKeyCounters();
}

QPiano::~QPiano()
{
    ReleaseKey();
}

// !!! See also: SetNoteRange() !!!
void QPiano::setKeyRange( BYTE startingNote, BYTE nNotes )
{
    if( nNotes < 1 )
        throw VT::CInvalidParameterException( VTT("Number of piano notes must be positive.") );
    if( startingNote > 127 )
        throw VT::CInvalidParameterException( VTT("Piano's starting note must be within MIDI range: [0..127].") );

    if( VT::Notes::IsBlackKey( startingNote ) ){
        VTDEBUGMSGT( "Can't set first QPiano note black - the range will be adjusted." );
        // The first black key is 1 which will turn into C0 / C-1 (depending on octave numbering system).
        startingNote--;
        nNotes++;
    }
    if( VT::Notes::IsBlackKey( startingNote + nNotes - 1 ) ){
        VTDEBUGMSGT( "Can't set last QPiano note black - the range will be adjusted." );
        // The last possible black key is 126 - will turn into 127 (which is a white key).
        nNotes++;
    }

    VT::Piano::CKeyboardPosCalc::Init( startingNote, nNotes,
        (isVertical())? height(): width() );
    CalcKeyRects();
    emit geometryChanged();
    if( getCursor()==VT::MIDI::kErrorNote ){
        setCursor( startingNote + nNotes / 2 );
    }else if( getCursor() < startingNote ){
        setCursor( startingNote );
    }else if( getCursor() > startingNote + nNotes - 1 ){
        setCursor( startingNote + nNotes - 1 );
    }
    update();
}

void QPiano::SetBlackKeyWidthK( double k )
{
    VT::Piano::CKeyboardPosCalc::SetBlackKeyWidthK( k );
    CalcKeyRects();
    emit geometryChanged();
    update();
}

void QPiano::setDefaultKeyColors()
{
    m_WhiteColor = kDefaultWhiteColor;
    m_BlackColor = kDefaultBlackColor;
    m_BassColor = kDefaultBassColor;
    m_BlackCursorColor = kDefaultBlackCursorColor;
    m_WhiteCursorColor = kDefaultWhiteCursorColor;
    update();
}

void QPiano::setFinePrint( bool fp )
{
    if( fp != m_FinePrint ){
        m_FinePrint = fp;
        if( m_FinePrint )
            CalcFinePrintRects();
        update();
    }
}

void QPiano::setFinePrintVertically( bool vertical )
{
    if( m_FinePrintVertically != vertical ){
        m_FinePrintVertically = vertical;
        update();
    }
}

void QPiano::copyState( const VT::Piano::CKeyboardStatePoll& state )
{
    // (Only the state is copied, no need to do anything about the geometry.)
    VT::Piano::CKeyboardStatePoll::CopyFrom( state );
    update();
}

void QPiano::setVertical( bool vert )
{
    if( m_bVertical != vert ){
        m_bVertical = vert;
        resizeEvent( NULL ); // Will recalculate the geometry
        emit geometryChanged();
        update();
    }
}

void QPiano::SetPianoColors( const VT::Piano::CPianoColors* colors )
{
    m_pColors = colors;
    update();
}

void QPiano::SetRainbowColors()
{
    SetPianoColors( &VT::Piano::CRainbowColors::instance() );
}

void QPiano::SetScriabinColors()
{
    SetPianoColors( &VT::Piano::CScriabinColors::instance() );
}

void QPiano::SetNoColors()
{
    SetPianoColors( NULL );
}


static VTINLINE QColor CombineKeyColors( const QColor& baseColor, const QColor& palette, qreal w1=0.3, qreal saturation=0.8 )
{
    if( palette==Qt::black )
        return baseColor;

    qreal h, s, v;
    palette.getHsvF ( &h, &s, &v );
    s *= saturation;
    QColor paletteColor = QColor::fromHsvF( h, s, v );

    qreal w2 = 1.0-w1;

    QColor combine;
    combine.setRgbF(
        baseColor.redF()*w1 + paletteColor.redF()*w2,
        baseColor.greenF()*w1 + paletteColor.greenF()*w2,
        baseColor.blueF()*w1 + paletteColor.blueF()*w2
    );
    return combine;
}

QColor QPiano::getWhiteKeyColor( BYTE key ) const
{
    if( m_pColors==NULL ){
        const QColor& baseColor = IsBassKey( key )? m_BassColor: m_WhiteColor;
        return baseColor;
    }else{
        if( IsBassKey( key ) )
            return CombineKeyColors( Qt::gray, m_pColors->GetQColor( key ), 0.5, 0.4 );
        else
            return CombineKeyColors( kDefaultWhiteColor, m_pColors->GetQColor( key ) );;
    }
}

QColor QPiano::getBlackKeyColor( BYTE key ) const {
    if( m_pColors!=NULL ){
        if( IsBassKey( key ) )
            return CombineKeyColors( kDefaultBlackColor, m_pColors->GetQColor( key ), 0.5, 0.4 );
        return CombineKeyColors( kDefaultBlackColor, m_pColors->GetQColor( key ) );
    }else{
        return getBlackColor();
    }
}

void QPiano::clearAllFinePrintText()
{
    for( int i = 0; i < 128; i++ )
        m_FinePrintTexts[i].clear();

    if( m_FinePrint )
        update();
}

void QPiano::setSplitMainPartStartKey( BYTE key )
{
    if( key != m_SplitMainPartStartKey ){
        m_SplitMainPartStartKey = key;
        update();
    }
}

void QPiano::Clear()
{
    VT::Piano::CKeyboardStatePoll::Clear();
    update();
}

void QPiano::Press( BYTE note )
{
    if( note < 128 ){
        VT::Piano::CKeyboardStatePoll::Press( note );
        if( getStandalone() )
            // For standalone mode, reset key down counter
            m_KeyDownCounter[ note ] = kKeyDownPeriods;
        updateUI();
    }
}

void QPiano::Release( BYTE note )
{
    if( note < 128 ){
        VT::Piano::CKeyboardStatePoll::Release( note );
        // if( note<128 )
        //     m_KeyDownCounter[ note ] = 0;
        updateUI();
    }
}

void QPiano::EndSynthStatePoll()
{
    VT::Piano::CKeyboardStatePoll::EndSynthStatePoll();
    update();
}

BYTE QPiano::testKey( qreal x, qreal y ) const
{
    if( isVertical() )
        VT::Math::Swap( &x, &y );
    BYTE whiteKey = VT::MIDI::kErrorNote;
    BYTE end = GetEndingNote();
    // !!! Could be rewritten using bisection (but actually OK as is)
    for( BYTE k = GetStartingNote(); k<=end; k++ )
        if( k <= kMaxPossibleNote )
            if( m_Rects[k].contains( x, y ) ){
                if( !IsWhite(k) )
                    return k;
                else{
                    whiteKey = k;
                    end = VT::Math::Min<BYTE>( end, k+1 );
                }
            }
    return whiteKey;
}

void QPiano::paintEvent( QPaintEvent* )
{
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );
    painter.setRenderHint( QPainter::TextAntialiasing );

    if( isVertical() ){
        painter.rotate( -90.0 );
        painter.translate( -height(), 0 );
    }

    BYTE maxk = VT::Math::Min<BYTE>( kMaxPossibleNote, GetEndingNote() );

    // Drawing white keys
    for( BYTE k=GetStartingNote(); k<=maxk; k++ )
        if( IsWhite(k) )
            drawWhiteKey( painter, k );

    // Black keys
    for( BYTE bk=GetStartingNote(); bk<=maxk; bk++ )
        if( !IsWhite(bk) )
            drawBlackKey( painter, bk );

    // !!!!!!!!! To do: mark notes which are outside of allowed range
}

qreal QPiano::getWhiteKeyLength() const
{
    if( isVertical() )
        return width();
    else
        return height();
}

qreal QPiano::getBlackKeyLength() const
{
    return getWhiteKeyLength() * m_fBlackKeyLengthK;
}

qreal QPiano::getKeyboardLength() const
{
    if( isVertical() )
        return height();
    else
        return width();
}


void QPiano::drawWhiteKey( QPainter& painter, BYTE key )
{
    bool bassKey = IsBassKey( key );
    const QRectF& size = m_Rects[key];
    const QColor gapColor( 0, 0, 0, 255 );
    QColor whiteColor;
    if( cursorVisible() && key==getCursor() )
        whiteColor = getWhiteCursorColor();
    else
        whiteColor = getWhiteKeyColor( key ); // Uses Keyboard part color

    QColor shadeColor( whiteColor.darker(220) );

    if( !IsKeyPressed(key) ){
        painter.setBrush( whiteColor );
        painter.fillRect( size, whiteColor );

        if( getShadows() ){
            // Shadows at beginning and end of the key
            painter.setPen( whiteColor.darker(125) );
            painter.drawLine( QPointF(size.left(), size.top()+1.0), QPointF(size.right(), size.top()+1.0) );
            painter.drawLine( QPointF(size.left(), size.bottom()-1.5), QPointF(size.right(), size.bottom()-1.5) );
            painter.drawLine( QPointF(size.left(), size.bottom()-1.0), QPointF(size.right(), size.bottom()-1.0) );

            // Diagonal shadow of a black key at left/right on a RELEASED white key
            if( LeftLight() ){
                if( key>0 && int(key)-1>=int(GetStartingNote()) ){
                    BYTE left = key-1;
                    if( !IsWhite(left) ){
                        painter.setPen( shadeColor );
                        qreal r = GetKeyRightX( left ), b = getBlackKeyLength();
                        if( IsKeyPressed(left) ){
                            painter.drawLine( QPointF(r+2.0, 0), QPointF(r+1.0, b ) );
                            painter.drawLine( QPointF(r+1.0, 0), QPointF(r+1.0, b ) );
                        }else{
                            painter.drawLine( QPointF(r+1.0, 0), QPointF(r+1.0, b ) );
                            painter.drawLine( QPointF(r+2.0, 0), QPointF(r+2.0, b-1 ) );
                        }
                    }
                }
            }else{
                if( key<int(GetEndingNote()) ){
                    BYTE right = key+1;
                    if( !IsWhite(right) ){
                        painter.setPen( shadeColor );
                        qreal r = GetKeyLeftX( right ), b = getBlackKeyLength();
                        if( IsKeyPressed(right) ){
                            painter.drawLine( QPointF(r-2.0, 0), QPointF(r-1.0, b ) );
                            painter.drawLine( QPointF(r-1.0, 0), QPointF(r-1.0, b ) );
                        }else{
                            painter.drawLine( QPointF(r-1.0, 0), QPointF(r-1.0, b ) );
                            painter.drawLine( QPointF(r-2.0, 0), QPointF(r-2.0, b-1 ) );
                        }
                    }
                }
            }
        } // end of drawing shadows for released white key

        // Gap around the key
        painter.setPen( gapColor );
        painter.setBrush( Qt::NoBrush );
        painter.drawRect( size );
    }else{
        //
        // Pressed white key

        QColor pressedColor( whiteColor.darker( VT::Math::URound(118.0 * m_PressedKeyDarkener) ) );

        // Fill
        painter.setBrush( pressedColor );
        painter.fillRect( size, pressedColor );

        if( getShadows() ){
            // Diagonal shadow of a white key at left
            bool whiteKeyShadow = false;
            if( key==GetStartingNote() )
                whiteKeyShadow = true;
            else{
                if( LeftLight() ){
                    int l = FindWhiteKeyAtLeft(key);
                    if( l>=GetStartingNote() )
                        if( !IsKeyPressed(BYTE(l)) )
                            whiteKeyShadow = true;
                }else{
                    int r = FindWhiteKeyAtRight(key);
                    if( r<=GetEndingNote() )
                        if( !IsKeyPressed(BYTE(r)) )
                            whiteKeyShadow = true;
                }
            }
            if( whiteKeyShadow ){
                painter.setPen( shadeColor );
                if( LeftLight() ){
                    painter.drawLine( QPointF(size.left(), size.top()), QPointF(size.left()+1, size.bottom() ) );
                    painter.drawLine( QPointF(size.left(), size.top()), QPointF(size.left()+2, size.bottom() ) );
                }else{
                    painter.drawLine( QPointF(size.right(), size.top()), QPointF(size.right()-1, size.bottom() ) );
                    painter.drawLine( QPointF(size.right(), size.top()), QPointF(size.right()-2, size.bottom() ) );
                }
            }

            // Diagonal shadow of a black key at left/right
            painter.setPen( shadeColor );
            if( LeftLight() ){
                if( key>0 && int(key)-1>=int(GetStartingNote()) ){
                    BYTE left = key-1;
                    if( !IsWhite(left) ){
                        qreal r = GetKeyRightX( left ), b = getBlackKeyLength();
                        if( IsKeyPressed(left) ){
                            // Both are pressed...
                            painter.drawLine( QPointF(r+1.5, 0), QPointF(r+1.5, b ) );
                            painter.drawLine( QPointF(r+1.0, 0), QPointF(r+1.0, b ) );
                        }else{
                            // Shadow of a released black key over a white key
                            painter.drawLine( QPointF(r+1.0, 0), QPointF(r+1.0, b+0.3 ) );
                            painter.drawLine( QPointF(r+1.5, 0), QPointF(r+1.5, b+0.5 ) );
                            painter.drawLine( QPointF(r+1.5, 0), QPointF(r+2.5, b+0.4 ) );
                        }
                    }
                }
            }else{
                if( key<GetEndingNote() ){
                    BYTE right = key+1;
                    if( !IsWhite(right) ){
                        qreal r = GetKeyLeftX( right ), b = getBlackKeyLength();
                        if( IsKeyPressed(right) ){
                            // Both are pressed...
                            painter.drawLine( QPointF(r-1.5, 0), QPointF(r-1.5, b ) );
                            painter.drawLine( QPointF(r-1.0, 0), QPointF(r-1.0, b ) );
                        }else{
                            // Shadow of a released black key over a white key
                            painter.drawLine( QPointF(r-1.0, 0), QPointF(r-1.0, b+0.3 ) );
                            painter.drawLine( QPointF(r-1.5, 0), QPointF(r-1.5, b+0.5 ) );
                            painter.drawLine( QPointF(r-1.5, 0), QPointF(r-2.5, b+0.4 ) );
                        }
                    }
                }
            }

            // Shadows at beginning and end of the key
            painter.setPen( shadeColor );
            painter.drawLine( QPointF(size.left(), size.bottom()-1.5), QPointF(size.right(), size.bottom()-1.5) );
            painter.drawLine( QPointF(size.left(), size.bottom()-1.0), QPointF(size.right(), size.bottom()-1.0) );
            painter.drawLine( QPointF(size.left(), size.top()+1.0), QPointF(size.right(), size.top()+1.0) );
        } // end of drawing shadows for pressed white key
    } // end of pressed key

    // Gap around the key
    painter.setPen( gapColor );
    painter.setBrush( Qt::NoBrush );
    painter.drawRect( size );

    // Note name
    if( getNoteNames() ){
        if( key!= VT::MIDI::kErrorNote ){

            bool dualNames;
            if( getSecondaryNoteNames() && !bassKey && getKeyNamesTransposing()!=getSecondaryKeyNamesTransposing() )
                dualNames = true;
            else
                dualNames = false;

            BYTE displayedNote;
            if( bassKey )
                displayedNote = VT::Notes::Transpose( key, getBassKeyNamesTransposing() );
            else
                displayedNote = VT::Notes::Transpose( key, getKeyNamesTransposing() );


            if( m_NoteText == kNamesAndNumbers ){
                drawNoteName( painter, key, displayedNote, 0.09, getNoteNameLabelYK(), false );
                drawNoteName( painter, key, displayedNote, 0.09, getNoteNameLabel2YK(), true );

            }else{
                bool asNumbers = (m_NoteText == kNumbersOnly);

                if( !dualNames ){
                    drawNoteName( painter, key, displayedNote, 0.20, getNoteNameLabelYK(), asNumbers );
                }else{
                    BYTE secondaryDisplayedNote = VT::Notes::Transpose( key, getSecondaryKeyNamesTransposing() );
                    drawNoteName( painter, key, displayedNote, 0.09, getNoteNameLabelYK(), asNumbers );
                    drawNoteName( painter, key, secondaryDisplayedNote, 0.09, getNoteNameLabel2YK(), asNumbers );
                }
            }
        }
    }

    if( m_FinePrint )
        drawFinePrint( painter, key );
}

void QPiano::drawNoteName( QPainter& painter, BYTE key, BYTE displayedNote,
                          double labelHeightFraction, double labelPosFraction, bool asNumber )
{
    const QRectF& size = m_Rects[key];

    QString labelText;

    if( asNumber )
        labelText.setNum( int( displayedNote ) );
    else
        labelText =
            VT::Notes::GetFullNoteNameQTr(
                displayedNote,
                true, // use one-letter notation
                (m_Octaves_0_11)? 0: -1 );


    // NOTE: size is the rectangle around the key

    qreal labelHeight = size.height() * labelHeightFraction;
    qreal fontSize = VT::Math::Min<qreal>(
        (size.width()-2.0) * ((labelText.length()<=2)? 0.5: 1.0/qreal(labelText.length())),
        labelHeight );

    m_NoteNameFont.setPixelSize( int( fontSize + 0.9 ) );
    painter.setFont( m_NoteNameFont );
    painter.setPen( QColor(0,0,0) );

    QRectF bounds( GetKeyLeftX(key)+1.0, size.height() * labelPosFraction,
        GetKeyWidth()-2.0, labelHeight );

    // If the key is down, the label should be offset a bit
    if( IsKeyPressed(key) )
        bounds.moveTo( bounds.left()+((LeftLight())? 0.2: -0.2), bounds.top()+1.0 );// !!!!!COPYPASTE W drawFinePrint
    painter.drawText( bounds, Qt::AlignCenter | Qt::AlignVCenter, labelText );
}

void QPiano::drawFinePrint( QPainter& painter, BYTE key )
{
    // #define QPIANO_DRAWFINEPRINTBOUNDS

    //bool m_FinePrintVertically = true; // !!!!!!!!!!!!!!!!!!

    if( !m_FinePrintTexts[key].isEmpty() ){
        painter.setFont( m_FinePrintFont );
        painter.setBrush( Qt::black );


        QRectF rect = m_FinePrintRects[key];
        if( IsKeyPressed(key) ){
            // rect.setLeft( rect.left()+1.0 );
            //rect.setTop( rect.top()+1.0 );
            rect.moveTo( rect.left()+((LeftLight())? 0.2: -0.2), rect.top()+1.0 );// !!!!!COPYPASTE W drawFinePrint
        }

        #if defined(QPIANO_DRAWFINEPRINTBOUNDS)
            painter.setPen( Qt::red );
            painter.setBrush( Qt::transparent );
            painter.drawRect( rect );
            painter.setBrush( Qt::black );
            painter.setPen( Qt::black );
        #endif

        if( !m_FinePrintVertically ){
            painter.drawText( rect, Qt::AlignCenter, m_FinePrintTexts[key] );

        }else{
            QTransform prevTransform = painter.transform();
            painter.rotate( -90.0 );

            int w = width();//, h = height(); // !!!!!!!!!!!!!! support vertical mode

            painter.translate( -w, 0 );

            QRectF rect2(
                w - rect.top() - rect.height(),
                rect.left(),
                rect.height(),
                rect.width() );

            #if defined(QPIANO_DRAWFINEPRINTBOUNDS)
                painter.setBrush( Qt::transparent );
                painter.setPen( Qt::blue );
                painter.drawRect( rect2 );
                painter.setPen( Qt::black );
                painter.setBrush( Qt::black );
            #endif

            painter.drawText( rect2, Qt::AlignLeft | Qt::AlignVCenter, m_FinePrintTexts[key] );
            painter.setTransform( prevTransform );
        }
    }
}

//
// WARNING!!! Shadows on the white keys around are drawn by drawWhiteKey().
//
void QPiano::drawBlackKey( QPainter& painter, BYTE key )
{
    const QRectF& size = m_Rects[key];
    const QColor gapColor( 0, 0, 0, 255 );

    QColor baseColor;
    if( cursorVisible() && key==getCursor() )
        baseColor = getBlackCursorColor();
    else
        baseColor = getBlackKeyColor( key );

    if( !IsKeyPressed(key) ){
        const qreal straightPart = 0.92;
        qreal edgePos = size.height()*straightPart, topEdgePos = size.height()*0.035;

        // Straight part of the key
        QRectF s( size.left(), size.top(), size.width(), edgePos );
        painter.setBrush( baseColor );
        painter.fillRect( s, baseColor );

        // Lowering part of the key
        QRectF s2( size.left(), s.bottom()-1.0, size.width(), size.height()*(1.0-straightPart)+1.0 );
        QColor fill2(baseColor.darker(120));
        painter.setBrush( fill2 );
        painter.fillRect( s2, fill2 );

        //
        // Edges
        //

        qreal edgeLeft = size.left()+size.width()*((LeftLight())? 0.15: 0.85);

        if( LeftLight() ){
            // Horizontal line at top of the key
            painter.setPen( baseColor.lighter(120) );
            painter.drawLine( QPointF(edgeLeft, topEdgePos), QPointF(size.right(), topEdgePos) );

            // Horizontal line at bottom of the key
            painter.setPen( baseColor.lighter(220) );
            painter.drawLine( QPointF(edgeLeft, edgePos), QPointF(size.right(), edgePos) );
        }else{
            // Horizontal line at top of the key
            painter.setPen( baseColor.lighter(120) );
            painter.drawLine( QPointF(size.left(), topEdgePos), QPointF(edgeLeft, topEdgePos) );

            // Horizontal line at bottom of the key
            painter.setPen( baseColor.lighter(220) );
            painter.drawLine( QPointF(size.left(), edgePos), QPointF(edgeLeft, edgePos) );
        }

        // Vertical line at left of the key
        painter.drawLine( QPointF(edgeLeft, topEdgePos), QPointF(edgeLeft, edgePos) );

        // Gap around the key
        painter.setPen( gapColor );
        painter.setBrush( Qt::NoBrush );
        painter.drawRect( size );
    }else{
        const qreal straightPart = 0.95;
        qreal edgePos = size.height()*straightPart, topEdgePos = size.height()*0.045;

        // Straight part of the key
        QRectF s( size.left(), size.top(), size.width(), edgePos );
        QColor fill2(baseColor.darker( VT::Math::URound(180.0*m_PressedKeyDarkener) ));
        painter.setBrush( fill2 );
        painter.fillRect( s, fill2 );

        // Lowering part of the key
        QRectF s2( size.left(), s.bottom()-1.0, size.width(), size.height()*(1.0-straightPart)+1.0 );
        QColor fill3(baseColor.darker( VT::Math::URound(270.0*m_PressedKeyDarkener) ));
        painter.setBrush( fill3 );
        painter.fillRect( s2, fill3 );

        //
        // Edges
        //

        qreal edgeLeft = size.left()+size.width()*((LeftLight())? 0.15: 0.85);

        if( LeftLight() ){
            // Horizontal line at top of the key
            painter.setPen( baseColor.lighter(110) );
            painter.drawLine( QPointF(edgeLeft, topEdgePos), QPointF(size.right(), topEdgePos) );

            // Horizontal line at bottom of the key
            painter.setPen( baseColor.lighter(180) );
            painter.drawLine( QPointF(edgeLeft, edgePos), QPointF(size.right(), edgePos) );
        }else{
            // Horizontal line at top of the key
            painter.setPen( baseColor.lighter(110) );
            painter.drawLine( QPointF(size.left(), topEdgePos), QPointF(edgeLeft, topEdgePos) );

            // Horizontal line at bottom of the key
            painter.setPen( baseColor.lighter(180) );
            painter.drawLine( QPointF(size.left(), edgePos), QPointF(edgeLeft, edgePos) );
        }

        // Vertical line at left of the key
        painter.setPen( baseColor.lighter(160) );
        painter.drawLine( QPointF(edgeLeft, topEdgePos), QPointF(edgeLeft, edgePos) );

        // Gap around the key
        painter.setPen( gapColor );
        painter.setBrush( Qt::NoBrush );
        painter.drawRect( size );
        painter.drawLine( QPointF(size.left(), size.bottom()-1.0), QPointF(size.right(), size.bottom()-1.0) );
    }

    if( m_FinePrint )
        drawFinePrint( painter, key );
}


void QPiano::resizeEvent( QResizeEvent *ev )
{
    QWidget::resizeEvent( ev );

    // Size of the widget has been changed - re-initialize key positions
    VT::Piano::CKeyboardPosCalc::Init( GetStartingNote(), GetNNotes(), getKeyboardLength() );

    CalcKeyRects(); // Also updates fine print rects, if necessary
}

void QPiano::mousePressEvent( QMouseEvent *me )
{
    QWidget::mousePressEvent( me );

    if( me->button()==Qt::LeftButton ){
        if( m_LastPressedKey!=VT::MIDI::kErrorNote )
            emit keyReleased( m_LastPressedKey );
        // Find the key under the mouse cursor
        m_LastPressedKey = testKey( me->x(), me->y() );
        if( m_LastPressedKey!=VT::MIDI::kErrorNote ){
            emit keyPressed( m_LastPressedKey );
            m_LastSentVelocity = VelocityFromClickPos( me->x(), me->y(), m_LastPressedKey );
            emit keyPressed( m_LastPressedKey, m_LastSentVelocity );
            if( getStandalone() )
                Press( m_LastPressedKey );
            setCursor( m_LastPressedKey );
        }
    }else
        ReleaseKey();
}

void QPiano::mouseReleaseEvent( QMouseEvent *me )
{
    ReleaseKey();
    QWidget::mouseReleaseEvent( me );
}

void QPiano::leaveEvent( QEvent *ev )
{
    ReleaseKey();
    QWidget::leaveEvent( ev );
}

void QPiano::keyPressEvent( QKeyEvent *ev )
{
    if( cursorVisible() ){
        switch( ev->key() ){
            case Qt::Key_Space:
                ev->setAccepted(true);
                Press( m_Cursor );
                emit keyPressed( m_Cursor );
                emit keyPressed( m_Cursor, 100 );
                break;
        }
    }
}

void QPiano::keyReleaseEvent( QKeyEvent *ev )
{
    if( cursorVisible() ){
        switch( ev->key() ){
            case Qt::Key_Space:
                ev->setAccepted(true);
                Release( m_Cursor );
                emit keyReleased( m_Cursor );
                break;
            case Qt::Key_Right: case Qt::Key_Down:
                {
                    ev->setAccepted(true);
                    int newCursor = int(m_Cursor)+1;
                    if( newCursor > int(GetEndingNote()) || newCursor < int(GetStartingNote()) )
                        newCursor = GetStartingNote();
                    setCursor( BYTE( newCursor ) );
                }
                break;
            case Qt::Key_Left: case Qt::Key_Up:
                {
                    ev->setAccepted(true);
                    int newCursor = int(m_Cursor)-1;
                    if( newCursor > int(GetEndingNote()) || newCursor < int(GetStartingNote()) )
                        newCursor = GetEndingNote();
                    setCursor( BYTE( newCursor ) );
                }
                break;
            case Qt::Key_PageUp:
                {
                    ev->setAccepted(true);
                    int newCursor = int(m_Cursor)-12;
                    if( newCursor > int(GetEndingNote()) || newCursor < int(GetStartingNote()) )
                        break;
                    setCursor( BYTE( newCursor ) );
                }
                break;
            case Qt::Key_PageDown:
                {
                    ev->setAccepted(true);
                    int newCursor = int(m_Cursor)+12;
                    if( newCursor > int(GetEndingNote()) || newCursor < int(GetStartingNote()) )
                        break;
                    setCursor( BYTE( newCursor ) );
                }
                break;
            case Qt::Key_Home:
                ev->setAccepted(true);
                setCursor( GetStartingNote() );
                break;
            case Qt::Key_End:
                ev->setAccepted(true);
                setCursor( GetEndingNote() );
                break;
        }
    }
}

void QPiano::showEvent( QShowEvent *ev )
{
    ResetKeyCounters();
    QWidget::showEvent( ev );
}

void QPiano::hideEvent( QHideEvent *ev )
{
    ReleaseKey();
    QWidget::hideEvent( ev );
}

void QPiano::closeEvent( QCloseEvent *ev )
{
    ReleaseKey();
    QWidget::closeEvent( ev );
}

void QPiano::focusInEvent( QFocusEvent * )
{
    update();
}

void QPiano::focusOutEvent( QFocusEvent * )
{
    update();
}

void QPiano::mouseMoveEvent( QMouseEvent *me )
{
    QWidget::mouseMoveEvent( me );

    if( me->buttons()==Qt::LeftButton ){
        // Find the key under the mouse cursor
        BYTE newKey = testKey( me->x(), me->y() );
        if( newKey == m_LastPressedKey ){
            //
            // After touch messages
            //
            BYTE newVel = VelocityFromClickPos( me->x(), me->y(), m_LastPressedKey );
            if( newVel != m_LastSentVelocity ){
                emit keyAfterTouch( m_LastPressedKey, newVel );
                m_LastSentVelocity = newVel;
            }
            return;
        }
        //
        // Mouse moved to another key
        //
        if( m_LastPressedKey!=VT::MIDI::kErrorNote )
            ReleaseKey();
        m_LastPressedKey = newKey;
        if( m_LastPressedKey!=VT::MIDI::kErrorNote ){ // Moved over some key
            emit keyPressed( m_LastPressedKey );
            m_LastSentVelocity = VelocityFromClickPos( me->x(), me->y(), m_LastPressedKey );
            emit keyPressed( m_LastPressedKey, m_LastSentVelocity );
            if( getStandalone() )
                Press( m_LastPressedKey );
            setCursor( m_LastPressedKey );
        }
    }else
        ReleaseKey();
}

void QPiano::ReleaseKey()
{
    if( m_LastPressedKey!=VT::MIDI::kErrorNote ){
        emit keyReleased( m_LastPressedKey );
        if( getStandalone() )
            Release( m_LastPressedKey );
        m_LastPressedKey = VT::MIDI::kErrorNote;
    }
}



void QPiano::CalcKeyRects()
{
    // Using VT::Piano::CKeyboardPosCalc functions and key length calculations
    // to fill the key rectangles.

    qreal wkl = getWhiteKeyLength(), wwidth = GetKeyWidth(),
        bkl = getBlackKeyLength(), bwidth = GetBlackKeyWidth();

    for( BYTE k=GetStartingNote(); k<=GetEndingNote(); k++ ){
        if( IsWhite(k) )
            m_Rects[k].setRect( GetKeyLeftX(k), 0, wwidth, wkl );
        else
            m_Rects[k].setRect( GetKeyLeftX(k), 0, bwidth, bkl );
    }

    if( getFinePrint() )
        CalcFinePrintRects(); // calls update()
    else
        update();
}

void QPiano::CalcFinePrintRects()
{
    qreal wwidth = GetKeyWidth(),
        bkl = getBlackKeyLength(),
        wkl = getWhiteKeyLength();

    qreal black_y1 = bkl*0.10;
    qreal black_y2 = bkl*0.87;
    qreal white_y1 = bkl + wkl*0.05;
    qreal white_y2 = wkl*getNoteNameLabelYK()*0.98;

    qreal black_x = GetBlackKeyWidth()*0.2;
    qreal white_x = wwidth*0.05;
    qreal black_w = GetBlackKeyWidth()*0.75;
    qreal white_w = wwidth*0.9;

    qreal black_h = black_y2 - black_y1;
    qreal white_h = white_y2 - white_y1;

    for( BYTE k=GetStartingNote(); k<=GetEndingNote(); k++ )
        if( IsWhite(k) )
            m_FinePrintRects[k].setRect( m_Rects[k].left() + white_x, m_Rects[k].top() + white_y1, white_w, white_h );
        else
            m_FinePrintRects[k].setRect( m_Rects[k].left() + black_x, m_Rects[k].top() + black_y1, black_w, black_h );

    update();
}

BYTE QPiano::VelocityFromClickPos( int x, int y, BYTE key ) const
{
    int len = IsWhite(key)? getWhiteKeyLength(): getBlackKeyLength();
    if( !len )
        return 0;
    int click = ( isVertical() )? x: y;
    qreal pc = VT::Math::Clip( qreal(click)/qreal(len), 0.0, 1.0 );
    // Never send 0 velocity - that would be a synonim for Note Off
    BYTE ret = VT::Math::Clip( VT::Math::URound( pc*126.0+1.0 ), 1, 127 );
    return ret;
}

void QPiano::Init( BYTE startingNote, BYTE nNotes, int )
{
    ResetKeyCounters();
    VT::Piano::CKeyboardPosCalc::Init( startingNote, nNotes, getKeyboardLength() );
    CalcKeyRects();
    emit geometryChanged();
}

void QPiano::CopyFrom( const CKeyboardPosCalc& src )
{
    VT::Piano::CKeyboardPosCalc::CopyFrom( src );
    CalcKeyRects();
    emit geometryChanged();
}

// !!! See also: SetKeyRange() !!!
/*
void QPiano::SetNoteRange( BYTE minNote, BYTE maxNote )
{
    VT::Piano::CKeyboardPosCalc::SetNoteRange( minNote, maxNote );
    CalcKeyRects();
    emit geometryChanged();
}*/

void QPiano::setStandalone( bool sa )
{
    if( sa != m_Standalone ){
        m_Standalone = sa;
        if( m_Standalone ){
            ResetKeyCounters();
            m_Timer.start( kKeyDownTimer );
        }else
            m_Timer.stop();
    }
}

bool QPiano::cursorVisible() const
{
    return m_bEnableCursor && focusPolicy() != Qt::NoFocus && hasFocus();
}

void QPiano::setCursor( BYTE pos )
{
    if( m_Cursor != pos){
        m_Cursor = pos;
        emit cursorChanged( pos );
        if( cursorVisible() )
            update();
    }
}

void QPiano::onTimer()
{
    if( !m_Standalone || !isVisible() )
        return;

    VTASSERT( m_Timer.interval()==kKeyDownTimer );

    // !!!!!!!! Add a faster function? (to avoid rescanning all keys)

    for( BYTE i = GetStartingNote(), e = GetEndingNote(); i<=e; i++ )
        if( IsKeyPressed(i) )
            if( --(m_KeyDownCounter[i]) <= 0 ){
                Release( i );
                //update(); - called by Release()
            }
}

void QPiano::ResetKeyCounters()
{
    memset( m_KeyDownCounter, 0, sizeof(m_KeyDownCounter) );
}

