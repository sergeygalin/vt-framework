//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// VT::MIDI::CMIDIMap editor dialog
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

#include "QMIDIMapWidget.h"


enum {
    kIn,
    kInNote,
    kComment1,
    kOut,
    kOutNote,
    kComment2,
    kNumColumns
};


QMIDIMapWidget::QMIDIMapWidget( QWidget* parent ):
    QTreeWidget( parent ),
    m_EditingItem( NULL ),
    m_LastEditingItem( NULL ),
    m_EditingColumn( -1 ),
    m_StartingOctave( -1 ),
    m_Modified( false )
{
    buildTable();

    connect( this, SIGNAL(itemPressed(QTreeWidgetItem*,int)), this, SLOT(openEditor(QTreeWidgetItem*,int)) );
    connect( this, SIGNAL(itemSelectionChanged()), this, SLOT(closeEditor()) );

}

QMIDIMapWidget::~QMIDIMapWidget()
{
}

const VT::MIDI::CMIDIMap& QMIDIMapWidget::getMap()
{
    syncUIAndMap();
    return m_Map;
}

void QMIDIMapWidget::setMap( const VT::MIDI::CMIDIMap& map )
{
    closeEditor();
    m_Map.CopyFrom( map );
    buildTable();
    setModified( false );
    emit mapAssigned();
}

const VT::CString& QMIDIMapWidget::getMapDescription() const
{
    return m_Map.GetDescription();
}

void QMIDIMapWidget::setMapDescription( LPCVTCHAR d )
{
    if( !m_Map.GetDescription().IsEqual( d ) ){
        m_Map.SetDescription( d );
        setModified();
    }
}

void QMIDIMapWidget::setModified( bool m )
{
    if( m_Modified != m ){
        m_Modified = m;
        emit modFlagChanged();
    }
}

void QMIDIMapWidget::keyPressEvent( QKeyEvent* ev )
{
    if( ev==NULL )
        return;
    switch( ev->key() ){
        case Qt::Key_Right:
        case Qt::Key_Left:
        case Qt::Key_Up:
        case Qt::Key_Down:
        //case Qt::Key_Tab:
        case Qt::Key_Enter:
        case Qt::Key_Return:
            return;
    }
    QTreeWidget::keyPressEvent( ev );
}

void QMIDIMapWidget::keyReleaseEvent( QKeyEvent* ev )
{
    if( ev==NULL )
        return;

    switch( ev->key() ){
        case Qt::Key_Right:        moveRight();    return;
        case Qt::Key_Left:        moveLeft();        return;
        case Qt::Key_Up:        moveUp();        return;
        case Qt::Key_Down:        moveDown();        return;

        case Qt::Key_Enter:
        case Qt::Key_Return:
            moveDown();
            return;

        /*case Qt::Key_Tab:

            if( (ev->modifiers() & Qt::ShiftModifier) == 0 ){
                if( m_EditingColumn < kNumColumns-1 )
                    moveRight();
                else
                    moveDown( true );
            }else{
                if( m_EditingColumn > kComment1 )
                    moveLeft();
                else
                    moveUp( true );
            }
            return;*/

    }
    QTreeWidget::keyReleaseEvent( ev );
}

void QMIDIMapWidget::hideEvent( QHideEvent* )
{
    syncUIAndMap();
}

void QMIDIMapWidget::focusOutEvent( QFocusEvent* ev )
{
    closeEditor();
    QTreeWidget::focusOutEvent( ev );
}

/*
void QMIDIMapWidget::leaveEvent( QEvent *ev )
{
    closeEditor();
    QTreeWidget::leaveEvent( ev );
}
*/

void QMIDIMapWidget::moveRight()
{
    if( m_EditingColumn < kNumColumns-1 ){
        closeEditor();
        openEditor( currentItem(), m_EditingColumn + 1 );
    }
}

void QMIDIMapWidget::moveLeft()
{
    if( m_EditingColumn > kComment1 ){
        closeEditor();
        openEditor( currentItem(), m_EditingColumn - 1 );
    }
}

void QMIDIMapWidget::moveUp( bool toLastColumn )
{
    QTreeWidgetItem* item = currentItem();
    if( item != NULL ){
        int note = item->data( 0, Qt::UserRole ).toInt();
        if( note > 0 ){
            closeEditor();
            openEditor( m_Items[note-1], (toLastColumn)? kNumColumns-1: m_EditingColumn );
        }
    }
}

void QMIDIMapWidget::moveDown( bool toFirstColumn )
{
    QTreeWidgetItem* item = currentItem();
    if( item != NULL ){
        int note = item->data( 0, Qt::UserRole ).toInt();
        if( note < 127 ){
            closeEditor();
            openEditor( m_Items[note+1], (toFirstColumn)? kComment1: m_EditingColumn );
        }
    }
}

QTreeWidgetItem* QMIDIMapWidget::getItemToEdit()
{
    if( m_LastEditingItem != NULL )
        return m_LastEditingItem;
    return currentItem();
}

void QMIDIMapWidget::syncUIAndMap()
{
    closeEditor(); // This will also sync current editing row
}

void QMIDIMapWidget::syncRow( QTreeWidgetItem* item )
{
    if( item == NULL )
        return;

    // Download data from UI table to map;
    // can use m_EditingColumn to select preferred note data (number or name).
    int inNote = item->data( 0, Qt::UserRole ).toInt();
    if( inNote < 0 || inNote > 127 ){
        VTDEBUGMSGT( "Failed check: inNote < 0 || inNote > 127" );
        return;
    }

    VT::CString c1( item->text( kComment1 ) );
    VT::CString c2( item->text( kComment2 ) );

    if( !c1.IsEqual( m_Map.GetComment1(inNote) ) ){
        m_Map.SetComment1( inNote, c1 );
        setModified();
    }

    if( !c2.IsEqual( m_Map.GetComment2(inNote) ) ){
        m_Map.SetComment2( inNote, c2 );
        setModified();
    }

    bool preferNoteName = (m_EditingColumn == kOutNote);
    int note, oldNote = m_Map.Map( inNote );
    if( preferNoteName ){
        if( item->text( kOutNote ).isEmpty() )
            note = VT::MIDI::kErrorNote;
        else{
            bool octaveKnown;
            note = VT::Notes::ParseNoteName( VT::CString( item->text( kOutNote ) ).c_str(),
                oldNote, // In case of error, fall back to old value
                getStartingOctave(), &octaveKnown );
            if( !octaveKnown )
                note = VT::Notes::NoteNumber( VT::Notes::GetInOctaveNumber( note ),
                    VT::Notes::GetOctaveNumber( oldNote, getStartingOctave() ), getStartingOctave() );
        }
    }else{
        if( item->text( kOut ).isEmpty() )
            note = VT::MIDI::kErrorNote;
        else{
            bool ok;
            note = item->text( kOut ).toInt( &ok );
            if( note < 0 || note > 127 || !ok )
                note = oldNote;
        }
    }
    if( m_Map.Map(inNote) != note ){
        m_Map.SetMapping( inNote, note );
        setModified();
    }

    // Update note name and number in UI
    setOutNote( item );
}

void QMIDIMapWidget::openEditor( QTreeWidgetItem* item, int column )
{
    closeEditor();

    if( item==NULL )
        return;

    if( column == kComment1 ||
        column == kComment2 ||
        column == kOut ||
        column == kOutNote )
    {
        openPersistentEditor( item, column );
        itemWidget( item, column )->setFocus();
        m_EditingItem = item;
        m_LastEditingItem = item;
        m_EditingColumn = column;
    }
}

void QMIDIMapWidget::closeEditor( QTreeWidgetItem* item, int column )
{
    closePersistentEditor( item, column );
    if( m_EditingItem == item )
        m_EditingItem = NULL;
}

void QMIDIMapWidget::closeEditor()
{
    if( m_EditingItem != NULL ){
        QTreeWidgetItem* closeit = m_EditingItem;
        for( int i = 0; i < kNumColumns; i++ )
            closeEditor( closeit, i ); // Flushes m_EditingItem
        syncRow( closeit );
    }
}


void QMIDIMapWidget::buildTable()
{
    // Old pointers, if set, will become invalid because the items will be trashed by clear().
    m_EditingItem = NULL;
    m_LastEditingItem = NULL;

    clear();
    setColumnCount( 6 );

    {
        QTreeWidgetItem *head = new QTreeWidgetItem();
        head->setText( 0, tr("In#") );
        head->setText( 1, tr("Note") );
        head->setText( 2, tr("Comment") );
        head->setText( 3, tr("Map#") );
        head->setText( 4, tr("Note") );
        head->setText( 5, tr("Comment") );

        head->setTextAlignment( 0, Qt::AlignHCenter );
        head->setTextAlignment( 1, Qt::AlignHCenter );
        head->setTextAlignment( 2, Qt::AlignHCenter );
        head->setTextAlignment( 3, Qt::AlignHCenter );
        head->setTextAlignment( 4, Qt::AlignHCenter );
        head->setTextAlignment( 5, Qt::AlignHCenter );

        setColumnWidth( 0, 40 );
        setColumnWidth( 1, 40 );
        setColumnWidth( 2, 120 );
        setColumnWidth( 3, 40 );
        setColumnWidth( 4, 40 );
        setColumnWidth( 5, 120 );

        setHeaderItem( head );
    }

    QColor whiteColor = palette().color( QPalette::Normal, QPalette::Base ).darker( 110 );
    QColor blackColor = whiteColor.darker( 120 );

    for( BYTE i = 0; i < 128; i++ ){
        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setTextAlignment( 0, Qt::AlignRight );
        item->setTextAlignment( 1, Qt::AlignLeft );
        item->setTextAlignment( 2, Qt::AlignLeft );
        item->setTextAlignment( 3, Qt::AlignRight );
        item->setTextAlignment( 4, Qt::AlignLeft );
        item->setTextAlignment( 5, Qt::AlignLeft );

        item->setText( 0, QString::number(int(i)) );
        item->setText( 1, VT::Notes::GetFullNoteNameQ( i, getStartingOctave() ) );
        item->setText( 2, m_Map.GetComment1(i).QStr() );


        item->setText( 5, m_Map.GetComment2(i).QStr() );
        item->setData( 0, Qt::UserRole, QVariant(int(i)) );
        setOutNote( item ); // After setting the data!!


        if( VT::Notes::IsBlackKey( i ) )
            for( int i = 0; i<item->columnCount(); i++ )
                item->setBackgroundColor( i, blackColor );
        else
            for( int i = 0; i<item->columnCount(); i++ )
                item->setBackgroundColor( i, whiteColor );

        m_Items[i] = item;

        addTopLevelItem( item );
        //openPersistentEditor( item, 2 ); // Comment 1
        //i->mapTree->openPersistentEditor( item, 3 ); //
        //i->mapTree->openPersistentEditor( item, 4 ); //
        //openPersistentEditor( item, 5 ); // Comment 2
    }
}

void QMIDIMapWidget::setOutNote( QTreeWidgetItem* item )
{
    int i = item->data( 0, Qt::UserRole ).toInt();
    if( m_Map.Map(i) != VT::MIDI::kErrorNote ){
        item->setText( 3, QString::number(int(m_Map.Map(i))) );
        item->setText( 4, VT::Notes::GetFullNoteNameQ( m_Map.Map(i), getStartingOctave() ) );
    }else{
        item->setText( 3, "" );
        item->setText( 4, "" );
    }

}



bool QMIDIMapWidget::openDialog()
{
    QString fn = QFileDialog::getOpenFileName( this, tr("Load MIDI Map"), GetMapDirectory().QStr(),
        tr("MIDI maps")+" (*."+VT::CString(VT::MIDI::CMIDIMap::kFileExtension).QStr()+")" );
    if( !fn.isEmpty() ){
        VT::MIDI::CMIDIMap map;
        if( !map.Load( VT::CString( fn ) ) ){
            QMessageBox::critical( this, tr("Error"), tr("The selected MIDI map file could not be loaded.") );
            return false;
        }else{
            m_Map.CopyFrom( map );
            buildTable();
            setModified( false );
            emit mapAssigned();
            return true;
        }
    }
    return false;
}

bool QMIDIMapWidget::saveDialog()
{
    QString fn = QFileDialog::getSaveFileName( this, tr("Save MIDI Map"), GetMapDirectory().QStr(),
        tr("MIDI maps")+" (*."+VT::CString(VT::MIDI::CMIDIMap::kFileExtension).QStr()+")" );
    if( !fn.isEmpty() ){
        syncUIAndMap();
        if( !m_Map.Save( VT::CString( fn ) ) ){
            QMessageBox::critical( this, tr("Error"), tr("An error occured while saving the map to the file.") );
            return false;
        }
        setModified( false );
        return true;
    }
    return false;
}


void QMIDIMapWidget::clearAll()
{
    syncUIAndMap();
    m_Map.Clear();
    buildTable();
    setModified( true );
}

void QMIDIMapWidget::clearAllConfirm()
{
    if( QMessageBox::question( this, tr("Clear All"),
        tr("Are you sure to clear the map? This will delete all mapping and all comments."),
        QMessageBox::Yes|QMessageBox::No )==QMessageBox::Yes )
        clearAll();
}

void QMIDIMapWidget::clearMap()
{
    syncUIAndMap();
    m_Map.Reset();
    m_Map.ClearComments( false, true );
    buildTable();
    setModified( true );
}

void QMIDIMapWidget::clearMapConfirm()
{
    if( QMessageBox::question( this, tr("Clear Mapping"),
        tr("Are you sure to clear the map? This will delete all mapping and comments to mapped values."),
        QMessageBox::Yes|QMessageBox::No )==QMessageBox::Yes )
        clearMap();
}

void QMIDIMapWidget::setInToGMDrumsConfirm()
{
    if( QMessageBox::question( this, tr("Please Confirm"),
        tr("Do you want to fill input note comments with corresponding drum names?"),
        QMessageBox::Yes|QMessageBox::No )==QMessageBox::Yes )
    {
        syncUIAndMap();
        m_Map.SetCommentsToGMStdDrumKit( true, false );
        buildTable();
        setModified( true );
    }

}

void QMIDIMapWidget::setOutToGMDrumsConfirm()
{
    if( QMessageBox::question( this, tr("Please Confirm"),
        tr("Do you want to fill output note comments with corresponding drum names?"),
        QMessageBox::Yes|QMessageBox::No )==QMessageBox::Yes )
    {
        syncUIAndMap();
        m_Map.SetCommentsToGMStdDrumKit( false, true );
        buildTable();
        setModified( true );
    }

}


