//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QJOGDIAL_H
#define QJOGDIAL_H

#include "QPixmapDial.h"

class QJogDial: public QPixmapDial
{
    Q_OBJECT

public:
    QJogDial( QWidget* parent = NULL );
    virtual ~QJogDial();

    // NOTE: The default value is 5 degrees.
    Q_PROPERTY( qreal clickAngle READ getClickAngle WRITE setClickAngle );
    qreal getClickAngle() const { return m_ClickAngleDeg; }
    void setClickAngle( qreal a ){ m_ClickAngleDeg = a; }

    // this is a test function, will be removed later
    qreal getTestValue() const { return m_Test; }

signals:
    //
    // Jogging signals
    //
    void jogLeft(); // Emitted once per each click
    void jogRight(); // Emitted once per each click
    void jog( bool forward ); // Emitted once per each click

    // This is the most efficient one: emitted once per mouse movement.
    // When the wheel is rotated CCW, the count is negative.
    void jogClicks( int count );

    // this is a test signal, will be removed later
    void jogTest();

protected:
    virtual void mousePressEvent( QMouseEvent *me );
    virtual void mouseReleaseEvent( QMouseEvent *me );
    virtual void mouseMoveEvent( QMouseEvent *me );
    virtual void showEvent( QShowEvent *e );
    virtual void wheelEvent( QWheelEvent *we );

    void RotateWheel( qreal rads );

private:

    bool m_Rotating;
    qreal
        m_ClickAngleDeg, // How much the wheel must be turned before "clicking"
        m_DownAngleRad,
        m_Test;
    qreal GetAngleRad( qreal x, qreal y ) const;
    int m_MouseWheelDelta;

    //
    // Hiding and blocking virtual functions
    //
    virtual void setVirtualSlider( bool virt ); // QPixmapDial
    virtual void setZeroAngle( qreal angle ); // QPixmapDial
    virtual void setMaxAngle( qreal angle ); // QPixmapDial
    virtual void setMinimum( int value ); // QDial
    virtual void setMaximum( int value ); // QDial
    virtual void setValue( int value ); // QDial

};


#endif


