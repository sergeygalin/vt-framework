//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QCURVEEDITOR_H
#define QCURVEEDITOR_H

#include <QtCore/QPointF>
#include <QtCore/QList>
#include <QtCore/QPointer>
#include <QtGui/QCursorShape>
#include "QPixmapReferencer.h"
#include "QRectangularMapEditorContainer.h"

//
// Typical curves - these constants can be given to constructors to ease
// creation of frequently used kinds.
//
enum TStdCurveTypes
{
    kUnknownCurveType,    // No special initializations
    kAmplitudeEG,        // Amplitude envelope, point X range: 0..10000 (msec), point Y range: 0..100 (%)
                        // Area size: ( 0..60000, 0..100 ).
    kBipolarEG            // Bipolar envelope, point X range: 0..10000 (msec), point Y range: -100..100 (%)
                        // Area size: ( 0..60000, -100..100 ).
};

//
// Signle curve node.
//
class QPolylineNode: public QObject
{
    Q_OBJECT
public:
    enum TNodeStyle
    {
        kFrameOnly,
        kSolid,
        kDefaultStyle = kFrameOnly
    };


    QPolylineNode();
    QPolylineNode( const QPolylineNode& src );
    QPolylineNode( qreal x, qreal y, bool ex = true, bool ey = true,
        bool specColor = false, QColor clr = QColor(255,0,0), TNodeStyle style = QPolylineNode::kDefaultStyle  );

    QPolylineNode( qreal x, qreal minx, qreal maxx, qreal y, qreal miny, qreal maxy,
        bool ex = true, bool ey = true,
        bool specColor = false, QColor clr = QColor(255,0,0), TNodeStyle style = QPolylineNode::kDefaultStyle  );

    QPolylineNode( TStdCurveTypes stdType, qreal x, qreal y, bool ex = true, bool ey = true,
        bool specColor = false, QColor clr = QColor(255,0,0), TNodeStyle style = QPolylineNode::kDefaultStyle  );

    void copyFrom( const QPolylineNode& src );
    QPolylineNode& operator=( const QPolylineNode& src ){ copyFrom(src); return *this; }

    bool isEditableX() const { return m_bEditableX; }
    void setEditableX( bool ex ){ m_bEditableX = ex; }
    bool isEditableY() const { return m_bEditableY; }
    void setEditableY( bool ey ){ m_bEditableY = ey; }
    bool isEditable() const { return isEditableX() || isEditableY(); }

    bool useSpecialColor() const { return m_bSpecialColor; }
    void setUseSpecialColor( bool use ){ m_bSpecialColor = use; emit appearanceChanged(); }

    QColor getSpecialColor() const { return m_Color; }
    void setSpecialColor( QColor clr ){ m_Color = clr; emit appearanceChanged(); }

    // Get point editing cursor shape:
    // Up/down arrow if point is editable vertically
    // Left/right arrow if point is editable horizontally
    // Arrow cross if point can be moved vertically & horizontally,
    // "blocked" cursor if can't be moved.
    Qt::CursorShape getCursorShape() const;

    double x() const { return m_X; }
    double y() const { return m_Y; }
    void setXRange( double minX, double maxX );
    void setYRange( double minY, double maxY );

    double getXMin() const { return m_fXMin; }
    double getXMax() const { return m_fXMax; }
    double getYMin() const { return m_fYMin; }
    double getYMax() const { return m_fYMax; }

    // Returns true if Y ranges from a negative value to positive value.
    bool isBipolar() const;

    // Assign X and Y. The coordinates are clipped to their min-max ranges.
    void setXY( double x, double y );
    void setX( double x );
    void setY( double y );

    // Move the point to (x, y) ("by user").
    // Returns if the point position has been actually changed.
    bool move( double x, double y );

    TNodeStyle getStyle() const { return m_Style; }
    void setStyle( TNodeStyle st ){ m_Style = st; emit appearanceChanged(); }

signals:
    void appearanceChanged(); // Changed appearance (color and etc.)
    void moved(); // Moved by user

private:
    bool m_bSpecialColor;
    TNodeStyle m_Style;
    QColor m_Color;
    double
        m_X, m_Y, // Current point coordinates
        m_fXMin, m_fXMax, m_fYMin, m_fYMax; // Point change ranges
    bool
        m_bEditableX, // Can the point move horizontally
        m_bEditableY; // Can the point move vertically
    void setDefaultMinMax(); // Resets m_f?Min/Max vars
};








//
// QEditablePolyline contains QPolylineNode list and implements Qt-based
// rendering functions and allows mouse-like and keyboard edits via virtual
// functions. It also uses Qt event system to notify about its changes.
//
class QEditablePolyline: public QObject
{
    Q_OBJECT
public:
    enum TNudge
    {
        kNudgeLeft,
        kNudgeRight,
        kNudgeUp,
        kNudgeDown,
    };

    // Construct a default polyline or some specified type.
    // stdAreaRectWidth is used to set width of AreaRect; if it's set to negative,
    // the function determines the width from the type created.
    // Same about stdVRectWidth.
    QEditablePolyline( TStdCurveTypes type = kUnknownCurveType, qreal stdAreaRectWidth = -1.0, qreal stdVRectWidth = -1.0 );
    void setDefaultColors();
    virtual ~QEditablePolyline();

    QEditablePolyline( const QEditablePolyline& src );
    QEditablePolyline& operator=( const QEditablePolyline& src ){ copyFrom(src); return *this; }
    void copyFrom( const QEditablePolyline& src );

    virtual QEditablePolyline* createObject() const { return new QEditablePolyline(); }
    QEditablePolyline* createopy() const;

    // Draw a curve on some surface.
    // guiRect - rectangle on the surface (pixels).
    // vRect - virtual rectangle on the surface with coordinates mapped
    //    to the space of the points.
    // painter - a QPainter to use
    virtual void draw( QPainter& painter ) const;

    qreal getNodeSize() const { return 6.0; }

    // Check that mouse at (x, y) hits the curve.
    // The check may be done twice for some cuves, first for nodes, then for lines.
    // If secondCheck is set, then "secondary" check should be performed.
    // If the curve ignores clicks on lines, then it should return false when
    // secondCheck is set.
    virtual bool mouseCheck( int x, int y, bool click, bool secondCheck = false );

    // Get cursor for current state. mouseCheck() should be called
    // prior to this function if mouse has been moved.
    virtual Qt::CursorShape getCursorShape() const;

    virtual bool startDrag(); // Start dragging last checked or selected notes
    virtual bool doDrag( int x, int y ); // Continue dragging (to the mouse coordinates)
    virtual bool endDrag(); // End dragging points
    bool isDragging() const { return m_bDragging; }

    virtual bool selectNode( int nodeIdx ); // Select a node by index
    virtual bool selectNode(); // Select the node which was the last under mouse
    virtual bool deselectNode(); // Deselect a node
    virtual bool isSelected() const; // Check if a node is selected
    virtual bool nudge( TNudge nudge ); // Nudge selected note (via keyboard)

    Q_PROPERTY( QString name READ getName WRITE setName );
    QString getName() const { return m_sName; }
    void setName( const QString &n ){ m_sName = n; }

    Q_PROPERTY( int tag READ getTag WRITE setTag );
    int getTag() const { return m_iTag; }
    void setTag( int tag ){ m_iTag = tag; }

    virtual bool isNodeSelected() const { return m_iNode>=0 && m_iNode<m_Nodes.size(); }
    virtual int getNodeIdx() const { return ( isNodeSelected() )? m_iNode: -1; }

    const QList<QPolylineNode*>& nodes(){ return m_Nodes; }
    bool hasNodes() const { return m_Nodes.length()>0; }
    bool hasSegments() const { return m_Nodes.length()>1; }
    void addNode( const QPolylineNode& src );
    void addNode( QPolylineNode* src ); // Takes ownership over the object
    void addNode( const QPolylineNode& src, int atIndex, bool preserveCurveLength = true );
    void deleteNode( int atIndex );
    void deleteAll();

    Q_PROPERTY( QColor lineColor READ getLineColor WRITE setLineColor );
    QColor getLineColor() const { return m_LineColor; }
    void setLineColor( QColor color ){ m_LineColor = color; emit updated(); }

    Q_PROPERTY( QColor nodeColor READ getNodeColor WRITE setNodeColor );
    QColor getNodeColor() const { return m_NodeColor; }
    void setNodeColor( QColor color ){ m_NodeColor = color; emit updated(); }

    Q_PROPERTY( QColor nodeSelectedColor READ getNodeSelectedColor WRITE setNodeSelectedColor );
    QColor getNodeSelectedColor() const { return m_NodeSelectedColor; }
    void setNodeSelectedColor( QColor color ){ m_NodeSelectedColor = color; emit updated(); }

    Q_PROPERTY( QColor lineSelectedColor READ getLineSelectedColor WRITE setLineSelectedColor );
    QColor getLineSelectedColor() const { return m_LineSelectedColor; }
    void setLineSelectedColor( QColor color ){ m_LineSelectedColor = color; emit updated(); }

    // Sets all four colors calculating them from the base color
    void setColor( QColor baseColor );

    Q_PROPERTY( bool forwardOnly READ getForwardOnly WRITE setForwardOnly );
    bool getForwardOnly() const { return m_ForwardOnly; }
    void setForwardOnly( bool fw = true ){ m_ForwardOnly = fw; endDrag(); emit updated(); }

    Q_PROPERTY( bool additive READ getAdditive WRITE setAdditive );
    bool getAdditive() const { return m_Additive; }
    void setAdditive( bool a = true ){ m_Additive = a; endDrag(); emit updated(); }

    Q_PROPERTY( bool visible READ isVisible WRITE setVisible );
    bool isVisible() const { return m_bVisible; }
    void setVisible( bool vis ){ if( m_bVisible!=vis ){ m_bVisible = vis; endDrag(); emit updated(); emit goneFromEditing(this); } }

    // See also: QCurveEditorField::setEnabledCurves().
    Q_PROPERTY( bool editEnabled READ getEditEnabled WRITE setEditEnabled );
    bool getEditEnabled() const { return m_bEditEnabled; }
    void setEditEnabled( bool en ){ if( en!=m_bEditEnabled ){ m_bEditEnabled = en; endDrag(); emit updated(); emit goneFromEditing(this); }; }

    virtual qreal getNodeVX( int node ) const;
    virtual qreal getNodeVY( int node ) const;
    virtual qreal getNodeX( int node ) const;
    virtual qreal getNodeY( int node ) const;

    qreal getLeftmostNodeVX() const;
    qreal getRightmostNodeVX() const;

    // Get % of area at which the leftmost/rightmost point is located
    // (typically used to zoom to all objects).
    qreal getLeftmostNodeVXPc() const;
    qreal getRightmostNodeVXPc() const;

    // Returns true if the curve contains any bipolar points.
    bool isBipolar() const;

    QRectangularMap& map(){ return m_Map; }
    const QRectangularMap& mapConst() const { return m_Map; }

    void reverse(double center);
    void reverse_LineMiddle();
    void flip(double center);
    void update()        {    emit updated();    }

signals:
    void goneFromEditing( QEditablePolyline *curve );
    void updated();
    void selected( QEditablePolyline *curve, int nodeIdx );
    void unselected( QEditablePolyline *curve, int nodeIdx );
    void changed( QEditablePolyline *curve );

protected slots:
    void mapChanged();

protected:
    virtual void drawNode( QPainter& painter, int nodeIdx ) const;

    // Returns true if node has been moved
    virtual bool validateNodeMoveAndDoIt( int node, qreal& newx, qreal& newy, bool emitEvents = true );


private:
    QRectangularMap m_Map;
    bool m_bVisible, m_bEditEnabled;
    int m_iTag;
    QColor m_LineColor, m_LineSelectedColor, m_NodeColor, m_NodeSelectedColor;
    bool m_ForwardOnly, m_Additive; // Curve moves forward only
    QList<QPolylineNode*> m_Nodes; // Array of nodes
    int m_iDownX, m_iDownY, m_iNode, m_iHoverNode; // Temp vars used when dragging nodes
    // QRect m_Rect; QRectF m_fRect; - now used QRectangularMap fields
    QString m_sName; // Name of the curve
    bool m_bDragging;

    void connectInternalSignals();
};







//
// Multi curve editor widget
//
class QCurveEditorField: public QRectangularMapEditor, public QPixmapReferencer
{
    Q_OBJECT
public:
    QCurveEditorField( QWidget* parent = 0 );
    virtual ~QCurveEditorField();

    virtual void setGUIRect( const QRect& src );
    virtual void setVRect( const QRectF& src );
    virtual void setFlip( bool fl );

    // Add a curve.
    // NOTE: QCurveEditorField will now take over ownership over the curve,
    // and will delete it when it's destroyed.
    virtual void addCurve( QEditablePolyline* curve );

    // Adds a copy of curve
    virtual void addCurve( const QEditablePolyline& curve );

    virtual void deleteCurve( int idx );
    virtual void deleteAll();
    void clear(){ deleteAll(); }

    // Fixed background always fills whole widget.
    // If "BG fixed" set to false, background fills area which corresponds
    // to m_BGVirtualPos rectangle. For example, a grid can be drawn
    // covering virtual coordinate system at (-3.0, -3.0, 10.0, 5.0).
    Q_PROPERTY( bool fixedBackground READ getFixedBackground WRITE setFixedBackground );
    void setFixedBackground( bool fixed ){ m_BGFixed = fixed; update(); }
    bool getFixedBackground() const { return m_BGFixed; }

    // Get/set virtual coordinate system area covered by background pixmap.
    void setBGVirtualPosition( const QRectF& src ){ m_BGVirtualPos = src; update(); }
    const QRectF& getBGVirtualPosition() const { return m_BGVirtualPos; }

    bool isDragging() const;
    bool endDrag( bool restoreCursor = true );

    QList< QPointer<QEditablePolyline> > curves(){ return m_Curves; }
    const QList< QPointer<QEditablePolyline> > curvesConst() const { return m_Curves; }

    // Sets editEnabled to 'enableedit' on all curves, except
    // 'except' curve which is set to the other value.
    // For example:
    // setEnabledCurves( true, 5 ); // Enable editing of all curves except #5
    // setEnabledCurves( false, 4 ); // Disable editing of all curves except #4
    void setEnabledCurves( bool enableedit, int except=-1 );

    int getSelectedCurveIdx() const { return m_iCurve; }
    virtual bool selectCurve( int index, int selectNode=0, bool reenableCurveEdit = true );
    virtual bool hasSelection() const { return m_iCurve>=0; }
    virtual void deselectAll();

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!! NOT DEBUGGED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // "Same maps" feature.
    // If sameMaps is enabled, all curves have totally the same map as
    // the control itself.
    // If it's disabled, only GUIRect is copied for all maps;
    // VRect/AreaRect are applied using scrolling/zooming functions.
    Q_PROPERTY( bool sameMaps READ getSameMaps WRITE setSameMaps );
    virtual bool getSameMaps() const { return m_bSameMaps; }
    virtual void setSameMaps( bool same );

    // Get % of area at which the leftmost/rightmost point is located
    // (typically used to zoom to all objects).
    qreal getLeftmostNodeVXPc() const;
    qreal getRightmostNodeVXPc() const;

    virtual bool zoomXToObjects();
    virtual bool zoomYToObjects(){ return false; }

    virtual bool zoomXReset(){ return zoomXToObjects(); }
    //virtual bool zoomYReset(){ return zoomYToAll(); }


signals:
    void curveChange( QEditablePolyline *curve ); // Nodes of the curve have been changed
    void selected();            // Something is selected
    void unselected();            // Something is un-selected
    void startedDragging();        // User started to drag some nodes
    void finishedDragging();    // User finished dragging some nodes
    void nudged();                // Something is "nudged" via keyboard arrow keys

protected slots:
    void curveUpdated(); // Some curve needs repaint
    void curveNodesChanged( QEditablePolyline *curve ); // Some curve node changed
    void fieldMapChanged();
    void goneFromEditing( QEditablePolyline *curve );


protected:
    // Updates mouse cursor if necessary; returns true if
    // mouse is over some curve.
    virtual bool updateOnMouseMove( QMouseEvent *me );


private:
    bool m_bSameMaps;
    QList< QPointer<QEditablePolyline> > m_Curves;
    bool m_BGFixed;
    int m_iCurve;
    QRectF m_BGVirtualPos;

protected:
    virtual void paintEvent( QPaintEvent *event );
    virtual void resizeEvent( QResizeEvent *event );
    virtual void mousePressEvent( QMouseEvent *me );
    virtual void mouseReleaseEvent( QMouseEvent *me );
    virtual void mouseMoveEvent( QMouseEvent *me );
    virtual void keyPressEvent( QKeyEvent *event );
};


class QCurveEditor: public QRectangularMapEditorContainer
{
    Q_OBJECT
public:
    QCurveEditor( QWidget *parent = 0 ):
      QRectangularMapEditorContainer( new QCurveEditorField, parent ){}

    QCurveEditorField& editor(){ return *((QCurveEditorField*)mapEditor()); }
    QCurveEditorField* editorPtr(){ return (QCurveEditorField*)mapEditor(); }
    const QCurveEditorField& editorConst() const { return *((QCurveEditorField*)mapEditorConst()); }


};


#endif
