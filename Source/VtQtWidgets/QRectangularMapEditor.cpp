//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "QRectangularMapEditor.h"
#include "vt_exceptions.hpp"


QRectangularMapEditor::QRectangularMapEditor( QWidget* parent ):
    QWidget( parent )
{
}

QRectangularMapEditor::~QRectangularMapEditor()
{
    deleteAllMarkers();
}

void QRectangularMapEditor::deleteAllMarkers()
{
    for( int i=0; i<m_Markers.length(); i++ )
        delete m_Markers.at(i);
    m_Markers.clear();
    update();
}

void QRectangularMapEditor::addMarker( QOnmapElement* element )
{
    if( element==NULL )
        throw VT::CException(VTT("Cannot add NULL marker object."));
    m_Markers.append( element );
    update();
}


void QRectangularMapEditor::paintEvent( QPaintEvent * )
{
    QPainter painter(this);
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );
    drawMarkers( painter );
}

void QRectangularMapEditor::drawMarkers( QPainter& painter )
{
    for( int i=0; i<m_Markers.length(); i++ )
        m_Markers[i]->draw( mapConst(), painter );
}

