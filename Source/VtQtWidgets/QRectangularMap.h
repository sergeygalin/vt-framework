//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A rectangular map is a class which "projects" a "view rectangle" onto some
// "real world" rectangle.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QRECTANGULARMAP_H
#define QRECTANGULARMAP_H

#include <QtCore/QRect>
#include <QtCore/QRectF>
#include <QtCore/QObject>
#include <QtGui/QPainter>

class QRectangularMap: public QObject
{
    Q_OBJECT
public:
    QRectangularMap();
    QRectangularMap( const QRectangularMap &src ): QObject(){ copyFrom(src); }
    QRectangularMap& operator=( const QRectangularMap &src ){ copyFrom(src); return *this; }
    void copyFrom( const QRectangularMap &src );

    void copyGuiGeometryFrom( const QRectangularMap &src );

    virtual void setGUIRect( const QRect& src );
    virtual void setVRect( const QRectF& src );
    virtual void setAreaRect( const QRectF& src );

    const QRect& getGUIRect() const { return m_GuiRect; }
    const QRectF& getVRect() const { return m_VRect; }
    const QRectF& getAreaRect() const { return m_AreaRect; }

    qreal screenXToVX( int x ) const;
    qreal screenYToVY( int y ) const;
    qreal VXtoScreenX( qreal x ) const;
    qreal VYtoScreenY( qreal y ) const;

    // Vertical flipping. Curves and etc. typically have Y axis going up,
    // while screen has Y axis down. By default flipping is enabled.
    virtual bool flip() const { return m_Flip; }
    virtual void setFlip( bool fl );

    //
    // Scrollbar utilites
    //

    // NOTE: zoom is a percent of Area visible throwgh the virtual window.
    // I.e. it is decreased when zooming in and increasing when zooming out.

    // Positions of scroll bars, in percent
    qreal scrollXPos() const;
    qreal scrollYPos() const;
    // Visible % of virtual area
    qreal zoomXArea() const;
    qreal zoomYArea() const;

    int getWorkingAreaWidth() const { return m_GuiRect.width()-m_iMargin-m_iMargin; }
    int getWorkingAreaHeight() const { return m_GuiRect.height()-m_iMargin-m_iMargin; }

    bool canScrollX() const;
    bool canScrollY() const;
    void scrollXToPos( qreal pcPos );
    void scrollYToPos( qreal pcPos );
    void zoomXToPc( qreal pcPos );
    void zoomYToPc( qreal pcPos );

    void zoomToArea(); // Zoom to 100%
    bool zoomXToRange( qreal x1, qreal x2 );
    bool zoomYToRange( qreal y1, qreal y2 );
    bool zoomXToRangePc( qreal x1pc, qreal x2pc );
    bool zoomYToRangePc( qreal y1pc, qreal y2pc );
    qreal VXtoAreaPc( qreal x ) const;
    qreal VYtoAreaPc( qreal y ) const;

    // Click-zooming functions
    void zoomInX();
    void zoomInY();
    void zoomOutX();
    void zoomOutY();

    // Setup clicking behaviour.
    // 'zoomChange' - how much zoom is changed by zoomIn/zoomOut functions, in percent.
    // If 'multiplication' is true 'zoomChange' multiplies current zoom value;
    // if it's false, then 'zoomChange' is added to the current value.
    void setClickZoom( qreal zoomChange, bool multiplication=true ){
        m_ClickZoomPc=zoomChange; m_ClickZoomMultiplies=multiplication; }

    // See comment for setClickZoom.
    Q_PROPERTY( qreal clickZoomStep READ getClickZoomStep WRITE setClickZoomStep );
    qreal getClickZoomStep() const { return m_ClickZoomPc; }
    void setClickZoomStep( qreal st ){ m_ClickZoomPc = st; }

    // See comment for setClickZoom.
    Q_PROPERTY( bool clickZoomMultiplies READ getClickZoomMultiplies WRITE setClickZoomMultiplies );
    bool getClickZoomMultiplies() const { return m_ClickZoomMultiplies; }
    void setClickZoomMultiplies( bool mul ){ m_ClickZoomMultiplies = mul; }

    // Limiting max zoom in for X
    Q_PROPERTY( qreal minXZoom READ getMinXZoom WRITE setMinXZoom );
    qreal getMinXZoom() const { return m_MinXZoom; }
    void setMinXZoom( qreal zPc ){ m_MinXZoom = zPc; } // !!!!!!!! Validate current zooming

    // Limiting max zoom in for Y
    Q_PROPERTY( int minYZoom READ getMinYZoom WRITE setMinYZoom );
    int getMinYZoom() const { return m_MinYZoom; }
    void setMinYZoom( int zPc ){ m_MinYZoom = zPc; } // !!!!!!!! Validate current zooming

    // Unused margin around working area in GUI rectangle, in pixels.
    // By default it's zero. Used e.g. by QCurveEditor to have node handles
    // fully drawn even when the node is at edge of virtual area.
    Q_PROPERTY( int margin READ getMargin WRITE setMargin );
    int getMargin() const { return m_iMargin; }
    void setMargin( int m ){ m_iMargin = m; emit mapChanged(); }

signals:
    void mapChanged();

private:
    int m_iMargin;
    QRect m_GuiRect;    // Screen coordinates where the mapped space is drawn (pixels)
    QRectF m_VRect;        // Virtual coordinates of rectangle visible on screen (mapped to m_GuiRect)
    QRectF m_AreaRect;  // Virtual coordinates of whole work area (used by scrolling functions)
    bool m_Flip; // Flip coordinates vertically
    qreal m_MinXZoom, m_MinYZoom, m_ClickZoomPc;
    bool m_ClickZoomMultiplies;

protected:
    qreal VXCenterShift() const;
    qreal VYCenterShift() const;
    qreal VXMaxAbsShift() const;
    qreal VYMaxAbsShift() const;
};


class QOnmapElement: public QObject
{
    Q_OBJECT
public:
    virtual void draw( const QRectangularMap& map, QPainter& painter )=0;
};


// Simple grid.
class QSimpleOnmapGrid: public QOnmapElement
{
    Q_OBJECT
public:
    QSimpleOnmapGrid();
    QSimpleOnmapGrid( const QSimpleOnmapGrid& src ): QOnmapElement(){ copyFrom(src); }
    QSimpleOnmapGrid( qreal xstep, int xbold, qreal ystep, int ybold,
        QColor color=QColor( 5, 100, 5 ),
        QColor boldColor=QColor( 7, 180, 7 ),
        QColor axisColor=QColor( 220, 220, 15 ),
        bool drawArrows = true );
    void copyFrom( const QSimpleOnmapGrid& src );
    QSimpleOnmapGrid& operator=( const QSimpleOnmapGrid& src ){ copyFrom(src); return *this; }
    virtual ~QSimpleOnmapGrid();
    virtual void draw( const QRectangularMap& map, QPainter& painter );

signals:
    void updated();

private:
    qreal m_XStep, m_YStep;
    int m_XBold, m_YBold;
    QColor m_Color, m_BoldColor, m_AxisColor;
    bool m_DrawArrows;
};







#endif

