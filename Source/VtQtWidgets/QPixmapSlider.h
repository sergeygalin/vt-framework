//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// A slider control which uses pixmaps.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QPIXMAPSLIDER_H
#define QPIXMAPSLIDER_H

#include <QtGui/QAbstractSlider>
#include "QPixmapReferencer.h"

class QPixmapSlider: public QAbstractSlider, public QMultiPixmapReferencer
{
    Q_OBJECT
public:
    QPixmapSlider( QWidget* parent = NULL );
    virtual ~QPixmapSlider();

    void init( const QPixmap* bg, const QPixmap* handle,
        const QPixmap* handleActive=NULL, const QPixmap* handleDisabled=NULL );

    void init( const QString& bg, const QString& handle,
        const QString& handleActive=QString(), const QString& handleDisabled=QString() );

protected:

    virtual void paintEvent( QPaintEvent* ev );
    virtual void mousePressEvent( QMouseEvent *me );
    virtual void mouseReleaseEvent( QMouseEvent *me );
    virtual void mouseMoveEvent( QMouseEvent *me );
    virtual void keyReleaseEvent( QKeyEvent *ev );

    void updateSizeConstraints();
    int handleX() const;
    int valueFromHandleX( int hx ) const;
    QRect handleRect() const;
    bool isHandlePixel( int x, int y ) const;

private:
    int m_MouseDownX;

};


#endif

