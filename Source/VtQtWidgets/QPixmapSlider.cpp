//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtGui/QPainter>
#include <QtCore/QRect>
#include <QtGui/QMouseEvent>
#include "QPixmapSlider.h"
#include "vt_audiomath.hpp"
#include "vt_messageboxes.hpp"

QPixmapSlider::QPixmapSlider( QWidget* parent ):
    QAbstractSlider( parent ),
    m_MouseDownX( 0 )
{
    setTracking( true );
    // setMouseTracking( true );
}

QPixmapSlider::~QPixmapSlider()
{
}

void QPixmapSlider::init( const QPixmap* bg, const QPixmap* handle,
    const QPixmap* handleActive, const QPixmap* handleDisabled )
{
    releasePixmap();
    setPixmap( 0, *bg );
    setPixmap( 1, *handle );
    if( handleActive != NULL ) setPixmap( 2, *handleActive );
    if( handleDisabled != NULL ) setPixmap( 3, *handleDisabled );
    updateSizeConstraints();
}

void QPixmapSlider::init( const QString& bg, const QString& handle,
    const QString& handleActive, const QString& handleDisabled )
{
    releasePixmap();
    setPixmap( 0, bg );
    setPixmap( 1, handle );
    if( handleActive.length() ) setPixmap( 2, handleActive );
    if( handleDisabled.length() ) setPixmap( 3, handleDisabled );
    updateSizeConstraints();
}

void QPixmapSlider::updateSizeConstraints()
{
    if( pixmap(0)!=NULL ){
        int w = pixmap(0)->width();
        int h = pixmap(0)->height();
        setMinimumHeight( h );
        setMaximumHeight( h );
        setMinimumWidth( w );
        setMaximumWidth( w );
    }
}

void QPixmapSlider::paintEvent( QPaintEvent* )
{
    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );
    painter.setRenderHint( QPainter::SmoothPixmapTransform );
    painter.setRenderHint( QPainter::HighQualityAntialiasing );
    painter.setRenderHint( QPainter::TextAntialiasing );

    if( pixmap(0) != NULL ){
        painter.drawPixmap( rect(), *(pixmap(0)), pixmap(0)->rect() );
    }else{
        VTDEBUGMSGT( "Background bitmap is not set." );
    }

    // Find the right bitmap for handle
    const QPixmap* handleBitmap = NULL;
    if( !isEnabled() )
        handleBitmap = pixmap( 3 );
    if( isSliderDown() && handleBitmap==NULL )
        handleBitmap = pixmap( 2 );
    if( handleBitmap==NULL )
        handleBitmap = pixmap( 1 );
    if( handleBitmap != NULL )
        painter.drawPixmap( handleRect(), *handleBitmap, handleBitmap->rect() );
}

int QPixmapSlider::handleX() const
{
    if( pixmap(1)==NULL )
        return 0;
    int maxPos = width() - pixmap(1)->width();
    if( minimum()==maximum() ) // Prevent division by 0
        return 0;
    int pos = int( double(maxPos) * double(value()-minimum())/double(maximum()-minimum()) + 0.5 );
    return pos;
}

int QPixmapSlider::valueFromHandleX( int hx ) const
{
    if( pixmap(1)==NULL )
        return minimum();
    int maxPos = width() - pixmap(1)->width();
    if( maxPos==0 ) // Prevent division by 0
        return minimum();
    double pos01 = double(hx) / double(maxPos);
    double valF = pos01 * double(maximum()-minimum()) + double(minimum());
    int val = VT::Math::Clip<int>( VT::Math::Round( valF ), minimum(), maximum() );
    return val;
}

QRect QPixmapSlider::handleRect() const
{
    if( pixmap(1)==NULL )
        return QRect();
    return QRect( handleX(), 0, pixmap(1)->width(), pixmap(1)->height() );
}

bool QPixmapSlider::isHandlePixel( int x, int y ) const
{
    QRect hrect = handleRect();
    if( !hrect.contains( x, y ) )
        return false;

    /*
    if( pixmap(1)==NULL )
        return true;
    if( !pixmap(1)->hasAlphaChannel() )
        return true;
    hrect.moveTo( 0, 0 );
    QPixmap channelImage = pixmap.alphaChannel();
    channelImage.getCo
    */

    return true;
}

void QPixmapSlider::mousePressEvent( QMouseEvent *me )
{
    if( isEnabled() ){
        if( me->button()==Qt::LeftButton ){
            if( isHandlePixel( me->x(), me->y() ) ){
                m_MouseDownX = me->x();
                setSliderDown( true );
            }
            update();
        }
    }
    QAbstractSlider::mousePressEvent( me );
}

void QPixmapSlider::mouseReleaseEvent( QMouseEvent *me )
{
    if( isSliderDown() && me->button()==Qt::LeftButton ){
        setSliderDown( false );
        update();
    }
    QAbstractSlider::mouseReleaseEvent( me );
}

void QPixmapSlider::mouseMoveEvent( QMouseEvent *me )
{
    if( isSliderDown() ){
        int delta = me->x() - m_MouseDownX;
        if( delta != 0 ){
            int oldHandleX = handleX();
            int newValue = valueFromHandleX( oldHandleX + delta );
            m_MouseDownX = me->x();
            setValue( newValue );
        }
    }
    QAbstractSlider::mouseMoveEvent( me );
}

void QPixmapSlider::keyReleaseEvent( QKeyEvent *ev )
{
    /*if( hasFocus() ){
        if( ev->key

    }*/
    QAbstractSlider::keyReleaseEvent( ev );
}

