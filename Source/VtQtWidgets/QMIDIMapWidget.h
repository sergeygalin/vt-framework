//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// VT::MIDI::CMIDIMap editor dialog
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef QMIDIMAPWIDGET_H
#define QMIDIMAPWIDGET_H

#include <QtGui/QTreeWidget>
#include <QtGui/QTreeWidgetItem>
#include <QtGui/QKeyEvent>

#include "vt_midi_map.hpp"

class QMIDIMapWidget: public QTreeWidget
{
    Q_OBJECT
public:
    QMIDIMapWidget( QWidget* parent = NULL );
    virtual ~QMIDIMapWidget();

    //Q_PROPERTY( startingOctave READ getStartingOctave WRITE setStartingOctave );
    int getStartingOctave() const { return m_StartingOctave; }
    void setStartingOctave( int oct ){ m_StartingOctave = oct; buildTable(); }

    const VT::MIDI::CMIDIMap& getMap();
    void setMap( const VT::MIDI::CMIDIMap& map );
    const VT::CString& getMapDescription() const;
    void setMapDescription( LPCVTCHAR d );

    // Directory where MIDI maps are stored, by default
    const VT::CString& GetMapDirectory() const { return m_MapDirectory; }
    void SetMapDirectory( LPCVTCHAR dir ){ m_MapDirectory.CopyFrom( dir ); }

    bool isModified() const { return m_Modified; }


signals:
    void mapAssigned();
    void modFlagChanged();


protected:
    virtual void keyPressEvent( QKeyEvent* ev );
    virtual void keyReleaseEvent( QKeyEvent* ev );
    virtual void hideEvent( QHideEvent* ev );
    virtual void focusOutEvent( QFocusEvent *ev );
    //virtual void leaveEvent( QEvent *ev );

private:
    QTreeWidgetItem *m_EditingItem, *m_LastEditingItem, *m_Items[128];
    int m_EditingColumn;
    VT::MIDI::CMIDIMap m_Map;
    int m_StartingOctave;
    VT::CString m_MapDirectory;
    bool m_Modified;

    void moveRight();
    void moveLeft();
    void moveUp( bool toLastColumn = false );
    void moveDown( bool toFirstColumn = false );

    QTreeWidgetItem* getItemToEdit();
    void syncUIAndMap();
    void syncRow( QTreeWidgetItem* item );
    // Sets out note columns from map; uses item's data to determine note number
    void setOutNote( QTreeWidgetItem* item );

public slots:
    void buildTable();
    bool openDialog();
    bool saveDialog();

    void clearAll();
    void clearAllConfirm();
    void clearMap();
    void clearMapConfirm();
    void setInToGMDrumsConfirm();
    void setOutToGMDrumsConfirm();

    // Set modification flag - this will also emit modFlagChanged().
    void setModified( bool m = true );

private slots:
    void openEditor( QTreeWidgetItem* item, int column );
    void closeEditor( QTreeWidgetItem* item, int column );
    void closeEditor();



};


#endif


