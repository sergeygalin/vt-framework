//-----------------------------------------------------------------------------
//
// VT Framework Qt Addons
//
// Buttons based on QPixmap images.
// Since Qt's button class is so great, it can be used as a radio button
// or a checkbox as well.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2006-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QPIXMAPBUTTON_H
#define QPIXMAPBUTTON_H

#include <QtGui/QAbstractButton>
#include "QPixmapReferencer.h"

//
// A graphical (painted in a pixmap) button.
// Source image can be a part of pixmap (specified via QRect).
// The image is split by parts (released button/pressed button)
// either vertically or horizontally, there can be 2, 3 or 4 parts:
//    1. Released
//    2. Pressed (default: same as 1)
//    3. Released & disabled (default: same as 1)
//    4. Pressed & disabled (default: same as 3, or as 1 if less than 3 states)
//
//    if MousedState is used ( initMoused(...) )
//    1. Released
//    2. Pressed (default: same as 1)
//    3. Over ( Mouse is over ) & Released
//    4. Released & disabled (default: same as 1)
//    5. Pressed & disabled (default: same as 3, or as 1 if less than 3 states)
//
class QPixmapButton: public QAbstractButton, protected QMultiPixmapReferencer
{
    Q_OBJECT
public:
    QPixmapButton( QWidget * parent = NULL );

    QPixmapButton( QWidget * parent, const QPixmap& pixmap, int nStates = 2,
        const QRect& buttonRect=QRect(0,0,0,0), bool vertical = true );
    QPixmapButton( QWidget * parent, const QString& file, int nStates = 2,
        const QRect& buttonRect=QRect(0,0,0,0), bool vertical = true );

    void init( const QPixmap& pixmap, int nStates = 2,
        const QRect& buttonRect=QRect(0,0,0,0), bool vertical = true );
    void init( const QString& file, int nStates = 2,
        const QRect& buttonRect=QRect(0,0,0,0), bool vertical = true );

    void init( const QString& file1, const QString& file2,
        const QString& file3=QString(), const QString& file4=QString() );

    void init( const QPixmap* img1, const QPixmap* img2,
        const QPixmap* img3=NULL, QPixmap* img4=NULL );

    void initMoused( const QString& file1, const QString& file2, const QString& file5,
        const QString& file3=QString(), const QString& file4=QString() );

    void initMoused( const QPixmap* img1, const QPixmap* img2, const QPixmap* img5,
        const QPixmap* img3=NULL, QPixmap* img4=NULL );

    //
    // Text caption rendering
    //
    Q_PROPERTY( bool showCaption READ getShowCaption WRITE setShowCaption );
    bool getShowCaption() const { return m_ShowCaption; }
    void setShowCaption( bool show ){ m_ShowCaption = show; update(); }
    void setCaptionPressedOffset( int dx = 0, int dy = 1 );
    void setShowCaption( bool show, int dx, int dy );

    //
    // Tag - each button may have a numeric tag (old-school compatibility ;))
    //
    Q_PROPERTY( int tag READ getTag WRITE setTag );
    Q_PROPERTY( int* tagOut READ getTagOut WRITE setTagOut );
    int getTag() const { return m_Tag; }
    void setTag( int tag ){ m_Tag = tag; }
    int* getTagOut() const{ return m_pTagOut; }
    void setTagOut( int* out ){ m_pTagOut = out; }

protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void enterEvent(QEvent * event);
    virtual void leaveEvent(QEvent * event);

private:
    bool m_ShowCaption;
    int m_Tag;
    int m_CaptionPressedXOffset, m_CaptionPressedYOffset;
    int *m_pTagOut;
    QRect m_SrcRectOff, m_SrcRectOn, m_SrcRectOffDisabled, m_SrcRectOnDisabled;

    bool m_useMousedState;
    bool m_isMouseOver;

    void initPixmapButton( const QRect& buttonRect, bool vertical, int nStates );
};




#endif

