//-----------------------------------------------------------------------------
//
// A library which sits over Steinberg VST SDK and provides cleaner
// programming interface and some additional functionality.
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2007 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/


#ifndef VT_VSTADDONS_HPP
#define VT_VSTADDONS_HPP

#include "vt_audiomath.hpp"
#include "vt_parameters.hpp"
#include "vt_string.hpp"

#include "audioeffectx.h"

using namespace VT;
using namespace VT::Params;

// Enables "programs are chunks" mode and getChunk/setChunk functions.
// This may be incompatible with parameter automation in some hosts.
#define VT_USE_CHUNKS_SAVING

namespace VT {

    const double
        VTMaxSampleRate = 192000.0;

    extern float VTTestValue;

    // ADDITIONAL INFO:
    // This class also sets application instance in VT; see vt_portable.hpp:
    //      void SetModuleInstance( HINSTANCE inst ),
    //   HINSTANCE GetModuleInstance().
    class CVTAudioEffectX: public AudioEffectX, public Params::CParameterValidator
    {
    public:
        enum TProcessingMode { mReplacing, mAccumulating };

        CVTAudioEffectX(
            audioMasterCallback audioMaster,
            VstInt32 numPrograms,
            VstInt32 numParams,
            bool hasDoublePrecision = true
        );
        virtual ~CVTAudioEffectX ();

        // Register parameter. If deleteIt flag is set, then 'newparam'
        // is deleted in CVTAudioEffectX's destructor.
        int VTAddParam(Params::CParameter *newparam, bool deleteIt = true);

        VTINLINE Params::CParameter* VTParam( VstInt32 index ) const { return vtParams[index]; }
        VstInt32 VTParamIdx( const char *name, bool bUseCodeName = false ) const;
        Params::CParameter* VTParam( const char *name ) const;
        void setParameter (const char *name, float value); // sets param by name, if one can be found
        virtual bool VTValidateParams( Params::CParameter *lastChangedParam=0 ){ return false; }
        int VTParamCount() const { return vtParamCount; }

        // Functions called by VST SDK. Do not change their prototypes!
        virtual bool canParameterBeAutomated(VstInt32 index){
            bool ret = VTParam(index)->CanBeAutomated();
            // VTDEBUGMSG( CString("idx=")<<index<<" - "<<LCYESNO(ret) );
            return ret;
        }
        virtual bool string2parameter (VstInt32 index, char *text);
        virtual void setParameter (VstInt32 index, float value);
        virtual float getParameter (VstInt32 index);
        virtual void getParameterLabel (VstInt32 index, char *label);
        virtual void getParameterDisplay (VstInt32 index, char *text);
        virtual void getParameterName (VstInt32 index, char *text);

        virtual void setProgramName (char *name);
        virtual void getProgramName (char *name);

        #if defined(VT_USE_CHUNKS_SAVING)
            virtual VstInt32 getChunk (void **data, bool isPreset=false);
             virtual VstInt32 setChunk (void *data, VstInt32 byteSize, bool isPreset=false);
        #endif

        virtual void resetToDefaults();

        //
        // Processes
        //
        // Stub for standard VST process() function. Called by VST host.
        virtual void process(float **inputs, float **outputs, VstInt32 sampleFrames){
            processor(inputs, outputs, sampleFrames, mAccumulating); }
        // Stub for standard VST processReplacing() function. Called by VST host.
        virtual void processReplacing (float **inputs, float **outputs, VstInt32 sampleFrames){
            processor(inputs, outputs, sampleFrames, mReplacing); }
        // Stub for standard VST processDoubleReplacing() function. Called by VST host.
        virtual void processDoubleReplacing (double **inputs, double **outputs, VstInt32 sampleFrames){
            processorDouble(inputs, outputs, sampleFrames, mReplacing); }

        virtual void editorOpen(){ vtEditorOpenCount++; }
        virtual void editorClosed(){ vtEditorOpenCount--; }
        bool isEditorOpen() const { return vtEditorOpenCount!=0; }

        // Get max possible sound frequency for current sampling rate, in Hz.
        VTINLINE double Nyquist() { return getSampleRate()/2.0; }

        bool inParameterLoading() const { return bReloading; }

        static void VSTParamStrCopy( char *dest, LPCVTCHAR src );

    protected:
        char VTProgramName[kVstMaxParamStrLen+1];

        bool reloading() const { return bReloading; }

        //
        // Sample output utilites
        //
        static VTINLINE void ApplySample(float *out, float in, TProcessingMode mode){
            (mode==mReplacing)?(*out=in):(*out+=in); }

        static VTINLINE void ApplySample(float *out, double in, TProcessingMode mode){
            (mode==mReplacing)?(*out=float(in)):(*out=float((*out)+in)); }

        static VTINLINE void ApplySample(double *out, double in, TProcessingMode mode){
            (mode==mReplacing)?(*out=in):(*out=(*out)+in); }

        //
        // Processing function prototypes
        //
        virtual void processor (float **inputs, float **outputs, VstInt32 sampleFrames, TProcessingMode mode)=0;
        virtual void processorDouble (double **inputs, double **outputs, VstInt32 sampleFrames, TProcessingMode mode)=0;

        //
        // These functions are called when global parameter reset or reloading
        // is starting / ending. They can be expanded in descendant classes
        // if some additional action is required.
        //
        virtual void beginParameterLoading();
        virtual void endParameterLoading();

    private:
        Params::CParameter **vtParams;
        bool *vtParamShouldBeDeleted;
        int vtParamCount, vtMaxParams;
        bool bReloading;
        volatile int vtEditorOpenCount;
    };




} // namespace


#endif
