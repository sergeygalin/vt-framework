//-----------------------------------------------------------------------------
//
// VST GUI Keyboard
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2007 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include "vt_messageboxes.hpp" // First include!
#include "vt_midi.hpp"
#include "vt_debug.hpp"
#include "vt_string.hpp"
#include "vt_vstkeyboard.hpp"

using namespace VT;



CKeyboard::CKeyboard( CPoint pos, CControlListener *ownr, long tag, CBitmap *pBackground,
                     BYTE startingNote, BYTE nNotes, CBitmap *keyboardOff, CBitmap *finger,
                     int blackKeyBottom, int topmargin, int bottommargin ):
    CControl(
        CRect(
            pos.x,
            pos.y,
            pBackground->getWidth()+pos.x,
            pBackground->getHeight()+pos.y ),
        ownr,
        tag,
        pBackground ),
    CKeyboardPosCalc( startingNote, nNotes, pBackground->getWidth() ),
    m_pDrawArea( NULL ),
    m_pKeyboardOff( keyboardOff ),
    m_pFinger( finger ),
    m_bKeysChanged( false ),

    m_iBlackKeyBottom( blackKeyBottom ),
    m_iTopMargin( topmargin ),
    m_iBottomMargin( bottommargin ),
    m_pKeyboardListener( NULL ),
    m_MousedKey( MIDI::kErrorNote )
{
    // m_myPosition = pos;
    m_pDrawArea = new CBitmap( *(this->getFrame()), pBackground->getWidth(), pBackground->getHeight() );
    m_pKeyboardBankState = new CBitmap( *(this->getFrame()), pBackground->getWidth(), pBackground->getHeight() );
    m_pKeyboardOff->remember();
    m_pFinger->remember();
}

CKeyboard::~CKeyboard()
{
    if( m_pDrawArea!=NULL ){
        m_pDrawArea->forget();
        m_pDrawArea = NULL;
    }
    if( m_pKeyboardOff!=NULL ){
        m_pKeyboardOff->forget();
        m_pKeyboardOff = NULL;
    }
    if( m_pKeyboardBankState!=NULL ){
        m_pKeyboardBankState->forget();
        m_pKeyboardBankState = NULL;
    }
    if( m_pFinger!=NULL ){
        m_pFinger->forget();
        m_pFinger = NULL;
    }
}

void CKeyboard::draw (CDrawContext *pContext)
{
    if( IsRangeChanged() || m_bKeysChanged ){ ///!!!!  If keyboard changed
        CRect drawsize( 0, 0, getBackground()->getWidth(), getBackground()->getHeight() );
        // Create draw context


        if( IsRangeChanged() ){
            //
            // Drawing keyboard in a state which doesn't change while bank
            // doesn't change, on m_pKeyboardBankState.
            //
            COffscreenContext *da = new COffscreenContext (pContext, m_pKeyboardBankState, true);
            da->setDrawMode(kCopyMode);
            // First, draw grayed keyboard
            m_pKeyboardOff->draw(da, drawsize);
            // Draw working part of the keyboard
            if( GetMinNote()<=GetEndingNote() && GetMaxNote()>=GetStartingNote() ){ // Check that something is visible at all
                BYTE
                    minvis = Math::Max( GetStartingNote(), GetMinNote() ),
                    maxvis = Math::Min( GetEndingNote(), GetMaxNote() );
                int left = GetKeyLeftXR( minvis ), right = GetKeyRightXR( maxvis );
                CRect hlsize( left, 0, right, getBackground()->getHeight() );
                // Draw the highlighter
                getBackground()->draw(da, hlsize, CPoint( left, 0 ));
            }
            delete da;
            FlushRangeChanged();
        }

        COffscreenContext *offDrawArea = new COffscreenContext (pContext, m_pDrawArea, true);

        //
        // Draw keyboard
        //
        offDrawArea->setDrawMode( kCopyMode );
        m_pKeyboardBankState->draw( offDrawArea, drawsize );

        //
        // Draw keys
        //
        BYTE end = GetEndingNote();

        int ywhite = getBackground()->getHeight() - m_pFinger->getHeight() - 1,
            yblack = m_iBlackKeyBottom - m_pFinger->getHeight() - 1,
            // Max<int>( getBackground()->getHeight()/3 - m_pFinger->getHeight()/2, 0 ),
            xshift = - m_pFinger->getWidth()/2;

        CRect fingerPos( 0, 0, m_pFinger->getWidth(), m_pFinger->getHeight() );
        for( BYTE key = GetStartingNote(); key<=end; key++ ){
            if( IsKeyPressed( key ) ){
                int x = GetKeyCenterXR( key ) + xshift;
                fingerPos.moveTo( x, IsWhite( key )? ywhite: yblack );
                m_pFinger->draw( offDrawArea, fingerPos );
            }
        }

        // Destroy draw context
        delete offDrawArea;
    }

    // Output completed m_pDrawArea
    pContext->setDrawMode(kCopyMode);
    m_pDrawArea->draw(pContext, size);
    setDirty(false);
}

void CKeyboard::mouse( CDrawContext *pContext, CPoint &pos, long buttons )
{
    // Some crap from CKickButton, I don't know what's that.
    // if( button == -1 )
    //     button = pContext->getMouseButtons ();
    // if (listener && button & (kAlt | kShift | kControl | kApple))
    //{
    //    if (listener->controlModifierClicked (pContext, this, button) != 0)
    //        return;
    // }
    // if (!(button & kLButton))
    //    return;
    beginEdit();
    if( pContext->getMouseButtons () == kLButton ){
        CRect viewSize;
        getViewSize(viewSize);
        CPoint clickPos;
        do{
            getMouseLocation (pContext, clickPos);
            clickPos.offset( -viewSize.x, -viewSize.y );
            BYTE newKey = WhichKey( clickPos );
            if( m_MousedKey != newKey ){ // Some key is pressed, different to old state
                if( m_MousedKey != MIDI::kErrorNote ){ // Some key was down, release it
                    onKeyboardKeyUp( m_MousedKey );
                    m_MousedKey = MIDI::kErrorNote;
                }
                // Set new key pressing
                m_MousedKey = newKey;
                if( m_MousedKey != MIDI::kErrorNote )
                    onKeyboardKeyDown( m_MousedKey );
            }
            doIdleStuff ();
        }while( pContext->getMouseButtons() == kLButton );
    }
    //
    // Left mouse button is up here.
    //
    if( m_MousedKey != MIDI::kErrorNote ){ // Something was playing
        onKeyboardKeyUp( m_MousedKey );
        m_MousedKey = MIDI::kErrorNote;
    }
    // CControl::mouse( pContext, pos, buttons );
    endEdit();
}



BYTE CKeyboard::WhichKey( const CPoint& pos )
{
    int
        mousableTop = m_iTopMargin,
        mousableBottom = getBackground()->getHeight() - m_iBottomMargin;

    // Checking whether it's a keyboard area
    if( pos.y<mousableTop || pos.y>mousableBottom || pos.x<0 || pos.x>=getBackground()->getWidth() )
        return MIDI::kErrorNote;

    // Checking for black key
    if( pos.y < m_iBlackKeyBottom )
        for( BYTE i=GetStartingNote(), end=GetEndingNote(); i<=end; i++ )
            if( !IsWhite(i) )
                 if( GetKeyLeftXR(i)<=pos.x &&
                    GetKeyRightXR(i)>=pos.x )
                    return i;

    // Checking for white key
    for( BYTE i=GetStartingNote(), end=GetEndingNote(); i<=end; i++ )
        if( IsWhite(i) )
             if( GetKeyLeftXR(i)<=pos.x &&
                GetKeyRightXR(i)>=pos.x )
                return i;

    // Checking for white key with looser tolerances
    // !!! Workaround for rounding errors !!!
    for( BYTE i=GetStartingNote(), end=GetEndingNote(); i<=end; i++ )
        if( IsWhite(i) )
             if( GetKeyLeftXR(i)-1<=pos.x &&
                GetKeyRightXR(i)+1>=pos.x )
                return i;

    // Nothing found!!!
    VTDEBUGMSGT("I didn't find a key here, how is that possible?!");
    return MIDI::kErrorNote;
}

void CKeyboard::onKeyboardKeyUp( BYTE key )
{
    // VTDEBUGMSG(CString(__FUNCTION__ ": RELEASED ")<<key);
    if( m_pKeyboardListener!=NULL )
        m_pKeyboardListener->onKeyboardKeyUp( key );
}

void CKeyboard::onKeyboardKeyDown( BYTE key )
{
    // VTDEBUGMSG(CString(__FUNCTION__ ": PUSHED ")<<key);
    if( m_pKeyboardListener!=NULL )
        m_pKeyboardListener->onKeyboardKeyDown( key );
}







CKeyboardRange::CKeyboardRange( CPoint pos, BYTE startingNote,
                               BYTE nNotes, CBitmap *blank, CBitmap *highlight ):
    CView( CRect( pos.x, pos.y, pos.x+blank->getWidth(), pos.y+blank->getHeight() ) ),
    CKeyboardPosCalc( startingNote, nNotes, blank->getWidth() ),
    m_pBlank( blank ),
    m_pHighlight( highlight )
{
    VTASSERT( blank->getWidth()==highlight->getWidth() );
    VTASSERT( blank->getHeight()==highlight->getHeight() );

    m_pBlank->remember();
    m_pHighlight->remember();
    m_pDrawArea = new CBitmap( *(this->getFrame()), blank->getWidth(), blank->getHeight() );

}

CKeyboardRange::~CKeyboardRange()
{
    m_pBlank->forget();
    m_pBlank = NULL;
    m_pHighlight->forget();
    m_pHighlight = NULL;
    m_pDrawArea->forget();
    m_pDrawArea = NULL;
}

void CKeyboardRange::draw( CDrawContext *pContext )
{
    CRect drawsize( 0, 0, m_pBlank->getWidth(), m_pBlank->getHeight() );

    if( IsRangeChanged() ){ ///  If keyboard changed
        // Create draw context
        COffscreenContext *offDrawArea = new COffscreenContext (pContext, m_pDrawArea, true);

        // First, draw background.
        offDrawArea->setDrawMode(kCopyMode);
        m_pBlank->draw(offDrawArea, drawsize);

        // Check that something is visible at all
        if( GetMinNote()<=GetEndingNote() && GetMaxNote()>=GetStartingNote() ){

            BYTE
                minvis = Math::Max( GetStartingNote(), GetMinNote() ),
                maxvis = Math::Min( GetEndingNote(), GetMaxNote() );

            int left = GetKeyLeftXR( minvis ), right = GetKeyRightXR( maxvis );

            CRect hlsize( left, 0, right, m_pHighlight->getHeight() );

            // Draw the highlighter
            m_pHighlight->draw(offDrawArea, hlsize, CPoint( left, 0 ));
        }
        // Destroy draw context
        delete offDrawArea;
        FlushRangeChanged();
    }

    // Output completed m_pDrawArea
    pContext->setDrawMode(kCopyMode);
    m_pDrawArea->draw(pContext, size);
    setDirty(false);
}

