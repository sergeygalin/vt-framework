//-----------------------------------------------------------------------------
//
// Qt+VST
// Qt DLL renaming patch.
// TO DO: patch for more Qt DLL's; versioning of DLL names via command line.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

#ifndef _CRT_SECURE_NO_DEPRECATE
    #define _CRT_SECURE_NO_DEPRECATE
#endif

struct TReplacement
{
    // Lengths of the "from" and "to" strings must be the same.
    const char *m_From, *m_To;
};

const TReplacement g_ReplTable[] =
    {
        // WARNING!!!!!!!!!
        // If you use another Qt version or compiler, change the right column
        // to something else to avoid DLL conflicts. The prefix("QPlg") should
        // be changed if the change is not made in the "official" branch of
        // the patcher.

        // Qt 4.5.0 MSC 2005 build replacements
        { "QtCore4.dll",    "QPlgCo0.dll" },
        { "QtGui4.dll",        "QPlgG0.dll" },
        { "QtCored4.dll",    "QPlgCoD0.dll" },
        { "QtGuid4.dll",    "QPlgGD0.dll" },
        { NULL, NULL }
    };

int DoReplacing( char *buffer, long sz )
{
    int count = 0;
    for( const TReplacement* rp = g_ReplTable; rp->m_From != NULL; rp++ ){
        size_t len = strlen( rp->m_From );
        if( len!=strlen( rp->m_To ) ){
            printf("Error in the replacement table!");
            exit( 100 );
        }
        for( long i=0, end = sz-long(len); i<end; i++ ){
            if( !_memicmp( buffer+i, rp->m_From, len ) ){
                printf(" [%ld] \"%s\" => \"%s\"\n", i, rp->m_From, rp->m_To );
                memcpy( buffer+i, rp->m_To, len );
                count++;
            }
        }
    }
    return count;
}

bool DoPatching( const char *fileName )
{
    FILE *fp = fopen( fileName, "r+b" );
    if( fp==NULL ){
        printf( "Failed to open file: %s\n", fileName );
        return false;
    }
    bool ret = true;
    try{
        printf( "Opened: %s\n", fileName );
        fseek( fp, 0, SEEK_END );
        long sz = ftell( fp );
        fseek( fp, 0, SEEK_SET );
        printf("Size: %ld bytes\n", sz );
        char *buffer = new char[sz];
        if( buffer==NULL ){
            printf("Memory allocation error!!\n");
            throw;
        }
        try{
            if( fread( buffer, sz, 1, fp )!=1 ){
                printf("Read error!\n");
                throw;
            }
            if( DoReplacing( buffer, sz ) ){
                printf("Patched, saving...\n");
                fseek( fp, 0, SEEK_SET );
                if( fwrite( buffer, sz, 1, fp )!=1 ){
                    printf("Write error!\n");
                    throw;
                }
            }else{
                printf("The file was not modified.\n");
            }
        }catch(...){
            ret = false;
        }
        delete buffer;
    }catch(...){
        ret = false;
    }
    fclose(fp);
    return ret;
}


int main( int argc, char *argv[] )
{
    printf(
        "Qt Plugin DLL Patcher ver. 0.0 built " __DATE__ "\n"
        "Copyright(C) Sergey A. Galin, 2009. http://sageshome.net/\n"
        "Licensed under terms of GNU GPL license ver. 2.0\n"
        "\n");


    printf("REPLACEMENT TABLE\n--------------------------------------\n");
    for( const TReplacement* rp = g_ReplTable; rp->m_From != NULL; rp++ )
        printf("%-18s %-18s\n", rp->m_From, rp->m_To );
    printf("--------------------------------------\n\n");

    if( argc<2 ){
        printf( "USAGE: %s <dll1> [<dll2>...]\n", argv[0] );
        printf( "\nThe DLL's to patch are:\n"
            " - Qt run-times (don't forget to rename them according to the table).\n"
            " - Plugin application DLL.\n"
            " - Plugin DLLs.\n");
        return 0;
    }

    for( int i=1; i<argc; i++ )
        if( !DoPatching( argv[i] ) )
            return 1;

    return 0;
}



