//-----------------------------------------------------------------------------
//
// Qt+VST
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <ctype.h>

#include <QWindowsStyle>
#include <QWindowsVistaStyle>
#include <QWindowsXPStyle>
#include <QMotifStyle>
#include <QCDEStyle>
#include <QPlastiqueStyle>
#include <QCleanlooksStyle>
#include <QString>
#include <QAction>
#include <QTextEdit>
#include <QLineEdit>
#include <QAbstractSpinBox>

#include <qt_windows.h>
#include <qevent.h>
#include <qmessagebox.h>
#include "qeffecteditor.hpp"
#include "vt_string.hpp"


CQEffectEditor::CQEffectEditor( AudioEffectX *effect ):
    AEffEditor( effect ),
    m_SysWidget( NULL ),
    m_GUIWidget( NULL ),
    m_PluginStyle( NULL ),
    m_SizeDetected( false )
{
    m_Rect.left = 0; m_Rect.top = 0;
    m_Rect.right = 300; m_Rect.bottom = 100;
    setEffect( effect );
}

CQEffectEditor::~CQEffectEditor()
{
    VT::CCSLock lock( &m_CS );
    try{
        DeleteWidgets();
        DeletePluginStyle();
        //VTDEBUGMSGT("Alive1!");
    }VT_DEBUGCATCH( lock.Disconnect() );
    //VTDEBUGMSGT("Alive2!");
}

void CQEffectEditor::setEffect( AudioEffectX *neweffect )
{
    effect = neweffect;
    if( effect!=NULL ){
        effect->setEditor (this);
        // QApplication
        //char text[256];
        //if( effect->getProductString( text ) )
        //    QApplication::setApplicationName( text );
        //QApplication::setApplicationVersion( QString().setNum( effect->getVendorVersion() ) );
    }
}

bool CQEffectEditor::open(void *ptr)
{
    VT::CCSLock lock( &m_CS );
    try{
        if( isOpen() )
            return false;
        AEffEditor::open(ptr);

        m_GUIWidget = CreateGUIWidget( NULL, (AudioEffectX*)getEffect() );
        QApplication::setPalette( m_GUIWidget->palette() );

        if( m_GUIWidget==NULL )
            return false;
        DetectSize(); // Will use the existing m_GUIWidget
        m_SysWidget = new QSysPluginWidget( HWND(ptr), this );
        if( m_SysWidget==NULL ){
            delete m_GUIWidget;
            return false;
        }
        m_SysWidget->move( 0, 0 );
        m_SysWidget->resize( m_Rect.right, m_Rect.bottom );

        m_SysWidget->setWindowIcon( m_Icon );

        // Using The Dark Side of The Force.
        // Whole host GUI will lock up without that because of eternal
        // loop in m_SysWidget's focusing routines.
        m_SysWidget->setFocusPolicy( Qt::NoFocus );
        m_GUIWidget->setFocusPolicy( Qt::StrongFocus );

        // Adoption!
        m_SysWidget->setPalette( m_GUIWidget->palette() );
        m_GUIWidget->setParent( m_SysWidget );
        m_GUIWidget->move( 0, 0 );
        m_SysWidget->move( 0, 0 );

        char text[256];
        if( ((AudioEffectX*)getEffect())->getProductString( text ) ){
            QString title(text);
            m_SysWidget->setWindowTitle( title );
            m_GUIWidget->setWindowTitle( title );
        }

        updatePluginStyle();

        // Display it! (Will also show the GUI widget, as it's already a child.)
        m_SysWidget->show();
        m_MB.setParent( m_SysWidget );

        return true;
    }VT_DEBUGCATCH( lock.Disconnect() );
    return false;
}

void CQEffectEditor::close()
{
    VT::CCSLock lock( &m_CS );
    try{
        AEffEditor::close();
        DeleteWidgets();
    }VT_DEBUGCATCH( lock.Disconnect() );
}

void CQEffectEditor::DeleteWidgets()
{
    VT::CCSLock lock( &m_CS );
    try{
        // Notifying message box about destruction of the widgets
        if( m_SysWidget!=NULL )
            m_MB.parentClosed( m_SysWidget );
        if( m_GUIWidget!=NULL ){
            m_GUIWidget->hide();
            m_GUIWidget->deleteLater(); // Will be deleted by m_SysWidget
            m_GUIWidget = NULL;
        }
        if( m_SysWidget!=NULL ){
            // Detaching widget from the effect
            m_SysWidget->setOwner( NULL );
            // Scheduling destruction of the widget; if there are
            // any dialog-locked functions, they have chance to exit
            // before the widget is actually destroyed.
            m_SysWidget->deleteLater();
            m_SysWidget = NULL;
        }
    }VT_DEBUGCATCH( lock.Disconnect() );
}

bool CQEffectEditor::getRect(ERect** rect)
{
    VT::CCSLock lock( &m_CS );
    try{
        DetectSize();
        *rect = &m_Rect;
        return true;
    }VT_DEBUGCATCH( lock.Disconnect() );
    return false;
}

void CQEffectEditor::DetectSize()
{
    VT::CCSLock lock( &m_CS );
    try{
        if( !m_SizeDetected ){
            if( m_GUIWidget==NULL ){
                QWidget* tmpWidget = CreateGUIWidget( NULL, (AudioEffectX*)getEffect() );
                m_Rect.right = tmpWidget->width();
                m_Rect.bottom = tmpWidget->height();
                delete tmpWidget;
            }else{
                m_Rect.right = m_GUIWidget->width();
                m_Rect.bottom = m_GUIWidget->height();
            }
            m_SizeDetected = true;
        }
    }VT_DEBUGCATCH( lock.Disconnect() );
}

bool CQEffectEditor::isOpen()
{
    VT::CCSLock lock( &m_CS );
    try{
        return m_SysWidget!=NULL;
    }VT_DEBUGCATCH( lock.Disconnect() );
    return false;
}

void CQEffectEditor::idle()
{
    /*
    VT::CCSLock lock( &m_CS );
    try{
        if( m_SysWidget!=NULL )
            m_SysWidget->VSTIdle();
    }VT_DEBUGCATCH( lock.Disconnect() );
    */
}

bool CQEffectEditor::onKeyDown(VstKeyCode& keyCode)
{
    VT::CCSLock lock( &m_CS );
    try{
        VSTKey( false, keyCode );
    }VT_DEBUGCATCH( lock.Disconnect() );
    return true;
}

bool CQEffectEditor::onKeyUp(VstKeyCode& keyCode)
{
    VT::CCSLock lock( &m_CS );
    try{
        VSTKey( true, keyCode );
    }VT_DEBUGCATCH( lock.Disconnect() );
    return  true;
}

bool CQEffectEditor::onWheel(float distance)
{
    /*
    VT::CCSLock lock( &m_CS );
    try{
    //QApplication::sendEvent( QApplication::focusWidget(), &ev );
    }VT_DEBUGCATCH( lock.Disconnect() );
    */
    return false;
}

bool CQEffectEditor::setKnobMode(VstInt32 val)
{
    /*
    VT::CCSLock lock( &m_CS );
    try{
        if( m_SysWidget!=NULL )
            return m_SysWidget->VSTSetKnobMode( int(val) );
    }VT_DEBUGCATCH( lock.Disconnect() );
    */
    return false;
}

// Update style on system and GUI widgets
void CQEffectEditor::updatePluginStyle()
{
    VT::CCSLock lock( &m_CS );
    try{
        if( m_PluginStyle!=NULL ){
            if( m_SysWidget!=NULL )
                updateWidgetStyle( m_SysWidget );
            if( m_GUIWidget!=NULL )
                updateWidgetStyle( m_GUIWidget );
        }
    }VT_DEBUGCATCH( lock.Disconnect() );
}

bool CQEffectEditor::setPluginStyle( const QString& name )
{
    VT::CCSLock lock( &m_CS );
    try{
        #define QSPW_STYLE( textname, classname )                        \
            if( name.compare( textname, Qt::CaseInsensitive )==0 ){        \
                DeletePluginStyle();                                    \
                m_PluginStyle = new classname();                        \
                updatePluginStyle();                                    \
                return true;                                            \
            }
        QSPW_STYLE( "windows", QWindowsStyle );
        QSPW_STYLE( "windowsvista", QWindowsVistaStyle );
        QSPW_STYLE( "vista", QWindowsVistaStyle );
        QSPW_STYLE( "windowsxp", QWindowsXPStyle );
        QSPW_STYLE( "xp", QWindowsXPStyle );
        QSPW_STYLE( "motif", QMotifStyle );
        QSPW_STYLE( "cde", QCDEStyle );
        QSPW_STYLE( "plastique", QPlastiqueStyle );
        QSPW_STYLE( "cleanlooks", QCleanlooksStyle );
    }VT_DEBUGCATCH( lock.Disconnect() );
    return false;
}

// Set style on this widget and all child widgets
void CQEffectEditor::updateWidgetStyle( QWidget *widget )
{
    VT::CCSLock lock( &m_CS );
    try{
        if( m_PluginStyle != NULL ){
            int childCount = widget->children().count();
            for( int i=0; i<childCount; i++ ){
                QObject *obj = widget->children().at(i);
                if( obj->isWidgetType() )
                    ((QWidget*)obj)->setStyle( m_PluginStyle );
                else{
                    /*
                    #if defined(_DEBUG)
                        QString str = "Non-widget child found in QWidget: ";
                        str += obj->objectName();
                        str += " Class: ";
                        str += obj->metaObject()->className();
                        VTDEBUGMSG( str.toLocal8Bit () );
                    #endif
                    */
                }
            }
            widget->setStyle( m_PluginStyle );
        }
    }VT_DEBUGCATCH( lock.Disconnect() );
}

void CQEffectEditor::DeletePluginStyle()
{
    VT::CCSLock lock( &m_CS );
    try{
        if( m_PluginStyle!=NULL ){
            // delete m_PluginStyle;
            m_PluginStyle->deleteLater();
            m_PluginStyle = NULL;
        }
    }VT_DEBUGCATCH( lock.Disconnect() );
}

void CQEffectEditor::setIcon( const QIcon& icon )
{
    VT::CCSLock lock( &m_CS );
    try{
        m_Icon = icon;
        if(    m_SysWidget!=NULL )
            m_SysWidget->setWindowIcon( m_Icon );
    }VT_DEBUGCATCH( lock.Disconnect() );
}

void CQEffectEditor::setIcon( const QPixmap& pm )
{
    setIcon(QIcon(pm));
}

void CQEffectEditor::setIcon( const QString& fileName )
{
    setIcon(QIcon(fileName));
}






void CQEffectEditor::VSTKey( bool up, const VstKeyCode& keyCode )
{
    QWidget* w = QApplication::focusWidget();
    if( w==NULL )
        return;

    QString text; // Here will be the text printed by the key (if any)
    VstInt32 k = keyCode.character;

    // Nuendo does not apply shift key to the character... Fix that
    if( keyCode.modifier & MODIFIER_SHIFT )
        k = ApplyShift( k );

    // If it's something printable, insert the character into the printable text
    if( (keyCode.modifier == 0 || keyCode.modifier == MODIFIER_SHIFT) && k>31 )
        text += char(k);
    else
        // Something unprintable, maybe a shortcut.
        // IMPORTANT: Any shortcuts in Qt must be with capital letters!
        k = toupper( k );

    // Convert modification keys
    Qt::KeyboardModifiers qmodif = VSTModifiers( keyCode.modifier );

    // Select type of Qt key event
    QEvent::Type t = (up)?QEvent::KeyRelease: QEvent::KeyPress;

    // Convert virtual keys to Qt key codes
    if( keyCode.virt ){
        k = VSTVirtualKeys( keyCode.virt );
        if( !k ){
            if( !up ){
                VTDEBUGMSG( VT::CString(VTT("Unknown virtual VST key: decimal# = ")).ConcatLong(keyCode.virt) ); }
            return;
        }
    }else{
        VTASSERT2T( k<128, VT::CString(VTT("Strange non-virtual key code received from host: k = ")).ConcatLong(k).
            ConcatStr(VTT(", keyCode.character=")).ConcatLong(keyCode.character) );
    }

    // Finally, create & send the event
    QKeyEvent ev( t, k, qmodif, text );
    QApplication::sendEvent( w, &ev );

    #if 0
        if( up ){
            VT::CString str;
            ((((str<<VTT("VSTKey: character=")).ConcatLong(keyCode.character, VTT("%lx"))<<VTT(", virt=")).
                ConcatLong(keyCode.virt, VTT("%lx")))<<
                VTT(", modifier=")).ConcatLong(keyCode.modifier, VTT("%lx"));
            (str<<VTT(", k=")).ConcatLong(k, VTT("%lx"));
            (str<<VTT(", qmodif=")).ConcatLong(qmodif, VTT("%lx"));
            str<<VTT(", text='")<<(VT::CString().CopyFrom((const char*)text))<<VTT("'");
            VT::VTMB::Info( MB(), str );
        }
    #endif
}

Qt::KeyboardModifiers CQEffectEditor::VSTModifiers( int modifier )
{
    Qt::KeyboardModifiers m = Qt::NoModifier;
    if( modifier & MODIFIER_SHIFT )        m |= Qt::ShiftModifier;
    if( modifier & MODIFIER_ALTERNATE )    m |= Qt::AltModifier;

    //
    // Qt:
    //
    // Note: On Mac OS X, the ControlModifier value corresponds to the Command
    // keys on the Macintosh keyboard, and the MetaModifier value corresponds
    // to the Control keys. The KeypadModifier value will also be set when
    // an arrow key is pressed as the arrow keys are considered part of the keypad.
    // Note: On Windows Keyboards, Qt::MetaModifier and Qt::Key_Meta
    // are mapped to the Windows key.
    //
    // VST:
    //
    // MODIFIER_COMMAND   = 1<<2, ///< Control on Mac
    // MODIFIER_CONTROL   = 1<<3  ///< Ctrl on PC, Apple on Mac

    if( modifier & MODIFIER_CONTROL )    m |= Qt::ControlModifier;
    if( modifier & MODIFIER_COMMAND )    m |= Qt::MetaModifier;

    // No matches for:
    // Qt::KeypadModifier
    // Qt::GroupSwitchModifier

    return m;
}


Qt::Key CQEffectEditor::VSTVirtualKeys( int virt )
{
    switch( virt ){
        case VKEY_BACK:            return Qt::Key_Backspace;
        case VKEY_TAB:            return Qt::Key_Tab;
        case VKEY_CLEAR:        return Qt::Key_Clear;
        case VKEY_RETURN:        return Qt::Key_Return; // Normal Enter key; keypad one is Key_Enter
        case VKEY_PAUSE:        return Qt::Key_Pause;
        case VKEY_ESCAPE:        return Qt::Key_Escape;
        case VKEY_SPACE:        return Qt::Key_Space;
        case VKEY_NEXT:            VTDEBUGMSGT("VKEY_NEXT pressed, please report the key pressed to the programmer"); return Qt::Key(0);
        case VKEY_END:            return Qt::Key_End;
        case VKEY_HOME:            return Qt::Key_Home;
        case VKEY_LEFT:            return Qt::Key_Left;
        case VKEY_UP:            return Qt::Key_Up;
        case VKEY_RIGHT:        return Qt::Key_Right;
        case VKEY_DOWN:            return Qt::Key_Down;
        case VKEY_PAGEUP:        return Qt::Key_PageUp;
        case VKEY_PAGEDOWN:        return Qt::Key_PageDown;
        case VKEY_SELECT:        return Qt::Key_Select;
        case VKEY_PRINT:        return Qt::Key_Print;
        case VKEY_ENTER:        return Qt::Key_Enter; // Keypad Enter
        case VKEY_SNAPSHOT:        VTDEBUGMSGT("VKEY_SNAPSHOT pressed, please report the key pressed to the programmer"); return Qt::Key(0);
        case VKEY_INSERT:        return Qt::Key_Insert;
        case VKEY_DELETE:        return Qt::Key_Delete;
        case VKEY_HELP:            return Qt::Key_Help;
        case VKEY_NUMPAD0:        return Qt::Key_0;
        case VKEY_NUMPAD1:        return Qt::Key_1;
        case VKEY_NUMPAD2:        return Qt::Key_2;
        case VKEY_NUMPAD3:        return Qt::Key_3;
        case VKEY_NUMPAD4:        return Qt::Key_4;
        case VKEY_NUMPAD5:        return Qt::Key_5;
        case VKEY_NUMPAD6:        return Qt::Key_6;
        case VKEY_NUMPAD7:        return Qt::Key_7;
        case VKEY_NUMPAD8:        return Qt::Key_8;
        case VKEY_NUMPAD9:        return Qt::Key_9;
        case VKEY_MULTIPLY:        return Qt::Key_multiply;
        case VKEY_ADD:            return Qt::Key_Plus;
        case VKEY_SEPARATOR:    VTDEBUGMSGT("VKEY_SEPARATOR pressed, please report the key pressed to the programmer"); return Qt::Key(0);
        case VKEY_SUBTRACT:        return Qt::Key_Minus;
        case VKEY_DECIMAL:        return Qt::Key_Period;
        case VKEY_DIVIDE:        return Qt::Key_division;
        case VKEY_F1:            return Qt::Key_F1;
        case VKEY_F2:            return Qt::Key_F2;
        case VKEY_F3:            return Qt::Key_F3;
        case VKEY_F4:            return Qt::Key_F4;
        case VKEY_F5:            return Qt::Key_F5;
        case VKEY_F6:            return Qt::Key_F6;
        case VKEY_F7:            return Qt::Key_F7;
        case VKEY_F8:            return Qt::Key_F8;
        case VKEY_F9:            return Qt::Key_F9;
        case VKEY_F10:            return Qt::Key_F10;
        case VKEY_F11:            return Qt::Key_F11;
        case VKEY_F12:            return Qt::Key_F12;
        case VKEY_NUMLOCK:        return Qt::Key_NumLock;
        case VKEY_SCROLL:        return Qt::Key_ScrollLock;
        case VKEY_SHIFT:        return Qt::Key_Shift;
        case VKEY_CONTROL:        return Qt::Key_Control;
        case VKEY_ALT:            return Qt::Key_Alt;
        case VKEY_EQUALS:        return Qt::Key_Equal;            // VKEY_EQUALS == 57

        // These keys are not defined in VST SDK headers
        case 59:                return Qt::Key_Play;
        case 60:                return Qt::Key_Stop;
        case 61:                return Qt::Key_Back;
        case 62:                return Qt::Key_Forward;
        case 63:                return Qt::Key_VolumeUp;
        case 64:                return Qt::Key_VolumeDown;

    }
    // Unknown keys are reported to user by VSTKey(), not here.
    return Qt::Key(0);
}

VstInt32 CQEffectEditor::ApplyShift( VstInt32 key )
{
    if( key<33 || key>127 )
        return key;
    if( isalpha(key) )
        return toupper(key);
    switch( key ){
        case '`': return '~';
        case '1': return '!';
        case '2': return '@';
        case '3': return '#';
        case '4': return '$';
        case '5': return '%';
        case '6': return '^';
        case '7': return '&';
        case '8': return '*';
        case '9': return '(';
        case '0': return ')';
        case '-': return '_';
        case '=': return '+';
        case '[': return '{';
        case ']': return '}';
        case ';': return ':';
        case '\'': return '"';
        case '\\': return '|';
        case '/': return '?';
    }
    return key;
}

