//-----------------------------------------------------------------------------
//
// Qt+VST
//
// Qt Plugin App
//
// This DLL provides shared QApplication instance for all Win32 plugins
// loaded under one host program and using the same version/build
// of Qt run-time libraries.
// See the .h file for more comments.
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <qapplication.h>
#include <qt_windows.h>
#include <qmessagebox.h>
#include <QPlastiqueStyle>
#include "qpluginapp.h"

#define QPLUGNIAPP_NAME                "Qt Plugin Proxy Application"
#define QPLUGINAPP_VERSIONNUMBER    "1.0.1"

const char* WINAPI QPluginAppVersion()
{
    return QPLUGNIAPP_NAME " v. " QPLUGINAPP_VERSIONNUMBER ", built " __DATE__;
}

const char* WINAPI QPluginAppCopyright()
{
    return
        "Copyright(C) Sergey A. Galin, 2009. http://sageshome.net/\r\n"
        "Licensed under the terms of GNU LGPL license v. 2.1.\r\n"
        "http://www.opensource.org/licenses/lgpl-2.1.php\r\n";
}

const char* WINAPI QPluginAppName()
{
    return QPLUGNIAPP_NAME;
}

const char* WINAPI QPluginAppVersionNumber()
{
    return QPLUGINAPP_VERSIONNUMBER;
}

//
// An instance of QApplication used in the plugin.
//
class QPluginApplication: public QApplication
{
public:
    QPluginApplication();
    virtual ~QPluginApplication();
private:
    int m_ArgcStub;
    static HHOOK m_Hook; // Used by WinEventHook(), so must be static as well.
    static LRESULT CALLBACK WinEventHook(int nCode, WPARAM wParam, LPARAM lParam);
};

HHOOK QPluginApplication::m_Hook = NULL;
#if defined(QWINPLUGINAPPLICATION_MENUKEYSUPPORT)
    bool QPluginApplication::m_winEventFilterRecursion = false;
#endif

static QPluginApplication g_PluginApplication;

QPluginApplication::QPluginApplication():
    m_ArgcStub( 0 ),
    QApplication( m_ArgcStub, NULL )
{
    if( m_Hook ){
        QMessageBox::critical( NULL, "Fatal QPluginApplication Error",
            "Second instance of VST Plugin Application created." );
        throw;
    }
    // Installing a hook into Windows message queue
    m_Hook = SetWindowsHookEx(
        WH_GETMESSAGE,            // Catching messages posted to message queue
        WinEventHook,            // The hook procedure address
        NULL,                    // Module containing the hook procedure, NULL => this module
        GetCurrentThreadId() ); // Thread Id (NULL => all threads on the same desktop)

    // Default application style. Should not appear except for
    // dialogs invoked without parent.
    setStyle( new QPlastiqueStyle );

    // Setting up informational properties
    setApplicationName( QPluginAppName() );
    setApplicationVersion( QPluginAppVersionNumber() );
}

QPluginApplication::~QPluginApplication()
{
    // Removing the hook from Windows message queue
    if( m_Hook != 0 ){
        UnhookWindowsHookEx( m_Hook );
        m_Hook = 0;
    }
}

LRESULT CALLBACK QPluginApplication::WinEventHook( int nCode, WPARAM wParam, LPARAM lParam )
{
    // Post Windows events to QApplication
    if( instance() != NULL )
        instance()->sendPostedEvents(0, -1);
    // Pass the events to next hook in the chain
    return CallNextHookEx( m_Hook, nCode, wParam, lParam );
}


// !!!!!?????? Clean this out?

void* hQPluginAppInstance = NULL;

BOOL WINAPI DllMain( HINSTANCE hInst, DWORD dwReason, LPVOID lpvReserved )
{
    hQPluginAppInstance = hInst;
    return 1;
}

