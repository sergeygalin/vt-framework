//-----------------------------------------------------------------------------
//
// Qt+VST
//
// Qt Plugin App
//
// This DLL provides shared QApplication instance for all Win32 plugins
// loaded under one host program and using the same version/build
// of Qt run-time libraries.
// TO DO:
// - Mac OS implementation
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef QPLUGINAPP_H
#define QPLUGINAPP_H

#include <qt_windows.h>

#ifdef QTAPPPROXY_LIB
    #define QTAPPPROXY_EXPORT __declspec(dllexport)
#else
    #define QTAPPPROXY_EXPORT __declspec(dllimport)
#endif

//
// Module information functions
//
extern "C" QTAPPPROXY_EXPORT const char* WINAPI QPluginAppVersion(); // Version as a text string
extern "C" QTAPPPROXY_EXPORT const char* WINAPI QPluginAppCopyright(); // Copyright
extern "C" QTAPPPROXY_EXPORT const char* WINAPI QPluginAppName(); // Name of the module
extern "C" QTAPPPROXY_EXPORT const char* WINAPI QPluginAppVersionNumber(); // Version number only

#endif
