// ---------------------------------------------------------------------------
//
// Qt+VST
// A base class for Qt widget used for VST (probably some others) plugin GUI,
// and system-specific instance of plugin application.
//
// ---------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef VT_UNICODE
    #undef VT_UNICODE
#endif

#if defined(_MSC_VER)
    #if defined(DEBUG) || defined(_DEBUG)
        #pragma comment(lib, "QPluginApp10D.lib")
    #else
        #pragma comment(lib, "QPluginApp10.lib")
    #endif
#endif

#include <qt_windows.h>
#include <qevent.h>
#include <qmessagebox.h>
#include "qsyspluginwidget.h"
#include "qeffecteditor.hpp"
#include "QPluginApp10/qpluginapp.h"


QSysPluginWidget::QSysPluginWidget( HWND parentWnd, CQEffectEditor* owner ):
    m_hParentWnd( parentWnd ),
    m_Owner( owner ),
    QWidget( NULL, 0 ) // Parameters: parent widget pointer and Qt::WFlags
{
    // This is just to prevent linker from removing dependency from QPluginApp.
    QPluginAppVersion();

    // Placing it to the left top corner
    move( 0, 0 );
    // We don't allow focus on the sys window (only its child can get focus).
    setFocusPolicy( Qt::NoFocus );
    // Setting parent wnd
    setParentWnd( m_hParentWnd );
}

void QSysPluginWidget::setParentWnd( HWND parentWnd )
{
    m_hParentWnd = parentWnd;
    // Now WinAPI shaman dance with a big tabmourine!!
    // Making parent widget's window style "child", so it can be inserted
    // into another window.
    SetWindowLong(
        winId(),            // Get Qt widget's hWnd
        GWL_STYLE,            // Setting window style
        WS_CHILD |            // It's a child window
        WS_CLIPCHILDREN |    // Excludes the area occupied by child windows
                            // when drawing occurs within the parent window.
                            // This style is used when creating the parent
                            // window. ((C) MSDN)
        WS_CLIPSIBLINGS        // Clips child windows relative to each other.
    );
    // System window adoption
    SetParent( winId(), m_hParentWnd );
    // Sending "ActiveX embedding" message, without that it won't work.
    QEvent evnt( QEvent::EmbeddingControl );
    QApplication::sendEvent( this, &evnt );
}

QSysPluginWidget::~QSysPluginWidget()
{
    // Nothing special so far...
}

void QSysPluginWidget::show()
{
    move( 0, 0 ); // Placing it to the left top corner.
    QWidget::show();
}

VT::VTMB& QSysPluginWidget::MB()
{
    if( m_Owner!=NULL )
        return m_Owner->MB();
    return *(VT::VTMB::GetDefaultMB());
}

void QSysPluginWidget::setOwner( CQEffectEditor* newOwner )
{
    m_Owner = newOwner;
    emit ownerChanged();
}

