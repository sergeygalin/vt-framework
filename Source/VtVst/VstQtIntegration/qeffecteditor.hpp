//-----------------------------------------------------------------------------
//
// Qt+VST
//
//-----------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QEFFECTEDITOR_HPP
#define QEFFECTEDITOR_HPP

#include <QIcon>
#include "QSysPluginWidget"
#include "aeffeditor.h"
#include "vt_messageboxes.hpp"
#include "vt_threading.hpp"

//
// A message box engine which should be used by VST plugin.
//
// The engine is held in effect editor object and therefore
// must have the same lifetime as the corresponding effect
// instance, so all messages can be safely relayed here.
//
// Also each instance of plugin has its own instance of the
// message box system.
//
class QVST_VTMB: public VT::VTMB
{
public:
    QVST_VTMB():
      m_Parent( NULL )
    {}
    // parentClosed() is already defined in VT::VTMB.
    virtual QWidget* GetParentWidget() const { return m_Parent; } // Overriding default version
    void setParent( QWidget* par ){ m_Parent = par;    }
protected:
    QWidget* m_Parent;
};


//
// Qt GUI editor - bridges AEffEditor with QSysPluginWidget.
// Use CQEffectEditorT to create version of the class for
// a specific widget.
//
class CQEffectEditor: public AEffEditor
{
public:
    CQEffectEditor( AudioEffectX *effect = NULL );
    virtual ~CQEffectEditor();

    // Warning: this function must be called right after constructor.
    // It should not be used to change pointer from one effect to another.
    virtual void setEffect( AudioEffectX *effect );

    //
    // Overriding virtual functions of AEffEditor
    //
    virtual bool open(void* ptr);
    virtual void close();
    virtual bool getRect(ERect** rect);
    virtual bool isOpen();

    // VST functions
    virtual void idle();
    virtual bool onKeyDown( VstKeyCode& keyCode );
    virtual bool onKeyUp( VstKeyCode& keyCode );
    virtual bool onWheel( float distance );
    virtual bool setKnobMode( VstInt32 val );

    // These functions return NULL when GUI is closed.
    QSysPluginWidget* GetBaseWidget(){ return m_SysWidget; }
    QWidget* GetGUIWidget(){ return m_GUIWidget; }

    //
    // Widget style functions
    //
    // Set widget style by name.
    // Supported names are: "windows", "windowsvista", "vista",
    // "windowsxp", "xp", "motif", "cde", "plastique", "cleanlooks"
    // (the names are not case-sensitive).
    // Note that only "plastique" and "cleanlooks" are Qt-only and
    // don't depend on OS settings, while looking pretty good
    // at the same time.
    // "Windows" styles look depends on currently selected Windows look.
    //
    bool setPluginStyle( const QString& name );
    // Update style on system and GUI widgets
    // Most unlikely you'll never need to call this, it is
    // done automaticaly.
    void updatePluginStyle();
    // Set style on this widget and all its child widgets.
    void updateWidgetStyle( QWidget *widget = NULL );

    // The icon will be set for sys widget from there it will be
    // inserted into all message boxes and probably some other stuff.
    void setIcon( const QIcon& icon );
    void setIcon( const QPixmap& pm );
    void setIcon( const QString& fileName );

    // Access to messagebox system
    VT::VTMB& MB(){ return m_MB; }

    // Access to widget locker
    VT::CCriticalSection* GetWidgetCS(){ return &m_CS; }

protected:
    QIcon m_Icon;
    QVST_VTMB m_MB;
    ERect m_Rect; // Plugin GUI size - detected from GUI widget
    bool m_SizeDetected;
    QSysPluginWidget* m_SysWidget;
    QWidget* m_GUIWidget;
    VT::CCriticalSection m_CS;

    // This function should be overriden in descendant classes
    // to create plugin GUI widget.
    virtual QWidget* CreateGUIWidget( QWidget* parent, AudioEffectX *effect )=0;

    // Deletes system & GUI widgets
    void DeleteWidgets();

    // Detect size of plugin GUI. Creates an instance of GUI widget
    // if it doesn't exist yet, then destroys it.
    // Detected size is saved in m_Rect.
    virtual void DetectSize();

    // Qt widget style stuff
    QStyle *m_PluginStyle;
    void DeletePluginStyle();
    void beforePluginGUIClose();

private:
    // Receives keyboard events from VST interface and feeds them into Qt
    void VSTKey( bool up, const VstKeyCode& keyCode );

    // VST <-> Qt stuff converters
    static Qt::KeyboardModifiers VSTModifiers( int modifier );    // Convert VST keyboard modifiers to Qt
    static Qt::Key VSTVirtualKeys( int virt ); // Convert VST virtual ke modifiers to Qt
    static VstInt32 ApplyShift( VstInt32 key ); // Gets "unshifted" key value and returns meaning of the key with SHIFT (US keyboard only!!!)
};

//
// A handy template which generates CQEffectEditor for specific
// QSysPluginWidget descendant.
//
template<class QPLUGNWIDGETCLASS> class CQEffectEditorT: public CQEffectEditor
{
public:

    CQEffectEditorT<QPLUGNWIDGETCLASS>( AudioEffectX *effect = NULL ):
      CQEffectEditor(effect)
      {}

    CQEffectEditorT<QPLUGNWIDGETCLASS>( AudioEffectX *effect, const QString& style):
      CQEffectEditor(effect)
    {
          setPluginStyle( style );
    }

    //virtual ~CQEffectEditorT<QPLUGNWIDGETCLASS>(){}
    QPLUGNWIDGETCLASS* GetWidget(){ return (QPLUGNWIDGETCLASS*)m_SysWidget; }
protected:
    virtual QWidget* CreateGUIWidget( QWidget* parent, AudioEffectX *effect ){
        return new QPLUGNWIDGETCLASS( parent, effect ); }
};



#endif
