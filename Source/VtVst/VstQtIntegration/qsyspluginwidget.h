// ---------------------------------------------------------------------------
//
// Qt+VST
// A base class for Qt widget used for plugin GUI.
//
// ---------------------------------------------------------------------------

/*
    BSD License

    Copyright (C) 2008-2010 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef QSYSPLUGINWIDGET_H
#define QSYSPLUGINWIDGET_H

#include <qapplication.h>
#include <qwidget.h>
#include <qt_windows.h>
#include "vt_messageboxes.hpp"

class CQEffectEditor;

//
// QSysPluginWidget - a widget which lays between system hWnd and
// plugin GUI's main widget (so GUI's main widget may know nothing
// about the underlying stuff).
//
// NOTE: Don't forget to update descendant's constructor ;-)
//
class QSysPluginWidget: public QWidget
{
    Q_OBJECT
public:
    QSysPluginWidget( HWND parentWnd, CQEffectEditor* owner );
    virtual ~QSysPluginWidget();
    void show();
    void setParentWnd( HWND parentWnd );
    HWND getParentWnd() const { return m_hParentWnd; }
    virtual void setOwner( CQEffectEditor* newOwner );
    virtual CQEffectEditor* getOwner(){ return m_Owner; }
    VT::VTMB& MB();

signals:
    void ownerChanged();

protected:
    HWND m_hParentWnd; // Keeping a copy of parent's hWnd, just in case
    CQEffectEditor* m_Owner;
};




#endif
