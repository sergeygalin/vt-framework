//-----------------------------------------------------------------------------
//
// Parameter classes, for interfacing program variables and GUI controls.
// The parameters were originally introduced to ease writing of VST plugins,
// hence the limitations they have to maintain (e.g. "float" values,
// short names...)
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include "vt_parameters.hpp"
#include "vt_string.hpp"

using namespace VT;
using namespace Params;

CParameter::CParameter(CParameterValidator *ownr, const VTCHAR *nme, const VTCHAR *lbl,
                               float min, float max, float def, bool logH, bool gridH):
    VT::CAssociatedValue<float>( def ),
    paramPos(-1),
    m_Tag(0),
    showPlusSign(false),
    logHandler(logH),
    gridHandler(gridH),
    m_bCanBeAutomated( true ),
    m_bAllowReset( true ),
    m_uRangeChange( 0 ),
    logMod(Math::kDefaultLogMod),
    gridSteps(100.0f),
    iGridSteps(100),
    handleLookupTable(NULL),
    owner(ownr),
    fMin(min),
    fMax(max),
    //fValue(def),
    fDefault(def)
    #if defined(VTEP_FAKE01)
        , bReturnFakeValue(true)
    #endif
{
    VTASSERT2( nme!=NULL, VTT("NULL parameter name!") );
    VTASSERT2( lbl!=NULL, nme );
    VTASSERT2( min<=max, VT::CString(nme)<<VTT(": min=")<<min<<VTT(", max=")<<max );
    VTASSERT2( def>=min, VT::CString(nme)<<VTT(": min=")<<min<<VTT(", def=")<<def );
    VTASSERT2( def<=max, VT::CString(nme)<<VTT(": max=")<<max<<VTT(", def=")<<def );

    SafeStrNCopy(name, nme, sizeof(name)-1);
    SafeStrNCopy(label, lbl, sizeof(label)-1);

    SafeStrNCopy(longName, nme, sizeof(longName)-1);
    SafeStrNCopy(codeName, nme, sizeof(codeName)-1);

    #if defined(VTEP_FAKE01)
        fFake01Value = as01(def); // Not just as01(), because it might return fFake01Value
    #endif
}

CParameter::~CParameter()
{
    deleteHandleLookupTable();
}

void CParameter::setShortName( LPCVTCHAR newName )
{
    VTASSERT( newName!=NULL );
    VTASSERT( VTSTRLEN( newName )<sizeof(name) );
    SafeStrNCopy(name, newName, sizeof(name)-1);
}

void CParameter::setLongName( LPCVTCHAR newName )
{
    VTASSERT( newName!=NULL );
    VTASSERT( VTSTRLEN( newName )<sizeof(longName) );
    SafeStrNCopy(longName, newName, sizeof(longName)-1);
}

void CParameter::setCodeName( LPCVTCHAR newName )
{
    VTASSERT( newName!=NULL );
    VTASSERT( VTSTRLEN( newName )<sizeof(codeName) );
    SafeStrNCopy(codeName, newName, sizeof(codeName)-1);
}

float CParameter::from01(float x) const
{
    if(handleLookupTable!=NULL){

        // Use lookup table
        #if defined(VTEP_USECRITICALSECTION)
            if(owner!=NULL)
                owner->effectCriticalSection.enter();
        #endif
        float ret = (handleLookupTable!=NULL)?
            handleLookupTable[ Math::Clip(Math::URound(gridSteps*x), SDWORD(0), iGridSteps) ]:
            from01_NLTG(x); // Let's just ignore the grid stuff in this case
        #if defined(VTEP_USECRITICALSECTION)
            if(owner!=NULL)
                owner->effectCriticalSection.leave();
        #endif
        return ret;
    }else{

        // Just calculate
        if(gridHandler)
            x = Math::GridRound01(x, gridSteps);
        return from01_NLTG(x);
    }
}

void CParameter::setValue(float x)
{
    setValueNE(x);
    if( owner!=NULL )
        owner->VTValidateParams(this);
}

#if defined(VTEP_FAKE01)
void CParameter::setValueNF01(float x)
{
    setValueNENF01(x);
    if( owner!=NULL )
        owner->VTValidateParams(this);
}
#endif

void CParameter::adjustLogHandler(float x, float y, double prec, double smin, double smax)
{
    float as_01 = as01_raw(x);
    logMod = Math::CalculateLogMod(as_01, y, prec, smin, smax);

    #if defined(_DEBUG)
        if( logHandler ){
            float new_as_01 = as01( x );
            VTASSERT2( Math::Abs( new_as_01 - y )<=prec, name );
        }
    #endif
}


void CParameter::createHandleLookupTable()
{
    deleteHandleLookupTable();
    handleLookupTable = new float[ iGridSteps+1 ];
    // Avoiding rounding errors at ends of the range -
    // setting their values directly from fMin and fMax.
    handleLookupTable[0] = fMin;
    for(SDWORD step=1; step<iGridSteps; step++)
        handleLookupTable[ step ] = from01_NLTG( float(step)/gridSteps );
    handleLookupTable[iGridSteps] = fMax;
}

void CParameter::copyHandleSettings(const CParameter *srcParam)
{
    deleteHandleLookupTable();
    logHandler = srcParam->logHandler;
    gridHandler = srcParam->gridHandler;
    logMod = srcParam->logMod;
    gridSteps = srcParam->gridSteps;
    iGridSteps = srcParam->iGridSteps;
    #if defined(VTEP_FAKE01)
        bReturnFakeValue = srcParam->bReturnFakeValue;
    #endif
    if( srcParam->handleLookupTable!=NULL ){
        handleLookupTable = new float[ iGridSteps+1 ];
        memcpy( handleLookupTable, srcParam->handleLookupTable,
            (iGridSteps+1)*sizeof(float) );
    }
}

void CParameter::deleteHandleLookupTable()
{
    if( handleLookupTable!=NULL ){
        #if defined(VTEP_USECRITICALSECTION)
            if(owner!=NULL)
                owner->effectCriticalSection.enter();
        #endif
        delete handleLookupTable;
        handleLookupTable = NULL;
        #if defined(VTEP_USECRITICALSECTION)
            if(owner!=NULL)
                owner->effectCriticalSection.leave();
        #endif
    }
}

void CParameter::getAsTextForVST( VTCHAR *out ) const
{
    #if defined(VTEP_FAKE01)
        if(bReturnFakeValue)
            getAsText( from01_NLTG(fFake01Value), out );
        else
            getAsText( out );
    #else
        getAsText( out );
    #endif
    VTASSERT( VTSTRLEN( out ) );
}

size_t CParameter::GetNValues()
{
    VTTHROWT("Attempt to get number of values from a parameter which doesn't support that.");
    #if defined(__BORLANDC__)
        #pragma warn -8066
    #endif
    return 0;
    #if defined(__BORLANDC__)
        #pragma warn .8066
    #endif
}

float CParameter::GetValueByIdx( size_t )
{
    VTTHROWT("Attempt to value-by-index from a parameter which doesn't support that.");
    #if defined(__BORLANDC__)
        #pragma warn -8066
    #endif
    return 0;
    #if defined(__BORLANDC__)
        #pragma warn .8066
    #endif
}

size_t CParameter::GetIdxOfValue( float )
{
    VTTHROWT("Attempt to index-of-value from a parameter which doesn't support that.");
    #if defined(__BORLANDC__)
        #pragma warn -8066
    #endif
    return 0;
    #if defined(__BORLANDC__)
        #pragma warn .8066
    #endif
}

void CParameter::visibleText2Visible(VTCHAR *string, float &output) const
{
    output = getValue();
    if( string==NULL ){
        VTDEBUGMSGT( "NULL string received" );
        return;
    }
    if( !VTSTRLEN(string) ){
        VTDEBUGMSGT( "Emptu string received" );
        return;
    }
    float scan;
    if( VTSSCANF(string, VTT("%f"), &scan)==1 )
        output = scan;
}

void CParameter::setMin(float m)
{
    fMin=m;
    if( getValue()<fMin )
        setValueNE(fMin);
    RangeChanged();
}

void CParameter::setMax(float m)
{
    fMax=m;
    if( getValue()>fMax )
        setValueNE(fMax);
    RangeChanged();
}

void CParameter::setFromSaveValue( float value, bool notifyOwner )
{
    if( notifyOwner )
        setValue( value );
    else
        setValueNE( value );
}







void CParameter_dryWet::getAsText( float x, VTCHAR *out, size_t maxLen ) const
{
    VTSNPRINTF( out, maxLen, VTT("%.0f/%.0f"), dryPercent(x), wetPercent(x) );
}







CParameter_combo::CParameter_combo(CParameterValidator *ownr, const VTCHAR *nme,
        const VTCHAR *const *values, int def):
    CParameter_int(ownr, nme, VTT(""), 0,
        def, // temp max value
        def ),
    labels( NULL )
{
    SetNewValues( values, false );
}

CParameter_combo::CParameter_combo( CParameterValidator *ownr, const VTCHAR *nme,
        const VT::CStringArray& strArray, int def ):
    CParameter_int(ownr, nme, VTT(""), 0,
        def, // temp max value
        def ),
    labels( NULL )
{
    SetNewValues( strArray, false );
}


void CParameter_combo::SetNewValues( const VTCHAR *const *values, bool notifyIfValueChanged )
{
    float value = getValue();
    // Delete old labels (if necessary)
    DeleteLabels();
    if( values!=NULL ){
        // Count new labels
        int count=0;
        const VTCHAR *const *ptr=values;
        while(*ptr){
            count++;
            ptr++;
        }
        // Allocate memory for pointer array
        labels = new LPVTCHAR[ count+1 ];
        if( labels==NULL )
            throw CException(VTT("Memory allocation error."));
        // Create copies of values
        ptr=values;
        size_t len;
        for(int i=0; i<count; i++){
            len = VTSTRLEN( *ptr );
            labels[i] = new VTCHAR[ len+1 ];
            if( labels[i]==NULL )
                throw CException(VTT("Memory allocation error."));

            SafeStrNCopy( labels[i], *ptr, len );
            VTASSERT( VTSTRLEN(labels[i])>0 );
            //SafeStrNCopy( labels[i], *ptr, len );
            //labels[i][len] = 0; // Stupid MSC?!
            ptr++;
        }
        // Last label pointer is a NULL-terminator
        labels[ count ] = NULL;
        // Updating range
        setMin( 0.0f );
        setMax( (count>0.0f)? float(count-1): 0.0f );

    }
    if( Math::SRound(value) > Math::SRound( getMax() ) ){
        if( notifyIfValueChanged )
            setValue( value ); // This
        else
            setValueNE( value );
    }
    // Flagging the update
    RangeChanged(); // Don't trust setMax() on that ;-)
}

void CParameter_combo::SetNewValues( const VT::CStringArray& strArray, bool notifyIfValueChanged )
{
    VT::CArray<LPCVTCHAR> ptrs( strArray.getSize()+1 );
    for( size_t i = 0; i < strArray.getSize(); i++ ){
        VTASSERT( !strArray[i].IsBlank() );
        ptrs[i] = strArray[i].Str();
        VTASSERT( ptrs[i] != NULL );
        VTASSERT( VTSTRLEN(ptrs[i])>0 );
    }
    ptrs.last() = NULL;
    SetNewValues( ptrs.getRODataPtr(), notifyIfValueChanged );
}

CParameter_combo::~CParameter_combo()
{
    DeleteLabels();
}

void CParameter_combo::DeleteLabels()
{
    if( labels!=NULL ){
        for( VTCHAR **ptr=labels; *ptr; ptr++ )
            FreeAndNull( *ptr );
        FreeAndNull( labels );
    }
}

void CParameter_combo::visibleText2Visible( VTCHAR *string, float &output ) const
{
    for(int i=0; labels[i]; i++)
        if( !VTSTRCMP( labels[i], string ) ){
            output = float(i);
            return;
        }
}

void CParameter_combo::getAsText( float x, VTCHAR *out, size_t maxLen ) const
{
    const LPCVTCHAR kStub = VTT("[---]");  // Will be used if label is not found
    LPCVTCHAR src = kStub;
    if( labels!=NULL ){
        size_t idx = Math::URound(validate(x));
        src = labels[idx];
    }
    if( src==NULL ){
        VTDEBUGMSGT("Null label in CParameter_combo!");
        src = kStub;
    }
    size_t len = Math::Min( maxLen, VTSTRLEN(src) );
    VTASSERT( len > 0 );
    memcpy( out, src, len*sizeof(VTCHAR) );
    out[len]=0;
}









CParameter_strval_combo::CParameter_strval_combo(
    CParameterValidator *ownr,
    const VTCHAR *nme, const VTCHAR *lbl,
    const VTCHAR *const *values,
    const float *realValues,
    float def ):

    CParameter( ownr, nme, lbl,
        Math::Min(0.0f, def), // temp min value
        def, // temp max value
        def, // default value
        false, // logH
        false  // gridH
    )
{
    m_uEntries = 0;
    const VTCHAR *const *ptr=values;
    // Counting values
    while(*ptr){
        m_uEntries++;
        ptr++;
    }
    if( m_uEntries==0 ){
        VTDEBUGMSGT("No entries!");
        throw CException(VTT("No entries."));
    }

    // Allocating memory
    m_pEntries = new TEntry[ m_uEntries ];
    if( m_pEntries==NULL )
        throw CException(VTT("Memory allocation error."));

    // Copying labels & parsing values
    for( size_t i=0; i<m_uEntries; i++ ){
        SafeStrNCopy( m_pEntries[i].m_Label, values[i], kMaxLabel );
        if( realValues==NULL ){
            // Parsing from string
            m_pEntries[i].m_fValue = 0.0f;
            // int scanfresult =
            VTSSCANF( values[i], VTT("%f"), &m_pEntries[i].m_fValue );
            // VTASSERT2T( scanfresult==1, values[i] );
        }else
            m_pEntries[i].m_fValue = realValues[i];
    }

    // Setting min and max values
    float min = m_pEntries[0].m_fValue, max = m_pEntries[0].m_fValue;
    // size_t minidx = 0, maxidx = 0;
    for( size_t i=1; i<m_uEntries; i++ ){
        if( m_pEntries[i].m_fValue<min ){
            min = m_pEntries[i].m_fValue;
            // minidx = i;
        }
        if( m_pEntries[i].m_fValue>max ){
            max = m_pEntries[i].m_fValue;
            // maxidx = i;
        }
    }
    setMin( min );
    setMax( max );
}


CParameter_strval_combo::~CParameter_strval_combo()
{
    delete[] m_pEntries;
}

void CParameter_strval_combo::getAsText(float x, VTCHAR *out, size_t maxLen) const
{
    SafeStrNCopy(out, m_pEntries[ FindClosestEntry(x) ].m_Label, maxLen);
}

void CParameter_strval_combo::textFromVisible(float x, VTCHAR *out, size_t maxLen) const
{
    SafeStrNCopy( out, m_pEntries[ FindClosestEntry(x) ].m_Label, maxLen );
}

float CParameter_strval_combo::getVisibleValue(float x) const
{
    return m_pEntries[ FindClosestEntry( x ) ].m_fValue;
}

float CParameter_strval_combo::fromVisibleValue(float x) const
{
    return m_pEntries[ FindClosestEntry(x) ].m_fValue;
}


// Find an entry which has a value closest to 'search'.
size_t CParameter_strval_combo::FindClosestEntry( float search ) const
{
    if( m_uEntries==0 || m_pEntries==NULL ){
        VTDEBUGMSGT("Combobox: no entries!");
        throw CException(VTT("Combobox: no entries!"));
    }
    size_t closestMatch = 0;
    float closestDiff = Math::Abs( m_pEntries[0].m_fValue - search );
    for( size_t idx=1; idx<m_uEntries; idx++ ){
        float diff = Math::Abs( m_pEntries[idx].m_fValue - search );
        if( diff<closestDiff ){
            closestDiff = diff;
            closestMatch = idx;
        }
    }
    return closestMatch;
}

CParameter_strval_combo::TEntry::TEntry():
    m_fValue(0.0f)
{
    m_Label[0]=0;
}





CParameter_dB::CParameter_dB( CParameterValidator *ownr, const VTCHAR *nme, float def, bool logH ):
    CParameter( ownr, nme, LCT("dB"),
        0.0f, 1.0f, def, // min, max, default
        logH ),
    max_dB(0),
    min_dB(float(Math::kMin_dB))
{
}

CParameter_dB::CParameter_dB( CParameterValidator *ownr, const VTCHAR *nme, float def, float maxdB, bool logH ):
    CParameter( ownr, nme, LCT("dB"),
        0.0f, float(Math::Convert_dBTo01_NoClip(maxdB)), def, // min, max, default
        logH ),
    max_dB(maxdB),
    min_dB(float(Math::kMin_dB))
{
}

CParameter_dB::CParameter_dB( CParameterValidator *ownr, const VTCHAR *nme, float def, float mindB, float maxdB, bool logH ):
    CParameter( ownr, nme, LCT("dB"), float(Math::Convert_dBTo01_NoClip(mindB) ),
    float(Math::Convert_dBTo01_NoClip(maxdB)), def, logH),
    max_dB(maxdB),
    min_dB(mindB)
{
}













// min, max, def are filter orders
CParameter_filterOrder::CParameter_filterOrder(CParameterValidator *ownr, const VTCHAR *nme,
    SDWORD min, SDWORD max, SDWORD def, CParameter_filterOrder::TOrderSet set ):
    CParameter_int(ownr, nme, LCT("dB/Oct"), min, max, def),
    m_Set( set )
{
    if( min<1 )
        VTTHROWT( "Minimal filter order must be 1 or greater." );
}

float CParameter_filterOrder::getVisibleValue(float x) const
{
    return float(Math::URound(CParameter_int::getVisibleValue(x)*6.0f));
}

float CParameter_filterOrder::fromVisibleValue(float x) const
{
    return CParameter_int::fromVisibleValue(x/6.0f);
}

void CParameter_filterOrder::setValue( float x )
{
    CParameter_int::setValue( fixFilterOrder(x) );
}

void CParameter_filterOrder::setValueNE(float x)
{
    CParameter_int::setValueNE( fixFilterOrder(x) );
}

float CParameter_filterOrder::fixFilterOrder( float x )
{
    x = Math::Max( x, 1.0f );
    if( m_Set == kAny )
        return x;
    SDWORD ix = Math::URound(x);
    if( m_Set == k1Evens && ix==1 )
        return x;
    if( m_Set == k123Evens && ix>=1 && ix<=3 )
        return x;
    if( ix & 1 )
        return Math::Clip<float>( x+1.0f, float(Math::URound(getMin())), float(Math::URound(getMax())) );
    return x;
}


