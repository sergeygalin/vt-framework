//-----------------------------------------------------------------------------
//
// Audio Stretching Interface - DIRAC implementation
//
// Programmer: Sergey A. Galin
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2009 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include "vt_audiostretching_dirac.hpp"

using namespace VT;
using namespace VT::AudioEngine;

#if defined(USE_DIRAC)

const long CDIRACInterface::kDiracBlockSizeFrames = 512; // DIRAC is freakin' slow, so it must be small

CDIRACInterface::CDIRACInterface():
    m_pDIRAC( NULL ),
    m_pIn( NULL ),
    m_pOut( NULL ),
    m_pTempFile1( NULL ),
    m_pTempFile2( NULL ),
    m_bDeleteTempFiles( false ),
    m_bDirectInput( false ),
    m_bDirectOutput( false ),
    m_uMaxTotalMemoryFileSize( kDefaultMaxMemoryFileSize ),
    m_iCurrentChannel( 0 ),
    m_bDIRACInstancePerChannel( false ),
    m_DIRACLambda( kDiracLambda3 ),
    m_DIRACQuality( kDiracQualityGood ), // kDiracQualityBest
    m_bReadCallbackError( false ),
    m_iSamplesFedToDIRAC( 0 ),
    m_iChannelBufferPos( -1 ),
    m_iChannelBufferSize( 0 ),
    // m_fTimeFactor( 1.0 ),
    // m_fPitchFactor( 1.0 ),
    m_fDIRACRate( 44100.0f ),
    m_iDIRACChannels( 1 ),
    m_DitheringMethod( kDitheringShaping ),
    m_BitMode( kForce32 ),
    m_iResamplerQuality( kQualityHigh ),
    m_fFadeInLengthMs( 10.0 ),
    m_fFadeOutLengthMs( 10.0 )
{
    for( size_t i=0; i<kMaxAudioChannels; i++ )
        m_pDIRACMono[i] = NULL;
}

CDIRACInterface::~CDIRACInterface()
{
    CloseProcess();
}

bool CDIRACInterface::PrepareProcess(
        CInputAudioStream *in, COutputAudioStream *out,
        CFile *tmpFile1, CFile *tmpFile2,
        bool deleteTempFiles )
{
    PDBeginOperation(LCT("Initializing..."));
    CloseProcess();

    m_pTempFile1 = tmpFile1;
    m_pTempFile2 = tmpFile2;
    m_pIn = in;
    m_pOut = out;



    //
    // Detecting DIRAC parameters: sample rate and channel mode
    //
    m_fDIRACRate = float(m_pOut->GetSampleRate());
    m_iDIRACChannels = int( Math::Min( m_pIn->GetNChannels(), m_pOut->GetNChannels() ) );

    #if defined(USE_DIRAC_LE)
        m_bDIRACInstancePerChannel = m_iDIRACChannels!=1;
        if( Math::Abs( m_fDIRACRate-44100.0f )>1e-6f && Math::Abs( m_fDIRACRate-48000.0f )>1e-6f ){
            // Output sample rate is not supported by DIRAC, so there's going
            // to be at least one resmpling (on output).
            // We can choose intermediate sample rate from 44100 and 48000 Hz.
            // If input sampling rate matches one of these values, we'd better
            // choose one to avoid input resampling (at least).
            if( Math::Abs( m_pIn->GetSampleRate()-44100.0f )<1e-6f )
                m_fDIRACRate = 44100.0f;
            else
                // Input sample rate either is 48000 Hz or something unsupported by DIRAC.
                m_fDIRACRate = 48000.0f;
        }
    #elif defined(USE_DIRAC_STUDIO)
        m_bDIRACInstancePerChannel = m_iDIRACChannels>2;
        if( m_fDIRACRate<8000.0f )
            m_fDIRACRate = 8000.0f;
        else if( m_fDIRACRate>96000f )
            m_fDIRACRate = 96000.0f;
    #else /* PRO */
        m_bDIRACInstancePerChannel = false;
        if( m_fDIRACRate<8000.0f )
            m_fDIRACRate = 8000.0f; // And no upper limit ;-)
    #endif

    //
    // Create DIRAC instance(s)
    //
    if( m_bDIRACInstancePerChannel ){
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("*** DIRAC IF: MULTIPLE DIRAC INSTANCES.\n"));
        #endif
        for( int i=0; i<m_iDIRACChannels; i++ ){
            m_pDIRACMono[i] = DiracCreate( m_DIRACLambda, m_DIRACQuality,
                1, // Only one channel can be processed by this instance :-(
                m_fDIRACRate, sReadFromChannelsCallback );
            if( m_pDIRACMono[i]==NULL ){
                VTDEBUGMSGT("DIRAC instance creation failed!");
                PDEndOperation(LCT("Failed to create pitch shifter instance."));
                CloseProcess();
                return false;
            }
        }
    }else{
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("*** DIRAC IF: SINGLE MULTI-CHANNEL DIRAC INSTANCE.\n"));
        #endif
        m_pDIRAC = DiracCreate( m_DIRACLambda, m_DIRACQuality,
            m_iDIRACChannels, m_fDIRACRate, sReadFromChannelsCallback );
        if(m_pDIRAC==NULL ){
            VTDEBUGMSGT("DIRAC instance creation failed!");
            PDEndOperation(LCT("Failed to create pitch shifter instance."));
            CloseProcess();
            return false;
        }
    }

    #if defined(VT_CONSOLEDEBUG)
        {
            CString fmt;
            m_pIn->AddAudioStreamInfo( &fmt );
            VTPRINTF(VTT("*** Source stream: %s\n"), fmt.Str());
        }
    #endif

    //
    // Checking whether direct input should be used
    //
    if( int(m_pIn->GetNChannels())!=m_iDIRACChannels ||
        Math::Abs<double>( m_pIn->GetSampleRate() - m_fDIRACRate )>0.001 ||
        m_pIn->GetSampleType()!=kFP32 ){
        m_bDirectInput = false;
    }else{
        m_bDirectInput = true;
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("*** DIRAC IF: DIRAC input stream: directly from input.\n"));
        #endif
    }
    //
    // Checking whether direct output should be used
    //
    if( int(m_pOut->GetNChannels())!=m_iDIRACChannels ||
        Math::Abs<double>( m_pOut->GetSampleRate() - m_fDIRACRate)>0.001 ||
        m_pOut->GetSampleType()!=kFP32 ){
        m_bDirectOutput = false;
    }else{
        m_bDirectOutput = true;
    }

    size_t frameSize = sizeof(float) * m_iDIRACChannels;

    //
    // Set up temporary files
    //
    if( m_pTempFile1==NULL && m_pTempFile2==NULL ){
        // Estimate temporary file size
        double diracinsize = m_pIn->GetLengthSec() * m_fDIRACRate * double(frameSize);
        TAudioStreamSize
            insz  = (m_bDirectInput) ? 0: TAudioStreamSize( diracinsize + 0.5 ) + 16,
            outsz = (m_bDirectOutput)? 0: TAudioStreamSize( diracinsize * GetTimeFactor() + 0.5 ) + 16;
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF("*** DIRAC IF: Temporary in size: %ld bytes, out size = %ld bytes, max memory usage is %ld bytes.\n",
                long(insz), long(outsz), long(m_uMaxTotalMemoryFileSize) );
        #endif

        enum TFileLocation { kNone, kDisk, kMemory };
        TFileLocation f1 = (m_bDirectInput)?kNone:kDisk, f2 = (m_bDirectOutput)?kNone:kDisk;

        if( insz + outsz <= TAudioStreamSize(m_uMaxTotalMemoryFileSize) ){
            f1 = (m_bDirectInput)?kNone: kMemory;
            f2 = (m_bDirectOutput)?kNone: kMemory;
        }else{
            if( !m_bDirectInput && m_bDirectOutput && insz <= TAudioStreamSize(m_uMaxTotalMemoryFileSize) )
                f1 = kMemory;
            else if( m_bDirectInput && !m_bDirectOutput && outsz <= TAudioStreamSize(m_uMaxTotalMemoryFileSize) )
                f2 = kMemory;
        };

        switch( f1 ){
            case kMemory:
                m_pTempFile1 = new CMemoryFile(0, size_t(insz), 102400);
                VTASSERT( m_pTempFile1 != NULL );
                #if defined(VT_CONSOLEDEBUG)
                    VTPRINTF("*** DIRAC IF: Created input in-memory temporary file, %ld bytes.\n", long(insz));
                #endif
                break;
            case kDisk:
                m_pTempFile1 = CreateTempFile();
                VTASSERT( m_pTempFile1 != NULL );
                #if defined(VT_CONSOLEDEBUG)
                    VTPRINTF("*** DIRAC IF: Created input disk temporary file.\n");
                #endif
                break;
            #if defined(VT_CONSOLEDEBUG)
                default:
                    VTPRINTF("*** DIRAC IF: No input temporary file.\n");
            #endif
        };


        switch( f2 ){
            case kMemory:
                m_pTempFile2 = new CMemoryFile(0, size_t(outsz), 102400);
                VTASSERT( m_pTempFile2 != NULL );
                #if defined(VT_CONSOLEDEBUG)
                    VTPRINTF("*** DIRAC IF: Created output in-memory temporary file, %ld bytes.\n", long(insz));
                #endif
                break;
            case kDisk:
                m_pTempFile2 = CreateTempFile();
                VTASSERT( m_pTempFile2 != NULL );
                #if defined(VT_CONSOLEDEBUG)
                    VTPRINTF("*** DIRAC IF: Created output disk temporary file.\n");
                #endif
                break;
            #if defined(VT_CONSOLEDEBUG)
                default:
                    VTPRINTF("*** DIRAC IF: No output temporary file.\n");
            #endif
        };
        m_bDeleteTempFiles = true;

    }else
        m_bDeleteTempFiles = deleteTempFiles;

    if( (m_pTempFile1==NULL && !m_bDirectInput) || (m_pTempFile2==NULL && !m_bDirectOutput) ){
        VTDEBUGMSGT("NULL temporary file.");
        PDEndOperation(LCT("Temporary file was not created properly."));
        CloseProcess();
        return false;
    }
    if( !m_bDirectInput )
        if( !m_pTempFile1->IsOK() ){
            VTDEBUGMSGT("Temporary file #1 is not OK.");
            PDEndOperation(LCT("Temporary file was not created properly."));
            CloseProcess();
            return false;
        }
    if( !m_bDirectOutput )
        if( !m_pTempFile2->IsOK() ){
            VTDEBUGMSGT("Temporary file #2 is not OK.");
            PDEndOperation(LCT("Temporary file was not created properly."));
            CloseProcess();
            return false;
        }

    if( IsAborted() ){
        PDEndOperation(LCT("Aborted."));
        CloseProcess();
        return false;
    }

    // Preparing DIRAC's input data
    if( !m_bDirectInput ){
        //
        // Intermediate input file should be created. It contains
        // float samples and number of channels (interleaved format)
        // matching output. If necessary, sample rate is changed to
        // one supported by DIRAC version used.
        //
        PDBeginOperation(LCT("Preparing audio data..."));
        CAudioStreamConverter m_InConverter;
        CFileOutputAudioStream m_TempStream1;
        VTASSERT( m_pTempFile1!=NULL );
        m_TempStream1.Open( m_pTempFile1,
            false,     // Don't close file
            true,   // Raw format
            kFP32, m_fDIRACRate, m_iDIRACChannels );
        m_InConverter.Init( m_pIn, &m_TempStream1, m_iResamplerQuality, m_DitheringMethod, m_BitMode );
        // Preparing data for DIRAC...
        if( !m_InConverter.ConvertAll( GetProgressDisplay(), GetAbortFlag() ) ){
            // !!!! Without this, BCC version crashes after aborting
            // proces with "Pure virtual function called" error on return.
            // Have to investigate more on that.
            m_TempStream1.Close();
            if( IsAborted() ){
                PDEndOperation(LCT("Aborted."));
                CloseProcess();
                return false;
            }
            VTDEBUGMSGT("Input stream converter failure!");
            PDEndOperation(LCT("Input stream conversion error."));
            CloseProcess();
            return false;
        }
        if( IsAborted() ){
            PDEndOperation(LCT("Aborted."));
            CloseProcess();
            return false;
        }
        // #if defined(VT_CONSOLEDEBUG)
        //    CString fmt;
        //    m_TempStream1.AddAudioStreamInfo( &fmt );
        //    VTPRINTF(VTT("*** DIRAC IF: input stream CONVERTED TO %s.\n"), fmt.Str());
        //#endif
        if( m_pTempFile1!=NULL )
            m_pTempFile1->seek( 0 );
        m_bDirectInput = false;
    }

    #if defined(VT_CONSOLEDEBUG)
        {
            CString fmt;
            m_pOut->AddAudioFormatInfo( &fmt );
            VTPRINTF(VTT("*** Output stream: %s\n"), fmt.Str());
            if( m_bDirectOutput )
                VTPRINTF(VTT("*** DIRAC IF: DIRAC output stream is saved directly to out.\n"));
            else{
                fmt.Empty();
                m_TempStream2.AddAudioFormatInfo( &fmt );
                VTPRINTF(VTT("*** DIRAC IF: DIRAC output stream is %s - WILL BE CONVERTED AFTERWARDS.\n"), fmt.Str());
            }
        }
    #endif

    return true;
}

bool CDIRACInterface::DoProcess()
{
    if( IsAborted() ){
        PDEndOperation(LCT("Aborted."));
        CloseProcess();
        return false;
    }

    size_t frameSize = sizeof(float) * m_iDIRACChannels;

    // Calculating output file size, in samples
    TAudioStreamSize totalFramesExpected =
        TAudioStreamSize(
            m_pIn->GetLengthSec() * m_fDIRACRate * GetTimeFactor() +
            0.49999999999999999999 // Rounding
        ),
        framesExpected = totalFramesExpected;
    #if defined(VT_CONSOLEDEBUG)
        VTPRINTF(VTT("*** DIRAC IF: DIRAC output should be %ld frames length (%.2f sec X %.2f Hz X %.2f time factor).\n"),
            long(framesExpected), float(m_pIn->GetLengthSec()), float(m_fDIRACRate), float(GetTimeFactor()));
    #endif

    // Doing process
    long outFrames = 0;
    m_DiracOut.resize( kDiracBlockSizeFrames * frameSize );
    if( m_bDIRACInstancePerChannel ){
        // One-instance, mono output buffer
        m_DiracOutMono.resize( kDiracBlockSizeFrames * sizeof(float) );
        // Reset channel input positions
        for( int i=0; i<m_iDIRACChannels; i++ )
            m_iChannelPositions[i] = 0;
    }
    m_bReadCallbackError = false;
    m_iSamplesFedToDIRAC = 0;
    m_iChannelBufferPos = -1;
    m_iChannelBufferSize = 0;
    TAudioStreamSize converted = 0;
    // CString statusMessage;
    do{

        if( IsAborted() ){
            PDEndOperation(LCT("Aborted."));
            CloseProcess();
            return false;
        }

        m_DiracOut.ZeroMemoryBytes();
        if( m_bDIRACInstancePerChannel ){
            for( m_iCurrentChannel = 0; m_iCurrentChannel<size_t(m_iDIRACChannels); m_iCurrentChannel++ ){
                m_DiracOutMono.ZeroMemoryBytes();
                outFrames = DiracProcess(
                    (float*)m_DiracOutMono.getDataPtr(), // Output buffer
                    kDiracBlockSizeFrames,                 // Frames to generate
                    this,                                 // Callback data pointer
                    m_pDIRACMono[m_iCurrentChannel]);     // DIRAC instance
                // Inserting data from mono buffer into interlaced buffer
                float *outPtr = ((float*)m_DiracOut.getDataPtr()) + m_iCurrentChannel;
                const float *inPtr = (float*)m_DiracOutMono.getRODataPtr();
                for( long frame=0; frame<outFrames; frame++ ){
                    *outPtr = *(inPtr++);
                    outPtr += m_iDIRACChannels;
                }
            }
        }else // DIRAC handles all channels at once
            outFrames = DiracProcess((float*)m_DiracOut.getDataPtr(), kDiracBlockSizeFrames, this, m_pDIRAC);
        if( m_bReadCallbackError ){
            VTDEBUGMSGT("Read callback error caught!");
            PDEndOperation(LCT("Error reading pitch shifter source stream."));
            CloseProcess();
            return false;
        }

        if( IsAborted() ){
            PDEndOperation(LCT("Aborted."));
            CloseProcess();
            return false;
        }

        if( TAudioStreamSize( outFrames ) > framesExpected )
            // DIRAC returned too much. Truncate it...
            // This will also cause the processing loop to end, because
            // 'outFrames==kDiracBlockSizeFrames' will be false.
            outFrames = long( framesExpected );
        else
            // Reducing output stream size
            framesExpected -= TAudioStreamSize( outFrames );

        if( outFrames>0 ){
            if( m_bDirectOutput ){
                if( !m_pOut->Write( m_DiracOut.getRODataPtr(), outFrames ) ){
                    VTDEBUGMSGT("Error writing to output stream (direct out)!");
                    PDEndOperation(LCT("Error writing output audio data."));
                    CloseProcess();
                    return false;
                }
            }else{
                if( !m_pTempFile2->write( m_DiracOut.getRODataPtr(), outFrames * frameSize ) ){
                    VTDEBUGMSGT("Error writing to intermediate output file!");
                    PDEndOperation(LCT("Error writing intermediate audio data."));
                    CloseProcess();
                    return false;
                }
            }
        }
        converted += TAudioStreamSize( outFrames );
        if( GetProgressDisplay()!=NULL ){
            // statusMessage.CopyFrom(LCT("Pitch correcting...Output: "))<<long(converted)<<LCT(" frames.");
            float progress = float(converted)/float(totalFramesExpected)*100.0f;
            PDShowProgress( Math::Clip(progress, 0.0f, 100.0f),    LCT("Pitch correction...") );
        }
    }while( outFrames==kDiracBlockSizeFrames );

    #if defined(VT_CONSOLEDEBUG)
        VTPRINTF(VTT("*** DIRAC IF: %ld samples have been fed to DIRAC.\n"), long(m_iSamplesFedToDIRAC));
        // for( size_t i=0; i<m_pOut->GetNChannels(); i++ ){
        //    VTPRINTF(VTT("Channel #%d pos = %ld\n"), int(i), long(m_iChannelPositions[i]));
        //}
    #endif

    //
    // Converting data from DIRAC to output stream's format (if neccessary)
    //
    if( !m_bDirectOutput ){
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("*** DIRAC IF: Converting data for output format (%ld frames)...\n"),
                long(m_pTempFile2->getSize()/frameSize));
        #endif
        VTASSERT( m_pTempFile2!=NULL );
        if( !(m_pTempFile2->rewind()) ){
            VTDEBUGMSGT("Failed to seek to beginning of intermediate output file!");
            PDEndOperation(LCT("Intermediate file seek error."));
            CloseProcess();
            return false;
        }
        m_TempStream2.Open( m_pTempFile2, false, true, kFP32, m_fDIRACRate, m_iDIRACChannels );
        m_OutConverter.Init( &m_TempStream2, m_pOut, m_iResamplerQuality, m_DitheringMethod, m_BitMode );
        if( !m_OutConverter.ConvertAll( GetProgressDisplay(), GetAbortFlag() ) ){
            // !!!! See the comment for input conversion.
            m_TempStream2.Close();
            if( IsAborted() ){
                PDEndOperation(LCT("Aborted."));
                CloseProcess();
                return false;
            }
            VTDEBUGMSGT("Output stream converter failure!");
            PDEndOperation(LCT("Output format converter error."));
            CloseProcess();
            return false;
        }
    } // else data already sent to output stream
    #if defined(VT_CONSOLEDEBUG)
        VTPRINTF(VTT("*** DIRAC IF: Success!\n"));
    #endif
    PDEndOperation(LCT("Pitch shifting / time stretching completed."));
    CloseProcess();
    return true;
}



void CDIRACInterface::CloseProcess()
{
    for( size_t i=0; i<kMaxAudioChannels; i++ )
        if( m_pDIRACMono[i]!=NULL ){
            DiracDestroy( m_pDIRACMono[i] );
            m_pDIRACMono[i] = NULL;
        }
    if( m_pDIRAC!=NULL ){
        DiracDestroy( m_pDIRAC );
        m_pDIRAC = NULL;
    }

    if( m_bDeleteTempFiles ){
        if( m_pTempFile1!=NULL ){
            delete m_pTempFile1;
            m_pTempFile1 = NULL;
        }
        if( m_pTempFile2!=NULL ){
            delete m_pTempFile2;
            m_pTempFile2 = NULL;
        }
    }

    m_pIn = NULL;
    m_pOut = NULL;
}

long CDIRACInterface::sReadFromChannelsCallback(float *data, long numFrames, void *userData)
{
    if( userData!=NULL ){
        CDIRACInterface *theobject = (CDIRACInterface*)userData;
        return theobject->readFromChannelsCallback( data, numFrames );
    }else{
        VTDEBUGMSGT("NULL userData in DIRAC callback function!");
        return 0;
    }
}

long CDIRACInterface::readFromChannelsCallback(float *data, long numFrames )
{
    if( numFrames<=0 || data==NULL )
        return 0;

    // Size of input stream frame, in bytes. This is not the same as
    // DIRAC input frame, in m_bDIRACInstancePerChannel mode.
    size_t frameSize = size_t( sizeof(float) * m_iDIRACChannels );

    // Zeroing DIRAC's input buffer.
    memset( data, 0, numFrames * ((m_bDIRACInstancePerChannel)?sizeof(float): frameSize) );

    //
    // Loading interleaved data for DIRAC into a buffer,
    // either into intermediate buffer for channel-splitting or
    // directly into DIRAC.
    //
    void *readBuffer;
    long readFrames;

    if( m_bDIRACInstancePerChannel ){
        // Each channel is processed by a separate instance of DIRAC, so
        // we have to read data into intermediate buffer then extract
        // the channel needed by current DIRAC instance (see m_iCurrentChannel).
        m_DiracIn.resize( numFrames * frameSize );
        readBuffer = m_DiracIn.getDataPtr();
    }else
        // DIRAC understands interleaved multi-channel data
        readBuffer = data;

    if( m_bDirectInput ){
        // Loading data directly from source stream...
        if( m_bDIRACInstancePerChannel ){
            // Have to reposition stream to beginning of the block
            if( !m_pIn->Seek( m_iChannelPositions[ m_iCurrentChannel ] ) ){
                VTDEBUGMSGT("Error seeking in input stream.");
                m_bReadCallbackError = true;
                return 0;
            }
        }

        // GetNFramesLeft() may return negative value.
        long toRead = long(Min( Max<TAudioStreamSize>(m_pIn->GetNFramesLeft(), 0), TAudioStreamSize(numFrames) ));

        // Check that data is not in the buffer already
        // !!!! Add a heuristic - if m_iChannelBufferPos != m_iChannelPositions[ m_iCurrentChannel ],
        // but the required block overlaps with buffer content, it can be (partially) retrieved from
        // the buffer.
        if( m_bDIRACInstancePerChannel==false ||
            toRead>m_iChannelBufferSize ||
            m_iChannelBufferPos != m_iChannelPositions[ m_iCurrentChannel ] )
        {
            if( toRead>0 ){
                if( !m_pIn->Read( readBuffer, toRead ) ){
                    VTDEBUGMSGT("Error reading input stream.");
                    m_bReadCallbackError = true;
                    return 0;
                }
            }else{
                VTASSERT2( toRead==0, CString(VTT("toRead=="))<<toRead );
            }
            if( m_bDIRACInstancePerChannel ){
                m_iChannelBufferSize = toRead;
                m_iChannelBufferPos = m_iChannelPositions[ m_iCurrentChannel ];
            }
        }else{
            // printf("Saved a read for %ld frames!\n", toRead);
        }
        readFrames = toRead;
    }else{
        // Loading data from intermediate file
        if( m_bDIRACInstancePerChannel ){
            // Have to reposition stream to beginning of the block
            VTFSIZE pos = m_iChannelPositions[ m_iCurrentChannel ]*frameSize;
            if( !m_pTempFile1->seekb( pos ) ){
                VTDEBUGMSGT("Error seeking in intermediate input file.");
                m_bReadCallbackError = true;
                return 0;
            }

            #if 0
                printf("CHANNEL = %d     FRAMEPOS = %ld     BYTEPOS = %ld     FILESIZE = %ld (%ld frames)\n",
                       int( m_iCurrentChannel ),
                       long( m_iChannelPositions[ m_iCurrentChannel ] ),
                       long( pos ),
                       long( m_pTempFile1->getSize() ),
                       long( m_pTempFile1->getSize()/frameSize ));
            #endif
        }
        VTFSIZE framesLeft = Max( (m_pTempFile1->getSize()-m_pTempFile1->tell())/VTFSIZE(frameSize), VTFSIZE(0) );
        long toRead = long(Min( framesLeft, VTFSIZE(numFrames) ));

        // Check that data is not in the buffer already
        // !!!! Add a heuristic - if m_iChannelBufferPos != m_iChannelPositions[ m_iCurrentChannel ],
        // but the required block overlaps with buffer content, it can be (partially) retrieved from
        // the buffer.
        if( m_bDIRACInstancePerChannel==false ||
            toRead>m_iChannelBufferSize ||
            m_iChannelBufferPos != m_iChannelPositions[ m_iCurrentChannel ] )
        {
            if( toRead>0 ){
                if( !m_pTempFile1->read( readBuffer, toRead*frameSize ) ){
                    VTDEBUGMSGT("Error reading intermediate input file.");
                    m_bReadCallbackError = true;
                    return 0;
                }
            }else{
                VTASSERT( toRead==0 );
            }
            if( m_bDIRACInstancePerChannel ){
                m_iChannelBufferSize = toRead;
                m_iChannelBufferPos = m_iChannelPositions[ m_iCurrentChannel ];
            }
        }else{
            // printf("Saved a read for %ld frames!\n", toRead);
        }
        readFrames = toRead;
    }

    if( m_bDIRACInstancePerChannel ){
        // Updating in-stream positions
        m_iChannelPositions[ m_iCurrentChannel ] += TAudioStreamSize(readFrames);

        // Extracting channel data from interleaved input buffer into
        // DIRAC's single-channel buffer.
        float *bufPtr = ((float*)readBuffer) + m_iCurrentChannel;
        for( long i=0; i<numFrames; i++ ){
            *(data++) = *bufPtr;
            bufPtr += m_iDIRACChannels;
        }
    } // else do nothing (data are already in DIRAC's 'data')

    m_iSamplesFedToDIRAC += TAudioStreamSize(readFrames);

    return readFrames;
}


#define DIRAC_SET_PROPERTY( id, value )                                                \
    if( m_bDIRACInstancePerChannel==false )                                            \
        DiracSetProperty(id, value, m_pDIRAC);                                        \
    else                                                                            \
        for( int i=0; i<m_iDIRACChannels; i++ )                            \
            DiracSetProperty(id, value, m_pDIRACMono[i]);

#define DIRAC_GET_PROPERTY( id )                                                    \
    if( m_bDIRACInstancePerChannel==false )                                            \
        return DiracGetProperty(id, m_pDIRAC);                                        \
    else                                                                            \
        return DiracGetProperty(id, m_pDIRACMono[0])


void CDIRACInterface::SetPitchFactor( long double factor )
{
    DIRAC_SET_PROPERTY( kDiracPropertyPitchFactor, factor );
}

long double CDIRACInterface::GetPitchFactor() const
{
    DIRAC_GET_PROPERTY( kDiracPropertyPitchFactor );
}

void CDIRACInterface::SetTimeFactor( long double factor )
{
    DIRAC_SET_PROPERTY( kDiracPropertyTimeFactor, factor );
}

long double CDIRACInterface::GetTimeFactor() const
{
    DIRAC_GET_PROPERTY( kDiracPropertyTimeFactor );
}

void CDIRACInterface::SetTimeSec( long double time )
{
    if( m_pIn->GetLengthSec()==0 )
        SetTimeFactor( 1.0 );
    else
        SetTimeFactor( time / m_pIn->GetLengthSec() );
}

LPCVTCHAR CDIRACInterface::GetDIRACVersion()
{
    static VTCHAR version[256]=VTT("");
    if( !version[0] ){
        const char *dirac = DiracVersion();
        VTASCII2VTCHAR( dirac, version, 255 );
    }
    return version;
}

LPCVTCHAR CDIRACInterface::GetDIRACCopyright()
{
    return VTT("Copyright (C) 2005 Stephan M. Bernsee, All Rights Reserved");
}

void CDIRACInterface::SetBias( TPitchShiftingBias bias )
{
    switch( bias )
    {
        case kPSBFast:                SetDIRACLambda( kDiracLambdaPreview );        return;
        case kPSBTime:                SetDIRACLambda( kDiracLambda1 );            return;
        case kPSBTimeEmphasis:        SetDIRACLambda( kDiracLambda2 );            return;
        case kPSBFrequencyEmphasis: SetDIRACLambda( kDiracLambda4 );            return;
        case kPSBFrequency:            SetDIRACLambda( kDiracLambda5 );            return;
        default:                    SetDIRACLambda( kDiracLambda3 );
    };
}

void CDIRACInterface::SetPitchShifterQuality( TSimpleQualitySelection quality )
{
    switch( quality )
    {
        case kQualityLowest:
        case kQualityLow:            SetDIRACQuality( kDiracQualityPreview );    return;
        case kQualityHigh:            SetDIRACQuality( kDiracQualityBetter );        return;
        case kQualityBest:            SetDIRACQuality( kDiracQualityBest );        return;
        default:                    SetDIRACQuality( kDiracQualityGood );
    };
}

LPCVTCHAR CDIRACInterface::GetDIRACName()
{
    static VTCHAR version[256]=VTT("");
    if( !version[0] ){
        // DIRAC reports its version (LE/STUDIO/PRO) after the version number.
        /*
        #if defined(USE_DIRAC_LE)
            VTSTRNCPY( version, VTT("DIRAC LE ver. "), 32 );
        #elif defined(USE_DIRAC_STUDIO)
            VTSTRNCPY( version, VTT("DIRAC STUDIO ver. "), 32 );
        #else
            VTSTRNCPY( version, VTT("DIRAC PRO ver. "), 32 );
        #endif
        */
        VTSTRNCPY( version, VTT("DIRAC ver. "), 32 );
        LPVTCHAR pos = version + VTSTRLEN(version);
        // VTASCII2VTCHAR( GetDIRACVersion(), pos, 255-32 );
        VTSTRNCPY( pos, GetDIRACVersion(), 255-32 );
    }
    return version;
}

#endif



