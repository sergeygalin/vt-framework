//-----------------------------------------------------------------------------
//
// Clip Conveyor
//
// Programmer: Sergey A. Galin
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2009 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#ifndef VT_CLIPCONVEYOR_HPP
#define VT_CLIPCONVEYOR_HPP

#include "vt_threading.hpp"
#include "vt_clipstretcher.hpp"
#include "vt_string.hpp"
#include "vt_clipjobs.hpp"

namespace VT {
    namespace AudioEngine {

        //
        // Queue of clip generation jobs. An application usually has either
        // one such queue, or two of them being executed in different priorities.
        // This class only handles job data.
        // The class is also multithreading-safe (uses critical section to
        // lock access to the queue).
        //
        class CClipQueue: protected CArray<TClipJob>
        {
        public:
            static const unsigned kSleepPeriodMs;
            static const size_t kDefaultQueueAlloc, kQueueGrowSize;

            CClipQueue();
            virtual ~CClipQueue();
            virtual size_t AddJob( const TClipJob &job ); // Adds new task (the data is copied into internal storage)
            const TClipJobState GetJobState( size_t id ) const; // Get job state only. On error returns kJobError.
            void SetJobState( size_t id, TClipJobState state ); // Set job state. Returns false on error.
            virtual void ReleaseJob( size_t idx );
            const TClipJob& GetJob( size_t id ) const; // May return NULL. Warning: the pointer may become invalid after AddJob().
            bool GetNextJobIdx( size_t *ret ); // Get ID of the next job which should be executed. Job state is NOT updated!
            const TClipJob* GetNextJob();
            virtual void EmptyQueue(); // Trash all queue data (may be needed e.g. when a project is closed).
            bool FindJob( LPCVTCHAR outFileName, size_t *out = NULL, size_t startingIdx = 0 ) const;
            bool FindAliveJob( LPCVTCHAR outFileName, size_t *out = NULL, size_t startingIdx = 0 ) const;

            bool IsEmpty() const { return m_bNoMoreWork; }

            size_t getSize() const { return CArray<TClipJob>::getSize(); }
            TClipJob& get(size_t idx){ return CArray<TClipJob>::get(idx); }
            const TClipJob& getConst(size_t idx) const { return CArray<TClipJob>::getConst(idx); }

            void ConcatShortDump( CString *out ) const; // Concats short dump of the queue

        private:
        protected:
            bool m_bNoMoreWork;
            CCriticalSection m_CS;
        };


        //
        // A class which runs CClipQueue in a thread
        //
        class CClipConveyor:
            public CClipQueue,        // Contains queue
            public CThread,            // Runs in a thread
            public CClipStretcher    // Stretching engine
            // public CAbortFlagChecker - use CThread::Terminate() to stop a conveyor.
            // public CProgressDisplayer - already inherited from CClipStretcher
        {
        public:
            CClipConveyor( LPCVTCHAR threadName, bool createSuspended, TPriority priority = kNormalPriority );
            virtual ~CClipConveyor();

            //
            // !! Bit mode and dithering functions are inherited from CClipStretcher.
            // Quality is controlled by jobs.
            //
            // virtual size_t GetMaxMemoryFileSize() const;
            // virtual void SetMaxMemoryFileSize( size_t maxSzBytes );
            // TDitheringMethod GetDitheringMethod() const;
            // void SetDitheringMethod( TDitheringMethod method );
            // TAudioProcessorBitBehaviour GetBitMode() const;
            // void SetBitMode( TAudioProcessorBitBehaviour mode );

            // Add job and awakens thread from lazy mode if necessary.
            virtual size_t AddJob( const TClipJob &job );

            // Finds a job in the queue and deletes it. Also aborts
            // the job if its currently being performed.
            virtual bool AbortJob( LPCVTCHAR outFileName );
            virtual void ReleaseJob( size_t idx ); // Aborts job first

            // In addition to CClipQueue::Empty(), also aborts current job.
            virtual void EmptyQueue();
            virtual void Terminate(); // Also sets abort flag to stop current job

            virtual void Process();

            // Copies current / last job into out.
            void GetLastJob( TClipJob *out );

            // Wait until current jobs are finished and the process
            // gets into sleepy mode. This function only uses volatile
            // flags and doesn't lock the critical section.
            // If time to wait is negative, would wait forever.
            // Returns true if reached idle state or thread was terminated,
            // false on time out.
            bool WaitForIdle( long timeToWaitMs = -1 );

            // Use this function to check if a conveyor has any work to do.
            bool IsIdle() const { return IsEmpty(); }

            //
            // Temporary file settings - inherited from CClipStretcher
            //
            // virtual void SetTempFileDirectory( LPCVTCHAR dir );
            // virtual void SetTempFilePrefix( LPCVTCHAR pfx );

        private:
            CCriticalSection m_m_JobCS;
            volatile bool
                m_bAbortFlag,        // Passes terminated/aborted state to sub-processes
                m_bLazySleeping,    // Set to true when conveyor is in sleeping mode (has no job)
                m_bQueueEmptied,    // Set to true when queue is purged, so current job won't return data into queue
                m_bJobEnded;        // Extra(?) flag used by WaitForIdle()
            TClipJob m_Job;            // A job copy which is used by DoJob().
                                    // Direct reference to a job in the queue is not OK
                                    // because queue may change while a job is performed.
            // Performs a job specified in m_Job.
            // The result is returned in m_Job.m_State.
            void DoJob();

            bool AbortCurrentJobIfOutFileNameIs( LPCVTCHAR outFileName );
        };





    } // namespace AudioEngine
} // namespace VT

#endif

