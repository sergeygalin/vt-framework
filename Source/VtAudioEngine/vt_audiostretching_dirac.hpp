//-----------------------------------------------------------------------------
//
// Audio Stretching Interface - DIRAC implementation
//
// Programmer: Sergey A. Galin
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2009 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#ifndef VT_AUDIOSTRETCHING_DIRAC_HPP
#define VT_AUDIOSTRETCHING_DIRAC_HPP

#include "vt_audiostretching.hpp"

#if !defined(USE_DIRAC)
    #define USE_DIRAC
#endif

// DIRAC version.
// "LE" supports only 1 channel and sample rates are limited to 44100 and 48000.
// "Studio" supports 1 or 2 channels and 8000..96000 KHz.
// If USE_DIRAC_LE and USE_DIRAC_STUDIO are not defined, then "PRO" version
// is assumed, which has no limitations.

#define USE_DIRAC_LE
// #define USE_DIRAC_STUDIO

#if defined(USE_DIRAC_LE) && defined(USE_DIRAC_STUDIO)
    #error "Both DIRAC LE and STUDIO are defined! %-\"
#endif


#if defined(USE_DIRAC)
    #if defined(__BORLANDC__)
        #include "DiracWrapper.h"
    #else
        #include "Dirac.h"
    #endif
#endif

//
// Linking to a proper DIRAC library
//

#if defined(_MSC_VER)
    #if defined(USE_DIRAC_LE)
        #pragma comment(lib, "../../../../Lib/DiracLE.lib")
    #endif
#endif

#if defined(__BORLANDC__)
    #if defined(VTDIRACWRAPPER_DLL)
        #pragma comment(lib, "../../../../ThirdParty/DIRAC/VT DIRAC Wrapper/Release/DIRACWrapper_omf.lib")
    #else
        #pragma comment(lib, "../../../../ThirdParty/DIRAC/VT DIRAC Wrapper/Static/DIRACWrapper_omf.lib")
        // #pragma comment(lib, "../../../../ThirdParty/DIRAC/DIRAC LE/Windows/DiracLE_omf.lib")
    #endif
#endif


namespace VT {
    namespace AudioEngine {


        #if defined(USE_DIRAC)
            //
            // DIRAC interface.
            //
            //
            class CDIRACInterface: public CPitchShifter
            {
            public:
                static const size_t kDefaultMaxMemoryFileSize = 1024*1024*10;

                CDIRACInterface();
                virtual ~CDIRACInterface();

                // Temporary files are used to store intermediate data.
                // E.g. TMemoryFile or CTempFile objects can be used; if deleteTempFiles
                // flag is set, these are deleted automatically.
                // If temporary file pointers are NULL, then they are created in-memory or
                // on disk automatically (basing on m_uMaxTotalMemoryFileSize).
                // NOTE: either BOTH of the temporary files should be set or BOTH shoul be NULL.
                virtual bool PrepareProcess( CInputAudioStream *in, COutputAudioStream *out,
                    CFile *tmpFile1 = NULL, CFile *tmpFile2 = NULL,
                    bool deleteTempFiles = false);

                virtual bool DoProcess();

                // *********************************************************************
                // *** Set between constructor and PrepareProcess() ********************
                // *********************************************************************

                // Control max memory size occupied by in-memory temporary files.
                // If temporary data doesn't fit into the specified size, then
                // disk temporary files are created.
                // This is used when NULL tem file pointers are passed to PrepareProcess().
                size_t GetTotalMaxMemoryFileSize() const { return m_uMaxTotalMemoryFileSize; }
                void SetTotalMaxMemoryFileSize( size_t maxMemFileMemoryUsage ){ m_uMaxTotalMemoryFileSize = maxMemFileMemoryUsage; }
                virtual size_t GetMaxMemoryFileSize() const { return GetTotalMaxMemoryFileSize()/GetNTempFiles(); }
                virtual void SetMaxMemoryFileSize( size_t maxMemFileMemoryUsage ){ SetTotalMaxMemoryFileSize( maxMemFileMemoryUsage*GetNTempFiles() ); }
                virtual size_t GetNTempFiles() const { return 2; }

                virtual TSampleTypeIdx GetPreferredSampleFormat() const { return kFP32; }

                // Set resampling quality. Resampling is done when input sample rate and
                // output sample rate differ, or output sample rate is not supported by DIRAC
                // (then the stream is resampled for DIRAC, then DIRAC's output is resampled
                // to the rate requested).
                virtual void SetResamplerQuality( TSimpleQualitySelection quality ){ m_iResamplerQuality = quality; }

                // Set dithering method. Dithering is applied when converting DIRAC's 32-bit
                // float out to integer output stream.
                virtual void SetDitheringMethod( TDitheringMethod dither ){ m_DitheringMethod = dither; }

                // Set bit mode. By default, kForce32 is used, since DIRAC is 32-bit
                // and switching to 64 bit adds no visible effect.
                virtual void SetBitMode( TAudioProcessorBitBehaviour mode ){ m_BitMode = mode; }

                // DIRAC lambda controls tradeoff between time localization and
                // frequency localization during time/pitch process.
                void SetDIRACLambda( long lambda ){ m_DIRACLambda = lambda; }

                // Sets lambda according to the bias
                virtual void SetBias( TPitchShiftingBias bias );

                // Set DIRAC process quality, from Preview to Best.
                // That greatly changes processing time, from around real-time (on fast PC)
                // to very slow.
                void SetDIRACQuality( long quality ){ m_DIRACQuality = quality; }

                // Sets quality for pitch-shifing engine
                virtual void SetPitchShifterQuality( TSimpleQualitySelection quality );

                // *********************************************************************
                // *** Set between PrepareProcess() and DoProcess(). *******************
                // *********************************************************************
                virtual void SetPitchFactor( long double factor ) ;
                virtual long double GetPitchFactor() const;

                virtual void SetTimeFactor( long double factor );
                virtual long double GetTimeFactor() const;

                // Set time factor from required length of resulting sound.
                virtual void SetTimeSec( long double time );

                static LPCVTCHAR GetDIRACVersion();
                static LPCVTCHAR GetDIRACCopyright();
                static LPCVTCHAR GetDIRACName(); // Full name of the pitch corrector engine, with version info

                virtual LPCVTCHAR GetPitchShifterVersion(){ return GetDIRACVersion(); }
                virtual LPCVTCHAR GetPitchShifterCopyright(){ return GetDIRACCopyright(); }
                virtual LPCVTCHAR GetPitchShifterName(){ return GetDIRACName(); }


            protected:
                static const long kDiracBlockSizeFrames; // DIRAC is freakin' slow, so it must be small
                void *m_pDIRACMono[kMaxAudioChannels]; // DIRAC instances for multi mono mode
                void *m_pDIRAC; // DIRAC instances for multi-channel DIRAC mode
                size_t m_iCurrentChannel; // Used by callback function in multi mono mode
                TAudioStreamSize m_iChannelPositions[kMaxAudioChannels]; // Input stream positions in multi mono mode
                TAudioStreamSize m_iChannelBufferPos; // Channel buffer pos, in multi mono mode
                long m_iChannelBufferSize; // Channel buffer size, in multi mono mode
                bool m_bDIRACInstancePerChannel; // Use multi mono mode?
                bool m_bReadCallbackError; // Set by DIRAC callback function to indicate error
                TAudioStreamSize m_iSamplesFedToDIRAC; // Monitoring/debug counter, updated by DIRAC callback
                size_t m_uMaxTotalMemoryFileSize;
                CAudioStreamConverter m_OutConverter;
                CInputAudioStream *m_pIn;
                COutputAudioStream *m_pOut;
                CFileInputAudioStream m_TempStream2;
                CFile *m_pTempFile1, *m_pTempFile2;
                bool m_bDeleteTempFiles, m_bDirectInput, m_bDirectOutput;
                // CProgressDisplay *m_pProgressDisplay;
                CArray<BYTE> m_DiracOut, m_DiracOutMono, m_DiracIn;
                // const volatile bool *m_pProcessAborted;
                float m_fDIRACRate;
                int m_iDIRACChannels;
                TDitheringMethod m_DitheringMethod;
                TAudioProcessorBitBehaviour m_BitMode;
                TSimpleQualitySelection m_iResamplerQuality;
                double m_fFadeInLengthMs, m_fFadeOutLengthMs;

                // DIRAC creation properties
                long m_DIRACLambda;
                long m_DIRACQuality;

                // Clean up
                void CloseProcess();

                // Relay from DIRAC callback to our member function
                static long sReadFromChannelsCallback(float *data, long numFrames, void *userData);

                // Implementation of DIRAC callback
                long readFromChannelsCallback(float *data, long numFrames );

                // void SetupTempFiles( CFile *tmpFile1, CFile *tmpFile2, bool deleteTempFiles );
            };


            typedef CDIRACInterface CPitchShifterImplementation;

        #endif /* USE_DIRAC */




    } // namespace AudioEngine
} // namespace VT



#endif

