//-----------------------------------------------------------------------------
//
// Circular audio buffers and related math (FIR filtration).
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#ifndef VT_BUFFERS_HPP
#define VT_BUFFERS_HPP

#include "vt_audiomath.hpp"
#include "vt_vectors.hpp"


#if defined( VTSIMD_ACCELERATION_POSSIBLE ) // See vt_simd.hpp
    // Enabling SIMD stuff only if there's any acceleration, otherwise
    // old good bidirectional C++ implementation would do better.
    #define VT_BUFFERS_USE_SIMD_FUNCTIONS
#endif

// Non-SIMD, plain C++ convolution implementation
#define VT_BUFFERS_BIDIFIR
// #define VT_BUFFERS_BIDIFIR_POINTERVERSION - doesn't work properly for some reason, do not enable



namespace VT {

    // Circular buffer with some DSP math functions (convolution, FIR MAC cycle).
    template<class T> class CCircularBuffer: public CVector<T>
    {
    public:
        static const size_t kDefaultAllocSize = 16;

        virtual LPCVTCHAR GetClassName() const { return VTT("CCircularBuffer"); }

        CCircularBuffer<T>(size_t size=kDefaultAllocSize, size_t alloc=kDefaultAllocSize):
            CVector<T>(size, alloc){ reset(); }

        virtual void resize(int newsz){ CVector<T>::resize(newsz); reset(); }

        // Zero memory and reset position pointer.
        void reset(){
            this->zero();
            if( this->getSize()>1 )
                // When multiple cyclic buffers of the sames size are run in a program,
                // randomizing their pointer positions significantly decreases load
                // peaks caused by splitting block operations (like FIR convolution)
                // when block overlaps end of a buffer.
                m_uBufferPointer = size_t( double(this->getSize()-1)*double(rand()) / double(RAND_MAX) );
            else
                m_uBufferPointer = 0;
        }

        // Get the oldest sample in the buffer (equal to sampleFO(0))
        VTINLINE T first() const;

        // Returns outgoing sample (one which was first() before the call)
        VTINLINE T insert(T value);

        // Doesn't return anything, so a bit faster
        VTINLINE void quickInsert(T value);

        // Get sample with offset 'offset' From the Newest sample
        VTINLINE T sampleFN( long offset ) const;

        // Get sample with offset 'offset' From the Oldest sample
        VTINLINE T sampleFO( size_t offset ) const;

        // Get middle sample
        VTINLINE T middle() const;

        // Calculates scalar multiplication of oldest samples in buffer
        // by the 'fir' vector, i.e. returns a point in convolution:
        // buffer[] * fir[]. By specifying 'shift' parameter, one may determine how many
        // of the oldest samples are skipped. For example, for a symmetrical
        // FIR kernel it may be a good idea to skip (buffer.getSize()-fir.getSize())/2
        // samples to always have central element of the kernel aigned against
        // the buffer center and therefore keep constant signal delay while
        // FIR is recalculated and its size is changed.
        //
        // NOTE: Trying to convolve with a vector which is longer than
        // (buffer size+shift) is purely on your conscience.
        //
        // oldest sample        newest sample
        //     |=======================|       buffer
        //               |==========|          fir
        //     |<-shift->|
        //
        //

        // This is the main implementation, all others are its wrappers.
        VTINLINE T convolution_point(const T* fir, size_t dataSize, size_t shift=0) const;

        VTINLINE T convolution_point(const CVector<T> &fir, size_t shift=0) const {
            return convolution_point( fir.getRODataPtr(), fir.getSize(), shift ); }

        // Scalar multiplication of central part of the buffer by FIR,
        // filter & buffer being aligned:
        //
        // oldest sample        newest sample
        //     |===========+===========|       buffer
        //          |======+======|            FIR filter
        //
        VTINLINE T convolution_point_symmet(const CVector<T> &fir) const {
            return convolution_point(fir, (this->getSize()-fir.getSize())/2);    }

        VTINLINE T convolution_point_symmet(const T* fir, size_t dataSize) const {
            return convolution_point(fir, dataSize, (this->getSize()-dataSize)/2);    }

        // FIR multiply-accummulate-cycle operation
        VTINLINE T fir_mac( T newsample, const CVector<T> &fir, size_t shift=0 ){
            quickInsert(newsample);
            return convolution_point(fir, shift); }

        // FIR multiply-accummulate-cycle operation of central part of the buffer
        VTINLINE T fir_mac_symmet( T newsample, const CVector<T> &fir ){
            quickInsert(newsample);
            return convolution_point_symmet(fir); }

    protected:
        size_t m_uBufferPointer;
    };


    typedef CCircularBuffer<float> CSPCircrularBuffer;
    typedef CCircularBuffer<double> CDPCircrularBuffer;








    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //
    // Implementation of the template class functions
    //





    // Get the oldest sample in the buffer
    template<class T> T CCircularBuffer<T>::first() const
    {
        if( m_uBufferPointer >= this->getSize() )
            return this->get(0);
        else
            return this->get( m_uBufferPointer );
    }

    template<class T> T CCircularBuffer<T>::middle() const
    {
        return this->sampleFO( (this->getSize())/2 );
    }

    template<class T> T CCircularBuffer<T>::insert(T value)
    {
        if( m_uBufferPointer >= this->getSize() )
            m_uBufferPointer = 0;
        T ret = this->get( m_uBufferPointer );
        this->set( m_uBufferPointer++, value);
        return ret;
    }

    template<class T> void CCircularBuffer<T>::quickInsert(T value)
    {
        if( m_uBufferPointer >= this->getSize() )
            m_uBufferPointer = 0;
        this->set( m_uBufferPointer++, value );
    }

    template<class T> T CCircularBuffer<T>::sampleFN( long offset ) const
    {
        // The newest sample lays under [m_uBufferPointer-1]...
        // Then, scrolling back 'offset' indexes.
        long index = long(m_uBufferPointer)-long(1)-long(offset);
        if( index < 0 ){
            index+=long(this->getSize());
            if( index < 0 )
                return 0;
        }
        return this->get(index);
    }

    template<class T> T CCircularBuffer<T>::sampleFO( size_t offset ) const
    {
        // The oldest sample if under bufBtr;
        // scroll forward 'offset' indexes.
        size_t index = size_t(m_uBufferPointer)+size_t(offset);
        if( index>=this->getSize() ){
            index-=this->getSize();
                if( index>=this->getSize() )
                    return 0;
        }
        return this->get(index);
    }

    #if defined(MSCFASTMATH)
        // Trying to speed up this function (the most time-critical piece
        // in any program which uses FIR filters). Really must be rewritten
        // using assembler one day.

        //    ??????????? I'm not sure if this works like that, around a template?

        #pragma float_control(push)
        #pragma float_control(precise, off)
        #pragma float_control(except, off)
    #endif


    template<class T> T CCircularBuffer<T>::convolution_point(const T* firData, size_t firSize, size_t shift) const
    {
        VTASSERT( firSize>0 );
        VTASSERT( firSize<=this->getSize()+shift );

        #if defined(VT_BUFFERS_USE_SIMD_FUNCTIONS)

            // ******************************************************************************
            // *** SIMD-accelerated version *************************************************
            // ******************************************************************************

            // Find address of beginning of buffer data which must be convolved with firData
            size_t start = m_uBufferPointer + shift;
            while( start >= this->getSize() )
                start -= this->getSize();
            // Find out how many samples must be convolved from start, that would be the first chunk
            size_t firstChunkSize, left = size_t(this->getSize() - start);
            // Rewritten from Math::Min because of annoying MSC compiler bug
            if( left < firSize )
                firstChunkSize = left;
            else
                firstChunkSize = firSize;
            T sum = SIMD::Convolve( this->getRODataPtr()+start, firData, firstChunkSize );
            if( firstChunkSize<firSize )
                sum += SIMD::Convolve( this->getRODataPtr(), firData+firstChunkSize, firSize-firstChunkSize );
            return sum;

        #else


            // ******************************************************************************
            // *** Version without SIMD acceleration ****************************************
            // ******************************************************************************


            register T sum = 0;
            #if !defined(VT_BUFFERS_BIDIFIR)
                //
                // One-way summing
                //
                register size_t i, firi=0, minsz=this->getSize(); //Min( this->getSize(), firSize );

                size_t start = m_uBufferPointer + shift;
                while( start >= this->getSize() )
                    start -= this->getSize();

                // Mutliplying by the overwritten (older) part
                // of the cyclic buffer (i = start..size-1)
                for( i=start; i<this->getSize() && firi<minsz; i++ )
                    sum+=T(this->get( i )) * firData[ firi++ ];

                // Mutliplying by the overwriting (newer) part
                // of the cyclic buffer (i = 0..start-1)
                for( i=0; i<start && firi<minsz; i++, firi )
                    sum+=T(this->get( i )) * firData[ firi++ ];

            #elif !defined(VT_BUFFERS_BIDIFIR_POINTERVERSION)
                //
                // Bi-directional summing (higher precision)
                //
                register int bufl = int(m_uBufferPointer + shift);
                while( bufl >= int(this->getSize()) )
                    bufl -= int(this->getSize());
                register int bufr = bufl + int(firSize) - 1;
                while( bufr >= int(this->getSize()) )
                    bufr -= int(this->getSize());

                register int bufszm1 = int(this->getSize())-1, firl = 0, firr = int(firSize-1);
                VTASSERT( firr>=firl );
                VTASSERT( bufszm1>=0 );
                while( firl < firr ){
                    sum +=
                        T(this->get( bufl++ )) * firData[firl++] +
                        T(this->get( bufr-- )) * firData[firr--];
                    if( bufl>bufszm1 )
                        bufl = 0;
                    if( bufr<0 )
                        bufr = bufszm1;
                }
                // If central element was not added yet...
                if( firl==firr )
                    sum += T(this->get( bufl )) * firData[ firl ];
            #else
                //
                // Bi-directional summing (higher precision),
                // version which compares pointers
                //

                // !!!!!!!!!!!!!!!! Doesn't work correctly !!!!!!!!!!!!!!!

                DOESNT_WORK_YET;

                register const T *pbufl, *pbufr;
                const T *pbufend = this->getRODataPtr()+(firSize-1);
                {
                    // Finding addresses of left and right ends
                    // of the buffer (pbufl and pbuf4).
                    register int bufl = int(m_uBufferPointer) + int(shift); // Index of the left side
                    while( bufl >= int(this->getSize()) ) // Cycling index (should execute one time max)
                        bufl -= int(this->getSize());
                    register int bufr = bufl + int(firSize) - 1; // Index of the right side
                    while( bufr >= int(this->getSize()) ) // Cycling index (should execute one time max)
                        bufr -= int(this->getSize());
                    pbufl = this->getRODataPtr() + bufl; // Converting index to pointer
                    pbufr = this->getRODataPtr() + bufr; // Converting index to pointer
                }
                register const T *pfirl = firData, *pfirr = pfirl + (firSize-1); // FIR left and right sides
                while( pfirl < pfirr ){
                    sum +=
                        T(*(pbufl++)) * (*(pfirl++)) +
                        T(*(pbufr--)) * (*(pfirr--));
                    if( pbufl>pbufend ) // Wrapping around over end of buffer
                        pbufl = this->getRODataPtr();
                    if( pbufr<this->getRODataPtr() ) // Wrapping around over beginning of buffer
                        pbufr = pbufend;
                }
                // If central element was not added yet...
                if( pfirl==pfirr )
                    sum += T(*pbufl) * (*pfirl);
            #endif

            return sum;
        #endif
    }

    #if defined(MSCFASTMATH)
        #pragma float_control(pop)
    #endif

} // namespace

#endif
