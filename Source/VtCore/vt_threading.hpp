//-----------------------------------------------------------------------------
//
// A cross-platform threading library, includes critical section
// locks and threads.
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/


#ifndef VT_THREADING_HPP
#define VT_THREADING_HPP

// This macro should not normall be defined, because pthreads has no
// suspend/resume and using this functionality will make program non-portable.
// #define VT_ALLOW_THREAD_PAUSING

//
// Define _WIN32_WINNT before including windows.h!
//
#if defined(_WIN32) || defined(VT_WINDOWS) || defined(WINDOWS) || defined(WIN32)
    #define VT_USE_WINCRITICALSECTION
    // This definition is needed to have access to TryEnterCriticalSection().
    #if defined(_WIN32_WINNT)
        #if _WIN32_WINNT<0x0400
            #undef _WIN32_WINNT
            #define _WIN32_WINNT 0x0400
        #endif
    #else
        #define _WIN32_WINNT 0x0400
    #endif
    #include <windows.h>
#endif

#include "vt_portable.hpp"

#if defined(VT_LINUX) || defined(VT_NIXGCC) || defined(VT_MAC)
    // Linux & Mac
    #if defined(PTHREADS)
        // Modern Linux and Mac use mutexes.
        // Older Linux can use spinlocks, but why?
        // #if defined(VT_MAC)
            #define VT_USE_PTHREADS_MUTEX
        // #else
        //    #define VT_USE_PTHREADS_SPINLOCK
        // #endif
        #include <pthread.h>
    #else
        #warning Using own implementation of critical sections. Maybe you should define PTHREADS?
    #endif
    #include <unistd.h>
#elif !defined(VT_WINDOWS)
    // For sleep()
    #include <unistd.h>
#endif

#if !defined(_CRT_SECURE_NO_DEPRECATE)
    #define _CRT_SECURE_NO_DEPRECATE 1
#endif

namespace VT {

    // Portable thread sleeping function
    VTINLINE void Sleep(unsigned time_ms)
    {
        #if defined(WIN32)
            ::Sleep(time_ms);
        #else
            usleep(time_ms*1000);
        #endif
    }

    // Cross-platform critical section implementation
    class CCriticalSection
    {
    public:
        // sleep_ms: shortest period while thread sleeps waiting for the section to unlock
        // maxLock: number of attempts to lock section for tryEnter()
        // entAtt: number of attempts to tryEnter() for enter(); not used with VT_USE_WINCRITICALSECTION
        CCriticalSection(unsigned sleep_ms=1, unsigned maxLock=100, unsigned entAtt=100);
        ~CCriticalSection();

        // Try to enter into critical section; if maxLock attempts are
        // failed, returns false.
        bool tryEnter();

        // Knock into critical section using tryEnter() enterAttempts times, then
        // if VT_EXCEPTIONS is enabled fire an exception, otherwise exit().
        // If compiled with USE_CRITCALSECION, simply calls EnterCriticalSection(),
        // which fires EXCEPTION_POSSIBLE_DEADLOCK in case of error.
        void enter();
        void lock(){ enter(); }

        // Leave critical section.
        void leave();
        void unlock(){ leave(); }

        // Reset lock count.
        #if !defined(VT_USE_WINCRITICALSECTION) && !defined(VT_USE_PTHREADS_SPINLOCK) && !defined(VT_USE_PTHREADS_MUTEX)
            void killLock();
        #endif

    protected:
        static void Sleep(unsigned time_ms){ VT::Sleep(time_ms); }

    private:
        #if defined(VT_USE_WINCRITICALSECTION)
            CRITICAL_SECTION critSect;
        #elif defined(VT_USE_PTHREADS_SPINLOCK)
            pthread_spinlock_t m_spinlock;
        #elif defined(VT_USE_PTHREADS_MUTEX)
            pthread_mutex_t m_mutex;
        #else
            volatile unsigned char theFlag;
        #endif
        volatile unsigned maxLockAttempts, sleepTime, enterAttempts;
    };

    // Auto locking object (section is locked when the object is created and
    // unlocked when the object is destroyed).
    // Note that it's possible to pass a NULL pointer to the locker.
    class CCSLock
    {
    public:
        CCSLock(CCriticalSection *csect): critSect(csect){ if(critSect) critSect->enter(); }
        ~CCSLock(){ if(critSect) critSect->leave(); }
        void Disconnect(){ if(critSect){ critSect->leave(); critSect=NULL; }; }
    protected:
        CCriticalSection *critSect;
    };

    // Global auto locking object (all "globally critical" sections are locked
    // when the object is created and unlocked when the object is destroyed).
    class CGlobalCSLocker
    {
    public:
        CGlobalCSLocker();
        ~CGlobalCSLocker();
    };

    // Use this macro as the most simple way to lock a piece of code.
    // Note 1: when you use it in a method, it will lock it for all objects
    // of the type, because it uses static CCriticalSection.
    // If you need to lock the method for just a specific object,
    // add a CCriticalSection into its members, and CCSLock
    // into the method.
    // Note 2: it's highly recommended to have try..catch statement
    // handling all code from VT_LOCK_SECTION() to the end of the {...}
    // block, otherwise in some situations the critical section object
    // is not destroyed and the section stays locked.
    #define VT_LOCK_SECTION \
        static CCriticalSection VT_BlockCS_; \
        CCSLock VT_BlockLock_(&VT_BlockCS_);


    class CThread;
    void CThreadStaticProcessEntry( CThread* object );

    //
    // Thread object prototype.
    // The main idea of the interface is basically requisitioned from Borland:
    // very clean and simple!
    //
    // All bool functions (except "Is...()") return false to indicate error.
    //
    class CThread
    {
    public:
        // System independent thread priority constants -
        // casted to system's priority / niceness.
        // Priorities higher than normal may be unavailable
        // to ordinary users under some systems (e.g. Linux).
        enum TPriority{
            kIdlePriority,          // Thread works only when system has nothing else to do
            kLowPriority,           // Low priority
            kLowerPriority,         // Slightly lower than normal
            kNormalPriority,        // Normal
            kHigherPriority,        // Slightly higher than normal
            kHighPriority,          // High
            kRealTimePriority,      // I want it all!!
            kUnknownPriority        // Can't tell you
        };

        // This is the default thread timeout.
        // See also: SetThreadDestructorTimeoutMs().
        static const long kDefaultThreadTimeoutMs = 10000;

        CThread( bool createSuspended, TPriority priority = kNormalPriority, bool deleteSelfWhenDone = false );
        virtual ~CThread();

        // Get/set process priority.
        TPriority GetPriority() const { return m_Priority; }
        // If forceUpdate is true, always does system call to change thread priority.
        // if forceUpdate is false, doesn't do the system call if m_Priority==priority.
        bool SetPriority( TPriority priority, bool forceUpdate = true );

        // Get/set destructor's thread timeout. If object
        // is destroyed while thread is still running, it gives
        // to thread the spectified amount of time to finish,
        // then kills it. If the time value is negative, then
        // the thread is killed immediately.
        long GetThreadDestructorTimeoutMs() const { return m_iThreadTimeoutMs; }
        void SetThreadDestructorTimeoutMs( long time ){ m_iThreadTimeoutMs = time; }

        // Returns true if thread exists (it may be either
        // running or suspended)
        bool IsWorking() const { return m_bWorking; }

        // Returns true if the thread can/has been suspended
        bool CanBeSuspended() const;
        bool IsSuspended() const { return m_bSuspended; }

        // Returns true if process has run at least once and finished.
        // When process restarted again, the flag is reset to false.
        bool IsFinished() const { return m_bFinished; }

        #if defined(VT_ALLOW_THREAD_PAUSING)
            // If the thread is running, it will be put into sleep.
            // It can be awaken by Resume() later.
            // !!! WARNING: !!! This function is implemented only for Windows!!!
            // So, avoid using it for now (make threads pause by themselves).
            virtual bool Suspend();
        #endif

        // If the thread is suspended, it is awaken.
        // It it was not started yet, or already finished, it is started.
        // NOTE 1: Killed threads cannot resume!
        // NOTE 2: With pthreads, you can only resume a thread if
        // it was not started yet (i.e. just created with createSuspended==true).
        virtual bool Resume();

        // Resume the thread if it was put into sleep.
        virtual bool ResumeSuspended();

        // Start thread only if it was not started yet.
        virtual bool Start();

        // Wait until the process finishes, if it's working.
        // If msec is negative, then waits "forever".
        bool Wait( long msec = -1 );

        // Sets "terminated" flag which must be periodically
        // checked by the thread. Used for graceful stopping
        // of the thread.
        virtual void Terminate();
        bool IsTerminated() const { return m_bTerminated; }

        // Sets "terminated" flag and then waits for process
        // to end in msec time (if msec = -1, then waits forever).
        bool TerminateAndWait( long msec = -1 );

        // Destroys thread using system thread killing.
        // If allowToFinish_ms is positive, allows thread
        // the time to finish gracefully using TerminateAndWait().
        // WARNING: If process failed to exit gracefully, it may
        // leave all used resources occupied, turn into zombie,
        // and so on. DO NOT USE THIS FUNCTION unless on program
        // urgent termination or something like that.
        virtual bool Kill( long allowToFinish_ms = -1 );

        // Object can delete itself after the process ended,
        // if this flag is set. Use it only for objects created
        // on heap of course. Create thread in suspended state
        // and set the flag before resuming it, otherwise
        // if the thread may finish before you set the flag
        // and won't destroy self.
        bool GetFreeOnFinish() const { return m_bFreeOnFinish; }
        void SetFreeOnFinish( bool freeOnFinish ){ m_bFreeOnFinish = freeOnFinish; }

        //
        // This is the only function which _must_ be overriden in any descendant
        // class; it contains the process which runs in the thread.
        // During the process, IsTerminated() should be checked periodically,
        // and if that returns true, then the process should consider
        // immediate exit.
        //
        virtual void Process() = 0;

        LPCVTCHAR GetThreadName() const { return m_ThreadName; }
        void SetThreadName( LPCVTCHAR str );

    protected:
        static const size_t kThreadNameSize = 32;

        // Locks most thread control functions, so they can
        // be called from different threads without any
        // additional critical sections. Also it may be used
        // by descendant classes to lock access to process data.
        CCriticalSection m_ThreadControlCS;

        // Just a handy wrapper.
        static void Sleep( unsigned time_ms ){ VT::Sleep(time_ms); }

        // Sleep for long period of time, periodically checking
        // for termination flag; if this function returns false
        // the thread should exit.
        bool LongSleep( unsigned time_ms, unsigned period = 5 ) const;

        // Function to change process priority - should be called
        // from the thread only. Sets priority according to m_Priority.
        bool UpdatePriority();

        // Checks if priority change is requested and updates
        // it if necessary. Should be called from the thread.
        void CheckUpdatePriority();

    private:
        VTCHAR m_ThreadName[ kThreadNameSize ];
        #if defined(VT_WINDOWS)
            volatile HANDLE m_Handle; // Windows thread handle
            DWORD m_dwThreadId; // Windows thread ID (stored but not used).
        #elif defined(PTHREADS)
            pthread_attr_t m_pthreadAttr;
            pthread_t m_pthread;
        #else
            ???
        #endif
        volatile TPriority m_Priority, m_DesiredPriority;
        volatile long m_iThreadTimeoutMs; // Used only by destructor
        volatile bool
            m_bWorking,
            m_bTerminated,
            m_bFinished,
            m_bFreeOnFinish,
            m_bDestroyed,
            m_bSuspended,
            m_bKilled;
        void ProcessEntry( bool *deleteMe );
        friend void VT::CThreadStaticProcessEntry( CThread* object ); // Entry point wrapped
    };



    class CTimerListener
    {
    public:
        virtual ~CTimerListener(){}

        virtual void onVTTimer( int tag )=0;
    };


    //
    // Simple timer which invokes listener's onTimer() periodically.
    //
    // NOTE 1: As the function is called from a separate thread
    // it must take care about thread locking if necessary.
    //
    // NOTE 2: The timer is not so precise so do not rely on that
    // onTimer() calls would occur at exactly specified intervals.
    //
    class CThreadSleepingTimer: protected CThread
    {
    public:
        CThreadSleepingTimer( CTimerListener* listener, int tag, unsigned intervalMs, bool runNow = true,
            TPriority priority = kNormalPriority );
        virtual ~CThreadSleepingTimer();

        bool IsEnabled() const { return m_Enabled; }
        void SetEnabled( bool e );

    protected:
        CTimerListener* m_Listener;
        volatile int m_Tag;
        volatile unsigned m_Interval;
        volatile bool m_Enabled, m_Kicked;

        virtual void Process();
    };





} // namespace

#endif
