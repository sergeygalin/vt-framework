//-----------------------------------------------------------------------------
//
// A cross-platform threading library, includes critical section
// locks and threads.
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include "vt_threading.hpp"
#include "vt_exceptions.hpp"
#include "vt_assert.hpp" // Last include!

#if !defined(_CRT_SECURE_NO_DEPRECATE)
    #define _CRT_SECURE_NO_DEPRECATE 1
#endif

#if defined(_MSC_VER)
    #pragma warning( disable: 4996 )
#endif

using namespace VT;

CCriticalSection::CCriticalSection(unsigned sleep_ms, unsigned maxLock, unsigned entAtt):
    #if defined(VT_USE_PTHREADS_SPINLOCK)
        m_spinlock(0),
    #endif
    maxLockAttempts(maxLock),
    sleepTime(sleep_ms),
    enterAttempts(entAtt)
{
    #ifdef VT_EXCEPTIONS
        if(!sleep_ms || !maxLock || !entAtt){
            VTDEBUGMSGT("Bad critical section initialization parameters.");
            throw CException("Bad critical section initialization parameters.");
        }
    #endif

    #if defined(VT_USE_WINCRITICALSECTION)
        InitializeCriticalSection(&critSect);
    #elif defined(VT_USE_PTHREADS_SPINLOCK)
        // The second parameters means: 1 - can be shared
        // with other processes, 0 - can't be shared.
        pthread_spin_init( &m_spinlock, 1 );
    #elif defined(VT_USE_PTHREADS_MUTEX)
        //pthread_mutexattr_t
        int result = pthread_mutex_init( &m_mutex, NULL );
        if( result!=0 ){
            printf( "Mutex initialization failure, code: %d\n", result );
            fflush(stdout);
            throw VT::CMemoryException( VTT("Mutex initialization failed.") );
        }
    #else
        theFlag = 0;
    #endif
}

CCriticalSection::~CCriticalSection()
{
    #ifdef VT_USE_WINCRITICALSECTION
        DeleteCriticalSection(&critSect);
    #elif defined(VT_US_PTHREADS_MUTEX)
        int result = pthread_mutex_destroy( &m_mutex );
        VTASSERT( result==0 )
    #endif
}

bool CCriticalSection::tryEnter()
{
    #if defined(VT_USE_WINCRITICALSECTION)
        unsigned cnt=0;
        while(cnt++ < maxLockAttempts){
            if( TryEnterCriticalSection(&critSect) )
                return true;
            VT::Sleep(sleepTime);
        }
        return false;
    #elif defined(VT_USE_PTHREADS_SPINLOCK)
        unsigned cnt=0;
        while(cnt++ < maxLockAttempts){
            if( pthread_spin_trylock(&m_spinlock) )
                return true;
            VT::Sleep(sleepTime);
        }
        return false;
    #elif defined(VT_USE_PTHREADS_MUTEX)
        unsigned cnt=0;
        while(cnt++ < maxLockAttempts){
            if( pthread_mutex_trylock(&m_mutex) )
                return true;
            VT::Sleep(sleepTime);
        }
        return false;
    #else
        unsigned cnt=0;
        theFlag++;
        while(theFlag>1){ // Someone else also increased theFlag
            if(++cnt > maxLockAttempts){ // Give up
                theFlag--;
                return false;
            }
            VT::Sleep(sleepTime);
        }
        return true;
    #endif
}

void CCriticalSection::enter()
{
    #if defined(VT_USE_WINCRITICALSECTION)
        EnterCriticalSection(&critSect);
    #elif defined(VT_USE_PTHREADS_SPINLOCK)
        pthread_spin_lock( &m_spinlock );
    #elif defined(VT_USE_PTHREADS_MUTEX)
        //printf("Entering CS..."); fflush(stdout);
        int result = pthread_mutex_lock( &m_mutex );
        if( result != 0 ){
            printf( "Mutex locking failure, code: %d\n", result );
            fflush(stdout);
            throw CException(VTT("Failed to lock a mutex."));
        }
    #else
        for(int i=0; i<enterAttempts; i++)
            if(tryEnter())
                return;
        throw CException(VTT("Failed to obtain a critical section lock."));
        exit(EXIT_FAILURE);
    #endif
}

void CCriticalSection::leave()
{
    #if defined(VT_USE_WINCRITICALSECTION)
        LeaveCriticalSection(&critSect);
    #elif defined(VT_USE_PTHREADS_SPINLOCK)
        pthread_spin_unlock( &m_spinlock );
    #elif defined(VT_USE_PTHREADS_MUTEX)
        int result = pthread_mutex_unlock( &m_mutex );
        if( result != 0 ){
            printf( "Mutex unlocking failure, code: %d\n", result );
            fflush(stdout);
            throw CException(VTT("Failed to unlock a mutex."));
        }
    #else
        if( theFlag )
            theFlag--;
    #endif
}

#if !defined(VT_USE_WINCRITICALSECTION) && !defined(VT_USE_PTHREADS_SPINLOCK) && !defined(VT_USE_PTHREADS_MUTEX)
void CCriticalSection::killLock()
{
    theFlag=0;
}
#endif

static CCriticalSection VTGlobalCriticalSection;

CGlobalCSLocker::CGlobalCSLocker()
{
    VTGlobalCriticalSection.enter();
}

CGlobalCSLocker::~CGlobalCSLocker()
{
    VTGlobalCriticalSection.leave();
}















CThread::CThread( bool createSuspended, TPriority priority, bool deleteSelfWhenDone ):
    #if defined(VT_WINDOWS)
        m_Handle( NULL ),
        m_dwThreadId( 0 ),
    #endif
    m_Priority( priority ),
    m_DesiredPriority( priority ),
    m_iThreadTimeoutMs( kDefaultThreadTimeoutMs ),
    m_bWorking( false ),
    m_bTerminated( false ),
    m_bFinished( false ),
    m_bFreeOnFinish( deleteSelfWhenDone ),
    m_bDestroyed( false ),
    m_bSuspended( true ),
    m_bKilled( false )
{
    VTSTRNCPY( m_ThreadName, VTT("[UnnamedThread]"), kThreadNameSize-1 );
    if( !createSuspended )
        Resume();
}

CThread::~CThread()
{
    #if defined(VT_CONSOLEDEBUG)
        VTPRINTF(VTT("~CThread(): %s is destructing...\n"), GetThreadName() );
    #endif
    try{
        if( this==NULL ){
            VTDEBUGMSGT("CThread destructor has been called with NULL 'this' pointer.");
            return;
        }

        // !!!!!!!!!!!!! shouldn't this be done AFTER the next check?
        #if defined(PTHREADS)
            pthread_attr_destroy( &m_pthreadAttr );
        #endif

        if( !m_bDestroyed ){
            m_bDestroyed = true;
            if( m_bWorking && !m_bKilled )
                Kill( m_iThreadTimeoutMs );
        }
    }VT_DEBUGCATCH(;);
    #if defined(VT_CONSOLEDEBUG)
        VTPRINTF(VTT("~CThread(): %s is destructed...\n"), GetThreadName() );
    #endif

}

void CThread::SetThreadName( LPCVTCHAR str )
{
    CCSLock lock( &m_ThreadControlCS );
    try{
        VTSTRNCPY( m_ThreadName, str, kThreadNameSize-1 );
    }VT_DEBUGCATCH(;);
}

bool CThread::SetPriority( TPriority priority, bool forceUpdate )
{
    CCSLock lock( &m_ThreadControlCS );
    try{
        if( m_bKilled )
            return false;
        if( !forceUpdate )
            if( m_Priority==priority )
                return false;
        m_Priority = priority;
        #if defined( VT_WINDOWS )
            m_DesiredPriority = m_Priority;
            if( m_Handle!=NULL ){
                int priority = THREAD_PRIORITY_NORMAL;
                switch( m_Priority ){
                    case kIdlePriority:        priority = THREAD_PRIORITY_IDLE;            break;
                    case kLowPriority:        priority = THREAD_PRIORITY_LOWEST;            break;
                    case kLowerPriority:    priority = THREAD_PRIORITY_BELOW_NORMAL;    break;
                    case kHigherPriority:    priority = THREAD_PRIORITY_ABOVE_NORMAL;    break;
                    case kHighPriority:        priority = THREAD_PRIORITY_HIGHEST;            break;
                    case kRealTimePriority:    priority = THREAD_PRIORITY_TIME_CRITICAL;    break;
                }
                return SetThreadPriority( m_Handle, priority )!=0;
            }else
                return true;
        #elif defined( PTHREADS )
            // Priority can be changed in thread.
            m_DesiredPriority = priority;
            return true;
        #else
            return false;
        #endif
    }VT_DEBUGCATCH(;);
    return false;
}

bool CThread::CanBeSuspended() const
{
    #if defined( PTHREADS )
        return false;
    #else
        return IsWorking();
    #endif
}

#if defined(VT_ALLOW_THREAD_SUSPENDING)
    bool CThread::Suspend()
    {
        CCSLock lock( &m_ThreadControlCS );
        try{
            if( m_bKilled )
                return false;
            if( !m_bSuspended ){
                bool ret = false;
                #if defined( VT_WINDOWS )
                    if(    m_Handle!=NULL )
                        ret = SuspendThread( m_Handle )!=DWORD(-1);

                #elif defined( PTHREADS )
                    #warning "Thread pausing not implemented for PTHREADS."
                    VTDEBUGMSGT("Thread pausing not implemented for PTHREADS.");
                    return false;
                #else
                    return false;
                #endif
                if( ret )
                    m_bSuspended = true;
                return ret;
            }else
                return true; // Already suspended
        }VT_DEBUGCATCH(;);
        return false;
    }
#endif


#if defined( PTHREADS )
    static void PTHREADSDeleteThread( void *thread )
    {
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("************* PTHREADSDeleteThread(): called for %s. *******************\n"),
                ((VT::CThread*)thread)->GetThreadName() );
        #endif
        delete (VT::CThread*)thread;
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("************* PTHREADSDeleteThread(): R.I.P., %s. *******************\n"),
                ((VT::CThread*)thread)->GetThreadName() );
        #endif
    }
#endif

void VT::CThreadStaticProcessEntry( CThread* object )
{
    VTASSERT( object!=NULL );
    bool deleteIt = false;

    // Launching the thread
    object->ProcessEntry( &deleteIt );

    // Did the thread asked to delete its object?
    if( deleteIt ){
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("CThreadStaticProcessEntry(): %s asked to delete him.\n"),
                object->GetThreadName() );
        #endif
        VTASSERT( !object->IsWorking() );
        object->m_bKilled = true;

        #if defined( VT_WINDOWS )
            delete object;
        #elif defined( PTHREADS )
            pthread_cleanup_push( PTHREADSDeleteThread, (void*)object );
            pthread_cancel( object->m_pthread );
            pthread_cleanup_pop( true );
        #else
            ???
        #endif

        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("CThreadStaticProcessEntry(): %s has been deleted.\n"),
                object->GetThreadName() );
        #endif
    }
}

#if defined( VT_WINDOWS )

    static DWORD WINAPI WINAPIProcessEntry( LPVOID object )
    {
        VT::CThreadStaticProcessEntry( (CThread*)object );
        ExitThread( 0 );
        return 0;
    }

#elif defined(PTHREADS)

    static void *PTHREADSProcessEntry( void *object )
    {
        VT::CThreadStaticProcessEntry( (CThread*)object );
        return NULL;
    }

#endif

bool CThread::Resume()
{
    CCSLock lock( &m_ThreadControlCS );
    try{
        if( m_bDestroyed || m_bKilled )
            return false;
        if( IsWorking() ){
            if( m_bSuspended ){
                //
                // Awaking suspeneded thread
                //
                bool ret = false;
                 #if defined( VT_WINDOWS )
                    if( m_Handle!=NULL )
                        ret = ResumeThread( m_Handle )!=DWORD(-1);

                #elif defined( PTHREADS )
                    VTDEBUGMSGT("Resuming is not implemented for PTHREADS yet.");
                #else
                    return false;
                #endif
                if( ret )
                    m_bSuspended = false;
                return ret;
            }else
                return true; // Already working and not suspended
        }else{

            //
            // The thread was not created, start it
            //
            #if defined( VT_WINDOWS )
                VTASSERT( m_Handle==NULL );
                SECURITY_ATTRIBUTES attr;
                memset( &attr, 0, sizeof(attr) );
                attr.nLength = sizeof(SECURITY_ATTRIBUTES);
                attr.lpSecurityDescriptor = NULL; // !!!!!!!!!!
                attr.bInheritHandle = false;
                m_Handle = CreateThread(
                     &attr,                    // No security attributes
                     0,                        // Default stack size
                     WINAPIProcessEntry,    // Entry point
                     this,                    // Pointer to the object for StaticProcessEntry
                     CREATE_SUSPENDED,        // Flags
                     &m_dwThreadId            // Thread ID
                );
                if( m_Handle==NULL )
                    return false;
                // Setting process priority
                SetPriority( m_Priority );
                // Resuming
                bool ret = ResumeThread( m_Handle )!=DWORD(-1);
                m_bSuspended = false;
                m_bTerminated = false;
                m_bFinished = false;
                return ret;
            #elif defined( PTHREADS )
                  pthread_attr_init( &m_pthreadAttr );
                  pthread_create( &m_pthread, NULL, PTHREADSProcessEntry, this );
            #else
                return false;
            #endif
        }
    }VT_DEBUGCATCH(;);
    return false;
}

bool CThread::ResumeSuspended()
{
    if( IsSuspended() )
        return Resume();
    else
        return IsWorking();
}

bool CThread::Start()
{
    if( !IsFinished() )
        return Resume();
    else
        return IsWorking();
}

bool CThread::Wait( long msec )
{
    // This function doesn't block, as it may be called
    // from different threads at once (and without any problems,
    // unless the object is destroyed during waiting).
    try{
        if( msec>0 ){
            // Waiting msec max
            long time = 0;
            while( time<msec ){
                if( !IsWorking() )
                    return true;
                #if defined(VT_CONSOLEDEBUG)
                    VTPRINTF(VTT(". "));
                #endif
                VT::Sleep( 10 );
                time += 10;
            }
        }else{
            // Waiting for Christmas
            for(;;){
                if( !IsWorking() )
                    return true;
                #if defined(VT_CONSOLEDEBUG)
                    VTPRINTF(VTT(". "));
                #endif
                VT::Sleep( 10 );
            }
        }
        return !IsWorking();
    }VT_DEBUGCATCH(;);
    return false;
}

void CThread::Terminate()
{
    m_bTerminated = true;
}

bool CThread::TerminateAndWait( long msec )
{
    // This function doesn't block, as it may be called
    // from different threads at once (and without any problems).
    try{
        if( IsWorking() ){
            Terminate(); // Signalling the process to stop ASAP
            #if defined(VT_CONSOLEDEBUG)
                VTPRINTF(VTT("CThread::TerminateAndWait(): Waiting for %s to finish...\n"),
                    GetThreadName() );
            #endif
            bool ret = Wait( msec  ); // Wating for requested time
            #if defined(VT_CONSOLEDEBUG)
                VTPRINTF(VTT("CThread::TerminateAndWait(): %s finished.\n"),
                    GetThreadName() );
            #endif
            return ret;
        }else
            return true; // Already (or yet) not working
    }VT_DEBUGCATCH(;);
    return false;
}

bool CThread::Kill( long allowToFinish_ms )
{
    try{
        if( m_bKilled )
            return false;
        m_bKilled = true;
        if( IsWorking() ){

            // Gracefully ask it to stop
            Terminate();
            if( allowToFinish_ms>0 )
                Wait( allowToFinish_ms );

            if( IsWorking() ){ // Still not finished?!
                // Good words have ended. Prepare to be punished, bad thread!

                bool ret = false;

                // Terminating thread using appropriate system call
                #if defined( VT_WINDOWS )
                    if( m_Handle!=NULL ){
                        ret = TerminateThread( m_Handle, 0 )!=0;
                        VTASSERT2T( ret, "TerminateThread() failed." );
                    }
                    if( ret ){
                        // The process was mercilessly killed.
                        // Let's handle all status vars here - probably it was
                        // unable to update them.
                        m_bWorking = false;
                        m_bFinished = true;
                        m_bTerminated = true;
                        #if defined( VT_WINDOWS )
                            m_Handle = 0;
                        #endif
                        if( m_bFreeOnFinish ){
                            m_bFreeOnFinish = false;
                            m_bDestroyed = true;
                            delete this;
                            return true; // Return immediately
                        }
                    }
                #elif defined( PTHREADS )
                    m_bWorking = false;
                    m_bFinished = true;
                    m_bTerminated = true;
                    if( m_bFreeOnFinish ){
                        m_bDestroyed = true;
                        pthread_cleanup_push( PTHREADSDeleteThread, (void*)this );
                        pthread_cancel( m_pthread );
                        pthread_cleanup_pop( true );
                    }else{
                        pthread_cancel( m_pthread );
                    }
                #else
                    ???
                #endif


                return ret;
            }else
                return true; // Thread gracefully ended via Terminate().
        }else
            return true;
    }VT_DEBUGCATCH(;);
    return false;
}


bool CThread::LongSleep( unsigned time_ms, unsigned period ) const
{
    while( time_ms > period ){
        if( IsTerminated() )
            return false;
        Sleep( period );
        time_ms -= period;
    }
    if( time_ms ){
        if( IsTerminated() )
            return false;
        Sleep( time_ms );
    }
    return true;
}

bool CThread::UpdatePriority()
{
    #if defined( PTHREADS )
        // Setting thread priority here
        int niceness = 0;
        switch( m_Priority ){
            case kIdlePriority:     niceness =  19; break;
            case kLowPriority:      niceness =  10; break;
            case kLowerPriority:    niceness =   5; break;
            case kHigherPriority:   niceness =  -5; break;
            case kHighPriority:     niceness = -10; break;
            case kRealTimePriority: niceness = -19; break;
            default:
                niceness = 0;
        }
        #if defined(VT_CONSOLEDEBUG)
            VTPRINTF(VTT("CThread::ProcessEntry(): Renicing %s to %d.\n"),
                GetThreadName(), niceness );
        #endif
        int gotniceness= nice( niceness );
        m_DesiredPriority = m_Priority;
        return (gotniceness==0) || (gotniceness==niceness);
    #else
        return SetPriority( m_Priority ); // Can use public function
    #endif
}

void CThread::CheckUpdatePriority()
{
    if( m_DesiredPriority != m_Priority ){
        m_Priority = m_DesiredPriority;
        UpdatePriority();
    }
}

void CThread::ProcessEntry( bool *deleteMe )
{
    {
        CCSLock locker1( &m_ThreadControlCS );
        if( deleteMe!=NULL ) // Should not happen, but...
            *deleteMe = m_bFreeOnFinish;
        m_bWorking = true;
        m_bTerminated = false;
        m_bFinished = false;

        #if defined( PTHREADS )
            UpdatePriority();
        #endif
    }

    Process();

    {
        CCSLock locker2( &m_ThreadControlCS );
        try{
            m_bWorking = false;
            m_bFinished = true;
            if( deleteMe!=NULL ) // Should not happen, but...
                *deleteMe = m_bFreeOnFinish; // Updating the variable - just in case...
            #if defined( VT_WINDOWS )
                m_Handle = NULL;
            #endif
        }VT_DEBUGCATCH(;);
    }
}





CThreadSleepingTimer::CThreadSleepingTimer( CTimerListener* listener, int tag, unsigned intervalMs,
                                           bool runNow, TPriority priority ):
    CThread( true, priority ),
    m_Listener( listener ),
    m_Tag( tag ),
    m_Interval( intervalMs ),
    m_Enabled( runNow ),
    m_Kicked( false )
{
    Resume();
}

CThreadSleepingTimer::~CThreadSleepingTimer()
{
    SetEnabled( false );
    TerminateAndWait();
}

void CThreadSleepingTimer::SetEnabled( bool e )
{
    if( e != m_Enabled ){
        m_Kicked = true;
        m_Enabled = e;
    }
}

void CThreadSleepingTimer::Process()
{
    while( !IsTerminated() ){

        //
        // Long waiting cycle
        //
        unsigned toWait = m_Interval;
        while( toWait ){
            if( IsTerminated() )
                return;
            if( m_Kicked )
                break;
            Sleep( 1 );
            toWait--;
        }
        if( m_Kicked ){
            m_Kicked = false;
            continue;
        }

        if( m_Enabled && m_Listener != NULL )
            m_Listener->onVTTimer( m_Tag );

    }
}
