//-----------------------------------------------------------------------------
//
// A CSV (comma-separated values spreadsheet) parser
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2008 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#ifndef VT_CSV_H
#define VT_CSV_H

#include "vt_file.hpp"
#include "vt_string.hpp"

namespace VT {

    //
    // CSV spreadsheet parser.
    //
    class CSimpleSpreadsheet: public CArray<CStringArray>
    {
    public:

        // Default MS Excel's column delimiter for CSV files is ';'.
        // Please do not change the default!
        static const VTCHAR kDefaultDelimiter = VTT(';');

        CSimpleSpreadsheet();
        virtual ~CSimpleSpreadsheet();

        virtual LPCVTCHAR GetClassName() const { return VTT("CSimpleSpreadsheet"); }

        // Get / set column delimiter (do it before calling any other functions).
        virtual VTCHAR GetDelimiter() const { return m_cDelimiter; }
        void SetDelimiter(VTCHAR delim){ m_cDelimiter = delim; }

        // Split line which starts at 'lineStart' into an array of cells ('row')
        // using 'delimiter'. Automatically handles quoted cells, the quotes
        // are removed from output cell values.
        static LPCVTCHAR ParseCSVLine( LPCVTCHAR lineStart, CArray<CString>& row, VTCHAR delimiter=0 );

        // Parses a text file loaded into 'text' and fills itself
        // with cells of the spreadhseet.
        void ParseCSVLines( LPCVTCHAR text );

        // Returns index of a column with specified header. If the header
        // was not found, returns negative value.
        int FindHeader(LPCVTCHAR text, bool bCaseSensitive );

        // Loads text from the file parses it. Returns false on error.
        bool Load( CFile *file, bool utf8 = true );
    protected:
        VTCHAR m_cDelimiter;
    };

} // namespace

#endif


