//-----------------------------------------------------------------------------
//
// Ini file support.
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2007 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/


#include <ctype.h>
#include "vt_inifile.hpp"
#include "vt_localization.hpp"
#include "vt_fileutils.hpp"

using namespace VT;

#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_DEPRECATE)
    #define _CRT_SECURE_NO_DEPRECATE
#endif

CIniFile::CIniFile():
    CStringArray(),
    m_iLineCursor(-1),
    m_PrevUTF8( true ),
    m_PrevFileName()
{
}

CIniFile::~CIniFile()
{
}

bool CIniFile::Load( CFile *file )
{
    return Load( file, false );
}

bool CIniFile::Load( CFile *file, bool utf8 )
{
    m_PrevUTF8 = utf8;
    if( !CStringArray::Load( file, utf8 ) )
        return false;
    TrimAll();
    m_iLineCursor = 0;
    return true;
}

bool CIniFile::Load( LPCVTCHAR fileName )
{
    return Load( fileName, false );
}

bool CIniFile::Load( LPCVTCHAR fileName, bool utf8 )
{
    m_PrevFileName.CopyFrom( fileName );
    CDiskFile file( fileName, kOpenForReading );
    if( !file.IsOK() )
        return false;
    return Load( &file, utf8 );
}

bool CIniFile::Load( LPCVTCHAR vendor, LPCVTCHAR appName, LPCVTCHAR fileName, bool systemWide, bool utf8 )
{
    VT::CString fn( GetApplicationDataDir( vendor, appName, systemWide ) );
    fn << fileName;
    return Load( fn, utf8 );
}

bool CIniFile::Load()
{
    if( m_PrevFileName.IsEmpty() )
        return false;
    return Load( m_PrevFileName, m_PrevUTF8 );
}

bool CIniFile::Save( CFile *file )
{
    return Save( file, false );
}

bool CIniFile::Save( LPCVTCHAR fileName )
{
    return Save( fileName, false );
}

bool CIniFile::Save( CFile *file, bool utf8 )
{
    m_PrevUTF8 = utf8;
    if( !file->IsOK() )
        return false;
    CString out;
    ToText( &out );
    out.TrimRight();
    out << VTENDLT;
    // return file->write( out.Str(), out.Length()*sizeof(VTCHAR) );
    return out.Save( file, utf8 );
}

bool CIniFile::Save( LPCVTCHAR fileName, bool utf8 )
{
    m_PrevFileName.CopyFrom( fileName );
    CDiskFile file( fileName, kOpenForWriting );
    if( !file.IsOK() )
        return false;
    return Save( &file, utf8 );
}

bool CIniFile::Save( LPCVTCHAR vendor, LPCVTCHAR appName, LPCVTCHAR fileName, bool systemWide, bool utf8 )
{
    VT::CString fn( GetApplicationDataDir( vendor, appName, systemWide ) );
    VT::MkDirP( fn );
    fn << fileName;
    return Save( fn, utf8 );
}

bool CIniFile::Save()
{
    if( m_PrevFileName.IsEmpty() )
        return false;
    return Save( m_PrevFileName, m_PrevUTF8 );
}


int CIniFile::FindNext( LPCVTCHAR str, bool bCaseSensitive )
{
    return Find( str, bCaseSensitive, m_iLineCursor+1 );
}

int CIniFile::FindSection(LPCVTCHAR str, bool bCaseSensitive, size_t startIndex)
{
    CString s;
    s<<VTT("[")<<str<<VTT("]");
    return Find( s.Str(), bCaseSensitive, startIndex );
}

int CIniFile::FindNextSection( LPCVTCHAR str, bool bCaseSensitive )
{
    return FindSection( str, bCaseSensitive, m_iLineCursor+1 );
}

bool CIniFile::GoToSection( LPCVTCHAR str, bool bCaseSensitive ){
    return GoTo( FindSection(str, bCaseSensitive) )>=0;
}

int CIniFile::FindNextValue(LPCVTCHAR key, bool bCaseSensitive, LPCVTCHAR *valuePosition)
{
    int kl = int(VTSTRLEN( key ));
    for( int i=m_iLineCursor+1; i<int(getSize()); i++ ){
        VTCHAR first = m_pData[i].FirstNonspaceChar();

        // Skipping comments (this may be an extra measure actually -
        // the alghorithm would anyway skip any comments without problems).
        if( first==VTT('\'') || first==VTT('#') || first==VTT(';') )
            continue;

        // Checking if the line is a section header. If it is so,
        // then the search is failed.
        if( first==VTT('[') )
            break;

        if( !m_pData[i].Compare(key, bCaseSensitive, kl ) ){ // Beginning of the line matches the key
            // Check if the key is followed by "[spaces]="
            for( int ch=kl; ; ch++ ){
                const VTCHAR *psymbol = m_pData[i].Str()+ch;
                if( !(*psymbol) ) // '\0' (end of the string)
                    break;
                if( (*psymbol)==VTT('=') ){ // This is what we need - now return what we have found
                    if( valuePosition!=NULL ){ // Have to return pointer to the value
                        ++psymbol; // Step over '='
                        while( VTISSPACE(*psymbol) && (*psymbol)!=0 ) // Skip spaces before the value
                            psymbol++;
                        *valuePosition = psymbol;
                    }
                    return i;
                }
                if( !VTISSPACE(*psymbol) ) // The key in the line is longer then the key we're looking for
                    break;
            }
        }
    }
    // Didn't find anything
    if( valuePosition!=NULL )
        *valuePosition = NULL;
    return -1;
}


LPCVTCHAR CIniFile::GetStrDef(LPCVTCHAR key, LPCVTCHAR defValue, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return defValue;
    else
        return valuePtr;
}

int CIniFile::GetIntDef(LPCVTCHAR key, int defValue, bool bCaseSensitive)
{
    int ret;
    if( GetInt( key, &ret, bCaseSensitive ) )
        return ret;
    return defValue;
}

long CIniFile::GetLongDef(LPCVTCHAR key, long defValue, bool bCaseSensitive)
{
    long ret;
    if( GetLong( key, &ret, bCaseSensitive ) )
        return ret;
    return defValue;
}

unsigned long CIniFile::GetHexDef(LPCVTCHAR key, unsigned long defValue, bool bCaseSensitive)
{
    unsigned long ret;
    if( GetHex( key, &ret, bCaseSensitive ) )
        return ret;
    return defValue;
}

float CIniFile::GetFloatDef(LPCVTCHAR key, float defValue, bool bCaseSensitive)
{
    float ret;
    if( GetFloat( key, &ret, bCaseSensitive ) )
        return ret;
    return defValue;
}

double CIniFile::GetDoubleDef(LPCVTCHAR key, double defValue, bool bCaseSensitive)
{
    double ret;
    if( GetDouble( key, &ret, bCaseSensitive ) )
        return ret;
    return defValue;
}

bool CIniFile::GetBoolDef(LPCVTCHAR key, bool defValue, bool bCaseSensitive)
{
    bool ret;
    if( GetBool( key, &ret, bCaseSensitive ) )
        return ret;
    return defValue;
}

bool CIniFile::GetInt(LPCVTCHAR key, int *out, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return false;
    if( !(*valuePtr) )
        return false;
    if( VTSSCANF( valuePtr, VTT("%d"), out )!=1 )
        return false;
    return true;
}

bool CIniFile::GetLong(LPCVTCHAR key, long *out, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return false;
    if( !(*valuePtr) )
        return false;
    if( VTSSCANF( valuePtr, VTT("%ld"), out )!=1 )
        return false;
    return true;
}

bool CIniFile::GetHex(LPCVTCHAR key, unsigned long *out, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return false;
    if( !(*valuePtr) )
        return false;
    if( VTSSCANF( valuePtr, VTT("%lx"), out )!=1 )
        return false;
    return true;
}

bool CIniFile::GetFloat(LPCVTCHAR key, float *out, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return false;
    if( !(*valuePtr) )
        return false;
    VT_C_LOCALE( true );
    bool ret = VTSSCANF( valuePtr, VTT("%f"), out )==1;
    VT_RESTORE_LOCALE( true );
    return ret;
}

bool CIniFile::GetDouble(LPCVTCHAR key, double *out, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return false;
    if( !(*valuePtr) )
        return false;
    VT_C_LOCALE( true );
    #if defined(VT_WINDOWS) && defined(_MSC_VER) && defined(VT_UNICODE)
        // %lf is not available for this combinaton
        CString extr( valuePtr );
        CCStrHelper extr_ch = extr.c_str();
        bool ret = sscanf( extr_ch, "%lf", out )==1;
    #else
        bool ret = VTSSCANF( valuePtr, VTT("%lf"), out )==1;
    #endif
    VT_RESTORE_LOCALE( true );
    return ret;
}

bool CIniFile::GetBool(LPCVTCHAR key, bool *out, bool bCaseSensitive)
{
    LPCVTCHAR valuePtr;
    int result = FindNextValue( key, bCaseSensitive, &valuePtr );
    if( result==-1 || valuePtr==NULL )
        return false;
    if( !(*valuePtr) )
        return false;

    // Text values
    CString str( valuePtr );
    str.Trim();
    static const LPCVTCHAR textyes[]={
        VTT("1"),
        VTT("y"),
        VTT("yes"),
        VTT("on"),
        VTT("true"),
        VTT("enable"),
        VTT("enabled"),
        NULL
    };
    static const LPCVTCHAR textno[]={
        VTT("0"),
        VTT("n"),
        VTT("no"),
        VTT("off"),
        VTT("false"),
        VTT("disable"),
        VTT("disabled"),
        NULL
    };
    for( const LPCVTCHAR *p=textyes; *p!=NULL; p++ )
        if( str.CompareCaseInsensitive( *p )==0 ){
            *out = true;
            return true;
        }

    for( const LPCVTCHAR *p2=textno; *p2!=NULL; p2++ )
        if( str.CompareCaseInsensitive( *p2 )==0 ){
            *out = false;
            return true;
        }

    // Boolean as a number (0 == false, anything else == true)
    int num=0;
    if( VTSSCANF( valuePtr, VTT("%d"), &num )!=1 )
        return false;
    *out = (num!=0);
    return true;
}


void CIniFile::InsertSection(LPCVTCHAR sectionName, bool bCaseSensitive)
{
    if( !GoToSection( sectionName, bCaseSensitive ) ){
        if( getSize()>0 ){
            // Adding section at end of the file.
            if( m_pData[getSize()-1].IsEmpty() )
                resize( getSize()+1, true );
            else{
                // Adding an empty line before new section,
                // for better readability.
                resize( getSize()+2, true );
                m_pData[ getSize()-2 ].Empty();
            }
        }else
            resize( 1 );
        m_pData[ getSize()-1 ].Empty()<<VTT("[")<<sectionName<<VTT("]");
        GoTo( int(getSize())-1 );
    }
}


void CIniFile::InsertValue(LPCVTCHAR key, LPCVTCHAR value, bool bCaseSensitive)
{
    if( m_iLineCursor<0 )
        throw CException(VTT("Ini file cursor was not positioned before inserting a value."));
    CString newLine;
    newLine<<key<<VTT("=")<<value;
    int pos = FindNextValue( key, bCaseSensitive, NULL );
    if( pos<0 ){ // Existing key=value pair not found
        InsertAt( m_iLineCursor+1, newLine );
    }else{
        VTASSERT( pos>=0 && pos<int(getSize()) );
        m_pData[ pos ].CopyFrom( newLine );
    }
}


void CIniFile::InsertInt( LPCVTCHAR key, int val, bool bCaseSensitive )
{
    InsertValue( key, CString()<<val, bCaseSensitive );
}

void CIniFile::InsertLong( LPCVTCHAR key, long val, bool bCaseSensitive )
{
    InsertValue( key, CString( val ), bCaseSensitive );
}

void CIniFile::InsertHex(LPCVTCHAR key, unsigned long val, bool bCaseSensitive)
{
    CString tmp;
    tmp.Format( 128, VTT("%lx"), val );
    InsertValue( key, tmp, bCaseSensitive );
}

void CIniFile::InsertFloat( LPCVTCHAR key, float val, bool bCaseSensitive )
{
    InsertDouble( key, val, bCaseSensitive );
}

void CIniFile::InsertDouble( LPCVTCHAR key, double val, bool bCaseSensitive )
{
    CString tmp( 0, 63, 0 );
    tmp.ConcatDouble_MaxPrecision( val, false ); // Using C locale
    InsertValue( key, tmp, bCaseSensitive );
}

// Using "1"/"0" notation, not "yes"/"no" etc., because it is
// more expandable (can be changed to integer later).
void CIniFile::InsertBool( LPCVTCHAR key, bool val, bool bCaseSensitive )
{
    InsertValue( key, ((val)? VTT("1"):VTT("0")), bCaseSensitive );
}


