//-----------------------------------------------------------------------------
//
// Base Classes
//
//-----------------------------------------------------------------------------

/*
    VT Framework License (BSL 1.0)

    Copyright (C) 2006 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#ifndef VT_BASECLASSES_H
#define VT_BASECLASSES_H

#include "vt_portable.hpp"


namespace VT
{

    //
    // A prototype of class which has a name.
    //
    class CNamedClass
    {
    public:
        virtual ~CNamedClass(){}
        virtual LPCVTCHAR GetUIClassName() const;
    };


    //
    // Simple one-event handler, which can also act as a link in a chain
    // which passes the event through it.
    //
    class CSimpleValidator
    {
    public:
        long m_iValidationTag; // Passed to event handler; initialized to -1
        CSimpleValidator(CSimpleValidator* validator=NULL, long tag=-1);
        virtual ~CSimpleValidator(){}
        virtual void OnChange(long tag = -1); // Passes event to the next validator
        virtual void SetValidator( CSimpleValidator* ptr ); // Changes next validator
    protected:
        CSimpleValidator* m_pNextValidator;
    };

    //
    // Progress Display Class
    //
    // NOTE: Advanced implementations of progress display can use
    // Begin Operation / End Operation to display operations "recursively",
    // i.e. reflect that some operations run "inside" other operations.
    // but keep in mind that in case of errors some routines may skip
    // their call to EndOperation. Such situation should be workarounded
    // elsewhere.
    //
    class CProgressDisplay
    {
    public:
        virtual ~CProgressDisplay(){}

        // Show operation initialization message; this also should reset
        // progress bars to zero.
        virtual void PDBeginOperation( LPCVTCHAR statusMessage=NULL );

        // Show operation progress message and update progress bar / % display.
        // percentDone can be negative, which indicates that % done cannot be estimated.
        virtual void PDShowProgress( float percentDone, LPCVTCHAR statusMessage=NULL ) = 0;

        // Show "operation complete" message.
        virtual void PDEndOperation( LPCVTCHAR statusMessage=NULL );
    };

    class CProgressDisplayer: public CProgressDisplay
    {
    public:
        CProgressDisplayer(): m_pDisplayer(NULL){}
        virtual ~CProgressDisplayer(){}

        void SetProgressDisplay( CProgressDisplay *disp ){ m_pDisplayer = disp; }
        CProgressDisplay *GetProgressDisplay(){ return m_pDisplayer; }

        virtual void PDBeginOperation( LPCVTCHAR statusMessage=NULL );
        virtual void PDShowProgress( float percentDone, LPCVTCHAR statusMessage=NULL );
        virtual void PDEndOperation( LPCVTCHAR statusMessage=NULL );
    private:
        CProgressDisplay *m_pDisplayer;
    };

    class CAbortFlagChecker
    {
    public:
        CAbortFlagChecker(): m_pFlag(NULL){}

        void SetAbortFlag( volatile bool *flag ){ m_pFlag = flag; }
        volatile bool *GetAbortFlag(){ return m_pFlag; }

        bool IsAborted() const {
            if( m_pFlag==NULL )
                return false;
            return *m_pFlag;
        }
    private:
        volatile bool *m_pFlag;
    };

    //
    // Quality selector
    //
    // Many algorithms in DSP have a quality setting.
    //
    enum TSimpleQualitySelection
    {
        kQualityLowest, // Fastest mode possible.
        kQualityLow,    // Real-time use for preview and etc.
        kQualityNormal, // Normal quality, used by default.
                        // Suitable both for real-time on good machine, in "monopolistic" mode,
                        // and even for output when quality is not of very high consideration.
        kQualityHigh,    // High quality, suitable for production if not super duper quality required.
        kQualityBest    // Time doesn't matter at all. Use for production.
    };




} // namespace VT

#endif



