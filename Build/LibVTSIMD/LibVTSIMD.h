//-----------------------------------------------------------------------------
//
// LibVTSIMD.hpp
//
// 2009
// Programmer: Sergey A. Galin
//
// This file is a C library interface to vt_simd module.
//
//---------------------------------------------------------------------------

#ifndef LIBLIBSIMD_HPP
#define LIBLIBSIMD_HPP

#if defined(__WIN32__) || defined(WIN32) || defined(WINDOWS) || defined(__WIN32)
    #include <windows.h>
    #define LIBSIMD_API WINAPI
#else
    #define LIBSIMD_API pascal
#endif

#ifdef LIBSIMD_EXPORTS
    #define LIBSIMD_EXP __declspec(dllexport)
#else
    #define LIBSIMD_EXP __declspec(dllimport)
    #if defined(__BORLANDC__)
        #pragma comment(lib, "LibVTSIMD/Release/simd_omf.lib")
    #endif
#endif


#if defined(__cplusplus)
    #define LIBSIMD_EXTC extern "C"
#else
    #define LIBSIMD_EXTC
#endif

// Info
LIBSIMD_EXTC LIBSIMD_EXP const char* LIBSIMD_API LIBSIMD_Info();

// CPU Flags. Although it may be faster to read CPU flags directly
// via CPUID instruction, it's good to have access to them as they
// are in the LIBSIMD DLL.
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetCPUIDSuccess();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetFPUFlag();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetMMXFlag();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetFXSRFlag();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetSSEFlag();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetSSE2Flag();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetSSE3Flag();
LIBSIMD_EXTC LIBSIMD_EXP int LIBSIMD_API LIBSIMD_GetHTTFlag();

// 32-bit vector functions
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_FillVector32( float* x, float fill, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_Convolve32( const float* data1, const float* data2, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_SumVector32( const float* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_VectorMin32( const float* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_VectorMax32( const float* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_VectorMinMax32( const float* x, size_t n, float *outmin, float *outmax  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_Clip32( float* x, size_t n, float min, float max  );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_SumVectorSquares32( const float* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_FillLinear32( float* out, float firstPoint, float step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_LinearFade32( float* out, float k, float step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_AddLinearFade32( float* out, const float *in, float k, float step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP float    LIBSIMD_API LIBSIMD_AddLinearCrossfade32( float* out, const float* fadein, const float* fadeout, float fadeink, float step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_AddVector32( float* out, const float* toadd, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_AddVectorMultiplied32( float* out, const float* toadd, float mul, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyVector32( float* out, const float* mul, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyVectorK32( float* out, const float* mul, float k, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyByScaledAndShiftedVector32( float* out, const float* x, float scale, float shift, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_Multiply32( float* out, float mul, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyAndShift32(float* out, float scale, float shift, size_t n);
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumScaledVectors2_32(float* result, const float* x1, float k1, const float* x2, float k2, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumScaledVectors3_32(float* result, const float* x1, float k1, const float* x2, float k2, const float* x3, float k3, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumScaledVectors4_32(float* result, const float* x1, float k1, const float* x2, float k2, const float* x3, float k3, const float* x4, float k4, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumVectorScaledVectors3_32( float* result, const float* x1, const float* k1, const float* x2, const float* k2, const float* x3, const float* k3, size_t n );

// 64-bit vector functions
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_FillVector64( double* x, double fill, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_Convolve64( const double* data1, const double* data2, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_SumVector64( const double* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_VectorMin64( const double* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_VectorMax64( const double* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_VectorMinMax64( const double* x, size_t n, double *outmin, double *outmax  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_Clip64( double* x, size_t n, double min, double max  );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_SumVectorSquares64( const double* x, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_FillLinear64( double* out, double firstPoint, double step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_LinearFade64( double* out, double k, double step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_AddLinearFade64( double* out, const double *in, double k, double step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP double    LIBSIMD_API LIBSIMD_AddLinearCrossfade64( double* out, const double* fadein, const double* fadeout, double fadeink, double step, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_AddVector64( double* out, const double* toadd, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_AddVectorMultiplied64( double* out, const double* toadd, double mul, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyVector64( double* out, const double* mul, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyVectorK64( double* out, const double* mul, double k, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyByScaledAndShiftedVector64( double* out, const double* x, double scale, double shift, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_Multiply64( double* out, double mul, size_t n  );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_MultiplyAndShift64(double* out, double scale, double shift, size_t n);
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumScaledVectors2_64(double* result, const double* x1, double k1, const double* x2, double k2, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumScaledVectors3_64(double* result, const double* x1, double k1, const double* x2, double k2, const double* x3, double k3, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumScaledVectors4_64(double* result, const double* x1, double k1, const double* x2, double k2, const double* x3, double k3, const double* x4, double k4, size_t n );
LIBSIMD_EXTC LIBSIMD_EXP void    LIBSIMD_API LIBSIMD_SumVectorScaledVectors3_64( double* result, const double* x1, const double* k1, const double* x2, const double* k2, const double* x3, const double* k3, size_t n );

#endif

