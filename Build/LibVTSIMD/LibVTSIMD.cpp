
#include "LibVTSIMD.h"
#include "vt_simd.hpp"

using namespace VT;
using namespace VT::SIMD;

const char* LIBSIMD_API LIBSIMD_Info()
{
    return
        "SIMD Library ver. 1.0, "
        __DATE__
        #if defined(_MSC_VER)
            " (MSC)"
        #elif defined(__GNUC__)
            " (G++)"
        #endif
        ;
}

int LIBSIMD_API LIBSIMD_GetCPUIDSuccess()    { return int(CPU::g_CPUID_Success); }
int LIBSIMD_API LIBSIMD_GetFPUFlag()        { return int(CPU::g_FPU); }
int LIBSIMD_API LIBSIMD_GetMMXFlag()        { return int(CPU::g_MMX); }
int LIBSIMD_API LIBSIMD_GetFXSRFlag()        { return int(CPU::g_FXSR); }
int LIBSIMD_API LIBSIMD_GetSSEFlag()        { return int(CPU::g_SSE); }
int LIBSIMD_API LIBSIMD_GetSSE2Flag()        { return int(CPU::g_SSE2); }
int LIBSIMD_API LIBSIMD_GetSSE3Flag()        { return int(CPU::g_SSE3); }
int LIBSIMD_API LIBSIMD_GetHTTFlag()        { return int(CPU::g_HTT); }


void LIBSIMD_API LIBSIMD_FillVector32( float* x, float fill, size_t n  ){ FillVector( x, fill, n ); }
float LIBSIMD_API LIBSIMD_Convolve32( const float* data1, const float* data2, size_t n  ){ return Convolve( data1, data2, n ); }
float LIBSIMD_API LIBSIMD_SumVector32( const float* x, size_t n  ){ return SumVector( x, n ); }
float LIBSIMD_API LIBSIMD_VectorMin32( const float* x, size_t n  ){ return VectorMin( x, n ); }
float LIBSIMD_API LIBSIMD_VectorMax32( const float* x, size_t n  ){ return VectorMax( x, n ); }
void LIBSIMD_API LIBSIMD_VectorMinMax32( const float* x, size_t n, float *outmin, float *outmax  ){ VectorMinMax( x, n, outmin, outmax ); }
void LIBSIMD_API LIBSIMD_Clip32( float* x, size_t n, float min, float max  ){ Clip( x, n, min, max ); }
float LIBSIMD_API LIBSIMD_SumVectorSquares32( const float* x, size_t n  ){ return SumVectorSquares( x, n ); }
float LIBSIMD_API LIBSIMD_FillLinear32( float* out, float firstPoint, float step, size_t n ){ return FillLinear( out, firstPoint, step, n ); }
float LIBSIMD_API LIBSIMD_LinearFade32( float* out, float k, float step, size_t n ){ return LinearFade( out, k, step, n ); }
float LIBSIMD_API LIBSIMD_AddLinearFade32( float* out, const float *in, float k, float step, size_t n ){ return AddLinearFade( out, in, k, step, n ); }
float LIBSIMD_API LIBSIMD_AddLinearCrossfade32( float* out, const float* fadein, const float* fadeout, float fadeink, float step, size_t n ){ return AddLinearCrossfade( out, fadein, fadeout, fadeink, step, n ); }
void LIBSIMD_API LIBSIMD_AddVector32( float* out, const float* toadd, size_t n  ){ AddVector( out, toadd, n ); }
void LIBSIMD_API LIBSIMD_AddVectorMultiplied32( float* out, const float* toadd, float mul, size_t n  ){ AddVectorMultiplied( out, toadd, mul, n ); }
void LIBSIMD_API LIBSIMD_MultiplyVector32( float* out, const float* mul, size_t n  ){ MultiplyVector( out, mul, n ); }
void LIBSIMD_API LIBSIMD_MultiplyVectorK32( float* out, const float* mul, float k, size_t n  ){ MultiplyVectorK( out, mul, k, n ); }
void LIBSIMD_API LIBSIMD_MultiplyByScaledAndShiftedVector32( float* out, const float* x, float scale, float shift, size_t n ){ MultiplyByScaledAndShiftedVector( out, x, scale, shift, n ); }
void LIBSIMD_API LIBSIMD_Multiply32( float* out, float mul, size_t n  ){ Multiply( out, mul, n ); }
void LIBSIMD_API LIBSIMD_MultiplyAndShift32(float* out, float scale, float shift, size_t n){ MultiplyAndShift( out, scale, shift, n ); }
void LIBSIMD_API LIBSIMD_SumScaledVectors2_32(float* result, const float* x1, float k1, const float* x2, float k2, size_t n ){ SumScaledVectors2( result, x1, k1, x2, k2, n ); }
void LIBSIMD_API LIBSIMD_SumScaledVectors3_32(float* result, const float* x1, float k1, const float* x2, float k2, const float* x3, float k3, size_t n ){ SumScaledVectors3( result, x1, k1, x2, k2, x3, k3, n ); }
void LIBSIMD_API LIBSIMD_SumScaledVectors4_32(float* result, const float* x1, float k1, const float* x2, float k2, const float* x3, float k3, const float* x4, float k4, size_t n ){ SumScaledVectors4( result, x1, k1, x2, k2, x3, k3, x4, k4, n ); }
void LIBSIMD_API LIBSIMD_SumVectorScaledVectors3_32( float* result, const float* x1, const float* k1, const float* x2, const float* k2, const float* x3, const float* k3, size_t n ){ SumVectorScaledVectors3( result, x1, k1, x2, k2, x3, k3, n ); }


void LIBSIMD_API LIBSIMD_FillVector64( double* x, double fill, size_t n  ){ FillVector( x, fill, n ); }
double LIBSIMD_API LIBSIMD_Convolve64( const double* data1, const double* data2, size_t n  ){ return Convolve( data1, data2, n ); }
double LIBSIMD_API LIBSIMD_SumVector64( const double* x, size_t n  ){ return SumVector( x, n ); }
double LIBSIMD_API LIBSIMD_VectorMin64( const double* x, size_t n  ){ return VectorMin( x, n ); }
double LIBSIMD_API LIBSIMD_VectorMax64( const double* x, size_t n  ){ return VectorMax( x, n ); }
void LIBSIMD_API LIBSIMD_VectorMinMax64( const double* x, size_t n, double *outmin, double *outmax  ){ VectorMinMax( x, n, outmin, outmax ); }
void LIBSIMD_API LIBSIMD_Clip64( double* x, size_t n, double min, double max  ){ Clip( x, n, min, max ); }
double LIBSIMD_API LIBSIMD_SumVectorSquares64( const double* x, size_t n  ){ return SumVectorSquares( x, n ); }
double LIBSIMD_API LIBSIMD_FillLinear64( double* out, double firstPoint, double step, size_t n ){ return FillLinear( out, firstPoint, step, n ); }
double LIBSIMD_API LIBSIMD_LinearFade64( double* out, double k, double step, size_t n ){ return LinearFade( out, k, step, n ); }
double LIBSIMD_API LIBSIMD_AddLinearFade64( double* out, const double *in, double k, double step, size_t n ){ return AddLinearFade( out, in, k, step, n ); }
double LIBSIMD_API LIBSIMD_AddLinearCrossfade64( double* out, const double* fadein, const double* fadeout, double fadeink, double step, size_t n ){ return AddLinearCrossfade( out, fadein, fadeout, fadeink, step, n ); }
void LIBSIMD_API LIBSIMD_AddVector64( double* out, const double* toadd, size_t n  ){ AddVector( out, toadd, n ); }
void LIBSIMD_API LIBSIMD_AddVectorMultiplied64( double* out, const double* toadd, double mul, size_t n  ){ AddVectorMultiplied( out, toadd, mul, n ); }
void LIBSIMD_API LIBSIMD_MultiplyVector64( double* out, const double* mul, size_t n  ){ MultiplyVector( out, mul, n ); }
void LIBSIMD_API LIBSIMD_MultiplyVectorK64( double* out, const double* mul, double k, size_t n  ){ MultiplyVectorK( out, mul, k, n ); }
void LIBSIMD_API LIBSIMD_MultiplyByScaledAndShiftedVector64( double* out, const double* x, double scale, double shift, size_t n ){ MultiplyByScaledAndShiftedVector( out, x, scale, shift, n ); }
void LIBSIMD_API LIBSIMD_Multiply64( double* out, double mul, size_t n  ){ Multiply( out, mul, n ); }
void LIBSIMD_API LIBSIMD_MultiplyAndShift64(double* out, double scale, double shift, size_t n){ MultiplyAndShift( out, scale, shift, n ); }
void LIBSIMD_API LIBSIMD_SumScaledVectors2_64(double* result, const double* x1, double k1, const double* x2, double k2, size_t n ){ SumScaledVectors2( result, x1, k1, x2, k2, n ); }
void LIBSIMD_API LIBSIMD_SumScaledVectors3_64(double* result, const double* x1, double k1, const double* x2, double k2, const double* x3, double k3, size_t n ){ SumScaledVectors3( result, x1, k1, x2, k2, x3, k3, n ); }
void LIBSIMD_API LIBSIMD_SumScaledVectors4_64(double* result, const double* x1, double k1, const double* x2, double k2, const double* x3, double k3, const double* x4, double k4, size_t n ){ SumScaledVectors4( result, x1, k1, x2, k2, x3, k3, x4, k4, n ); }
void LIBSIMD_API LIBSIMD_SumVectorScaledVectors3_64( double* result, const double* x1, const double* k1, const double* x2, const double* k2, const double* x3, const double* k3, size_t n ){ SumVectorScaledVectors3( result, x1, k1, x2, k2, x3, k3, n ); }


