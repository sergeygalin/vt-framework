TARGET = ../../Lib/VT-Qt
TEMPLATE = lib
CONFIG += staticlib

INCLUDEPATH += \
    ../../Source \
    ../../Source/VtCore \
    ../../Source/VtAudio \
    ../../Source/VtAudioPluginUtil \
    ../../Source/VtBindings \
    ../../Source/VtCore \
    ../../Source/VtDsp \
    ../../Source/VtMidi \
    ../../Source/VtQtWidgets

DEFINES += \
    QT_CORE_LIB \
    QT_GUI_LIB

unix {
    LIBS += -lpthreads
    DEFINES += PTHREADS
}

macx {
    DEFINES += VT_MAC VT_NO_CONSOLE_PROMPT
}

linux {
    DEFINES += VT_LINUX _FILE_OFFSET_BITS=64
}

OTHER_FILES += \
    ../../Source/VtCore/vt_base64_b64.inc \
    ../../Source/VtQtWidgets/QVTTableWidget \
    ../../Source/VtQtWidgets/QRectangularMapEditorContainer \
    ../../Source/VtQtWidgets/QRectangularMapEditor \
    ../../Source/VtQtWidgets/QRectangularMap \
    ../../Source/VtQtWidgets/QPixmapSlider \
    ../../Source/VtQtWidgets/QPixmapRadioBar \
    ../../Source/VtQtWidgets/QPiano \
    ../../Source/VtQtWidgets/QMorphingArea \
    ../../Source/VtQtWidgets/QMIDIMapWidget \
    ../../Source/VtQtWidgets/QPixmapDial \
    ../../Source/VtQtWidgets/QPixmapButtonBar \
    ../../Source/VtQtWidgets/QPixmapButton \
    ../../Source/VtQtWidgets/QPianoPCKeyHelpBar \
    ../../Source/VtQtWidgets/QJogDial \
    ../../Source/VtQtWidgets/QImageSpin \
    ../../Source/VtQtWidgets/QCurveEditor \
    ../../Source/VtQtWidgets/QClickableLabel \
    ../../Source/VtQtWidgets/QBlinkingLed

FORMS += \
    ../../Source/VtQtWidgets/QMIDIMapDialog.ui

HEADERS += \
    ../../Source/VtCore/vt_vectors.hpp \
    ../../Source/VtCore/vt_time.hpp \
    ../../Source/VtCore/vt_threading.hpp \
    ../../Source/VtCore/vt_syslauncher.hpp \
    ../../Source/VtCore/vt_string.hpp \
    ../../Source/VtCore/vt_smartpointers.hpp \
    ../../Source/VtCore/vt_simd.hpp \
    ../../Source/VtCore/vt_set.hpp \
    ../../Source/VtCore/vt_registry.hpp \
    ../../Source/VtCore/vt_references.hpp \
    ../../Source/VtCore/vt_portable.hpp \
    ../../Source/VtCore/vt_messageboxes.hpp \
    ../../Source/VtCore/vt_logger.hpp \
    ../../Source/VtCore/vt_localization.hpp \
    ../../Source/VtCore/vt_inifile.hpp \
    ../../Source/VtCore/vt_handystuff.hpp \
    ../../Source/VtCore/vt_fileutils.hpp \
    ../../Source/VtCore/vt_file.hpp \
    ../../Source/VtCore/vt_exceptions.hpp \
    ../../Source/VtCore/vt_csv.hpp \
    ../../Source/VtCore/vt_cpu.hpp \
    ../../Source/VtCore/vt_comparators.hpp \
    ../../Source/VtCore/vt_buffers.hpp \
    ../../Source/VtCore/vt_baseclasses.hpp \
    ../../Source/VtCore/vt_base64.hpp \
    ../../Source/VtCore/vt_audiomath.hpp \
    ../../Source/VtCore/vt_assert.hpp \
    ../../Source/VtCore/vt_arrays_sortable.hpp \
    ../../Source/VtCore/vt_arrays.hpp \
    ../../Source/VtCore/vt_arrayreversal.hpp \
    ../../Source/VtAudio/vt_channelmixing.hpp \
    ../../Source/VtAudio/vt_rtaudiospeedchanger.hpp \
    ../../Source/VtAudio/vt_riff.hpp \
    ../../Source/VtAudio/vt_rawaudio.hpp \
    ../../Source/VtAudio/vt_meter.hpp \
    ../../Source/VtAudio/vt_audiotypes.hpp \
    ../../Source/VtAudio/vt_spreadsheet.hpp \
    ../../Source/VtAudioPluginUtil/vt_paramstorage.hpp \
    ../../Source/VtAudioPluginUtil/vt_parameters.hpp \
#    ../../Source/VtAudioEngine/vt_audiocropping.hpp \
#    ../../Source/VtAudioEngine/vt_audiopool.hpp \
#    ../../Source/VtAudioEngine/vt_audioprototypes.hpp \
#    ../../Source/VtAudioEngine/vt_audiostreamconverter.hpp \
#    ../../Source/VtAudioEngine/vt_audiostreamplayer.hpp \
#    ../../Source/VtAudioEngine/vt_audiostreams.hpp \
#    ../../Source/VtAudioEngine/vt_audiostretching_dirac.hpp \
#    ../../Source/VtAudioEngine/vt_audiostretching.hpp \
#    ../../Source/VtAudioEngine/vt_clipconveyor.hpp \
#    ../../Source/VtAudioEngine/vt_clipfactory.hpp \
#    ../../Source/VtAudioEngine/vt_clipjobs.hpp \
#    ../../Source/VtAudioEngine/vt_clipprocessor.hpp \
#    ../../Source/VtAudioEngine/vt_clipstream.hpp \
#    ../../Source/VtAudioEngine/vt_clipstretcher.hpp \
#    ../../Source/VtAudioEngine/vt_filereference.hpp \
#    ../../Source/VtAudioEngine/vt_peakfiles.hpp \
    ../../Source/VtDsp/vt_curves.hpp \
    ../../Source/VtDsp/vt_dsp_processor.hpp \
    ../../Source/VtDsp/vt_resampling.hpp \
    ../../Source/VtDsp/vt_iirs.hpp \
    ../../Source/VtDsp/vt_fourier.hpp \
    ../../Source/VtDsp/vt_firs.hpp \
    ../../Source/VtDsp/vt_fir_pools.hpp \
    ../../Source/VtDsp/vt_fir_polyphase.hpp \
    ../../Source/VtDsp/vt_dspwindows.hpp \
    ../../Source/VtDsp/vt_dithering.hpp \
    ../../Source/VtDsp/vt_chebyshev.hpp \
    ../../Source/VtDsp/vt_reverberation.hpp \
    ../../Source/VtMidi/vt_piano.hpp \
    ../../Source/VtMidi/vt_midi_processor.hpp \
    ../../Source/VtMidi/vt_midi_map.hpp \
    ../../Source/VtMidi/vt_midi.hpp \
    ../../Source/VtMidi/vt_midi_files.hpp \
    ../../Source/VtQtWidgets/QVTTableWidget.h \
    ../../Source/VtQtWidgets/QRectangularMapEditorContainer.h \
    ../../Source/VtQtWidgets/QRectangularMapEditor.h \
    ../../Source/VtQtWidgets/QRectangularMap.h \
    ../../Source/VtQtWidgets/QPixmapSlider.h \
    ../../Source/VtQtWidgets/QPixmapReferencer.h \
    ../../Source/VtQtWidgets/QPixmapRadioBar.h \
    ../../Source/VtQtWidgets/QPiano.h \
    ../../Source/VtQtWidgets/QMorphingArea.h \
    ../../Source/VtQtWidgets/QMIDIMapWidget.h \
    ../../Source/VtQtWidgets/QMIDIMapDialog.h \
    ../../Source/VtQtWidgets/QJogDial.h \
    ../../Source/VtQtWidgets/QPixmapDial.h \
    ../../Source/VtQtWidgets/QPixmapButtonBar.h \
    ../../Source/VtQtWidgets/QPixmapButton.h \
    ../../Source/VtQtWidgets/QPianoPCKeyHelpBar.h \
    ../../Source/VtQtWidgets/QImageSpin.h \
    ../../Source/VtQtWidgets/QCurveEditor.h \
    ../../Source/VtQtWidgets/QClickableLabel.h \
    ../../Source/VtQtWidgets/QBlinkingLed.h

SOURCES += \
    ../../Source/VtCore/vt_time.cpp \
    ../../Source/VtCore/vt_threading.cpp \
    ../../Source/VtCore/vt_syslauncher.cpp \
    ../../Source/VtCore/vt_string.cpp \
    ../../Source/VtCore/vt_smartpointers.cpp \
    ../../Source/VtCore/vt_simd.cpp \
    ../../Source/VtCore/vt_registry.cpp \
    ../../Source/VtCore/vt_references.cpp \
    ../../Source/VtCore/vt_portable.cpp \
    ../../Source/VtCore/vt_messageboxes.cpp \
    ../../Source/VtCore/vt_logger.cpp \
    ../../Source/VtCore/vt_localization.cpp \
    ../../Source/VtCore/vt_inifile.cpp \
    ../../Source/VtCore/vt_fileutils.cpp \
    ../../Source/VtCore/vt_file.cpp \
    ../../Source/VtCore/vt_exceptions.cpp \
    ../../Source/VtCore/vt_csv.cpp \
    ../../Source/VtCore/vt_cpu.cpp \
    ../../Source/VtCore/vt_baseclasses.cpp \
    ../../Source/VtCore/vt_base64.cpp \
    ../../Source/VtCore/vt_audiomath.cpp \
    ../../Source/VtCore/vt_arrays.cpp \
    ../../Source/VtCore/vt_arrayreversal.cpp \
    ../../Source/VtAudio/vt_audiotypes.cpp \
    ../../Source/VtAudio/vt_channelmixing.cpp \
    ../../Source/VtAudio/vt_riff.cpp \
    ../../Source/VtAudioPluginUtil/vt_parameters.cpp \
#    ../../Source/VtAudioEngine/vt_audiocropping.cpp \
#    ../../Source/VtAudioEngine/vt_audiopool.cpp \
#    ../../Source/VtAudioEngine/vt_audioprototypes.cpp \
#    ../../Source/VtAudioEngine/vt_audiostreamconverter.cpp \
#    ../../Source/VtAudioEngine/vt_audiostreamplayer.cpp \
#    ../../Source/VtAudioEngine/vt_audiostreams.cpp \
#    ../../Source/VtAudioEngine/vt_audiostretching.cpp \
#    ../../Source/VtAudioEngine/vt_audiostretching_dirac.cpp \
#    ../../Source/VtAudioEngine/vt_clipconveyor.cpp \
#    ../../Source/VtAudioEngine/vt_clipfactory.cpp \
#    ../../Source/VtAudioEngine/vt_clipjobs.cpp \
#    ../../Source/VtAudioEngine/vt_clipprocessor.cpp \
#    ../../Source/VtAudioEngine/vt_clipstream.cpp \
#    ../../Source/VtAudioEngine/vt_clipstretcher.cpp \
#    ../../Source/VtAudioEngine/vt_filereference.cpp \
#    ../../Source/VtAudioEngine/vt_peakfiles.cpp \
    ../../Source/VtDsp/vt_curves.cpp \
    ../../Source/VtDsp/vt_dsp_processor.cpp \
    ../../Source/VtDsp/vt_fir_pools.cpp \
    ../../Source/VtDsp/vt_reverberation.cpp \
    ../../Source/VtMidi/vt_piano.cpp \
    ../../Source/VtMidi/vt_midi_processor.cpp \
    ../../Source/VtMidi/vt_midi_map.cpp \
    ../../Source/VtMidi/vt_midi_files.cpp \
    ../../Source/VtMidi/vt_midi.cpp \
    ../../Source/VtQtWidgets/QVTTableWidget.cpp \
    ../../Source/VtQtWidgets/QRectangularMapEditorContainer.cpp \
    ../../Source/VtQtWidgets/QRectangularMapEditor.cpp \
    ../../Source/VtQtWidgets/QRectangularMap.cpp \
    ../../Source/VtQtWidgets/QPixmapSlider.cpp \
    ../../Source/VtQtWidgets/QPixmapReferencer.cpp \
    ../../Source/VtQtWidgets/QPixmapRadioBar.cpp \
    ../../Source/VtQtWidgets/QPiano.cpp \
    ../../Source/VtQtWidgets/QMorphingArea.cpp \
    ../../Source/VtQtWidgets/QMIDIMapWidget.cpp \
    ../../Source/VtQtWidgets/QMIDIMapDialog.cpp \
    ../../Source/VtQtWidgets/QPixmapDial.cpp \
    ../../Source/VtQtWidgets/QPixmapButtonBar.cpp \
    ../../Source/VtQtWidgets/QPixmapButton.cpp \
    ../../Source/VtQtWidgets/QPianoPCKeyHelpBar.cpp \
    ../../Source/VtQtWidgets/QJogDial.cpp \
    ../../Source/VtQtWidgets/QImageSpin.cpp \
    ../../Source/VtQtWidgets/QCurveEditor.cpp \
    ../../Source/VtQtWidgets/QClickableLabel.cpp \
    ../../Source/VtQtWidgets/QBlinkingLed.cpp
