

Known issues:

1) After creating Qt project via wizard, change the following project settings:

  - Set character type to UNICODE.
  - Enable "threat wchar_t as built-in type".

2) Add to Linker Settings -> Input -> Ignore specific library: libcmt;libcmtd

Otherwise, the library won't link properly.


