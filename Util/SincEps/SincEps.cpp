
#include <math.h>


template<class typ> void DoIt( typ x )
{
    int found = 0;
    const typ k=typ(0.999999);
    printf("Using %d-bit float.\n", sizeof(typ)*8);

    while( !(found==(1|2)) ){

        x = x*k;
        typ s = sin(x);

        if( x==s ){
            printf("x==sin(x): %.30le\n", double(x) );
            found|=1;
        }

        if( s/x == typ(1.0) ){
            printf("sin(x)/x==1: %.30le\n", double(x) );
            found|=2;
        }

        if( x==0 ){
            printf("x reached 0!\n");
            break;
        }

    }
}

int main( int argc, char *argv[] )
{
    printf("Hello Sinc world!\n");

    DoIt<float>( 1e-5f );
    DoIt<float>( -1e-5f );
    DoIt<double>( 1e-5 );
    DoIt<double>( -1e-5 );
    DoIt<long double>( 1e-5 );
    DoIt<long double>( -1e-5 );

    return 0;
}

