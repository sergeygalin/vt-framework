//
// WAV to 32-bit IEEE float Raw converter
//

#include "vt_string.hpp"
#include "vt_fileutils.hpp"
#include "vt_rawaudio.hpp"

using namespace VT;

int main(int argc, char *argv[])
{

    if( argc!=2 ){
        VTPRINTF(VTT("USAGE: Wav2Raw <wavfile>\n"));
        return 1;
    }

    LPCVTCHAR wavfn = argv[1];
    VTCHAR rawfn[MAX_PATH+1];

    VTSTRNCPY( rawfn, argv[1], MAX_PATH );
    VT::CutoffFileExt( rawfn );
    VTSTRNCAT( rawfn, VTT(".raw"), 4 );

    VTPRINTF(VTT("Source file: %s\nDestination file: %s\n"), wavfn, rawfn );

    CInMemoryAudio<float, 1> audio;
    if( !audio.LoadPCMWav( wavfn ) ){
        VTPRINTF(VTT("Error loading source file, message: \"%s\"\n"), audio.GetLastErrorMessage() );
        return 1;
    }

    VTPRINTF(VTT("Loaded. Writing raw file...\n"));

    if( !audio.SaveRaw(rawfn) ){
        VTPRINTF(VTT("Write error :-(\n"));
        return 1;
    }

    printf("Success!\n\n");
    return 0;
}


