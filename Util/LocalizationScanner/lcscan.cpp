//--------------------------------------------------------------------------
// Message translation module
// An utility to collect all localized text messages in multiple C/C++
// source files.
// 2007
// Programmer: Sergey A. Galin
//--------------------------------------------------------------------------



// Known Issues
// - Doesn't detect namespaces, scope resolution, pointers, fields. For example:
//     Something::LC
//     LC->Something
//   Will be treated as LC.


#define _CRT_SECURE_NO_DEPRECATE

#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "vt_portable.hpp"
#include "vt_fileutils.hpp"

const int kMaxFileSize = 1024*1024*10;
const LPCVTCHAR kFileExts[] = { "C", "CPP", "H", "HPP", NULL };
const LPCVTCHAR kLCFunctions[] = { "LC", "LCT", NULL };

// Function prototypes
void ScanFile(LPCVTCHAR fileName);
void CheckFileExt(LPCVTCHAR fileName);
const char* ExtractLCText(const char *str, const char *file, int *line);



void CheckFileExt(LPCVTCHAR fileName)
{
    VTCHAR fileExt[MAX_PATH+1];
    VT::ExtractFileExt(fileName, fileExt);
    for(const LPCVTCHAR* ext = kFileExts; *ext!=NULL; ext++)
        if(!VTSTRICMP(*ext, fileExt)){
            ScanFile(fileName);
            return;
        }
}


const char* FindNextQuote(const char* str, char quotesymbol, int* lineNumber)
{
    while( *str ){
        if( *str=='\\' ){
            str++;
            if( !(*str) )
                return NULL;
            else if( *str=='\n' )
                (*lineNumber)++;
        }else if( *str==quotesymbol )
            return str;
        else if( *str=='\n' )
            (*lineNumber)++;
        str++;
    }
    return NULL;
}

VTINLINE bool IsIdentChar(char c)
{
    return isalpha(c) || isdigit(c) || c=='#' || c=='_' || c=='$';
}

VTINLINE int FastCompare(const char *shorter, const char *longer){
    for(;;){
        if( !(*shorter) )
            return 0;
        if( !(*longer) )
            return 1;
        if( (*shorter) != (*longer) )
            return 1;
        shorter++;
        longer++;
    }
}

void ScanFile(LPCVTCHAR fileName)
{
    //printf("// FILE: %s\n", fileName);
    static char buffer[ kMaxFileSize ];
    static char fileNameNoPath[MAX_PATH];
    int lineNumber = 1;
    VT::ExtractFileName(fileName, fileNameNoPath);

    FILE *fp = fopen(fileName, "r");
    if( fp ){
        size_t sz = fread(buffer, 1, kMaxFileSize-2, fp);
        fclose(fp);

        // Make sure that the buffer ends with 2 zeroes, so for loop will
        // exit for sure, if ptr is positioned to the first zero
        buffer[sz] = 0;
        buffer[sz+1] = 0;

        // File scanning loop
        for(const char* ptr = buffer; *ptr; ptr++){
            // Skip quoted characters
            if( *ptr=='\'' ){
                ptr++;
                if( (ptr = FindNextQuote(ptr, '\'', &lineNumber)) == NULL )
                    break; // The closing quote will be skipped by the for loop
                continue;
            }

            // Skip quoted strings
            if( *ptr=='\"' ){
                ptr++;
                if( (ptr = FindNextQuote(ptr, '\"', &lineNumber)) == NULL )
                    break; // The closing quote will be skipped by the for loop
                continue;
            }

            // Skip C comments
            if( *ptr=='/' && *(ptr+1)=='*' ){
                ptr+=2;
                while( *ptr ){
                    if( *ptr=='*' && *(ptr+1)=='/' ){
                        ptr++; // Skip '*', the '/' will be skipped by for loop
                        break;
                    }else{
                        if( *ptr=='\n' )
                            lineNumber++;
                        ptr++;
                    }
                }
                continue;
            }

            // Skip C++ comments
            if( *ptr=='/' && *(ptr+1)=='/' ){
                ptr+=2;
                while( *ptr ){
                    if( *ptr=='\n' ){
                        lineNumber++;
                        break;
                    }else
                        ptr++;
                }
                continue;
            }

            // Finally, check for LC functions
            for(const LPCVTCHAR* fn = kLCFunctions; *fn!=NULL; fn++){
                if( !FastCompare(*fn, ptr) ){
                    const char *nextChar = ptr+VTSTRLEN(*fn);
                    if( !IsIdentChar(*nextChar) ){
                        // Extract the LC!
                        ptr = ExtractLCText(nextChar, fileNameNoPath, &lineNumber);
                        break;
                    }
                }
            }

            // Skip all other identifiers
            if( IsIdentChar(*ptr) ){
                ptr++;
                while( *ptr ){
                    if( !IsIdentChar(*ptr) ){
                        if( (*ptr)=='\n' )
                            lineNumber++;
                        break;
                    }else
                        ptr++;
                }
                continue;
            }

            // Do nothing, just count lines
            if( (*ptr)=='\n' )
                lineNumber++;

        }
    }
}

const char* ExtractLCText(const char *str, const char *file, int *line)
{
    int inPars = 0;
    printf("%s\t%d\t", file, *line);
    while(*str){
        if( *str=='(' )
            inPars++;
        else if( *str=='\"' ){
            str++;
            while(*str){
                if( *str=='\\' && *(str+1)=='\"' ){ // \" found
                    putchar(*str++);
                    putchar(*str);
                }else if( *str=='\"' ){
                    break;
                }else{
                    putchar(*str);
                    if( *str=='\n' )
                        (*line)++;
                }
                str++;
            }
        }else if( *str==')' ){
            inPars--;
            if(inPars<=0)
                break;
        }else if( *str=='\n' )
            (*line)++;
        str++;
    }
    putchar('\n');
    return str;
}

void ScanDirectory(LPCVTCHAR dirName)
{
    //printf("// DIRECTORY: %s\n", dirName);
    VTCHAR subDir[MAX_PATH+1], searchMask[MAX_PATH+1];
    WIN32_FIND_DATA fileData;
    size_t dirNameLastChar;
    bool addSlash;
    // Creating search mask
    VTSTRNCPY( searchMask, dirName, MAX_PATH );
    if( dirName[ dirNameLastChar = (VTSTRLEN(dirName)-1) ] != VTT('\\') ){
        // dirName doesn't end with a slash, so we have to add it when
        // creating sub-searches and etc.
        addSlash = true;
        VTSTRNCAT( searchMask, VTT("\\"), MAX_PATH );
    }else
        addSlash = false;
    VTSTRNCAT( searchMask, VTT("*"), MAX_PATH );
    HANDLE hFile = FindFirstFile(searchMask, &fileData);
    if (hFile != INVALID_HANDLE_VALUE){
        if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
            if( fileData.cFileName[0] != VTT('.') ){
                VTSTRNCPY( subDir, dirName, MAX_PATH );
                if( addSlash )
                    VTSTRNCAT( subDir, VTT("\\"), MAX_PATH );
                VTSTRNCAT( subDir, fileData.cFileName, MAX_PATH );
                VTSTRNCAT( subDir, VTT("\\"), MAX_PATH );
                ScanDirectory( subDir );
            }
        }else{
            VTSTRNCPY( subDir, dirName, MAX_PATH );
            if( addSlash )
                VTSTRNCAT( subDir, VTT("\\"), MAX_PATH );
            VTSTRNCAT( subDir, fileData.cFileName, MAX_PATH );
            CheckFileExt( subDir );
        }

        while (FindNextFile(hFile, &fileData) != NULL)
            if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
                if( fileData.cFileName[0] != VTT('.') ){
                    VTSTRNCPY( subDir, dirName, MAX_PATH );
                    if( addSlash )
                        VTSTRNCAT( subDir, VTT("\\"), MAX_PATH );
                    VTSTRNCAT( subDir, fileData.cFileName, MAX_PATH );
                    VTSTRNCAT( subDir, VTT("\\"), MAX_PATH );
                    ScanDirectory( subDir );
                }
            }else{
                VTSTRNCPY( subDir, dirName, MAX_PATH );
                if( addSlash )
                    VTSTRNCAT( subDir, VTT("\\"), MAX_PATH );
                VTSTRNCAT( subDir, fileData.cFileName, MAX_PATH );
                CheckFileExt( subDir );
            }

        FindClose(hFile);
    }
}

int main(int argc, char* argv[])
{

    if( argc<2 )
        printf("USAGE: lcscan <directories to scan>\n");
    else
        for(int i=1; i<argc; i++){
            CheckFileExt(argv[i]);
            ScanDirectory(argv[i]);
        }
    return 0;
}

