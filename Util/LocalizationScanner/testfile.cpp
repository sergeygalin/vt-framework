
Abcdef;

LC(VTT("Hello world!"));

'LCT(\"You should not see this, it\'s in single quotes\")'
"LCT(\"You should not see this, it\'s in double quotes\")"

LCT2("You should not see this, there's an extra character after LCT.");

/* LCT("You should not see this, it's in C comment */

BlaBlaBla; // LCT("You should not see this, it's in C++ comment");

LC("Dolor ipsum etc. "
   VTT("Multi-line stuff!"));



