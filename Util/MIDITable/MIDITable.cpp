
#ifndef VT_MIDI_OCTAVE_COMPLIANCE
    #error "Define VT_MIDI_OCTAVE_COMPLIANCE!!!"
#endif

#include "../../vt_midi.hpp"

using namespace VT;

/*
    ...In this system A4 is nowadays standardised to 440 Hz,
    lying in the octave containing notes from C4 (middle C) to B4.
    The lowest note on most pianos is A0, the highest C8.
    The MIDI system for electronic musical instruments and computers
    uses a straight count starting with note 0 for C-1 at 8.1758 Hz
    up to note 127 for G9 at 12,544 Hz.
*/

int main(int argc, char *argv[])
{
    printf(
        "// MIDI Table generator, " __DATE__ "\n"
        "const float Notes::kNoteFrequencies[128] = {\n"
    );

    for( BYTE note=0; note<128; note ++ ){
        printf("\t%.11ff,\t// %s%d\t\t%03d\t\t%02x\n",
            float(Notes::CalcNoteFrequencyHz( note )),
            Notes::GetNoteName( note ),
            Notes::GetOctaveNumber( note ),
            int( note ), int( note ) );

    }

    printf("};\n\n");


}


