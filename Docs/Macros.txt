
=============================================================================
== OPERATING SYSTEMS ========================================================
=============================================================================

VT_WINDOWS - MS Windows compilation; automatically triggered by standard
compiler's macros.
VT_LINIX - triggered by LINUX, Q_OS_LINUX.
VT_MAC - Mac OS X compilation; automatically triggered by: Q_OS_DARWIN.
VT_DOS - Used in some places, but not fully implemented.


=============================================================================
=== BUILD MODES & FLAGS =====================================================
=============================================================================

VT_UNICODE - triggered by UNICODE and _UNICODE; makes VTCHAR a wide char.

_FILE_OFFSET_BITS=64 - on some *nixes, this macro switches file I/O 
    to 64-bit. VT notices this macro and enables its 64-bit file mode.
VT_64BIT_FILEOIO is automatically defined under MSC or if 
    _FILE_OFFSET_BITS==64; if you define it externally, it will cause
    defining _FILE_OFFSET_BITS=64 but that's not a recommended behaviour.

CONSOLE - causes messagebox system to use console.

VT_DISABLE_CPU_IDENTIFICATION - completely disable all processor-specific code
VTSIMD_NOSIMD - disable all SIMD code
VTSIMD_SSE_GCC_O3 - in GCC version, disables intrinsic-based implementations
    of functions which are known to run faster in C++ version when compiled
    with -O3 switch.

 Disable CPU capabilities check for certain SIMD extensions:
     VTSIMD_NOCHECK_MMX
     VTSIMD_NOCHECK_SSE -- implies VTSIMD_NOCHECK_MMX
     VTSIMD_NOCHECK_SSE2 -- implies VTSIMD_NOCHECK_SSE, VTSIMD_NOCHECK_MMX
     VTSIMD_NOCHECK_SSE3 -- implies VTSIMD_NOCHECK_SSE2, ...
     VTSIMD_NOCHECK_SSE4 -- implies VTSIMD_NOCHECK_SSE2, ...
     VTSIMD_NOCHECK_3DNOW -- implies VTSIMD_NOCHECK_MMX
     VTSIMD_NOCHECK_3DNOW2 -- implies VTSIMD_NOCHECK_3DNOW, ...


VERBOSE - causes compiler to print more messages to check for correct
    configuration (via #warning and etc.)
VT_ASSERTIONS - turns on assetion macros even in release builds
    (automatically defined for debug builds).
USE_CASSERT - uses standard assert() instead of VT_ASSERT.
VT_CONSOLEDEBUG - turns on verbose debug output to stdout.
_DEBUG, NDEBUG are used.

=============================================================================
=== THIRD-PARTY LIBRARIES ===================================================
=============================================================================

Qt - automatically enabled by QT_CORE_LIB, QT_GUI_LIB, and etc.
USE_BOOST - enables use of some boost stuff.
USE_LSGUI, USE_LSSTRING - enables LSGUI library bindings.
USE_CRYPTOPP, USE_CRYPTOPP_STATIC enable linking to Crypto++; one of these MUST be defined when using any cryptographic stuff.
__BORLANDC__ should have been enabling bindings to VLC stuff, like AnsiString, but it all seems to be broken now.

