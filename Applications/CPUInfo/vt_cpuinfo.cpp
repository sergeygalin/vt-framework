/*
    BSD License

    Copyright (C) 2009 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "vt_localization.hpp"
#include "vt_simd.hpp"

using namespace VT;

void ShowCPUInfo()
{
    #if defined(VT_64)
        printf("Compiled for a 64-bit platform.\n");
    #else
        printf("Compiled for a 32-bit platform.\n");
    #endif
    printf("Int size:         %d bytes\n", int(sizeof(int)));
    printf("Long size:        %d bytes\n", int(sizeof(long)));
    printf("Long long size:   %d bytes\n", int(sizeof(long long)));
    printf("Pointer size:     %d bytes\n", int(sizeof(void*)));
    printf("Float size:       %d bytes\n", int(sizeof(float)));
    printf("Double size:      %d bytes\n", int(sizeof(double)));
    printf("Long double size: %d bytes\n", int(sizeof(long double)));
    printf("File offset size: %d bytes (off_t), %d bytes (VT)\n", int(sizeof(off_t)), int(sizeof(VTFSIZE)));

    printf("\n"
           "Detected CPU configuration\n"
           "--------------------------\n");

    printf("CPU Vendor: \"%s\"\n", CPU::GetVendor());
    printf("Found CPU cores: %u\n", CPU::GetNumberOfCPUs() );

    if( CPU::g_CPUID_Success ){
    printf(
        "FPU on chip......... %s\n"
        "MMX................. %s\n"
        "FXSR................ %s\n"
        "SSE................. %s\n"
        "SSE2................ %s\n"
        "SSE3................ %s\n"
        "Hyper Threading..... %s\n"
        "\n",
        LCYESNO(CPU::g_FPU),
        LCYESNO(CPU::g_MMX),
        LCYESNO(CPU::g_FXSR),
        LCYESNO(CPU::g_SSE),
        LCYESNO(CPU::g_SSE2),
        LCYESNO(CPU::g_SSE3),
        LCYESNO(CPU::g_HTT));
    }else
        printf("CPUID was not performed.\n\n");
}

int main( int argc, char *argv[] )
{
    ShowCPUInfo();
    return 0;
}

