
VT=$$PWD/../../Source
QT =

SOURCES += averagespectrum.cpp

OTHER_FILES += \
    KnownIssues.txt \
    averagespectrum.ini \
    Makefile.linux

DEFINES += VT_CRTDEBUG _DEBUG

linux* {
    DEFINES += LINUX CONSOLE PTHREADS
    LIBS += -lpthread
    target.path += /usr/local/bin
    INSTALLS += target
    QMAKE_CXXFLAGS += -ffast-math -msse2 -msse3 -m3dnow -mfpmath=sse,387 -O3
}

linux-g++-64 {
    QMAKE_CXXFLAGS += -mtune=athlon64
}

INCLUDEPATH = \
	$$VT/VtCore \
	$$VT/VtAudio \
	$$VT/VtAudioEngine \
	$$VT/VtDsp

HEADERS += \
	$$VT/VtAudioEngine/vt_audioprototypes.hpp \
	$$VT/VtAudio/vt_audiotypes.hpp \
	$$VT/VtAudio/vt_riff.hpp \
	$$VT/VtCore/vt_arrayreversal.hpp \
	$$VT/VtCore/vt_arrays.hpp \
	$$VT/VtCore/vt_audiomath.hpp \
	$$VT/VtCore/vt_baseclasses.hpp \
	$$VT/VtCore/vt_cpu.hpp \
	$$VT/VtCore/vt_csv.hpp \
	$$VT/VtCore/vt_exceptions.hpp \
	$$VT/VtCore/vt_file.hpp \
	$$VT/VtCore/vt_fileutils.hpp \
	$$VT/VtCore/vt_inifile.hpp \
	$$VT/VtCore/vt_localization.hpp \
	$$VT/VtCore/vt_messageboxes.hpp \
	$$VT/VtCore/vt_spreadsheet.hpp \
	$$VT/VtCore/vt_portable.hpp \
	$$VT/VtCore/vt_references.hpp \
	$$VT/VtCore/vt_simd.hpp \
	$$VT/VtCore/vt_string.hpp \
	$$VT/VtCore/vt_threading.hpp \
	$$VT/VtDsp/vt_fir_pools.hpp

SOURCES += \
	$$VT/VtAudioEngine/vt_audioprototypes.cpp \
	$$VT/VtAudio/vt_audiotypes.cpp \
	$$VT/VtAudio/vt_riff.cpp \
	$$VT/VtCore/vt_arrayreversal.cpp \
	$$VT/VtCore/vt_arrays.cpp \
	$$VT/VtCore/vt_audiomath.cpp \
	$$VT/VtCore/vt_baseclasses.cpp \
	$$VT/VtCore/vt_cpu.cpp \
	$$VT/VtCore/vt_csv.cpp \
	$$VT/VtCore/vt_exceptions.cpp \
	$$VT/VtCore/vt_file.cpp \
	$$VT/VtCore/vt_fileutils.cpp \
	$$VT/VtCore/vt_inifile.cpp \
	$$VT/VtCore/vt_localization.cpp \
	$$VT/VtCore/vt_messageboxes.cpp \
	$$VT/VtCore/vt_portable.cpp \
	$$VT/VtCore/vt_references.cpp \
	$$VT/VtCore/vt_simd.cpp \
	$$VT/VtCore/vt_string.cpp \
	$$VT/VtCore/vt_threading.cpp \
	$$VT/VtDsp/vt_fir_pools.cpp

