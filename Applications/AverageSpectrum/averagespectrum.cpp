/*
    BSD License

    Copyright (C) 2009 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>

#include "vt_audiotypes.hpp"
#include "vt_rawaudio.hpp"
#include "vt_vectors.hpp"
#include "vt_fourier.hpp"
#include "vt_firs.hpp"
#include "vt_inifile.hpp"
#include "vt_fileutils.hpp"
#include "vt_file.hpp"
#include "vt_simd.hpp"
#include "vt_audioprototypes.hpp"

// #define VT_SPECTRUM_STEP_RANDOMIZATION
#define VT_SPECTRUM_KAISER

using namespace VT;
using namespace VT::AudioEngine;

// Go through FFT output amplitudes and convert them to decibel.
template<class T> void SpectrumTo_dB( CVector<T>* out )
{
	for( size_t i=0; i<out->getSize(); i++ )
		out->set( i, Math::Convert_01TodB_NoClip( out->get( i ) ) );
}

// Calculate average spectrum of a sound file.
template<class T>
	void AverageSpectrum( const CInMemoryAudio<T, 1>& sound,
						  size_t start, size_t maxLen, size_t fftSize, size_t stepping,
						  CVector<T>* out, bool decibel = true )
{
	CVector<T> fft_re( fftSize ), fft_im( fftSize ), window( fftSize );

	// FFT converts [0..N-1] points into two [0..N/2] chunks; therefore "+1":
	size_t spectrumLength = fftSize / 2 + 1;
	out->resize( spectrumLength );
	out->ZeroMemoryBytes();
	size_t count = 0;

	window.resize( fftSize-1 );
	window.fill( T(1.0) );
	#if defined(VT_SPECTRUM_KAISER)
		Windowing::KaiserWindow( window );
	#else
		Windowing::HanningWindow( window );
	#endif
	window.push( T(0.0) );

	for( size_t ptr = start, max = Math::Min<size_t>( sound.getLength(), maxLen ); ptr + fftSize < max; count++ )
	{
		// Get source audio fragment
		const T* srcPtr = (const T*)sound.GetData() + ptr;
		memcpy( fft_re.getDataPtr(), srcPtr, fftSize * sizeof(T) );
		// Apply window
		SIMD::MultiplyVector( fft_re.getDataPtr(), window.getDataPtr(), fftSize );
		// Find spectrum using FFT
		Fourier::RealFFT( fft_re.getDataPtr(), fft_im.getDataPtr(), fftSize );

		/*
		// Make amplitude values absolute
		for( size_t x = 0; x<spectrumLength; x++ )
			if( fft_re.get(x)<T(0.0) )
				fft_re.set( x, -fft_re.get(x) );
		*/

		// Set fft_re to magnitude of the points
		for( size_t x = 0; x<spectrumLength; x++ )
			fft_re[x] = Fourier::Magnitude( fft_re[x], fft_im[x] );

		// Sum spectrums. Remember that spectrum is symmetric about its center
		// (Nyquist frequency).
		SIMD::AddVector( out->getDataPtr(), fft_re.getDataPtr(), spectrumLength );

		#if !defined(VT_SPECTRUM_STEP_RANDOMIZATION)
			ptr += stepping;
		#else
			ptr += size_t( double(rand())/double(RAND_MAX)*double(stepping) ) + 1;
		#endif
	}
	// Divide to get average from the sum
	SIMD::Multiply( out->getDataPtr(), T( 1.0 / double(count) ), spectrumLength );

	// Convert amplitude to decibels, if necessary
	if( decibel )
		SpectrumTo_dB( out );
}

// Calculate Herz frequencies of FFT-generated spectrum (the resulting array is fftSize/2 length).
template<class T> void SpectrumFrequencies( double srate, size_t fftSize, CVector<T>* out )
{
	size_t spectrumLength = fftSize / 2;
	out->resize( spectrumLength );
	double freq = 0, step = srate / double(fftSize);
	for( size_t x = 0; x < spectrumLength; x++, freq += step )
		out->set( x, freq );
}

// Create log growing list of frequencies (e.g. to calculate positions of X grid bars in a log graph)
template<class T> void LogFrequencies( double starting, double multiplier, double max, CVector<T>* out )
{
	out->resize( 0 );
	size_t idx = 0;
	for( double freq = starting; freq <= max; freq *= multiplier ){
		if( out->getSize() <= idx )
			out->resize( idx+1, true );
		out->set( idx, freq );
		idx++;
	}
	if( out->last()<max )
		out->push( max );
}

// Calculated smoothed log-X plot (typically, from an unsmoothed linear-X plot).
// Original (linear) plot: in_hz, in_amp
// Resulting plot X values: log_hz
// multiplier: defines smoothing width; e.g. 2.0 means two octave smoothing.
// out_amp: output Y values.
// logarithmic: use logarithmic smoothing with 'logStep'.
template<class T> void SmoothSpectrum( const CVector<T>& in_hz, const CVector<T>& in_amp, const CVector<T>& log_hz,
	double multiplier, CVector<T>* out_amp, bool logarithmic = true, T logStep = T(1.005) )
{
	if( multiplier < 1.0 )
		multiplier = 1.0 / multiplier;
	out_amp->resize( log_hz.getSize() );
	//
	// Go through log spectrum points
	//
	for( size_t i = 0; i<log_hz.getSize(); i++ ){
		T sum = T(0.0), centralFreq = log_hz.get(i), left = centralFreq/multiplier, right = centralFreq*multiplier;
		size_t sumCount = 0;

		if( !logarithmic ){
			//
			// Linear summing
			//
			// !!! to do: binary search of the left index
			for( size_t summIdx = 0; summIdx<in_hz.getSize(); summIdx++ ){
				if( in_hz.get(summIdx)>right ){
					break;
				}else if( in_hz.get(summIdx)>=left ){
					sum += in_amp.get( summIdx );
					sumCount++;
				}
			}
		}else{
			//
			// Log summing
			//
			for( T freq = left; freq <= right; freq *= logStep ){
				sum += SpectrumAt_Linear( in_hz, in_amp, freq );
				sumCount++;
			}
		}
		if( sumCount > 0 )
			sum /= T(sumCount);
		out_amp->set( i, sum );
	}
}

// A helper/wrapper for SmoothSpectrum which works with amplitude table straight from FFT.
// in_amp is an FFT result for the according FFT length.
// srate is a sample rate of the stream.
template<class T> void SmoothFFTSpectrum( const CVector<T>& in_amp, double srate, double startingHz,
	double multiplier, CVector<T>* out_hz, CVector<T>* out_amp, bool logarithmic = true, T logStep = T(1.005) )
{
	CVector<T> linearHz;
	// Create Hz table for the given FFT length and sample rate
	SpectrumFrequencies( srate, in_amp.getSize()*2, &linearHz );
	// Create log Hz table which goes from starting frequency to the Nyquist frequency.
	LogFrequencies( startingHz, multiplier, srate/2.0, out_hz );
	// Create the smoothed spectrum.
	SmoothSpectrum( linearHz, in_amp, *out_hz, multiplier, out_amp, logarithmic, logStep );
}

// A helper/wrapper for SmoothSpectrum which works with amplitude table straight from FFT
// and smoothing width is given in octaves.
// in_amp is an FFT result for the according FFT length.
// srate is a sample rate of the stream.
template<class T> void SmoothFFTSpectrumOctaves( const CVector<T>& in_amp, double srate, double startingHz,
	double widthOctaves, CVector<T>* out_hz, CVector<T>* out_amp )
{
	// double OctavesToPitch( double octaves ){ return pow( 2.0, octaves );	}
	// Half of the range up and half of the range down, therefore: widthOctaves/2.0.
	double multiplier = pow( 2.0, widthOctaves/2.0 );
	SmoothFFTSpectrum( in_amp, srate, startingHz, multiplier, out_hz, out_amp );
}

// Loads a PCM WAV file and calculates its average spectrum.
template<class T> bool FileSpectrum( LPCVTCHAR fn, size_t fftSize, size_t step, CVector<T>* hz, CVector<T>* amp, bool decibel = true )
{
	CInMemoryAudio<double, 1> sound;
	if( !sound.LoadPCMWav( fn ) )
		return false;

	// !!! To do - add SMART!! truncation of input file

	AverageSpectrum( sound, sound.getLength()*0.01, sound.getLength()*0.99, fftSize, step, amp, decibel );
	SpectrumFrequencies( sound.GetSampleRate(), fftSize, hz );
	return true;
}

// Estimate value of the spectrum at specified frequency, using linear interpolation
// between points actually present in the spectrum.
template<class T> T SpectrumAt_Linear( const CVector<T>& hzs, const CVector<T>& amp, T hz )
{
	if( hz <= hzs.first() )
		return amp.first();
	if( hz >= hzs.last() )
		return amp.last();
	// !!!! To do - use binary search!
	size_t left = 0, right = 1; // Start with the first interval
	for( ; right < hzs.getSize(); left++, right++ )
		if( hzs[left]>=hz && hz<=hzs[right] ) // The point is within the interval
			break;
	T width = hzs[right] - hzs[left];
	T leftWeight = T(1.0) - (hz - hzs[left]) / width;
	T rightWeight = T(1.0) - (hzs[right]-hz) / width;
	return amp[left] * leftWeight + amp[right] * rightWeight;
}

template<class T> T RangeAverage( const CVector<T>& hzs, const CVector<T>& amp, T hz1, T hz2,
		bool logarithmic = true, T logStep = T(1.005) )
{
	// If starting & ending frequency is the same we just peek the point value and return it
	if( hz2==hz1 )
		return SpectrumAt_Linear( hzs, amp, hz1 );
	// Count of the points summed
	size_t count = 0;
	// Sum value
	T sum = 0;
	if( !logarithmic ){
		// Running linearly
		size_t i = 0;
		// Finding the first point in the spectrum which fits the specified range
		while( i<hzs.getSize() && i<amp.getSize() && hzs[i]<hz1 )
			i++;
		// Summing until the last point in the spectrum which fits the specified range
		while( i<hzs.getSize() && i<amp.getSize() && hzs[i]<=hz2 ){
			i++;
			count++;
			sum += amp[i];
		}
	}else{
		// Running through the range logarithmically
		for( T freq = hz1; freq <= hz2; freq *= logStep ){
			// Summing interpolated points
			sum += SpectrumAt_Linear( hzs, amp, freq );
			count++;
		}
	}
	// If we didn't find enough spectrum points in the range, just return
	// linearly interpolated value.
	if( count<2 )
		return SpectrumAt_Linear( hzs, amp, (hz1+hz2)/T(2.0f) );
	// Returning the average value.
	return sum / T(count);
}

// Add a correction table to the spectrum.
// 'k' is coeffient on which the correction table is multiplied (e.g. use -1 to subtract).
template<class T> void AddSpectrum( CVector<T>* hz, CVector<T>* amp, const CVector<T>& hz2, const CVector<T>& amp2, T k )
{
	for( size_t i=0; i<hz->getSize(); i++ )
		amp->set( i,
			amp->get(i) +
			SpectrumAt_Linear( hz2, amp2, hz->get(i) ) * k );
}

// Add a constant to each spectrum's value
template<class T> void ShiftSpectrum( CVector<T>* amp, T change )
{
	// TODO: Use SIMD?
	for( size_t i=0; i<amp->getSize(); i++ )
		amp->set( i, amp->get(i) + change );
}

// Normalize logarithmic spectrum so it has specified amplitude at specified frequency
template<class T> T NormalizeSpectrum( const CVector<T>& hz, CVector<T>* amp, double freq, double freq2, double required_amp )
{
	T theValue = RangeAverage( hz, *amp, freq, freq2 );
	T change = required_amp - theValue;
	ShiftSpectrum( amp, change );
	return change;
}

template<class T> bool LoadSpectrum_LspTable( LPCVTCHAR fn, CVector<T>* hz, CVector<T>* amp )
{
	VT::CStringArray lines, fields;
	if( !lines.Load( fn, true ) )
		return false;
	// Remove empty lines
	while( lines.getSize()>0 && lines.last().IsEmpty() )
		lines.resize( lines.getSize()-1 );
	// Must be at least 1 non-empty line!
	if( lines.getSize()<2 )
		return false;
	hz->resize( lines.getSize()-1 );
	amp->resize( lines.getSize()-1 );
	for( size_t l = 1, outIdx = 0; l<lines.getSize(); l++ ){
		// Parse each line. It has the following format:
		// <Frequency> <decibel> <phase in degrees>
		// The colums are separated with any whitespaces.
		lines[l].Replace( VTT('\t'), VTT(' ') );
		lines[l].Replace( VTT(','), VTT('.') ); // Decimal separator
		lines[l].Trim();
		double this_hz = 0, this_amp = 0;
		if( lines[l].CharAt(0)!=VTT(';') && lines[l].CharAt(0)!=VTT('#') ){
			fields.Explode( lines[l], VTT(' ') );
			for( size_t count = 0, field = 0; field<fields.getSize(); field++ ){
				fields[field].Trim();
				if( !fields[field].IsEmpty() ){ // A table cell
					++count;
					bool ok = false;
					double number = fields[field].ToDouble( &ok, 0.0, false );
					if( !ok && count<2 )
						break; // Bad line
					if( count==1 ){
						this_hz = number;
					}else if( count==2 ){
						this_amp = number;
						break;
					}
				}
			}
		}
		if( this_hz > 0.0 ){
			hz->set( outIdx, this_hz );
			amp->set( outIdx, this_amp );
			outIdx++;
		}else{
			// Invalid line - skip it
			size_t reduced = hz->getSize()-1;
			hz->resize( reduced, true );
			amp->resize( reduced, true );
		}
	}
	return hz->getSize()>0;
}

template<class T> CString Spectrum_TabDelimited(const CVector<T>& hz, const CVector<T>& amp )
{
	CString result = VTT("Freq\tDb\n");
	for( size_t i = 1; i < hz.getSize(); i++ ) {
		result.ConcatDouble(static_cast<double>(hz.get(i)), 3, false, false);
		result.ConcatChar('\t');
		result.ConcatDouble(static_cast<double>(amp.get(i)), 2, false, false);
		result.ConcatChar('\n');
	}
	return result;
}

template<class T> bool Spectrum_TabDelimited( CFile* out, const CVector<T>& hz, const CVector<T>& amp )
{
	return out->WriteString(Spectrum_TabDelimited(hz, amp).c_str());
}

int main(int argc, VTCHAR *argv[])
{
	LPCVTCHAR recordFile = NULL, sourceFile = NULL;
	CString micFile;
	double normHz = 1000.0, normHz2=2000.0, norm_dB = 0.0;
	bool absoluteCorrection = false;
	size_t
		fftSize = 65536, // Power of 2
		stepping = 6007; // Just some good number
	double widthOctaves = 1.0/3.0, stepOctaves = widthOctaves/2.0 / 4.0;

	fprintf( stderr,
		"Spectrum Averager alpha v. " __DATE__ " " __TIME__ ", (C) Sergey A. Galin\n"
		"USAGE: %s <ref frequency>[-<end ref. range>] [#]<ref dB> <recorded wav file> [<source wav file>]\n"
		"If ref_dB is prefixed with '#' it is interpreted as absolute level change\n"
		"and the ref frequency parameter is ignored.\n",
		argv[0]
	);

	const VTCHAR iniName[] = "averagespectrum.ini";

	CIniFile ini;
	if( ini.Load( iniName, true ) ){
		fprintf( stderr, "Loaded INI file from current directory.\n" );
	}else if( ini.Load( GetUserDocDir()<<iniName, true ) ){
		fprintf( stderr, "Loaded INI file from home directory.\n" );
	#ifndef VT_WINDOWS
		}else if( ini.Load( CString(VTT("/etc/"))<<iniName, true ) ){
			fprintf( stderr, "Loaded INI file from /etc.\n" );
	#endif
	}else
		fprintf( stderr, "INI file not found - using defaults.\n" );

	if( ini.GoToSection(VTT("AverageSpectrum")) ){
		fftSize = ini.GetIntDef( "fft", fftSize );
		stepping = ini.GetIntDef( "fftstep", stepping );
		micFile = ini.GetStrDef( "mic", NULL );
		widthOctaves = ini.GetDoubleDef( "smoothing", widthOctaves );
		stepOctaves = ini.GetDoubleDef( "smoothingstep", stepOctaves );

	}else
		fprintf( stderr, "WARNING: INI file does not contain the required section - skipping.\n" );

	if( micFile.CharAt(0)=='~' && micFile.CharAt(1)=='/' ){
		CString tmp( GetUserDocDir() );
		tmp<<(micFile.Str()+2);
		micFile.CopyFrom( tmp );
	}

	fprintf( stderr,
		"FFT Size: %d, step: %d\n"
		"Smoothing: %.3f octaves, point step: %.3f octaves\n"
		"Microphone correction file: %s\n",
		int( fftSize ), int( stepping ), float( widthOctaves ), float( stepOctaves ),
		( (micFile.IsEmpty())? "Not using microphone compensation.": micFile.c_str() ) );


	if( argc>=4 ){
		CString refHz( argv[1] ), ref_dB( argv[2] );

		if( ref_dB.CharAt(0)=='#' ){
			CString ref_dB2( ref_dB.Str()+1 );
			absoluteCorrection = true;
			norm_dB = ref_dB2.ToDouble();
			fprintf( stderr, "Using absolute correction: %+.5f dB\n", float(norm_dB) );
		}else{
			CStringArray expl;
			expl.Explode( refHz, VTT('-') );
			if( expl.getSize()>=2 ){
				normHz = expl[0].ToDouble();
				normHz2 = expl[1].ToDouble();
				if( normHz2 < normHz )
					Swap( &normHz, &normHz2 );
			}else
				normHz2 = normHz = refHz.ToDouble();

			norm_dB = ref_dB.ToDouble();
			fprintf( stderr, "Reference sensitivity: %.3f dB @ %.3f-%.3f Hz\n", float(norm_dB), float(normHz), float(normHz2) );
		}

		recordFile = argv[3];
		fprintf( stderr, "Recorded file: \"%s\"\n", recordFile );
		if( argc>=5 ){
			sourceFile = argv[4];
			fprintf( stderr, "Source file: \"%s\"\n", sourceFile );
		}
	}else{
		fprintf( stderr, "Not enough parameters.\n" );
		return 1;
	}




	CVector<double> sourceHz, sourceAmp, recordHz, recordAmp, micHz, micAmp, outHz, outAmp;
	recordHz.zero();
	recordAmp.zero();
	micHz.zero();
	micAmp.zero();

	fprintf( stderr, "Creating %s spectrum...\n", recordFile );
	fflush(stderr);
	if( !FileSpectrum( recordFile, fftSize, stepping, &recordHz, &recordAmp ) ){
		fprintf( stderr, "Error building spectrum!\n" );
		return 20;
	}

	if( sourceFile!=NULL ){
		fprintf( stderr, "Creating %s spectrum...\n", sourceFile );
		fflush(stderr);
		if( !FileSpectrum( sourceFile, fftSize, stepping, &sourceHz, &sourceAmp ) ){
			fprintf( stderr, "Error building spectrum!\n" );
			return 20;
		}
	}

	if( !micFile.IsEmpty() ){
		fprintf( stderr, "Loading microphone correction file %s...\n", micFile.c_str() );
		fflush(stderr);
		if( !LoadSpectrum_LspTable( micFile, &micHz, &micAmp ) ){
			fprintf( stderr, "Error loading mic file!\n" );
			return 20;
		}
	}

	// Resulting spectrum is:
	// [record] - [mic] - [source]
	if( sourceFile!=NULL ){
		fprintf( stderr, "Subtracting source spectrum...\n" );
		fflush(stderr);
		AddSpectrum( &recordHz, &recordAmp, sourceHz, sourceAmp, -1.0 );
	}

	if( micFile!=NULL ){
		fprintf( stderr, "Subtracting mic spectrum...\n" );
		fflush(stderr);
		AddSpectrum( &recordHz, &recordAmp, micHz, micAmp, -1.0 );
	}

	fprintf( stderr, "Averaging...\n");
	fflush(stderr);

	double stepMultiplier = pow( 2.0, stepOctaves );
	double widthMultiplier = pow( 2.0, widthOctaves/2.0 );
	LogFrequencies( 10.0, stepMultiplier, recordHz.last(), &outHz );
	SmoothSpectrum( recordHz, recordAmp, outHz, widthMultiplier, &outAmp );

	if( absoluteCorrection ){
		fprintf( stderr, "Normalizing by +%.6f dB...\n", float(norm_dB) );
		fflush(stderr);
		ShiftSpectrum( &outAmp, norm_dB );
	}else{
		fprintf( stderr, "Normalizing for %.3f dB at %.3f-%.3f Hz...\n", float(norm_dB), float(normHz), float(normHz2) );
		fflush(stderr);
		double diff = NormalizeSpectrum( outHz, &outAmp, normHz, normHz2, norm_dB );
		fprintf( stderr, "Normalization value: %.6f dB\n", float(diff) );
	}

	CFILEFile my_stdout(stdout);
	Spectrum_TabDelimited( &my_stdout, outHz, outAmp );
}
