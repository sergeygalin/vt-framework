/*
    BSD License

    Copyright (C) 2009 Sergey A. Galin
    E-mail: sergey.galin@gmail.com Website: http://sageshome.net

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in
          the documentation and/or other materials provided with the distribution.
        * Neither the name of Sergey Galin nor the names of his contributors may
          be used to endorse or promote products derived from this software
          without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
    OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "vt_riff.hpp"
#include "vt_localization.hpp"
#include "vt_midi.hpp"

using namespace VT;

void dump( const char *comment, const BYTE *src, int sz )
{
    printf("%s: ", comment);
    for( int i=0; i<sz; i++ )
        printf("%02X ", int(src[i]));
    printf("\n");
}

int main( int argc, char *argv[] )
{
    printf("ACID Wav file information - (C) Sergey A. Galin, 2010\n");

    if( argc!=2 ){
        printf("USAGE: %s <filename>\n", argv[0]);
        return 0;
    }

    const char *fn = argv[1];

    CDiskFile file( fn, "rb" );

    if( !file.IsOK() ){
        printf("Error opening file: %s\n", fn);
        return 1;
    }

    RIFF::CRIFFReader riff;
    riff.Open( &file );

    if( !riff.SeekToChunk( 'a', 'c', 'i', 'd' ) ){
        printf("Failed to find ACID information chunk!\n");
        return 1;
    }

    RIFF::TACIDChunk acid;
    memset( &acid, 0, sizeof(acid) );

    file.read( &acid, sizeof(acid), 1 );

    int test1 = int(acid.bDownbeatOffset[0])+int(acid.bDownbeatOffset[1])*256;

    printf(
        "ACID Information for \"%s\"\n"
        "File ACID data Size: %d (+8) bytes, our structure size: %d bytes\n"
        "Type: \"%s\" (#%d)\n"
        "\n"
        "Note/Transposing....... %s\n"
        "One Shot............... %s\n"
        "Looping................ %s\n"
        "Beatmapped............. %s\n"
        "Disk-Based............. %s\n"
        "Tempo Used............. %s\n"
        "\n"
        "Note................... %d (%s)\n"
        "Number of Beats........ %d (raw=%d / %02X h / %d)\n"
        "Downbeat Offset........ %d (raw=%02X %02X h)\n"
        "Tempo.................. %.2f BPM\n"
        "\n",
            fn,
            int(acid.chunkSize), int(sizeof(acid)),
            acid.GetTypeName(),
            int(acid.bType),
            LCYESNO(acid.IsTransposing()),
            LCYESNO(acid.IsOneShot()),
            LCYESNO(acid.IsLoop()),
            LCYESNO(acid.IsBeatmapped()),
            LCYESNO(acid.IsDiskBased()),
            LCYESNO(acid.HasTempo()),
            int(acid.GetRootNote()), Notes::GetFullNoteName(acid.GetRootNote()),
            int(acid.GetNBeats()), int(acid.bBeats), int(acid.bBeats), int((signed char)acid.bBeats),
            acid.GetDownbeatOffsetSamples(), int(acid.bDownbeatOffset[0]), int(acid.bDownbeatOffset[1]),
            acid.fTempo
    );

    /*
    RIFF::TACIDChunk defacid;
    defacid.Init();
    dump( "Unknown1", acid.unknown1, sizeof(acid.unknown1) );
    dump( "Unknown2", acid.unknown2, sizeof(acid.unknown2) );
    dump( "Unknown3", acid.unknown3, sizeof(acid.unknown3) );
    //dump( "Unknown1 Std...", defacid.unknown1, sizeof(defacid.unknown1) );
    //dump( "Unknown2 Std...", defacid.unknown2, sizeof(defacid.unknown2) );
    //dump( "Unknown3 Std...", defacid.unknown3, sizeof(defacid.unknown3) );
    if( memcmp( acid.unknown1, defacid.unknown1, sizeof(acid.unknown1) ) ||
        memcmp( acid.unknown2, defacid.unknown2, sizeof(acid.unknown2) ) ||
        memcmp( acid.unknown3, defacid.unknown3, sizeof(acid.unknown3) ) )
        printf("NEW VALUES FOR UNKNOWN FIELDS!!!\n");
    else
        printf("All unknown fields are set as usually.\n");
    */

    /*
    printf("Testing downbeat offset calculator...\n");
    for( unsigned off=0; off<300000; off++ ){
        defacid.SetupBeatmapped( true, 30, 120.0, off );
        unsigned res = defacid.GetDownbeatOffsetSamples();
        if( res!=off ){
            printf("Error: requested=%u result=%u\n", off, res);
        }
    }
    printf("Done.\n");
    */

    return 0;
}




